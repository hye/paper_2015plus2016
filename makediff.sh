#!/bin/bash

# takes in tag in form XX-XX-XX as argument, then tags and copies the draft into "drafts" 
SVNPATH="svn+ssh://svn.cern.ch/reps/atlasphys-hsg6/Physics/Higgs/HSG6/notes/Winter2017/Htautau/Paper"
TAGDIR="../tags"
TOPDIR=${PWD}
PACKAGE="Paper"
PDF="paper"

echo "Making latex diff wrt tag: $1"

set -e

if (( $# != 1 ))
then
  echo "Must provide tag number"
  exit 1
fi

## must be of form XX-XX-XX
if [ ! `echo $1 | grep -E "[0-9][0-9]-[0-9][0-9]-[0-9][0-9]"` ];
then
  echo "Invalid tag format, must be XX-XX-XX"
  exit 1
fi
TAG=$1

## check if tag exists locally
if [ ! -d "${TAGDIR}/${PACKAGE}-${TAG}" ]; then 
    echo "Tag ${TAG} not found locally, checking repository..."

    ## check if tag exists in repo
    if [ -z "$(svn ls ${SVNPATH}/tags | grep ${PACKAGE}-$1)" ]; then
        echo "Tag ${TAG} not found in repository, aborting!"
        exit 1
    fi

    ## checkout tag
    cd ${TAGDIR}; svn co ${SVNPATH}/tags/${PACKAGE}-${TAG}
    echo "Checked out tag"
    cd ${TOPDIR} 
else
    echo "Found tag locally"
fi

latexdiff --flatten ${TAGDIR}/${PACKAGE}-${TAG}/${PDF}.tex ${PDF}.tex > ${PDF}-diff.tex
pdflatex ${PDF}-diff
biber ${PDF}-diff
pdflatex ${PDF}-diff



