%-------------------------------------------------------------------------------
\section{Data and simulated event samples}
\label{sec:samples}
%-------------------------------------------------------------------------------

The results in this paper use proton--proton collision data at a centre-of-mass
energy of $\sqrt{s}=\SI{13}{\TeV}$ collected by the ATLAS detector at the LHC
during 2015 and 2016. The data correspond to a total integrated luminosity of
\lumi after requiring that all relevant components of the ATLAS detector are in good
working condition. Selected events must satisfy criteria designed to reduce
backgrounds from cosmic rays, beam-induced events and calorimeter
noise~\cite{ATLAS-CONF-2015-029}.  They must also contain at least one primary
vertex with at least two associated tracks. The primary vertex is chosen as the
proton--proton vertex candidate with the highest sum of the squared transverse
momenta of the associated tracks.


Simulated events are used to estimate the signal efficiencies and some of the
background contributions. The simulated event samples are normalised using
their theoretical cross sections and the integrated luminosity.  Simulated
events with a heavy neutral MSSM Higgs boson produced via gluon--gluon fusion
and in association with $b$-quarks were generated at next-to-leading order
(NLO) with \POWHEGBOXV{ v2}~\cite{Nason:2004rx,Frixione:2007vw,Powheg3} and
MG5\_aMC@NLO~2.1.2~\cite{Alwall:2014hca,Wiesemann:2014ioa} (using the
four-flavour scheme), respectively.  The CT10~\cite{Lai:2010vv} set of parton
distribution functions (PDFs) was used in the generation of gluon--gluon fusion
events while CT10nlo\_nf4~\cite{Dulat:2015mca} was used to produce the
$b$-associated signal samples.  \PYTHIAV{8.210}~\cite{PYTHIA82} with the
AZNLO~\cite{AZNLO:2014} (A14~\cite{ATL-PHYS-PUB-2014-021}) set of tuned
parameters (tune) was used together with the CTEQ6L1~\cite{Pumplin:2002vw}
(\textsc{NNPDF2.3LO}~\cite{Ball:2012cx}) PDF set for the parton shower
calculation at leading order (LO), underlying event and hadronisation in the
gluon--gluon fusion ($b$-associated) production.  The gluon--gluon fusion
sample was generated assuming SM couplings and underestimates the loop
contribution from $b$-quarks at high \tanb, which can impact the Higgs boson
\pt spectrum.  Generator-level studies indicate this has a negligible impact on
the final mass distribution and only a few percent impact on the signal
acceptance, except for mass hypotheses below \SI{400}{\GeV} where the impact
can be up to 10\%, so the effect is neglected.
 

The production cross sections and branching fractions for the various MSSM
scenarios are taken from Ref.~\cite{LHCHXSWG2016}.  The cross sections for
gluon--gluon fusion production are calculated using
\SUSHI~\cite{Harlander:2012pb}, including NLO supersymmetric-QCD
corrections~\cite{Spira:1995rr, Harlander:2004tp, Harlander:2005rq,
Degrassi:2010eu, Degrassi:2011vq, Degrassi:2012vt},
next-to-next-to-leading-order (NNLO) QCD corrections for the top
quark~\cite{Harlander:2002wh, Anastasiou:2002yz, Ravindran:2003um,
Harlander:2002vv, Anastasiou:2002wq}, as well as light-quark electroweak
effects~\cite{Aglietti:2004nj, Bonciani:2010ms}.  The $b$-associated production
cross sections in the five-flavour scheme are also calculated using \SUSHI
based on bbh@nnlo~\cite{Harlander:2003ai}, and those for $b$-associated
production in the four-flavour scheme (where $b$-quarks are not considered as
partons) are calculated according to Refs.~\cite{Dittmaier:2003ej,
Dawson:2003kb}.  The final $b$-associated production cross section is obtained
by using the method described in Ref.~\cite{SantanderMatching} to match the
four-flavour and five-flavour scheme cross sections.  The masses and mixing
(and effective Yukawa couplings) of the Higgs bosons are computed with
\FEYNHIGGS~\cite{Heinemeyer:1998yj, Heinemeyer:1998np, Degrassi:2002fi,
Frank:2006yh, Hahn:2013ria, Williams:2011bu} for all scenarios, with the
exception of the hMSSM. In the case of the hMSSM scenario, Higgs masses and
branching fractions are computed using \HDECAY \cite{Djouadi:1997yw,
Djouadi:2006bz}.  Branching fractions for all other scenarios use a combination
of results calculated by \HDECAY, \FEYNHIGGS and
PROPHECY4f~\cite{Bredenstein:2006rh, Bredenstein:2006ha}.

\begin{sloppypar}
The \Zprime{} signal events are modelled with a LO \DY{} sample that is
reweighted with the \TAUSPINNER algorithm~\cite{Czyczula:2012ny,
Kaczmarska:2014eoa, TauSpinnerBSM}, which correctly accounts for spin effects
in the $\tau$-lepton decays.  The \DY{} sample, enriched in events with high
invariant mass, was generated with \PYTHIAV{8.165}~\cite{Pythia8, Pythia} using
the \textsc{NNPDF2.3LO} PDF set and the A14 tune for the parton-shower and
underlying-event parameters.  Interference between the \Zprime{} and the SM
\DY{} production is not included, as it is highly model dependent.
Higher-order QCD corrections are applied to the simulated event samples.  These
corrections to the event yields are made with a mass-dependent rescaling to
NNLO in the QCD coupling constant, as calculated with VRAP
0.9~\cite{Anastasiou:2003ds} and the CT14NNLO PDF set.  Electroweak corrections
are not applied to the \Zprime{} signal samples due to the large model
dependence.
\end{sloppypar}

The \multijet background in both channels is estimated using data, while
non-\multijet backgrounds in which a quark- or gluon-initiated jet is
misidentified as a hadronic tau decay (predominantly \Wjets and \ttbar) are
modelled using data in the \lephad channel and simulation with data-driven
corrections in the \hadhad channel, as described in Section~\ref{sec:bkg}. The
remaining background contributions arise from  \DYjets, \Wjets, \ttbar,
\singletop and \diboson ($WW$, $WZ$ and $ZZ$) production. These contributions
are estimated using the simulated event samples described below.


Events containing \DYjets were generated with \POWHEGBOX{ v2}~\cite{POWHEG}
interfaced to the \PYTHIAV{8.186} parton shower model. The CT10 PDF set was used
in the matrix element. The AZNLO tune was used, with PDF set CTEQ6L1, for the
modelling of non-perturbative effects. 
Photon emission from electroweak vertices and charged leptons was performed 
with \PHOTOSppV{3.52}~\cite{Davidson:2010ew}. The same setup was used to simulate
\Wjets events for background subtraction in the control regions of
the \lephad channel. The \DYjets samples were simulated in slices with different
masses of the off-shell boson. The event yields are corrected with a mass-dependent
rescaling at NNLO in the QCD coupling constant, computed with VRAP 0.9 and the
CT14NNLO PDF set.  Mass-dependent electroweak corrections are computed at NLO
with \MCSANC{1.20}~\cite{Bondarenko:2013nu}, and these include photon-induced
contributions ($\gamma\gamma\rightarrow\ell\ell$ via $t$- and $u$-channel
processes) computed with the MRST2004QED PDF set~\cite{Martin:2004dh}.

\begin{sloppypar}
The modelling of the \Wjets process in the case of the $\tauhad\tauhad$ channel
was done with the \SHERPAV{2.2.0}~\cite{Gleisberg:2008ta} event generator.
Matrix elements were calculated for up to two partons at NLO and four partons
at LO using \COMIX~\cite{Gleisberg:2008fv} and
\OPENLOOPS~\cite{Cascioli:2011va} merged with the \SHERPA parton
shower~\cite{Schumann:2007mg} using the ME+PS@NLO
prescription~\cite{Hoeche:2012yf}. The CT10nlo PDF set was used in conjunction
with dedicated parton shower tuning developed by the \SHERPA authors.  The
\Wjets production is normalised to the NNLO cross sections with
\FEWZ~\cite{Anastasiou:2003ds, Melnikov:2006kv, Gavin:2010az}.
\end{sloppypar}

For the generation of \ttbar or a single top quark in the $Wt$-channel and $s$-channel, the
\POWHEGBOX{ v2} event generator was used with the CT10 PDF set in the matrix element
calculation. Electroweak $t$-channel single-top-quark events were
generated with the \POWHEGBOX{ v1} event generator. This event generator uses the
four-flavour scheme for the NLO matrix elements calculations together with the
fixed four-flavour PDF set CT10f4. For all top processes, top-quark spin
correlations were preserved (for $t$-channel, top quarks were decayed with 
\MADSPIN~\cite{Artoisenet:2012st}). The parton shower, hadronisation, and the
underlying event were simulated using \PYTHIAV{6.428} with the CTEQ6L1 PDF sets
and the corresponding \Perugia 2012 tune~\cite{Skands:2010ak}. The top mass was
set to \SI{172.5}{\GeV}. The \ttbar production sample is normalised to the predicted
production cross section as calculated with the \TOPpp program to NNLO in
perturbative QCD, including soft-gluon resummation to
next-to-next-to-leading-log (NNLL) order (Ref.~\cite{Czakon:2011xx} and references
therein). For the single-top-quark event samples, an approximate
calculation at NLO in QCD for the $s$-channel and 
$t$-channel~\cite{Kant:2014oha,Aliev:2010zk} and an NLO+NNLL calculation for 
the $Wt$-channel~\cite{Kidonakis:2010ux} are used for the normalisation. 

\Diboson processes were modelled using the \SHERPAV{2.1.1} event generator and they
were calculated for up to one ($ZZ$) or no ($WW$, $WZ$) additional partons at NLO
and up to three additional partons at LO using \COMIX and \OPENLOOPS 
merged with the \SHERPA parton shower using the ME+PS@NLO
prescription. The CT10 PDF set was used in conjunction with dedicated parton
shower tuning developed by the \SHERPA authors. The event generator cross sections are
used in this case (already at NLO). In addition, the \SHERPA diboson sample
cross section was scaled down to account for its use of
$\alpha_\textrm{QED} =$ 1/129 rather than 1/132 corresponding to the use of
current PDG parameters as input to the $\mathrm{G}_\mu$ scheme.

Properties of the bottom and charm hadron decays were set with the 
\EVTGENV{v1.2.0} program~\cite{EvtGen} in samples that were not produced with \SHERPA.
Simulated minimum-bias events were overlaid on all simulated samples to include
the effect of multiple proton--proton interactions in the same and neighbouring
bunch crossings (``pile-up''). These minimum-bias events were generated with
\PYTHIAV{8.186}, using the  A2 tune~\cite{ATL-PHYS-PUB-2012-003} and the MSTW2008LO
PDF~\cite{Martin:2009iq}.
%
Each sample was simulated using the full \GEANTV{4}~\cite{Geant4,ATLASSIM}
simulation of the ATLAS detector, with the exception of the $b$-associated MSSM
Higgs boson signal, for which the \AFII~\cite{ATL-PHYS-PUB-2010-013} fast
simulation framework was used.  Finally, the simulated events are processed
through the same reconstruction software as the data.
