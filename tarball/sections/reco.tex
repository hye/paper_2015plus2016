%-------------------------------------------------------------------------------
\section{Event reconstruction}
\label{sec:reco}
%-------------------------------------------------------------------------------

Electron candidates are reconstructed from energy deposits in the
electromagnetic calorimeter associated with a charged-particle track measured
in the inner detector~\cite{PERF-2013-03,PERF-2013-05,ATLAS-CONF-2016-024}.
The electron candidates are required to pass a ``loose'' likelihood-based
identification selection, to have a transverse momentum \mbox{$\pt >
\SI{15}{\GeV}$} and to be in the fiducial volume of the inner detector,
$|\eta|<2.47$.  The transition region between the barrel and endcap
calorimeters ($1.37<|\eta|<1.52$) is excluded. 

Muon candidates are reconstructed in the range $|\eta|<2.5$ by matching tracks
found in the muon spectrometer to tracks found in the inner
detector~\cite{PERF-2015-10}.  The tracks of the muon candidates are re-fitted
using the complete track information from both detector systems. They are
required to have a transverse momentum $\pt>\SI{7}{\GeV}$ and to pass a
``loose'' muon identification requirement.

The selected lepton (electron or muon) in the \lephad channel must then have
$\pt>\SI{30}{\GeV}$ and pass a ``medium'' identification requirement. This lepton is
considered isolated if it meets $\pt$- and $\eta$-dependent isolation criteria utilising
calorimetric and tracking information. The criteria correspond to an efficiency
of 90\% (99\%) for a transverse momentum of $\pt=25~(60)\,\GeV$. The efficiency 
increases with lepton \pt as the requirements are relaxed to account for the 
decreased background from misidentified jets. 

Jets are reconstructed from topological clusters of energy depositions
\cite{Aad:2016upy} in the calorimeter using the anti-$k_t$
algorithm~\cite{AntiKT, IsolationPileUp2}, with a radius parameter value $R
=0.4$. The average energy contribution from pile-up is subtracted according to
the jet area and the jets are calibrated as described in
Ref~\cite{PERF-2016-04}.  They are required to have $\pt > \SI{20}{\GeV}$ and
$|\eta| < 2.5$. To reduce the effect of pile-up, a jet vertex tagger algorithm
is used for jets with $\pt < \SI{60}{\GeV}$ and $|\eta| < 2.4$.  It employs a
multivariate technique based on jet energy, vertexing and tracking variables to
determine the likelihood that the jet originates from or is heavily
contaminated by pile-up~\cite{PERF-2014-03}.  In order to identify jets
containing $b$-hadrons ($b$-jets), a multivariate algorithm is used, which is
based on the presence of tracks with a large impact parameter with respect to
the primary vertex, the presence of displaced secondary vertices and the
reconstructed flight paths of $b$- and $c$-hadrons associated with the
jet~\cite{Aad:2015ydr, ATL-PHYS-PUB-2016-012}.  The algorithm has an average
efficiency of 70\% for $b$-jets and rejections of approximately 13, 56 and 380
for $c$-jets, hadronic tau decays and jets initiated by light quarks or gluons,
respectively, as determined in simulated \ttbar events. 

Hadronic tau decays are composed of a neutrino and a set of visible decay
products (\tauhadvis), typically one or three charged pions and up to two
neutral pions. The reconstruction of the visible decay products is seeded by
jets~\cite{PERF-2013-06}. The \tauhadvis candidates must have $\pt >
25~(45)\,\GeV$ in the \lephad (\hadhad) channel, $|\eta|<2.5$ excluding
$1.37<|\eta|<1.52$, one or three associated tracks and an electric charge of
$\pm 1$. The leading-\pt \tauhadvis candidate in the \lephad channel and the
two leading-\pt \tauhadvis candidates in the \hadhad channel are then selected
and all remaining candidates are considered as jets. A Boosted Decision Tree
(BDT) identification procedure, based on calorimetric shower shapes and
tracking information is used to reject backgrounds from
jets~\cite{ATL-PHYS-PUB-2015-045-fixed,ATLAS-CONF-2017-029}.  Two $\tauhadvis$
identification criteria are used: ``loose'' and ``medium'', specified in
Section~\ref{sec:sel}.  The criteria correspond to efficiencies of about 60\%
(50\%) and 55\% (40\%) in \DYtt events and rejections of about 30 (30)
and 50 (100) in \multijet events, for one-track (three-track) $\tauhadvis$
candidates, respectively.  An additional dedicated likelihood-based veto is
used to reduce the number of electrons misidentified as $\tauhadvis$ in the
\lephad channel, providing 95\% efficiency and a background rejection between
20 and 200, depending on the pseudorapidity of the $\tauhadvis$ candidate.

Geometrically overlapping objects are removed in the following order: 
\begin{enumerate*}[(a)]
\item jets within $\Delta R = 0.2$ of selected \tauhadvis candidates are excluded, 
\item jets within $\Delta R = 0.4$ of an electron or muon are excluded, 
\item any \tauhadvis candidate within $\Delta R = 0.2$ of an electron or muon is excluded,
\item electrons within $\Delta R$ = 0.2 of a muon are excluded.
\end{enumerate*}

The missing transverse momentum, \metvec, is calculated as the
negative vectorial sum of the $\mathbf{p}_\mathrm{T}$ of all fully
reconstructed and calibrated physics
objects~\cite{ATL-PHYS-PUB-2015-027,ATL-PHYS-PUB-2015-023}. This procedure
includes a ``soft term'', which is calculated using the inner-detector
tracks that originate from the hard-scattering vertex but are not associated with
reconstructed objects. 
 
