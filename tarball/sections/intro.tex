%-------------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
%-------------------------------------------------------------------------------

The discovery of a scalar particle~\cite{ATLASHiggsJuly2012, CMSHiggsJuly2012}
at the Large Hadron Collider (LHC)~\cite{LHC} has provided important insight
into the mechanism of electroweak symmetry breaking.  Experimental studies of
the new particle
\cite{Aad:2015mxa,Aad:2015gba,Khachatryan:2014jba,Khachatryan:2014kca,HIGG-2015-07}
demonstrate consistency with the Standard Model (SM) Higgs boson
\cite{ENGLERT,HIGGS,HIGGS2,HIGGS3, Guralnik:1964eu,Kibble:1967sv}. However, it
remains possible that the discovered particle is part of an extended scalar
sector, a scenario that is predicted by a number of theoretical arguments
\cite{Djouadi:2005gj,Branco:2011iw}.

The Minimal Supersymmetric Standard Model
(MSSM)~\cite{Djouadi:2005gj,Fayet:1976et,Fayet:1977yc} is the simplest
extension of the SM that includes supersymmetry. The MSSM requires two Higgs
doublets of opposite hypercharge.  Assuming that CP symmetry is conserved, this
results in one CP-odd ($A$) and two CP-even ($h$, $H$)  neutral Higgs bosons
and two charged Higgs bosons ($H^{\pm}$).  At tree level, the properties of the
Higgs sector in the MSSM depend on only two non-SM parameters, which can be
chosen to be the mass of the CP-odd Higgs boson, $m_A$, and the ratio of the
vacuum expectation values of the two Higgs doublets, $\tan\beta$. Beyond tree
level, a number of additional parameters affect the Higgs sector, the choice of
which defines various MSSM benchmark scenarios. In the 
$m_{h}^{\text{mod}+}$ scenario~\cite{MSSMBenchmarks}, the top-squark mixing parameter is
chosen such that the mass of the lightest CP-even Higgs boson, $m_h$, is close
to the measured mass of the Higgs boson that was discovered at the LHC.  A
different approach is employed in the hMSSM
scenario~\cite{Djouadi:2013uqa,Bagnaschi:2015hka} in which the measured value
of $m_h$ can be used, with certain assumptions, to predict the remaining masses
and couplings of the MSSM Higgs bosons without explicit reference to the soft
supersymmetry-breaking parameters.  The couplings of the MSSM heavy Higgs
bosons to down-type fermions are enhanced with respect to the SM Higgs boson
for large $\tan\beta$ values, resulting in increased branching fractions to
$\tau$-leptons and $b$-quarks, as well as a higher cross section for Higgs
boson production in association with $b$-quarks.  This has motivated a variety
of searches for a scalar boson (generically called $\phi$) in $\tau\tau$ and
$bb$ final states\footnote{The notation $\tau\tau$ and $bb$ is used as
shorthand for $\tau^+\tau^-$ and $b\bar{b}$ throughout this paper.} at
LEP~\cite{LEPLimits}, the
Tevatron~\cite{TevatronLimits1,TevatronLimits2,TevatronLimits3} and the
LHC~\cite{Khachatryan:2014wca, CMS-HIG-14-017,
LHCbHtautau, HIGG-2015-08}.

Heavy \Zprime{} gauge bosons appear in many extensions of the
SM~\cite{Hewett:1989,Cvetic:1995zs,Leike:1998wr,Diener:2010sy,Langacker:2008}
and while they are typically considered to obey lepton universality, this is
not necessarily a requirement. In particular, models in which the \Zprime{}
boson couples preferentially to third-generation fermions may be linked to the
high mass of the top quark~\cite{ZprimeToThirdGen,sfm,G221,TopFlavor} or to
recent indications of lepton flavour universality violation in semi-tauonic $B$
meson decays~\cite{Faroughy:2016osc}.  One such model is the non-universal
$G(221)$ model~\cite{sfm,TopFlavor,G221}, which contains a \ZprimeSFM{} boson
that can exhibit enhanced couplings to tau leptons.  In this model the SM
SU(2) gauge group is split into two parts: one coupling to fermions of the
first two generations and one coupling to third generation fermions. The mixing
between these groups is described by the parameter $\sin^{2}\phi$, with
$\sin^{2}\phi<0.5$ corresponding to enhanced third generation couplings. A
frequently used benchmark model is the Sequential Standard Model (SSM), which
contains a \ZprimeSSM{} boson with couplings identical to the SM $Z$ boson. By
evaluating the impact on the signal sensitivity from changing the \ZprimeSSM{}
couplings, limits on \ZprimeSSM{} can be reinterpreted for a broad range of
models. Indirect limits on \Zprime{} bosons with non-universal flavour
couplings have been derived from measurements at
LEP~\cite{ZprimeMassLimitsFromLEP}.  The most sensitive direct searches for
high-mass  resonances decaying to \ditau final states have been performed by
the ATLAS and CMS collaborations using data collected at $\sqrt{s}=8$ and
$\SI{13}{\TeV}$~\cite{EXOT-2014-05, HIGG-2015-08, CMS-EXO-16-008}.

This paper presents the results of a search for neutral MSSM Higgs bosons as
well as high-mass \Zprime{} resonances in the \ditau decay mode using \lumi of
proton--proton collision data at a centre-of-mass energy of \SI{13}{\TeV}
collected with the ATLAS detector \cite{ATLASDetector} in 2015 and 2016.  The
search is performed in the $\taulep\tauhad$ and $\tauhad\tauhad$ decay modes,
where $\taulep$ represents the decay of a $\tau$-lepton to an electron or a
muon and neutrinos, whereas $\tauhad$ represents the decay to one or more
hadrons and a neutrino.  The search considers narrow resonances\footnote{A
resonance is considered ``narrow'' if the lineshape has no impact on
experimental observables.} with masses of \hmin--\SI{\hmax}{\TeV} and
$\tan\beta$ of 1--58 for the MSSM Higgs bosons. For the \Zprime{} boson search,
a mass range of \zmin--\SI{\zmax}{\TeV} is considered.  Higgs boson production
through gluon--gluon fusion and in association with $b$-quarks is considered
(Figures~\ref{fig:feyn:ggH}--\ref{fig:feyn:bbH5}), with the latter mode
dominating for high $\tan\beta$ values.  Hence, both the $\taulep\tauhad$ and
$\tauhad\tauhad$ channels are split into $b$-tag and $b$-veto categories, based
on the presence or absence of jets tagged as originating from $b$-quarks in the
final state.  Since a \Zprime{} boson is expected to be predominantly produced
via a Drell--Yan process (Figure~\ref{fig:feyn:zprime}), there is little gain
in splitting the data into $b$-tag and $b$-veto categories.  Hence, the
\Zprime{} analysis uses an inclusive selection instead.

\begin{figure}[tp]
  \centering
  \subfigure[\label{fig:feyn:ggH}]{\includegraphics[width=0.33\textwidth]{figures/gfusion.pdf} }
  \hspace{1.5cm}  
  \subfigure[\label{fig:feyn:bbH4}]{\includegraphics[width=0.31\textwidth]{figures/bbH_4FS.pdf} } \\
  \subfigure[\label{fig:feyn:bbH5}]{\includegraphics[width=0.31\textwidth]{figures/bbH_5FS.pdf} }
  \hspace{2.0cm} 
  \subfigure[\label{fig:feyn:zprime}]{\includegraphics[width=0.26\textwidth]{figures/zprime.pdf} }
  \caption{Lowest-order Feynman diagrams for (a) gluon--gluon fusion and $b$-associated 
production of a neutral MSSM Higgs boson in the (b) four-flavour and (c) five-flavour schemes
and (d) Drell--Yan production of a \Zprime{} boson.}
\label{fig:feynman}
\end{figure}

The paper is structured as follows. Section~\ref{sec:atlas} provides an
overview of the ATLAS detector.  The event samples used in the analysis,
recorded by the ATLAS detector or simulated using the ATLAS simulation
framework, are reported in Section~\ref{sec:samples}. The event reconstruction
is presented in Section~\ref{sec:reco}. A description of the event selection
criteria is given in Section~\ref{sec:sel}. Section~\ref{sec:bkg} explains the
estimation of background contributions, followed by a description of systematic
uncertainties in Section~\ref{sec:sys}. Results are presented in
Section~\ref{sec:results}, followed by concluding remarks in
Section~\ref{sec:conclusion}.

