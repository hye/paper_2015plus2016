#!/bin/bash
export COPYFILE_DISABLE=1; tar -cvzf resub-arxiv.tar.gz \
    Acknowledgements.tex \
    atlas_authlist.tex \
    atlas_authlist.xml \
    bib/*.bib \
    figures/*.pdf \
    latex/*.sty \
    latex/*.cls \
    logos/*.pdf \
    paper-defs.sty \
    paper-metadata.tex \
    paper.bbl \
    paper.bib \
    paper.pdf \
    paper.tex \
    sections/*.tex \
    Metadata.txt
