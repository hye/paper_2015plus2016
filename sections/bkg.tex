%-------------------------------------------------------------------------------
\section{Background estimation}
\label{sec:bkg}
%-------------------------------------------------------------------------------

The dominant background contribution in the \lephad channel arises from
processes where the \tauhadvis candidate originates from a quark- or
gluon-initiated jet (henceforth called jet). This contribution is estimated
using a data-driven fake-factor technique, described in
Section~\ref{sec:bkg:lephad}. The events are divided into those where the
selected lepton is correctly identified, predominantly from \Wjets (\ttbar)
production in the \bveto (\btag) channel, and those where the selected lepton
arises from a jet, predominantly from \multijet production.  Backgrounds where both
the \tauhadvis and lepton candidates originate from electrons, muons or taus
(\tworeal) arise from \DYtt production in the \bveto category and \ttbar
production in the \btag category, with minor contributions from \DYll, \diboson
and \singletop production. These contributions are estimated using simulation.
Corrections are applied to the simulation to account for mismodelling of the
trigger, reconstruction, identification and isolation efficiencies, the
electron to \tauhadvis misidentification rate and the momentum scales and
resolutions.  To help constrain the normalisation of the \ttbar contribution, a
control region rich in \ttbar events (CR-T) is defined and included in the
statistical fitting procedure. The other major background contributions can be
adequately constrained in the signal regions. Events in this control region
must pass the signal selection for the \btag category, but the
$\mT(\ptvec^{\ell},\metvec)$ selection is replaced by
$\mT(\ptvec^{\ell},\metvec) > 110\, (100)\,\GeV$ in the \ehad (\muhad) channel.
The tighter selection in the \ehad channel is used to help suppress the larger
\multijet contamination. The region has $\sim$90\% \ttbar purity.

The dominant background in the \hadhad channel is \multijet production, which
is estimated using a fake-factor technique similar to the \lephad channel, 
described in Section~\ref{sec:bkg:hadhad}.
Other important background contributions come from \DYtt production at
high \mttot in the \bveto category, \ttbar production in the \btag category, and to a lesser extent
\Wlnujets, \singletop, \diboson and \DYlljets production.  These
contributions are estimated using simulation. To improve the modelling, events
in the simulation that contain jets misidentified as \tauhadvis candidates are
weighted by fake-rates measured in \Wjets and \ttbar control regions in data.

Fake-factor techniques are employed in both channels and use events in control regions (CRs) where a
selected \tauhadvis fails identification or a selected lepton fails isolation.
The events are weighted by transfer-factors (fake-factors) measured in orthogonal 
fakes regions (FRs) to predict a given background contribution. The fake-factors are
defined as the number of events in data that pass ($N_\mathrm{data}^\mathrm{pass}$)
divided by the number that fail ($N_\mathrm{data}^\mathrm{fail}$) a specified selection
requirement. The corresponding number of background events ($N_\mathrm{bkg}$) is subtracted and they are parameterised 
with a set of auxiliary variables ($\mathbf{x}$): 
\begin{equation*}
f(\mathbf{x}) \equiv \frac{N_\mathrm{data}^\mathrm{pass}(\mathbf{x}) - N_\mathrm{bkg}^\mathrm{pass}(\mathbf{x})}{N_\mathrm{data}^\mathrm{fail}(\mathbf{x}) - N_\mathrm{bkg}^\mathrm{fail}(\mathbf{x})}\,.
\end{equation*}
For reference, a summary of the signal, control and fakes regions used for the 
background estimation techniques described in this section is provided in Table~\ref{tab:regions}.

\begin{table}[t]
%\footnotesize
\small
\begin{center}

\caption{Definition of signal, control and fakes regions used in the analysis. The symbol 
$\ell$ represents the selected electron or muon candidate and $\tau_1$ ($\tau_2$) 
represents the leading (sub-leading) \tauhadvis candidate. } 
\label{tab:regions}

\begin{tabular}{lll}
\toprule
Channel & Region & Selection \\
\midrule 
\lephad & SR    & $\ell$ (trigger, isolated), $\tau_1$ (medium), $q(\ell)\times q(\tau_1)<0$, $|\Delta\phi(\ptvec^{\ell},\ptvec^{\tau_1})|>2.4$, \\
&& $\mT(\ptvec^{\ell},\metvec)<\SI{40}{\GeV}$, veto $80 < m(\pvec^{\ell},\pvec^{\tau_1}) < \SI{110}{\GeV}$ (\ehad channel only) \\
\cmidrule{2-3}
& CR-1  & Pass SR except: $\tau_1$ (very-loose, fail medium) \\
& CR-2  & Pass SR except: $\tau_1$ (very-loose, fail medium), $\ell$ (fail isolation) \\
& MJ-FR & Pass SR except: $\tau_1$ (very-loose), $\ell$ (fail isolation) \\
& W-FR  & Pass SR except: 70\,(60)$ < \mT(\ptvec^{\ell},\metvec) < \SI{150}{\GeV}$ in \ehad (\muhad) channel \\
& CR-T  & Pass SR except: $\mT(\ptvec^{\ell},\metvec)> 110\,(100)\,\GeV$ in the \ehad (\muhad) channel, \\
&       & \btag category only \\
& L-FR  & $\ell$ (trigger, selected), jet (selected), no loose $\tauhadvis$, $\mT(\ptvec^{\ell},\metvec) < \SI{30}{\GeV}$ \\
%\\
\midrule
\hadhad & SR    & $\tau_{1}$ (trigger, medium), $\tau_{2}$ (loose), $q(\tau_1)\times q(\tau_2)<0$, $|\Delta\phi(\ptvec^{\tau_1},\ptvec^{\tau_2})|>2.7$ \\
\cmidrule{2-3}
& CR-1  & Pass SR except: $\tau_2$ (fail loose) \\
& DJ-FR & jet trigger, $\tau_1$+$\tau_2$ (no identification), $q(\tau_1)\times q(\tau_2)<0$, $|\Delta\phi(\ptvec^{\tau_1},\ptvec^{\tau_2})|>2.7$ \\
&& $\pt^{\tau_2}/\pt^{\tau_1} > 0.3$ \\
&  W-FR & $\mu$ (trigger, isolated), $\tau_1$ (no identification), $|\Delta\phi(\ptvec^{\mu},\ptvec^{\tau_1})|>2.4$ \\
&& $\mT(\ptvec^{\mu},\metvec)>\SI{40}{\GeV}$, \bveto category only \\
&  T-FR & Pass W-FR except: \btag category only \\
\bottomrule
\end{tabular}
\end{center}
\end{table}




%-------------------------------------------------------------------------------
\subsection{Jet background estimate in the \lephad channel}
\label{sec:bkg:lephad}
%-------------------------------------------------------------------------------

The background contribution in the \lephad channel where the \tauhadvis candidate originates from a
jet is estimated using a fake-factor method to weight events in a control
region which has the same selection as the signal region (\sr), but where the
\tauhadvis candidate fails identification (\failcr).  The method,
however, must be extended to account for the fact that \multijet and 
\Wjets (or \ttbar) events have significantly different fake-factors, which is mainly due
to a different fraction of quark-initiated jets, which are typically more 
narrow and produce fewer hadrons than gluon-initiated jets, 
and are thus more likely to pass the \tauhadvis identification. The procedure, 
depicted in Figure~\ref{fig:lephad_crs}, is described in the 
following.

\begin{figure}[htp]
\begin{center}
\includegraphics[width=0.80\columnwidth]{figures/LepHadCRs}

\caption{
Schematic of the fake-factor background estimation in the \lephad channel. The
fake-factors, $f_\textrm{X}$ (X = MJ, W, L), are defined as the ratio of events
in data that pass/fail the specified selection requirements, measured in the
fakes-regions: MJ-FR, W-FR and L-FR, respectively.  The \multijet contribution
is estimated by weighting events in \mjcrtwo by the product of \flep and \fMJ.
The contribution from \Wjets and \ttbar events where the \tauhadvis candidate
originates from a jet is estimated by subtracting the \multijet contribution
from \failcr and then weighting by \fW.  There is a small overlap of events 
between L-FR and the CR-1 and CR-2 regions. The contribution where both the
selected \tauhadvis and lepton originate from leptons is estimated using
simulation (not shown here).
}
\label{fig:lephad_crs}
\end{center}
\end{figure}


The contribution from \Wjets (and \ttbar) events  where the \tauhadvis candidate 
originates from a jet is estimated from events in
\failcr that remain after subtracting the \multijet contribution (estimated
using the technique described below) and the \tworeal contribution (estimated
using simulation). The events are weighted by the \Wjets tau fake-factor (\fW):
\begin{equation*}
N_\mathrm{\Wjets}^\mathrm{SR}(v;\mathbf{x}) = \fW(\mathbf{x}) \times 
\left [ N_\mathrm{data}^\mathrm{\failcr}(v;\mathbf{x}) 
      - N_\mathrm{\twofake}^\mathrm{\failcr}(v;\mathbf{x}) 
      - N_\mathrm{\tworeal}^\mathrm{\failcr}(v;\mathbf{x}) \right ]\,,
\end{equation*}
where $v$ is the variable being modelled (e.g.\ \mttot) and $\mathbf{x}$ is the 
set of auxiliary variables from the \fW parameterisation: \tauhadvis \pt,
\tauhadvis track multiplicity and the azimuthal $\tauhadvis$--$\metvec$ separation,
$|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$.
%
The fake-factor \fW is measured in the \Wjets fakes-region (\wcr) defined in Section~\ref{sec:lephad:tidff}.


The \multijet contributions in both \failcr ($N_\mathrm{\multijet}^\mathrm{\failcr}$) 
and \sr ($N_\mathrm{\multijet}^\mathrm{\sr}$) are estimated from events where the 
\tauhadvis fails identification and the selected lepton fails isolation (\mjcrtwo). 
The non-\multijet background is subtracted using simulation and the 
events are weighted first by the lepton-isolation fake-factor (\flep), yielding 
$N_\mathrm{\multijet}^\mathrm{\failcr}$, and then by the \multijet tau fake-factor (\fMJ):
\begin{align*}
N_\mathrm{\twofake}^\mathrm{\sr}(v;\mathbf{x},\etalep) 
&= \fMJ(\mathbf{x}) \times N_\mathrm{\twofake}^\mathrm{\failcr}(v;\mathbf{x},\etalep)\,,\\
N_\mathrm{\twofake}^\mathrm{\failcr}(v;\mathbf{x},\etalep)
&= \flep(\etalep) \times 
\left [ N_\mathrm{data}^\mathrm{\mjcrtwo}(v;\mathbf{x}, \etalep) 
      - N_\mathrm{non-MJ}^\mathrm{\mjcrtwo}(v;\mathbf{x},\etalep)  \right ]\,,
\end{align*}
where \etalep is the pseudorapidity of the isolated lepton, which is used to
parameterise \flep.  The fake-factor \fMJ is measured in the \multijet fakes-region (\mjcr) defined in
Section~\ref{sec:lephad:tidff} and the fake-factor \flep is measured in the 
lepton fakes-region (\flcr) defined in Section~\ref{sec:lephad:lepff}.


\subsubsection{Tau identification fake-factors}
\label{sec:lephad:tidff}

\begin{sloppypar}
Both \fW and \fMJ are parameterised as functions of \tauhadvis \pt,
\tauhadvis track multiplicity and $|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$.  
The $|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$ dependence 
is included to encapsulate correlations between the \tauhadvis identification and 
energy response, which impact the \metvec calculation. Due to the limited size of the control regions,
the $|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$ dependence is extracted as a sequential
correction and is only applied in the \bveto channel.
% 
The selection for \wcr and \mjcr are the same as for \sr with
modifications described in the following. 
%
%The \tauhadvis identification
%criterion is removed from both regions; events passing the identification enter
%the fake-factor numerators, while those failing enter the denominators.  To
%reduce differences between \fW and \fMJ, a very loose \tauhadvis identification
%criterion is introduced, which tends to reject gluon-initiated jets, enhancing
%the fraction of quark-initiated jets.
%The criterion corresponds to an efficiency of about 99\% for \tauhadvis
%and a rejection of about 2 (3) for one-track (three-track) jets. 
%This selection is also applied consistently to \failcr.  
%
The medium \tauhadvis identification criterion is replaced by a very loose
criterion with an efficiency of about 99\% for \tauhadvis and a rejection of
about 2 (3) for one-track (three-track) jets.  Events passing the medium
identification criterion enter the fake-factor numerators, while those failing
enter the denominators.  The very loose identification reduces differences
between \fW and \fMJ, as it tends to reject gluon-initiated jets, enhancing the
fraction of quark-initiated jets in \wcr and \mjcr. This selection is also 
applied consistently to \failcr.  
%
A comparison of
the two fake-factors and their respective $|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$ 
corrections are shown in Figures~\ref{fig:lephad_ffonly} and \ref{fig:lephad_wcorr}.
\end{sloppypar}

\begin{figure}[htp]
\begin{center}
\subfigure[\tauhadvis fake-factors\label{fig:lephad_ffonly}]{\includegraphics[width=0.45\columnwidth]{\figpath/c512_lephad_ff_split_2x2.pdf}}
\subfigure[$|\Delta\phi(\ptvec^{\tauhadvis}, \metvec)|$ correction\label{fig:lephad_wcorr}]{\includegraphics[width=0.45\columnwidth]{\figpath/c513_lephad_ff_wcorr.pdf}}
\caption{The \tauhadvis identification fake-factors and the sequential $|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$ 
correction in the \lephad channel. 
The \multijet fake-factors are for the 2016 dataset only.
The bands include all uncertainties. 
}
\label{fig:lephad_fakefactors}
\end{center}
\end{figure}

In the \wcr, the $\mT(\ptvec^{\ell},\metvec)$ criterion is replaced by $70 (60) <
\mT(\ptvec^{\ell},\metvec) < \SI{150}{\GeV}$ in the \ehad (\muhad) channel. The purity of
\Wjets events that pass the \tauhadvis identification is $\sim$85\% in the
\bveto category. The \btag category is dominated by \ttbar events, but 
the purity of events where the \tauhadvis candidate originates from 
a jet is only $\sim$40\% due to the significant fraction of \tauhadvis from $W$ boson decays.
The \multijet and \tworeal backgrounds are subtracted from \wcr analogously to \failcr in the \Wjets
estimate.  Due to the large \tauhadvis contamination in the \btag region, \fW is not split by category,
but the \bveto parameterisation is used in the \btag region, with a \pt-independent
correction factor of 0.8 (0.66) for one-track (three-track) \tauhadvis. The
correction factor is obtained from a direct measurement of the fake-factors in
\btag events. 
%
The subtraction of the simulated samples is affected by experimental
uncertainties and uncertainties in production cross sections, which 
amount to 10\%. The total uncertainty in the \multijet estimate is
also propagated to the subtraction. Due to the large contamination for \btag events
in the \wcr, a 50\% uncertainty is assigned to the correction factor applied to
the \bveto parameterisation.  The applicability of \fW measured in the \wcr to
\failcr is investigated by studying \fW as a function of $\mT(\ptvec^{\ell},\metvec)$ and
the observed differences (up to $\sim$10\%) are assigned as a systematic
uncertainty. A 30\% uncertainty is assigned to the sequential
$|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$ correction, based on variations observed as a
function of \tauhadvis \pt.


In \mjcr, the selected lepton must fail isolation. The \multijet purity for
events that pass the \tauhadvis identification in this region is $\sim$88\% for
the \bveto category and $\sim$93\% for the \btag category.   All non-\multijet
contamination is subtracted from \mjcr using simulation.  The fake-factor \fMJ is further
split by category (\bveto, \btag) and by data-taking period (2015, 2016) to
account for changing isolation criteria in the trigger that affect \mjcr
differently to \sr. 
%
The applicability of \fMJ measured in \mjcr to \failcr is investigated by
studying \fMJ as a function of the lepton isolation and the observed
differences are assigned as a systematic uncertainty. The statistical
uncertainty from the limited size of \mjcr is significant, particularly for the
smaller 2015 dataset. The impact of a potential mismodelling in the subtraction
of simulated non-\multijet events containing non-isolated leptons is
investigated by varying the subtraction by 50\%, but is found to be small
compared to the other sources of systematic uncertainty.  A constant
uncertainty of 20\% in \fMJ is used to envelop these variations. A 50\%
uncertainty is assigned to the sequential $|\Delta\phi(\ptvec^{\tauhadvis},\metvec)|$
correction. 


\subsubsection{Lepton isolation fake-factor}
\label{sec:lephad:lepff}

The fake-factor \flep is measured in \flcr, which must have exactly one selected lepton,
\mbox{$\mT(\ptvec^{\ell},\metvec)<\SI{30}{\GeV}$} and no \tauhadvis candidates passing the loose
identification but rather at least one selected jet (not counting the
$b$-tagged jet in the \btag region). The selection is designed to purify
\multijet events while suppressing \Wjets and \ttbar events. Events where the selected
lepton passes (fails) isolation enter the \flep numerator (denominator).  All
non-\multijet contributions are subtracted using simulation. The
fake-factors are parameterised as a function of lepton $|\eta|$, and are
further split by lepton type (electron, muon), category (\bveto, \btag) and
into two regions of muon \pt, due to differences in the isolation criteria of
the low- and high-\pt triggers in the \muhad channel. A 20\% uncertainty
in the background subtraction is considered, motivated by observations of the 
tau identification performance in \Wjets events. The applicability of \flep measured in 
\flcr to events in \mjcr is investigated by altering the $\mT(\ptvec^{\ell},\metvec)$ selection and
the observed differences are assigned as a systematic uncertainty. The
statistical uncertainty from the limited size of \flcr is also considered,
but is relatively small. The total uncertainty in \flep is 5--50\%. 


%-------------------------------------------------------------------------------
\subsection{Jet background estimate in the \hadhad channel}
\label{sec:bkg:hadhad}
%-------------------------------------------------------------------------------

The fake-factor technique used to estimate the dominant \multijet background 
in the \hadhad channel is described in Section~\ref{sec:hadhad:multijet}.
The method used to weight simulated events to estimate the remaining background 
containing events with \tauhadvis candidates that originate from jets 
is described in Section~\ref{sec:hadhad:non-multijet}.

\subsubsection{\Multijet events}
\label{sec:hadhad:multijet}

The contribution of \multijet events in \sr is estimated by weighting events in a
control region that has the same selection as \sr, but where the
sub-leading \tauhadvis candidate fails identification (\failcr), by
the \dijet tau fake-factor (\fDJ), which is measured in the \dijet fakes-region (\djcr): 
\begin{equation*}
N_\mathrm{\multijet}^\mathrm{SR}(v;\mathbf{y}) = \fDJ(\mathbf{y}) \times 
\left [ N_\mathrm{data}^\mathrm{\failcr}(v;\mathbf{y}) 
      - N_\mathrm{non-MJ}^\mathrm{\failcr}(v;\mathbf{y})  \right ]\,,
\end{equation*}
%
where $\mathbf{y}$ is the set of auxiliary variables from the \fDJ parameterisation:
sub-leading \tauhadvis \pt and sub-leading \tauhadvis track multiplicity.  Unlike in the \lephad
channel, \failcr is completely dominated by \multijet events, so a single
fake-factor is sufficient. The non-\multijet contamination in \failcr,
$N_\mathrm{non-MJ}^\mathrm{\failcr}$, amounts to $\sim$$1.6\%$ ($\sim$$7.0\%$) in the \bveto (\btag) channel, and is subtracted using simulation.
 
The selection for the \djcr is designed to be as similar to the signal
selection as possible, while avoiding contamination from \tauhadvis. Events are
selected by single-jet triggers with \pt thresholds ranging from
\SIrange{60}{380}{\GeV}, with all but the highest-threshold trigger being
prescaled.  They must contain at least two \tauhadvis candidates, where the
leading candidate has $\pt>\SI{85}{\GeV}$ and also exceeds the trigger threshold by $10\%$,
and the sub-leading candidate has $\pt>\SI{65}{\GeV}$. The
\tauhadvis candidates must have opposite charge sign, be back to back in the
transverse plane, $|\Delta\phi(\ptvec^{\tau_1},\ptvec^{\tau_2})| > 2.7$ rad and the \pt of the
sub-leading \tauhadvis must be at least 30\% of the leading \tauhadvis \pt.
Unlike in the \lephad channel, the very loose \tauhadvis identification is not
required as the background is dominated by a single process (\multijet production), so
there is no need to restrict the contribution from gluon-initiated
jets. The fake-factors are measured using the sub-leading \tauhadvis candidate
to avoid trigger bias and to be consistent with their application in \failcr.
The purity of \multijet events that pass the \tauhadvis identification is 
98--99\% (93--98\%) for the \bveto (\btag) categories.  The non-\multijet
contamination is subtracted using simulation.  The dominant uncertainty in the
fake-factors is typically due to the limited size of the \djcr, and 
is 10--50\%; however, the subtraction of the non-\multijet contamination can
also result in uncertainties up to 50\%. An additional uncertainty is
considered when applying the fake-factors in the \btag category, which accounts
for changes in the jet composition with respect to the inclusive selection of
the \djcr. As the differences are extracted from comparisons in control
regions, they are one-sided. The fake-factors are shown in
Figure~\ref{fig:hadhad_fakefactors}. 

\begin{figure}[htp]
\begin{center}
\includegraphics[width=0.49\columnwidth]{\figpath/c520_hadhad_ff_split.pdf}
\caption{The \tauhadvis identification fake-factors in the \hadhad channel.
The red band indicates the total uncertainty when used with a 
$b$-inclusive or $b$-veto selection. The blue band indicates the 
additional uncertainty when used with a \btag selection.}
\label{fig:hadhad_fakefactors}
\end{center}
\end{figure}



\subsubsection{Non-\multijet events}
\label{sec:hadhad:non-multijet}

In the \hadhad{} channel, backgrounds originating from jets that are
misidentified as \tauhadvis in processes other than \multijet{} production
(predominantly \Wjets{} in the \bveto and \ttbar in the \btag categories) are
estimated using simulation.  Rather than applying the \tauhadvis identification
to the simulated jets, they are weighted by \frate{}s as in
Ref.~\cite{EXOT-2014-05}. This not only ensures the correct \frate{}, but
also enhances the statistical precision of the estimate, as events failing the
\tauhadvis identification are not discarded.  The \frate{} for the sub-leading
\tauhadvis candidate is defined as the ratio of the number of candidates that
pass the identification to the total number of candidates.  The \frate{} for
the leading \tauhadvis candidate is defined as the ratio of the number of
candidates that pass the identification and the single-tau trigger requirement
to the total number of candidates. 

The fake-rates applied to \ttbar and single-top-quark events are calculated from a
fakes region enriched in \ttbar events (T-FR), while the fake-rates for all other
processes are calculated in a fakes region enriched in \Wjets events (W-FR). Both
T-FR and W-FR use events selected by a single-muon trigger with a \pt
threshold of \SI{50}{\GeV}. They must contain exactly one isolated muon with
$\pt>\SI{55}{\GeV}$ that fired the trigger, no electrons and at least one
\tauhadvis candidate with $\pt>\SI{50}{\GeV}$. The events must also satisfy
$|\Delta\phi(\ptvec^{\mu},\ptvec^{\tauhadvis})|>\SI{2.4}{rad}$ and 
$\mT(\ptvec^{\mu},\metvec)>\SI{40}{\GeV}$.
The events are then categorised into \btag and \bveto categories, defining
T-FR and W-FR, respectively. Backgrounds from
non-\ttbar (non-\Wjets) processes are subtracted from T-FR (W-FR)
using simulation.  The fake-rates are measured using
the leading-\pt \tauhadvis candidate and are parameterised as functions of the
\tauhadvis \pt and track multiplicity.  The uncertainty in the fake-rates is
dominated by the limited size of the fakes regions and can reach 40\%.



