%-------------------------------------------------------------------------------
\section{Systematic uncertainties}
\label{sec:sys}
%-------------------------------------------------------------------------------

In this section, uncertainties affecting the simulated signal and background
contributions are discussed. These include uncertainties associated with the
determination of the integrated luminosity, the detector simulation,  the
theoretical cross sections and the modelling from the event generators.  The
uncertainties associated with the data-driven background estimates are
discussed in Section~\ref{sec:bkg}.


The uncertainty in the combined 2015+2016 integrated luminosity is 3.2\%, which
affects all simulated samples. It is derived, following a methodology similar
to that detailed in Ref.~\cite{DAPR-2013-01}, from a preliminary calibration of
the luminosity scale using $x$--$y$ beam-separation scans performed in August
2015 and May 2016.  The uncertainty related to the overlay of pile-up events is
estimated by varying the average number of interactions per bunch crossing by
9\%.  The uncertainties related to the detector simulation manifest themselves through the
efficiency of the reconstruction, identification and triggering algorithms, and
the energy scale and resolution for electrons, muons, \tauhadvis, ($b$-)jets
and the \metvec soft term. These uncertainties are considered for all simulated
samples; their impact is taken into account when estimating signal and 
background contributions and when subtracting contamination from regions 
in the data-driven estimates.  The effects of the particle energy-scale uncertainties
are propagated to \metvec.  The uncertainty in the \tauhadvis identification
efficiency as determined from measurements of \Ztautau events is 5--6\%. 
At high \pt{}, there are no abundant sources of
real hadronic tau decays from which an efficiency measurement could be made.
Rather, the tau identification is studied in high-\pt{} dijet events as a
function of the jet \pt{}, which indicates that there is no degradation in the
modelling of the detector response as a function of the \pt{} of tau
candidates.  Based on the limited precision of these studies, an additional
uncertainty of 20\%/\TeV{} (25\%/\TeV) for one-track (three-track) \tauhadvis candidates with
$\pt>\SI{150}{\GeV}$ is assigned.  The \tauhadvis trigger efficiency uncertainty 
is 3--14\%.
The uncertainty in the \tauhadvis energy scale is 2--3\%. The probability for
electrons to be misidentified as \tauhadvis is measured with a precision of
3--14\%~\cite{ATLAS-CONF-2017-029}.  The electron, muon, jet and \metvec
systematic uncertainties described above are found to have a very small impact.
 

Theoretical cross-section uncertainties are taken into account for all
backgrounds estimated using simulation.  For \DYjets production, uncertainties
are taken from Ref.~\cite{EXOT-2015-07} and include variations of the PDF
sets, scale, \alphas, beam energy, electroweak corrections and photon-induced
corrections. A single 90\% CL eigenvector variation uncertainty is used, based
on the CT14nnlo PDF set. The variations amount to a $\sim$$5\%$ uncertainty in
the total number of \DYjets events within the acceptance. For \diboson production,
an uncertainty of $10\%$ is used~\cite{Campbell:2011bn,Gleisberg:2008ta}. For
\ttbar~\cite{Czakon:2011xx} and \singletop~\cite{Aliev:2010zk, Kant:2014oha}
production, the assigned $6\%$ uncertainty is based on PDF, scale and top-quark
mass variations.  Additional uncertainties related to initial- and final-state
radiation modelling, tune and (for \ttbar only) the choice of 
\texttt{hdamp} parameter value in \POWHEGBOXV{ v2}, which controls the amount
of radiation produced by the parton shower,  are also taken into
account~\cite{ATL-PHYS-PUB-2016-004}.  The uncertainty due to the hadronisation model is
evaluated by comparing \ttbar events generated with \POWHEGBOXV{ v2} interfaced
to either \HERWIGpp~\cite{Herwigpp} or \PYTHIAV{6}. To estimate the uncertainty
in generating the hard scatter, the \POWHEG{} and MG5\_aMC@NLO event generators are
compared, both interfaced to the \HERWIGpp  parton shower model. The
uncertainties in the \Wjets cross section have a negligible impact in the
\hadhad channel and the \Wjets simulation is not used in the \lephad channel.

For MSSM Higgs boson samples, various sources of uncertainty 
which impact the signal acceptance are considered. The impact from varying the factorisation
and renormalisation scales up and down by a factor of two, either coherently or
oppositely, is taken into account.  Uncertainties due to the modelling of
initial- and final-state radiation, as well as multiple parton interaction are
also taken into account. These uncertainties are estimated from variations of
the \PYTHIAV{8} A14 tune~\cite{ATL-PHYS-PUB-2014-021} for the  $b$-associated
production and  the AZNLO \PYTHIAV{8} tune~\cite{AZNLO:2014} for the gluon--gluon
fusion production.  The envelope of the variations resulting from the use of
the alternative PDFs in the
PDF4LHC15$\_$nlo$\_$nf4$\_$30\,(PDF4LHC15$\_$nlo$\_$100)~\cite{Ball:2014uwa}
set is used to estimate the PDF uncertainty for the $b$-associated
(gluon--gluon fusion) production.  The total uncertainty for the MSSM Higgs boson
samples is typically 1--4\%, which is dominated by variations of the radiation
and multiple parton interactions, with minor impact from scale variations. 
The \Zprime signal acceptance uncertainties are expected to be negligible.

For both the MSSM Higgs boson and \Zprime samples, uncertainties in the
integrated cross section are not included in the fitting procedure used to
extract experimental cross-section limits. The uncertainty for \Zprime is
included when overlaying model cross sections, in which case it is calculated
using the same procedure as for the \DYjets background. 




