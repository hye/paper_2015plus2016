#!/bin/bash
#TAG=02-00-01
#TAG=03-02-00
TAG=04-01-00
#SVNBASE="svn+ssh://svn.cern.ch/reps"

function setlinks ()
{
    DIRS="bib journal latex logos"
    for DIR in $DIRS; do
        rm -f $DIR
        ln -s atlaslatex-$1/$DIR $DIR
    done

}

#svn co ${SVNBASE}/atlasgroups/pubcom/latex/atlaslatex/tags/atlaslatex-${TAG}
git clone -b atlaslatex-${TAG} ssh://git@gitlab.cern.ch:7999/atlas-physics-office/atlaslatex.git atlaslatex-${TAG}
setlinks ${TAG}

