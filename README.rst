==============================
2017 MSSM A/H/Z'->tautau Paper
==============================

Latex
=====
Instructions for building the latex part of the note

lxplus
~~~~~~
If you are on lxplus, please do the following before starting::

    setupATLAS
    lsetup git
    export PATH=/afs/cern.ch/sw/XML/TL2016/bin/x86_64-linux:$PATH

checkout dependencies
~~~~~~~~~~~~~~~~~~~~~
::

    source co_atlaslatex.sh

(note, if you are on lxplus, you'll probably need to do `setupATLAS; lsetup git` first)

build note
~~~~~~~~~~
::

    make


Figures 
=======
Instructions for building the figures

All following commands should be performed in the figures directory::

    cd figures


checkout dependencies
~~~~~~~~~~~~~~~~~~~~~
::

    source setup.sh

set input path
~~~~~~~~~~~~~~
set input path in Makefile file.


sync input data
~~~~~~~~~~~~~~~
::

    make sync

figures configuration
~~~~~~~~~~~~~~~~~~~~~
config files for plotting can be found in myconfig/ folder.


build figures
~~~~~~~~~~~~~
::

    make


