#!/bin/bash
echo "Generating inputs..."
#python make_hepinputs.py > /dev/null 

#echo "Creating hepdata_limit_zprime.yaml..."
#python print_hepdata_limit_Zprime.py > hepdata_limit_zprime.yaml
#echo "Creating hepdata_limit_ggH.yaml..."
#python print_hepdata_limit_ggH.py > hepdata_limit_ggH.yaml
#echo "Creating hepdata_limit_bbH.yaml..."
#python print_hepdata_limit_bbH.py > hepdata_limit_bbH.yaml
#echo "Creating hepdata_limit2D.yaml..."
#python print_hepdata_limit2D.py > hepdata_limit2D.yaml

#echo "Creating hepdata_acc_ggH.yaml..."
#python print_hepdata_acc_ggH.py > hepdata_acc_ggH.yaml
#echo "Creating hepdata_acc_bbH.yaml..."
#python print_hepdata_acc_bbH.py > hepdata_acc_bbH.yaml
#echo "Creating hepdata_acc_zprime.yaml..."
#python print_hepdata_acc_zprime.py > hepdata_acc_zprime.yaml

#python dump_hepdata_stacks.py
#python dump_hepdata_lims.py
python dump_hepdata_acc.py
#python dump_hepdata_nll.py
#python dump_hepdata_zpmodels.py
