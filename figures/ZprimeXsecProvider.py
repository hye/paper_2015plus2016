from pprint import pprint


class ZprimeXsecProvider:
    def __init__(self, model):
        self.dMassPoint = dict()
        for sLine in open(model+"_xsec"):
            if "#" in sLine: continue
            sLine = sLine.replace("\n","")
            sLine_parameter, sLine_values = sLine.split(" : ")
            sLine_parameter = " ".join([str(float(str(x.replace("m","-")))) for x in sLine_parameter.split(" ")])
            #sLine_parameter = int(float(sLine_parameter))
            self.dMassPoint[sLine_parameter] = sLine_values.split(" ")

    def getXsec(self, point):
        if not point in self.dMassPoint:
            print "point {} not found".format(point)
            print self.dMassPoint
            return float(self.dMassPoint[point.replace(" ","")][0]) * 1E-3
        else:
            return float(self.dMassPoint[point][0]) * 1E-3

    def getXsecUnc(self, point):
        if not point in self.dMassPoint:
            return float(self.dMassPoint[point.replace(" ","")][1]) * 1E-3
        else:
            return float(self.dMassPoint[point][1]) * 1E-3

    def getXsecUncAsymm(self, point):
        if not point in self.dMassPoint:
            return [float(self.dMassPoint[point.replace(" ","")][1]) * 1E-3,
                    float(self.dMassPoint[point.replace(" ","")][2]) * 1E-3]
        else:
            return [float(self.dMassPoint[point][1]) * 1E-3,
                    float(self.dMassPoint[point][2]) * 1E-3]

