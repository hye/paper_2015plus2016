#!/bin/bash
make -s atlasstyle
make -s figures
make -s inputs
export PYTHONPATH="${PWD}/figures:${PYTHONPATH}"
export PYTHONPATH="${PWD}:${PYTHONPATH}"
export PATH="${PWD}/figures/scripts:${PATH}"
#export PYTHONPATH="${PWD}/Htautau-limit-tools/scripts:${PYTHONPATH}"
