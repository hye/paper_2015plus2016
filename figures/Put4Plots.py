#!/usr/bin/env python
import ROOT,os

#ROOT.gROOT.SetBatch(ROOT.kTRUE)

def Put4Plots():
  f1 = ROOT.TFile.Open(input_dir+'c1170_postfit_lephad_bveto_MTTOT_ratio.root','r')
  f2 = ROOT.TFile.Open(input_dir+'c1171_postfit_lephad_btag_MTTOT_ratio.root','r')
  f3 = ROOT.TFile.Open(input_dir+'c1020_postfit_hadhad_bveto_MTTOT_ratio.root','r')
  f4 = ROOT.TFile.Open(input_dir+'c1021_postfit_hadhad_btag_MTTOT_ratio.root','r')

  c1 = f1.Get('rc')
  c2 = f2.Get('rc')
  c3 = f3.Get('rc')
  c4 = f4.Get('rc')

  c0 = ROOT.TCanvas('c1','',0,0,1400,1400)
  c0.Divide(2,2)
  c0.cd(1)
  c1.DrawClonePad()
  c0.cd(2)
  c2.DrawClonePad()
  c0.cd(3)
  c3.DrawClonePad()
  c0.cd(4)
  c4.DrawClonePad()

  c0.cd()

  c0.Update()
  c0.SaveAs('ctotal_postfit_MTTOT_ratio.pdf')
  c0.SaveAs('ctotal_postfit_MTTOT_ratio.eps')

  f1.Close()
  f2.Close()
  f3.Close()
  f4.Close()

if __name__ == '__main__':
  input_dir = '/afs/cern.ch/work/h/hye/SystematicsAnalysis_BSMtautau/paper_2015plus2016/figures/'
  Put4Plots()

