import pickle
from pdground import pdgRound

## dynamic config
hadhad = "HadHad"
lephad = "AllHad"
muhad = "MuHad"
ehad  = "ElHad"
cat_bveto  = "BMin0_J0_T0_isMVA0_L1_Y2015"
cat_btag   = "BMin0_J0_T1_isMVA0_L1_Y2015"
cat_bveto_hh = "BMin0_J0_T0_isMVA0_L0_Y2015"
cat_btag_hh  = "BMin0_J0_T1_isMVA0_L0_Y2015"

## static config
#fin_name = "inputs/lephad/postfityields.dat"
#fin_name = "inputs/yields/postfityieldslephadpost.dat"
#fin_name_hh = "inputs/yields/postfityieldshadhadpost.dat"
fin_name = "inputs/postfit/2017-07-14/postfityields.dat"
fin_name_hh = "inputs/postfit/2017-07-14/postfityields.dat"
region = "Region_{cat}_distMTtot_D{chan}"
namebkg = "L_x_{samp}_Region_{cat}_distMTtot_D{chan}_overallSyst_x_StatUncert"
namesig = "L_x_{samp}_Region_{cat}_distMTtot_D{chan}_overallSyst_x_HistSyst"
bkgs = [
        ("ZplusJets", r"\DYtt"), 
        ("Diboson",   r"\Diboson"),
        #("Tophh",     r"\ttbar and \singletop"),
        ("Top",     r"\ttbar and \singletop"),
        ("Fakes",     r"\jetfakes"),
        ("DYZ",       r"\DYll"),
        ]
sigs = [
    ("$A+H$", [
        "bbHlh500",
        "ggHlh500",
        ])
    ]

bkgs_hh = [
    ("Multijet", r"\Multijet"),
    ("Ztautau",  r"\DYtt"),
    ("Wtaunu",   r"\Wtaunujets"),
    ("Top",      r"\ttbar and \singletop"),
    ("Others",   r"\Others"),
    ]



header = r"{:40s} & {:24s} & {:24s} \\".format("Process", r"\bveto", r"\btag")
line   = r"{:40s} & {:10s} +- {:10s} & {:10s} +- {:10s} \\"
linedat= r"{:40s} & {:24d} & {:24d} \\"

def get_nums_bkg(d, samp, chan, cat): 
    r = region.format(cat=cat, chan=chan)
    s = namebkg.format(samp=samp, cat=cat, chan=chan)
    (n, en) = d[r][s]
    return pdgRound(n, en)

def get_ndat(d, chan, cat): 
    r = region.format(cat=cat, chan=chan)
    return int(d[r]["data"])

def get_ntot(d, chan, cat): 
    r = region.format(cat=cat, chan=chan)
    (n, en) = d[r]["Bkg"]
    return pdgRound(n, en)

def print_chan(d, chan): 
    print ""
    print "Channel: ", chan 
    print ""
    bkgs_tmp = bkgs_hh if chan=="HadHad" else bkgs
    cat_bveto_tmp = cat_bveto_hh if chan=="HadHad" else cat_bveto
    cat_btag_tmp = cat_btag_hh if chan=="HadHad" else cat_btag
    nbkg_btag = nbkg_bveto = 0.0
    for (sname, stitle) in bkgs_tmp: 
        """
        if chan == "AllHad": 
            (n_bveto_mh, _) = get_nums_bkg(d, sname, muhad, cat_bveto) 
            (n_btag_mh,  _)  = get_nums_bkg(d, sname, muhad, cat_btag) 
            (n_bveto_eh, _) = get_nums_bkg(d, sname, ehad, cat_bveto) 
            (n_btag_eh,  _)  = get_nums_bkg(d, sname, ehad, cat_btag) 
            n_bveto = str(float(n_bveto_mh) + float(n_bveto_eh))
            n_btag = str(float(n_btag_mh) + float(n_btag_eh))
            en_bveto = "0"
            en_btag = "0"
        else: 
        """
        (n_bveto, en_bveto) = get_nums_bkg(d, sname, chan, cat_bveto_tmp) 
        (n_btag,  en_btag)  = get_nums_bkg(d, sname, chan, cat_btag_tmp) 
        nbkg_btag += float(n_btag)
        nbkg_bveto += float(n_bveto)


        print line.format(stitle, n_bveto, en_bveto, n_btag, en_btag)
    print r"\midrule"

    #if chan == "HadHad": 
    #    print line.format("SM Total", str(nbkg_bveto), "0", str(nbkg_btag), "0")
    #    print linedat.format("Data",get_ndat(d,chan,cat_bveto_tmp), get_ndat(d,chan,cat_btag_tmp)) 
    """
    if chan == "AllHad":
        sname = ""
        (n_bveto_mh, _) = get_ntot(d, muhad, cat_bveto) 
        (n_btag_mh,  _)  = get_ntot(d, muhad, cat_btag) 
        (n_bveto_eh, _) = get_ntot(d, ehad, cat_bveto) 
        (n_btag_eh,  _)  = get_ntot(d, ehad, cat_btag) 
        n_bveto = str(float(n_bveto_mh) + float(n_bveto_eh))
        n_btag = str(float(n_btag_mh) + float(n_btag_eh))
        en_bveto = "0"
        en_btag = "0"
        print line.format("SM Total", n_bveto, en_bveto, n_btag, en_btag)
        
        n_bveto_mh = get_ndat(d, muhad, cat_bveto) 
        n_btag_mh  = get_ndat(d, muhad, cat_btag) 
        n_bveto_eh = get_ndat(d, ehad, cat_bveto) 
        n_btag_eh  = get_ndat(d, ehad, cat_btag) 
        n_bveto = n_bveto_mh + n_bveto_eh
        n_btag = n_btag_mh + n_btag_eh
        print linedat.format("Data",n_bveto, n_btag) 
    else: 
    """
    print line.format("SM Total", *get_ntot(d,chan,cat_bveto_tmp)+get_ntot(d,chan,cat_btag_tmp))
    print linedat.format("Data",get_ndat(d,chan,cat_bveto_tmp), get_ndat(d,chan,cat_btag_tmp)) 

## run
with open(fin_name) as fin: 
    d = pickle.load(fin)
with open(fin_name_hh) as fin: 
    d_hh = pickle.load(fin)

print header

#print_chan(d, ehad)
#print_chan(d, muhad)
print_chan(d, lephad)
print_chan(d_hh, hadhad)

















