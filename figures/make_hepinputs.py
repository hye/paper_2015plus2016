from myconfig.limits import generate_hepdata_inputs
#generate_hepdata_inputs()

from myconfig.lephad import dump_hepdata as dump_lephad
dump_lephad()

from myconfig.hadhad import dump_hepdata as dump_hadhad
dump_hadhad()
