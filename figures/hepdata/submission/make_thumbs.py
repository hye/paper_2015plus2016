from glob import glob
import subprocess

for fname in glob("*.png"): 
    if fname.startswith("thumb"): continue
    subprocess.call(["convert", fname, "-thumbnail", "20%", "thumb_{0}".format(fname)])

