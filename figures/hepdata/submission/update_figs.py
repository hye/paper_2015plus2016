import os, shutil, subprocess

## This was for pre-submission figure area
"""
DIR = "../.."
figs = {
    "fig_05a":"c170_postfit_lephad_bveto_MTtot.pdf",
    "fig_05b":"c171_postfit_lephad_btag_MTtot.pdf",
    "fig_05c":"c20_postfit_hadhad_bveto_MTtot.pdf",
    "fig_05d":"c21_postfit_hadhad_btag_MTtot.pdf",
    "fig_14a":"c180_postfit_lephad_bincl_MTtot.pdf",
    "fig_14b":"c30_postfit_hadhad_bincl_MTtot.pdf",

    "fig_07a": "c221_limit_ggH.pdf",
    "fig_07b": "c220_limit_bbH.pdf",
    "fig_07c": "c224_limit_zprime.pdf",

    "fig_08a": "c310_limit_frac_vs_mass.pdf", 
    "fig_08b": "c311_limit_frac_vs_mass_exp.pdf",
    
    "fig_21": "c241_limit_zpoverlay.pdf",
    "fig_22a":"c530_acc_ggH.pdf",
    "fig_22b":"c531_acc_bbH.pdf",
    "fig_22c":"c535_btagfrac_ggH.pdf",
    "fig_22d":"c534_btagfrac_bbH.pdf",
    "fig_23":"c532_acc_Zprime.pdf",
    
    "fig_24":"c340_dchi2_onslaught.pdf",
    "fig_25":"c341_dchi2_onslaught_exp.pdf",
    }

for (name, fname) in figs.items():
    subprocess.call(['convert', '-density', '100', '-trim', os.path.join(DIR,fname), '-quality', '100', name+".png"])
"""

## Post-submission figures
DIR="../../../web/Paper"
figs = ["fig_05a", "fig_05b", "fig_05c", "fig_05d", 
        "figaux_03a", "figaux_03b", 
        "fig_07a", "fig_07b", "fig_07c", 
        "fig_08a", "fig_08b", 
        "figaux_10",
        "figaux_12a", "figaux_12b", "figaux_12c", "figaux_12d",
        "figaux_13",
        "figaux_14", "figaux_15",
        ]

for name in figs:  
    subprocess.call(['convert', '-density', '100', '-trim', os.path.join(DIR,name)+".pdf", '-quality', '100', name+".png"])






