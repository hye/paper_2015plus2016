import pickle
from pdground import pdgRound
import ROOT
import os

def get_mssm_xsecs():
    g_gg = ROOT.TGraph2D()
    g_bb = ROOT.TGraph2D()
    with open("mssm_xsecs.txt") as f:
        for l in f:
            (m, tb, gg, bb) = l.split(",")
            m = int(m)
            tb = int(tb)
            gg = float(gg)
            bb = float(bb)
            g_gg.SetPoint(g_gg.GetN(), m, tb, gg)
            g_bb.SetPoint(g_bb.GetN(), m, tb, bb)

    return (g_gg, g_bb)

def get_inputs(fname):
    d = dict()
    with open(fname) as f: 
        for l in f.readlines():
            (_, proc, nums) = l.split(":")
            (mass, prod) = proc.strip().split(" ")
            (n, en) = nums.strip().split("+-")
            
            mass = int(mass)
            n = float(n)
            en = float(en)

            if mass not in d: d[mass] = dict()
            d[mass][prod] = [n, en]

    return d


header = r"{:40s} & {:24s} & {:24s} \\".format("Process", r"\bveto", r"\btag")
line   = r"{:40s} & {:10s} +- {:10s} & {:10s} +- {:10s} \\"

#tanb = 15.0
tanb = 10.0

basedir = "inputs/yields/v02"
fin_hh_btag = os.path.join(basedir, "hadhad_btag.txt")
fin_hh_bveto = os.path.join(basedir, "hadhad_bveto.txt")
fin_lh_btag = os.path.join(basedir, "lephad_btag.txt")
fin_lh_bveto = os.path.join(basedir, "lephad_bveto.txt")

d_lh_btag = get_inputs(fin_lh_btag)
d_lh_bveto = get_inputs(fin_lh_bveto)
d_hh_btag = get_inputs(fin_hh_btag)
d_hh_bveto = get_inputs(fin_hh_bveto)

"""
d_lephad = {
    300: {
        'bbH': {
            'btag': [457.409, 0.1],
            'bveto':[1080.24, 0.1],
            },
        'ggH': {
            'btag': [17.1963, 0.1],
            'bveto':[1446.49, 0.1],
            }
        },
    500: {
        'bbH': {
            'btag': [1.0, 0.1],
            'bveto':[1.0, 0.1],
            },
        'ggH': {
            'btag': [1.0, 0.1],
            'bveto':[1.0, 0.1],
            }
        },
    800: {
        'bbH': {
            'btag': [839.155, 0.1],
            'bveto':[1341.99, 0.1],
            },
        'ggH': {
            'btag': [42.5096, 0.1],
            'bveto':[2353.91, 0.1],
            }
        },
    }

d_hadhad = {
    300: {
        'bbH': {
            'btag': [84.2567, 0.1],
            'bveto':[191.659, 0.1],
            },
        'ggH': {
            'btag': [3.99263, 0.1],
            'bveto':[238.367, 0.1],
            }
        },
    500: {
        'bbH': {
            'btag': [1.0, 0.1],
            'bveto':[1.0, 0.1],
            },
        'ggH': {
            'btag': [1.0, 0.1],
            'bveto':[1.0, 0.1],
            }
        },
    800: {
        'bbH': {
            'btag': [897.113, 0.1],
            'bveto':[1381.82, 0.1],
            },
        'ggH': {
            'btag': [38.8893, 0.1],
            'bveto':[2233.76, 0.1],
            }
        },
    }
"""

(g_xsec_gg, g_xsec_bb) = get_mssm_xsecs()



if __name__ == "__main__": 

    print ""
    print "Lep-had" 
    print header

    masses = [300, 500, 800]

    for mass in masses: 
        name = 'H{}'.format(mass)
        xsec_gg = g_xsec_gg.Interpolate(mass, tanb)
        xsec_bb = g_xsec_bb.Interpolate(mass, tanb)
     
        
        ## btag 
        d = d_lh_btag[mass]
        nbbH_btag = d['bbH'][0] * xsec_bb
        enbbH_btag = d['bbH'][1] * xsec_bb
        nggH_btag = d['ggH'][0] * xsec_gg
        enggH_btag = d['ggH'][1] * xsec_gg
        n_btag = nbbH_btag + nggH_btag
        en_btag = enbbH_btag + enggH_btag

        ## bveto 
        d = d_lh_bveto[mass]
        nbbH_bveto = d['bbH'][0] * xsec_bb
        enbbH_bveto = d['bbH'][1] * xsec_bb
        nggH_bveto = d['ggH'][0] * xsec_gg
        enggH_bveto = d['ggH'][1] * xsec_gg
        n_bveto = nbbH_bveto + nggH_bveto
        en_bveto = enbbH_bveto + enggH_bveto


        vals_btag = pdgRound(n_btag, en_btag)
        vals_bveto = pdgRound(n_bveto, en_bveto)
        print line.format(name, *vals_bveto+vals_btag) 

    print ""
    print "Had-had" 
    print header

    for mass in masses: 
        name = 'H{}'.format(mass)
        xsec_gg = g_xsec_gg.Interpolate(mass, tanb)
        xsec_bb = g_xsec_bb.Interpolate(mass, tanb)
     
        
        ## btag 
        d = d_hh_btag[mass]
        nbbH_btag = d['bbH'][0] * xsec_bb
        enbbH_btag = d['bbH'][1] * xsec_bb
        nggH_btag = d['ggH'][0] * xsec_gg
        enggH_btag = d['ggH'][1] * xsec_gg
        n_btag = nbbH_btag + nggH_btag
        en_btag = enbbH_btag + enggH_btag

        ## bveto 
        d = d_hh_bveto[mass]
        nbbH_bveto = d['bbH'][0] * xsec_bb
        enbbH_bveto = d['bbH'][1] * xsec_bb
        nggH_bveto = d['ggH'][0] * xsec_gg
        enggH_bveto = d['ggH'][1] * xsec_gg
        n_bveto = nbbH_bveto + nggH_bveto
        en_bveto = enbbH_bveto + enggH_bveto


        vals_btag = pdgRound(n_btag, en_btag)
        vals_bveto = pdgRound(n_bveto, en_bveto)
        print line.format(name, *vals_bveto+vals_btag) 


