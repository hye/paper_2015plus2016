#!/usr/bin/env python
# encoding: utf-8
"""

  TODO:

  file:    plots.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

"""

## modules
import argparse, re, os

# ________________________________________________________________________________________________
def main():
    ## Define the Module
    parser = argparse.ArgumentParser(description="ATLAS figure generator")
    parser.add_argument("-c", "--config", default="myconfig/plots.py",
                        help="Plot configuration script (default: myconfig/plots.py)")
    parser.add_argument("-p", "--plots",  help="comma separated list of plot indices, can contain ranges (eg. 1,3-5)")
    parser.add_argument("-e", "--exts", default="png", help="comma separated list of file extensions (default: png)")
    parser.add_argument("-n", "--name", action="store_true", help="don't plot, but print plot name")
    parser.add_argument("-a", "--approved", action="store_true", help="only parse approved plots")
    parser.add_argument("-f", "--file", help="explicitly pass the output file name to be built")
    parser.add_argument("-g", "--gray", action="store_true", help="use gray-scale")
    args = parser.parse_args()
    
    from plotter.plotutils import SetAtlasStyle, import_module
    from ROOT import gROOT

    # process args
    exts = args.exts.split(',')

    ## remove shitty ansi-escapes on lxplus
    if args.plots:
        ansi_escape = re.compile(r'\x1b[^h]*h')
        args.plots = ansi_escape.sub('', args.plots)

    # import config
    mod = import_module(args.config)
    if not mod:
        print "ERROR, failed to import config module: {}".format(fname)
        parser.print_help()
        exit(1)

    # get plot list
    if not hasattr(mod, 'plots'):
        print "ERROR, 'plots' not defined in config module: {}".format(fname)
        parser.print_help()
        exit(1)
    plots = mod.plots
    avail_indices = [p.index for p in plots]

    # explicit listing of output filename
    if args.file:
        (fbase, fext) = os.path.splitext(args.file)

        # check ext
        if not fext:
            print "ERROR: file has no extension: {}".format(args.file)
            exit(1)
        if fext.startswith('.'): fext = fext[1:]
        exts = [fext]

        # get plot
        plot = None
        if args.gray:
            if fbase.endswith("_gray"):
                fbase = fbase[:-5]
            else:
                print "ERROR: grayscale plot must end in '_gray.ext'"
                exit(1)
        for p in plots:
            if p.cname() == fbase:
                plot = p
                break
        if not plot:
            print "ERROR: plot with name {} not found!".format(args.file)
            exit(1)
        plots = [plot]

    # list using indices
    else:
        # filter plots by index
        if args.plots:
            plotstr = args.plots.replace(" ","")
            listed_plots = []
            for pstr in plotstr.split(","):
                if "-" in pstr:
                    (pmin, pmax) = pstr.split("-")
                    listed_plots += range(int(pmin), int(pmax)+1)
                else:
                    listed_plots += [int(pstr)]

            # check indices exist
            missing_indices = [i for i in listed_plots if i not in avail_indices]
            if missing_indices:
                print "WARNING: no plots for: {}".format(str(missing_indices))
            # filter
            plots = [p for p in plots if p.index in listed_plots]

        # filter approved plots
        if args.approved:
            plots = [p for p in plots if p.approved]


    # all the ROOT stuff
    gROOT.SetBatch(True)

    ## Main plots
    if not args.name:
        SetAtlasStyle()
    for p in plots:
        if args.name:
            print p.cname()
        else:
            outputs = [p.cname() + "." + ext for ext in exts]
            print "building ", outputs
            p.plot(exts=exts, gray=args.gray)


if __name__ == '__main__': main()

#EOF
