# encoding: utf-8
'''
histutils.py
~~~~~~~~~~~~

This module provides tools for getting histograms from input files
and manipulating them into a format appropriate for ATLAS publications.

'''
__author__    = "Will Davey"
__email__     = "will.davey@cern.ch"
__created__   = "2017-03-07"
__copyright__ = "Copyright 2017 Will Davey"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# imports
from ROOT import TH1F, THStack, TGraph, TGraphAsymmErrors,  \
                 TLatex, TLegend, TLine, TFile,             \
                 Double, gROOT, gDirectory, kBlack
from math import sqrt, ceil, floor, log, exp, isinf
import random
from array import array
import itertools


#------------------------------------------------------------
class HistGrabber(object):
    """Grab histograms from input file

    python string formatting is used to easily define how
    the HistGrabber should grab histograms for different samples,
    variables, systematics etc. from input files.

    The filename and hist name are passed via *filestr* and
    *histstr*, which can contain key-word python formatting
    directives, such as::

        filestr = "inputs/{chan}/limit_inputs_feb23.root"
        histstr = "{samp}_{cat}_{chan}_{var}"

    The user then specifies their values by passing key-word
    arguments to the :func:`hist` function. Eg.::

        hist(samp="Ztautau", chan="hadhad", cat="btag0", var="mTtot")

    As we're exploiting the **kwargs python functionality, these
    can take any form you like.

    Any arguments passed to the :func:`hist` function can also be
    passed to the HistGrabber constructor, in which case they will
    act as default values.

    HistGrabber also provides options for on-the-fly rebinning and
    scaling of the histograms.

    Fixed width rebinning can be achieved by providing the rebinning
    number as *rebin* (as in ROOT TH1::Rebin).

    Variable width binning can be provided as a set of bin edges using
    *bins*. By default an alignment check is performed between the old
    and new bin edges. To disable set *checkbins=False*.

    .. note: Do not provide both *rebin* and *bins*

    The bin content can be divided by the bin width by setting
    *wcorr=True*. This is the recommendation from ATLAS when using
    variable-width binning.

    For details on the rebinning see :func:`rebin_hist`.

    You can also obtain the sum of numerous contributions by
    passing lists rather than single values in *kwargs*. Eg.::

        samp = ["Ztautau", "DYtautau"]

    You can also provide a 'fallback' HistGetter, which will be
    used incase the current HistGrabber fails to retrive a histogram.
    This can be useful eg. when grabbing systematic variations,
    if the variations are only provided for some of the input samples,
    you can default to the nominal if the hist isn't found.

    :param filestr: filename format string
    :param histstr: histogram name format string
    :param rebin: fixed-width rebin factor (int)
    :param bins: variable-width rebin edges (list float)
    :param wcorr: divide bin content by bin width (bool)
    :param checkbins: check that new and old bin edges align (bool)
    :param scale: apply constant scaling factor to hists (float)
    :param fallback: fallback HistGrabber
    :param kwargs: key-word args passed to the formatting strings
    """
    #____________________________________________________________
    def __init__(self, filestr, histstr, rebin=None, bins=None,
                 wcorr=None, checkbins=None, scale=None,
                 fallback=None, **kwargs):
        self.filestr = filestr
        self.histstr = histstr
        self.rebin=rebin
        self.bins = bins
        self.wcorr = wcorr
        self.checkbins = checkbins
        self.scale = scale
        self.fallback = fallback
        self.kwargs = kwargs
        self.files = dict()

    #____________________________________________________________
    def hist(self, name=None, rebin=None, bins=None,
             wcorr=None, checkbins=None, scale=None, **kwargs):
        """Return histogram retrieved from input file.

        The arguments take preference over those defined in the
        :class:`HistGrabber` constructor. See class description
        for more details.

        If *name* or *bins* provided, a new (or clone) histogram is made.

        If name="" is specified, a unique random name will be assigned
        to the histogram (useful to avoid ROOT issues with multiple
        histograms with same name).

        :param name: name of the new histogram
        :param rebin: fixed-width rebin factor (int)
        :param bins: variable-width rebin edges (list float)
        :param wcorr: divide bin content by bin width (bool)
        :param checkbins: check that new and old bin edges align (bool)
        :param scale: apply constant scaling factor to hists (float)
        :param kwargs: key-word args passed to the formatting strings
        :return: histogram (TH1)
        """
        assert not (rebin and bins), "Don't set both 'rebin' and 'bins'"
        # set defaults
        #if samp is None: samp = self.samp
        if rebin is None: rebin = self.rebin
        if bins is None: bins = self.bins
        if wcorr is None: wcorr = self.wcorr
        if checkbins is None: checkbins = self.checkbins
        if scale is None: scale = self.scale
        if name=="": name = get_unique_hist_name()

        # merge default+specific hist path args
        kw = dict(self.kwargs)
        for (k,v) in kwargs.items():
            if v is not None:
                kw[k] = v
        #print "args: ", kw


        # Get all combinations of kw input sets
        options = {key: kw[key] if isinstance(kw[key], list) else [kw[key]] for key in kw}
        keys, vals = options.keys(), options.values()
        product = itertools.product(*vals)
        options = [dict(zip(keys, x)) for x in product]

        # if multiple inputs defined, sum them
        if len(options) > 1:
            hists = [self.hist(name=get_unique_hist_name(), rebin=rebin, bins=bins,
                               wcorr=wcorr, checkbins=checkbins, scale=scale, **opt)
                     for opt in options]
            h = sum_hists(hists, name)
            for hist in hists:
                if not hist.GetDirectory(): hist.Delete()
            return h

        # Else, process single
        h = self.__get_single__(name=name, rebin=rebin, bins=bins, wcorr=wcorr,
                                checkbins=checkbins, scale=scale, **kw)

        if not h and self.fallback:
            print "Calling fallback grabber"
            h = self.fallback.hist(name=name, rebin=rebin, bins=bins, wcorr=wcorr,
                                   checkbins=checkbins, scale=scale, **kwargs)

        return h

    # ____________________________________________________________
    def __get_single__(self, name=None, bins=None, wcorr=None, checkbins=None, rebin=None, scale=None, **kw):
        """Return single histogram"""
        # get input file
        fname = self.filestr.format(**kw)
        if not fname in self.files:
            f = TFile.Open(fname)
            if not f:
                print "ERROR: couldn't open file: ", fname
                return None
            self.files[fname] = f
        f = self.files[fname]

        # get hist
        hname = self.histstr.format(**kw)
        h = f.Get(hname)
        if not h:
            print "ERROR: couldn't get hist: ", hname
            return None

        if bins or rebin or wcorr:
            if not name:
                # generate unique name for hist
                name += get_unique_hist_name()
            h = rebin_hist(h, bins=bins, factor=rebin, wcorr=wcorr, name=name, checkbins=checkbins)
        elif name is not None:
            h = h.Clone(name)
            h.SetDirectory(0)

        if scale:
            h.Scale(scale)

        return h


## ROOT object creation and manipulation
#______________________________________________________________________________
def get_unique_hist_name():
    """Return random unique histogram name"""
    gDirectory.cd("")
    while True:
        name = str(random.randint(0,9999999))
        if gDirectory.Get(name): continue
        return name


#______________________________________________________________________________
def sum_hists(hists, name=None):
    """Return sum of histograms

    :param hists: list of histograms
    :param name: name of histogram sum (default: 'h_merge')
    :return: summed histogram
    """
    if not name: name = "h_merge"
    h_sum = None
    for h in hists:
        if not h_sum:
            h_sum = h.Clone(name)
            h_sum.SetDirectory(0)
        else: h_sum.Add(h)
    return h_sum


#______________________________________________________________________________
def special_hist_merge(h1,h2):
    """Add second histogram (*h2*) to first histogram (*h1*)

    The two histograms may have different binning.

    :param h1: first histogram
    :param h2: second histogram
    """
    for ibin2 in range(1,h2.GetNbinsX()+1):
        x2 = h2.GetBinCenter(ibin2)
        y2 = h2.GetBinContent(ibin2)
        ey2 = h2.GetBinError(ibin2)
        ibin1 = h1.FindBin(x2)
        y1 = h1.GetBinContent(ibin1)
        ey1 = h1.GetBinError(ibin1)
        y = y1 + y2
        ey = sqrt(ey1**2 + ey2**2)
        h1.SetBinContent(ibin1,y)
        h1.SetBinError(ibin1,ey)


# ________________________________________________________________________________________________
def build_stack(hists):
    """Return stack of background (and sig) and hist of total background

    Total made only off bkg. Allow sig incase you want to stack it
    on top of bkg (not preffered at the moment)
    """
    if not hists: hists = []
    h_stack = THStack('h_stack', 'h_stack')
    for h in reversed(hists): h_stack.Add(h)
    return h_stack


# ________________________________________________________________________________________________
def build_total_stat_plus_sys(name=None, sys_sym=None, sys_asym=None,
                              grabber=None, grabber_sym = None, grabber_up = None, grabber_dn = None,
                              verbose = None, **kwargs):
    """Return summed histogram including systematics in the error band

    The systematic deviations from the nominal are symmetrised (average of up and down variation taken).
    The deviation from each systeamtic is then summed in quadrature, including the statistical uncertainty.

    :param name: name of new histogram
    :param samps: sample names
    :param systematics: list of systematics
    :param grabber: nominal grabber (:class:`HistGrabber`)
    :param grabber_up: 1sigma up systematic variation grabber (:class:`HistGrabber`)
    :param grabber_dn: 1sigma down systematic variation grabber (:class:`HistGrabber`)
    :param kwargs: key-word args passed to the grabbers
    :return: summed histogram
    """
    h_tot = grabber.hist(name=name, **kwargs)
    if verbose:
        ntot = h_tot.Integral()
        print "Nominal: {:10.1f}".format(ntot)

    # Asymmetric Systematics
    for sys in sys_asym:
        h_up = h_tot.Clone(get_unique_hist_name())
        h_up.Reset()
        h_dn = h_tot.Clone(get_unique_hist_name())
        h_dn.Reset()
        if verbose: print "\n Systematic: ", sys

        h_up_tmp = grabber_up.hist(name=get_unique_hist_name(), sys=sys, **kwargs)
        h_dn_tmp = grabber_dn.hist(name=get_unique_hist_name(), sys=sys, **kwargs)
        h_up.Add(h_up_tmp)
        h_dn.Add(h_dn_tmp)
        h_up_tmp.Delete()
        h_dn_tmp.Delete()
        """
        for s in samps:
            name_up = get_unique_hist_name()
            h_up_tmp = grabber_up.hist(name=name_up, sys=sys, **kwargs)
            if not h_up_tmp:
                h_up_tmp = grabber.hist(name=name_up, samp=s, **kwargs)
                if verbose: print "Adding nominal{}: {}".format(s, h_up_tmp.Integral())
            else:
                if verbose: print "Adding sys up {}: {}".format(s, h_up_tmp.Integral())
            name_dn = get_unique_hist_name()
            h_dn_tmp = grabber_dn.hist(name=name_dn, samp=s, sys=sys, **kwargs)
            if not h_dn_tmp:
                h_dn_tmp = grabber.hist(name=name_dn, samp=s, **kwargs)
                if verbose: print "Adding nominal{}: {}".format(s, h_dn_tmp.Integral())
            else:
                if verbose: print "Adding sys dn {}: {}".format(s, h_dn_tmp.Integral())
            h_up.Add(h_up_tmp)
            h_dn.Add(h_dn_tmp)
            h_up_tmp.Delete()
            h_dn_tmp.Delete()
        """


        if verbose:
            nup = h_up.Integral()
            ndn = h_dn.Integral()
            print "{:40s}: +{:10.3f} -{:10.3f}".format(sys, (nup-ntot)/ntot, (ndn-ntot)/ntot)
            print "{:40s}: +{:10.1f} -{:10.1f}".format(sys, nup, ndn)
        for ibin in range(0, h_tot.GetNbinsX() + 2):
            n = h_tot.GetBinContent(ibin)
            en = h_tot.GetBinError(ibin)
            esys = (abs(h_up.GetBinContent(ibin) - n) + abs(h_dn.GetBinContent(ibin) - n))/2.0
            h_tot.SetBinError(ibin, sqrt(en*en + esys*esys))
        h_up.Delete()
        h_dn.Delete()

    # Symmetric Systematics
    for sys in sys_sym:
        h_sys = h_tot.Clone(get_unique_hist_name())
        h_sys.Reset()
        if verbose: print "\n Systematic: ", sys
        h_sys_tmp = grabber_sym.hist(name=get_unique_hist_name(), sys=sys, **kwargs)
        h_sys.Add(h_sys_tmp)
        h_sys_tmp.Delete()
        """
        for s in samps:
            name_sys = get_unique_hist_name()
            h_sys_tmp = grabber_sym.hist(name=name_sys, samp=s, sys=sys, **kwargs)
            if not h_sys_tmp:
                h_sys_tmp = grabber.hist(name=name_sys, samp=s, **kwargs)
                if verbose: print "Adding nominal{}: {}".format(s, h_sys_tmp.Integral())
            else:
                if verbose: print "Adding sys sym {}: {}".format(s, h_sym_tmp.Integral())
            h_sys.Add(h_sys_tmp)
            h_sys_tmp.Delete()
        """

        if verbose:
            nsys = h_sys.Integral()
            print "{:40s}: {:+10.3f}".format(sys, (nsys-ntot)/ntot)
            print "{:40s}: {:+10.1f}".format(sys, nsys)
        for ibin in range(0, h_tot.GetNbinsX() + 2):
            n = h_tot.GetBinContent(ibin)
            en = h_tot.GetBinError(ibin)
            esys = (abs(h_sys.GetBinContent(ibin) - n))
            h_tot.SetBinError(ibin, sqrt(en*en + esys*esys))
        h_sys.Delete()



    return h_tot


# ________________________________________________________________________________________________
def build_error_hist(h_nom, h_up, h_dn, name="tot"):
    """Build a symmetric error band from nominal, 1sigma up and 1sigma down variations

    :param h_nom: nominal hist (TH1)
    :param h_up: 1sigma up variation (TH1)
    :param h_dn: 1sigma down variation (TH1)
    :return: TH1
    """
    h_err = h_nom.Clone(name)
    for i in xrange(1, h_nom.GetNbinsX()+1):
        h_err.SetBinError(i, abs(h_up.GetBinContent(i) - h_dn.GetBinContent(i)) / 2.0)
    return h_err


#______________________________________________________________________________
def apply_blind(h,val):
    """Blind histogram above threshold value

    Blind means "set content and error to zero".

    :param h: histogram
    :param val: threshold value
    """
    for i in xrange(h.GetNbinsX()):
        if h.GetBinLowEdge(i+2)>val: h.SetBinContent(i+1,0.0)
    pass


#______________________________________________________________________________
def set_axis_labels(h, labels):
    """Set histogram bin labels

    :param h: histogram
    :type h: TH1
    :param labels: bin label dict, key-val format bin-index: name
    :type labels: dict
    """
    for ibin in range(1, h.GetNbinsX()+1):
        if ibin in labels:
            h.GetXaxis().SetBinLabel(ibin, labels[ibin])


# ______________________________________________________________________________
def set_frame_axis_labels(fr, h, labels):
    """Set frame bin labels

    If you create a frame (eg. with TCanvas.DrawFrame), its difficult to use
    bin labels, since the frame has an arbitrary number of bins (typically 100 or so).
    This function determines where the labels should be placed on the frame axis,
    and then labels those bins accordingly.

    :param fr: frame
    :type fr: TH1
    :param h: histogram
    :type h: TH1
    :param labels: bin label dict, key-val format bin-index: name
    :type labels: dict
    """
    frbins = fr.GetNbinsX()
    frxmax = fr.GetBinLowEdge(frbins + 1)
    frxmin = fr.GetBinLowEdge(1)
    hbins = h.GetNbinsX()
    hxmax = h.GetBinLowEdge(hbins + 1)
    hxmin = h.GetBinLowEdge(1)
    for ibin in labels:
        hcent = h.GetBinCenter(ibin)
        hf = (hcent - hxmin) / (hxmax - hxmin)
        frpos = frxmin + hf * (frxmax - frxmin)
        bin_for_label = fr.FindBin(frpos)
        if bin_for_label <= frbins:
            fr.GetXaxis().SetBinLabel(bin_for_label, labels[ibin])
        fr.GetXaxis().LabelsOption("h")
        fr.GetXaxis().SetLabelSize(0.21)

#______________________________________________________________________________
def rebin_hist(h, bins=None, factor=None, wcorr=None, name=None, checkbins=None):
    """Rebin histogram

    Directly adds contents of old bins into new bins based on the bin center.

    By default the bin entries are divided by the width of the bin, which is
    the recommended procedure in ATLAS for histograms with varying bin width.
    To prevent this set *wcorr=True*.

    By default a check is performed to ensure that each new bin edge aligns
    with one of the old bin edges. This functionality can be switched off
    by setting *checkbins=False*. But please use with care!

    :param h: histogram
    :type h: TH1
    :param bins: new bin edges
    :type bins: list
    :param factor: number of bins for factor-based rebinning
    :type factor: int
    :param wcorr: appy width correction
    :type wcorr: bool
    :param name: name of new histogram
    :type name: str
    :param checkbins: requre bin alignment
    :type checkbins: bool
    """
    if checkbins is None: checkbins = True
    if wcorr is None: wcorr = False
    #print "rebin hist: ", h

    # variable width rebinning
    if bins:
        if checkbins:
            oldbins = [h.GetBinLowEdge(i) for i in range(1, h.GetNbinsX()+2)]
            for bin in bins:
                assert bin in oldbins, "Error rebinning hist: bin misalignment"

        if name is None:
            name = h.GetName()
            if not wcorr: name += "_nocorr"
        hnew = TH1F(name, h.GetTitle(), len(bins)-1, array('f', bins))
        hnew.Sumw2()
        hnew.SetDirectory(0)
        for i in range(0, h.GetNbinsX()+2):
            x = h.GetBinCenter(i)
            inew = hnew . FindBin(x)

            n = h.GetBinContent(i) + hnew. GetBinContent(inew)
            en = sqrt(h.GetBinError(i)**2 + hnew.GetBinError(inew)**2)
            hnew.SetBinContent(inew, n)
            hnew.SetBinError(inew, en)
    # constant width rebinning
    else:
        if name:
            hnew = h.Clone(name)
            hnew.SetDirectory(0)
        else:
            hnew = h
        if factor:
            hnew.Rebin(factor)
    #else:
    #    print "ERROR - must provide 'bins' or 'factor' to rebin_hist"
    #    return None

    if wcorr:
        for inew in range(0, hnew.GetNbinsX() +2 ):
            hnew.SetBinContent(inew, hnew.GetBinContent(inew) / hnew.GetBinWidth(inew))
            hnew.SetBinError(inew, hnew. GetBinError(inew) / hnew.GetBinWidth(inew))


    return hnew

#______________________________________________________________________________=buf=
def log_bins(nbins,xmin,xmax):
    """Returns list of log-separated bin edges
    :param nbins: number of bins
    :type nbins: int
    :param xmin: first bin low edge
    :type xmin: float
    :param xmin: last bin high edge
    :type xmin: float
    :rtype: float list
    """
    xmin_log = log(xmin)
    xmax_log = log(xmax)
    bins = [float(i) / float(nbins) * (xmax_log - xmin_log) \
                + xmin_log for i in xrange(nbins + 1)]
    bins = [exp(x) for x in bins]
    return bins


# ______________________________________________________________________________
def fix_mev_binning(h):
    """Divide histogram bin edges by 1000 (ie convert from MeV to GeV)

    :param h: histogram (TH1)
    """
    bins = array('d', [h.GetBinLowEdge(i) / 1000. for i in range(1, h.GetNbinsX() + 2)])
    h.SetBins(h.GetNbinsX(), bins)


#______________________________________________________________________________
def set_bin_edges(h, bins=None, wcorr=None, name=None):
    """Set bin edges

    Designed to be used to reset bin edges correctly if the input hist has
    had a fake-binning implemented (eg. like is used as input to hist-factory,
    which requires fixed-width bins)

    By default the bin entries are divided by the width of the bin, which is
    the recommended procedure in ATLAS for histograms with varying bin width.
    To prevent this set *wcorr=True*.

    :param h: histogram
    :type h: TH1
    :param bins: new bin edges
    :type bins: list
    :param wcorr: appy width correction
    :type wcorr: bool
    :param name: name of new histogram
    :type name: str
    """
    assert h.GetNbinsX() == len(bins)-1, "set_bin_edges(): Number of bin edges doesnt match input hist: {}, {} vs {}".format(h.GetName(), len(bins), h.GetNbinsX())

    hname = get_unique_hist_name()
    barr = array('f', bins)
    hnew = TH1F(hname, "", len(bins)-1, barr)
    for i in xrange(0, h.GetNbinsX()+2):
        n = h.GetBinContent(i)
        en = h.GetBinError(i)
        print i
        lastBin = h.GetNbinsX()
        if i == lastBin:
          overflowBinContent = h.GetBinContent(lastBin+1)
          overflowBinError = h.GetBinError(lastBin+1)
 
          n = n+overflowBinContent
          en = sqrt(en*en+overflowBinError*overflowBinError)

        if wcorr:
            n /= hnew.GetBinWidth(i)
            en /= hnew.GetBinWidth(i)

        hnew.SetBinContent(i, n)
        hnew.SetBinError(i, en)

        if i == lastBin:
          hnew.SetBinContent(lastBin+1, 0)
          hnew.SetBinError(lastBin+1, 0)
 

    hnew.SetDirectory(0)
    hname = name or h.GetName()
    if hname == h.GetName(): h.Delete()
    hnew.SetName(hname)

    return hnew

def set_sig_bin_width_corr(h, wcorr=None, name=None):
    hname = get_unique_hist_name()
    for i in xrange(0, h.GetNbinsX()+2):
        n = h.GetBinContent(i)
        en = h.GetBinError(i)
        if wcorr:
            n /= h.GetBinWidth(i)
            en /= h.GetBinWidth(i)

        h.SetBinContent(i, n)
        h.SetBinError(i, en)
    hname = name or h.GetName()

    hnew = h.Clone("")
    hnew.SetDirectory(0)
    if hname == h.GetName(): h.Delete()
    hnew.SetName(hname)
    return hnew
# ______________________________________________________________________________
def kill_uncertainties(h):
    """Set all bin uncertainties to zero

    :param h: histogram (TH1)
    """
    for i in range(0, h.GetNbinsX() + 2): h.SetBinError(i, 0.0)


# ______________________________________________________________________________
def kill_below(h, x):
    """Set all bin contents to zero below x value

    :param h: histogram (TH1)
    :param x: x value minimum
    """
    print "running kill below"
    for i in range(0, h.GetNbinsX() + 2):
        if h.GetBinLowEdge(i+1) <= x:
            print "killing bin {}, edges: {}, {}".format(i, h.GetBinLowEdge(i), h.GetBinLowEdge(i+1))
            h.SetBinContent(i, 0.0)
            h.SetBinError(i, 0.0)
        else:
            print "not killing bin {}, edges: {}, {}".format(i, h.GetBinLowEdge(i), h.GetBinLowEdge(i + 1))
    return


# ______________________________________________________________________________
def kill_above(h, x):
    """Set all bin contents to zero below x value

    :param h: histogram (TH1)
    :param x: x value maximum
    """
    for i in range(0, h.GetNbinsX() + 2):
        if h.GetBinLowEdge(i) >= x:
            h.SetBinContent(i, 0.0)
            h.SetBinError(i, 0.0)


# ______________________________________________________________________________
def get_poissonized_graph(h):
    """Return graph representing histogram with Poisson uncertainties

    :param h: histogram (TH1)
    :return: graph (TGraphAsymmErrors)
    """
    graph = TGraphAsymmErrors()
    graph.SetName(h.GetName())
    j=0
    for i in range(1, h.GetNbinsX()+1):
        if h.GetBinContent(i)!=0:
            graph.SetPoint(j, h.GetBinCenter(i), h.GetBinContent(i))
            graph.SetPointError(j, 0., 0.,
                                error_poisson_down(h.GetBinContent(i), h.GetBinError(i)),
                                error_poisson_up(h.GetBinContent(i), h.GetBinError(i)))
            j+=1
    return graph


# ______________________________________________________________________________
def get_poissonized_ratio_graph(h_data, h_mc):
    """Return Data/MC ratio graph accounting for Poisson uncertainty on data.

    :param h_data: data histogram (TH1)
    :param h_mc: MC histogram (TH1)
    :return: ratio graph (TGraphAsymmErrors)
    """
    graph = TGraphAsymmErrors()
    j=0
    for i in range(1, h_data.GetNbinsX()+1):
        nmc   = h_mc.GetBinContent(i)
        ndata = h_data.GetBinContent(i)
        endata = h_data.GetBinError(i)
        if nmc !=0 and ndata!=0 :
            graph.SetPoint(j, h_data.GetBinCenter(i), ndata/ nmc )
            graph.SetPointError(j, 0., 0.,
                                error_poisson_down(ndata, endata)/ nmc ,
                                error_poisson_up(ndata, endata)/ nmc
                                )
            j+=1
    return graph


# ______________________________________________________________________________
def get_mc_ratio_band(h_mc):
    """Return MC uncertainty band for Data/MC ratio

    :param h_mc: total MC histogram
    :return: uncertainty band graph (TGraphAsymmErrors)
    """
    graph = TGraphAsymmErrors()
    j=0
    for i in range(0, h_mc.GetNbinsX()+2) :
        nmc   = h_mc.GetBinContent(i)
        enmc   = h_mc.GetBinError(i)
        enmcfrac = enmc/ nmc if nmc else 0.0
        graph.SetPoint(j, h_mc.GetBinCenter(i),1. )
        graph.SetPointError(j,
                            h_mc.GetBinWidth(i)/ 2. ,
                            h_mc.GetBinWidth(i)/ 2. ,
                            enmcfrac,
                            enmcfrac,
                            )
        j+= 1
    return graph


# ______________________________________________________________________________
def get_significance_ratio(h_data, h_mc, ismc=False):
    """Return histogram of bin-wise significance of data given mc expectation

    The uncertainty of the mc expectation will be used in the calculation.

    If *ismc* is set true, then *h_data* is treated as a signal histogram,
    and the expected S+B significance is returned.

    Currently using calculation from::

        https://root.cern.ch/root/html526/RooStats__NumberCountingUtils.html

    TODO: should check out the modifications proposed in https://arxiv.org/pdf/1111.2062v4.pdf.

    :param h_data: data (or signal if (ismc=True))
    :param h_mc: total background histogram (TH1)
    :param ismc: set true to return expected S+B significance (TH1)
    :return: histogram of bin-wise significance (TH1)
    """
    from ROOT import RooStats
    h_ratio = h_data.Clone("ratio")
    h_ratio.Reset()
    for i in range(1, h_data.GetNbinsX()+1 ) :
        ndata = h_data.GetBinContent(i)
        nmc = h_mc.GetBinContent(i)
        enmc = h_mc.GetBinError(i)
        enmc_frac = enmc / nmc if nmc else 0.0
        if not ndata:
            z = 0.0
        else:
            if not ismc:
                z = RooStats.NumberCountingUtils.BinomialObsZ(ndata, nmc, enmc_frac)
            else:
                z = RooStats.NumberCountingUtils.BinomialExpZ(ndata, nmc, enmc_frac)
                if z < 0:
                    print "WARNING Sig: {}, nsig: {}, nbkg: {} +- {}".format(z, ndata, nmc, enmc)

        if isinf(z): z = 999.
        h_ratio.SetBinContent(i, z)
        h_ratio.SetBinError(i, 0)

        #if ismc: print "nsig: {}, nbkg: {} +- {}, z: {}".format(ndata, nmc, enmc, z)

    return h_ratio


# ______________________________________________________________________________
def get_significance_ratio_diego(h_data, h_mc, ismc=False):
    """Return histogram of bin-wise significance of data given mc expectation

    The uncertainty of the mc expectation will be used in the calculation.

    If *ismc* is set true, then *h_data* is treated as a signal histogram,
    and the expected S+B significance is returned.

    Follows prescription from: http://arxiv.org/abs/1111.2062
    Using code from: http://svn.cern.ch/guest/psde/

    Also linked off Diego's home page:
    http://casadei.web.cern.ch/casadei/programming/sw.html

    TODO: add the source code into this package

    :param h_data: data (or signal if (ismc=True))
    :param h_mc: total background histogram (TH1)
    :param ismc: set true to return expected S+B significance (TH1)
    :return: histogram of bin-wise significance (TH1)
    """
    import ROOT
    ROOT.gROOT.ProcessLine(".L pValuePoissonError.C+")
    ROOT.gROOT.ProcessLine(".L CompareHistograms.C+")

    if ismc:
        h_data = h_data.Clone("{}_splusb".format(h_data.GetName()))
        h_data.Add(h_mc)

    return ROOT.CompareHistograms(h_data, h_mc, False, True)


#______________________________________________________________________________
def make_band(g1,g2):
    """Return band graph from upper and lower band envelope graphs

    :param g1: envelope graph 1 (TGraph)
    :param g2: envelope graph 2 (TGraph)
    :return: band graph (TGraph)
    """
    x = array('d',[])
    y = array('d',[])

    for i in xrange(g1.GetN()):
        x.append(g1.GetX()[i])
        y.append(g1.GetY()[i])
    for i in xrange(g2.GetN()):
        x.append(g2.GetX()[g2.GetN()-1-i])
        y.append(g2.GetY()[g2.GetN()-1-i])

    return TGraph(g1.GetN()+g2.GetN(),x,y)


#______________________________________________________________________________
def invert_eff(h,name=None):
    """Return efficiency histogram with 1-e in each bin.

    :param h: input efficiency histogram (TH1)
    :param name: name of output histogram (default: <input-hist-name>_inv)
    :return: inverse efficiency histogram (TH1)
    """
    if name == None: name = "%s_inv"%h.GetName()
    hnew = h.Clone(name)
    for i in range(0,hnew.GetNbinsX()+2):
        hnew.SetBinContent(i,1.0-h.GetBinContent(i))
    return hnew


#______________________________________________________________________________
def get_eff_graph(h_pass,h_tot,name=None):
    """Return efficiency graph with correct uncertainties

    Uses BayesDivide integrated in TGraphAsymmErrors::Divide method.

    :param h_pass: histogram with pass events
    :param h_tot: histogram with total events
    :param name: name of output graph (default: "g_eff")
    :return: efficiency graph (TGraphAsymmErrors)
    """
    if name is None: name = "g_eff"
    g = TGraphAsymmErrors()
    g.SetName(name)
    g.Divide(h_pass,h_tot,'cl=0.683 b(1,1) mode')
    return g


#______________________________________________________________________________
def get_roc(h_sig,h_bkg):
    """Return ROC curve

    :param h_sig: signal histogram (TH1)
    :param h_bkg: background histogram (TH1)
    :return: ROC curve (TGraph)
    """
    xarr = array('d',[])
    yarr = array('d',[])
    nbins = h_sig.GetNbinsX()
    tot_sig = h_sig.Integral(0,nbins+2)
    tot_bkg = h_bkg.Integral(0,nbins+2)
    for i in xrange(h_sig.GetNbinsX()+2):
        xeff = h_sig.Integral(i,nbins+2) / tot_sig
        yeff = 1.0 - h_bkg.Integral(i,nbins+2) / tot_bkg
        xarr.append(xeff)
        yarr.append(yeff)

    g = TGraph(len(xarr),xarr,yarr)
    return g




## Numerical calculations
# ______________________________________________________________________________
def error_poisson_up(data, edata):
    """Return upper edge of 68% Poisson interval given observation.

    If the data has been scaled, the error on the data will be used to
    infer the original number of events (an integer).

    :param data: number of observed events
    :param edata: Gaussian uncertianty on number of observed events
    :return: upper edge of 68% interval
    """
    data_corr = data
    if not isinstance(data, int):
        # print 'warning, data not int: ',data, ' +- ', edata, ',  correcting...'
        data_corr = pow(data/ edata,2)
        if abs(floor(data_corr)- data_corr) < abs(ceil(data_corr)- data_corr):
            data_corr = floor(data_corr)
        else:
            data_corr = ceil(data_corr)
        # print 'corrected entries: ', data_corr

    y1 = data_corr + 1.0
    d = 1.0 - 1.0/(9.0 * y1) + 1.0/(3* sqrt ( y1 ))
    if data_corr <= 0 or data <= 0:
        return 0.0000001
    return (y1*d*d*d- data_corr ) / data_corr * data


# ______________________________________________________________________________
def error_poisson_down(data,edata):
    """Return lower edge of 68% Poisson interval given observation.

    If the data has been scaled, the error on the data will be used to
    infer the original number of events (an integer).

    :param data: number of observed events
    :param edata: Gaussian uncertianty on number of observed events
    :return: lower edge of 68% interval
    """
    data_corr = data
    if not isinstance(data,int): # print 'warning, data not int: ',data, ' +- ', edata, ',  correcting...'
        data_corr = pow(data/edata,2)
        if abs( floor( data_corr)-data_corr) < abs(ceil( data_corr)-data_corr): data_corr = floor( data_corr)
        else: data_corr = ceil(data_corr)
    # print 'corrected entries: ', data_corr

    y = data_corr
    if y == 0.0: return 0.0
    d = 1.0 - 1.0/(9.0*y) - 1.0/(3.0*sqrt(y))
    if data_corr <= 0 or data <= 0:
        return 0.0000001
    return (data_corr-y*d*d*d)/data_corr * data


#______________________________________________________________________________
def integral_and_error(h, xmin=None, xmax=None):
    """Return integral and error of a histogram

    Range is specified using *xmin* and *xmax* as floating point values (not bin indices).
    Range will include under/overflow if not specified.

    :param h: histogram
    :param xmin: lower bound of integral (float)
    :param xmax: upper bound of integral (float)
    :return: tuple (integral, error on integral)
    """
    nbins = h.GetNbinsX()
    error = Double(0)
    if xmin is None:
        bin1 = 0
    else:
        bin1 = h.FindBin(xmin)
    if xmax is None:
        bin2 = nbins+1
    else:
        bin2 = h.FindBin(xmax)-1
    integral = h.IntegralAndError(bin1, bin2 , error)
    error = float(error)
    return integral, error


#______________________________________________________________________________
def full_integral(h):
    """Return integral of histogram including over and underflow"""
    return h.Integral(0,h.GetNbinsX()+2)


#______________________________________________________________________________
def full_integral_and_error(h):
    """Return integral and error of histogram including over and underflow"""
    error = Double(0)
    h.IntegralAndError(0,h.GetNbinsX()+2,error)
    return error



#______________________________________________________________________________=buf=
def graphs_compatible(g1, g2):
    """Retrun True if graphs are compatible

    Checks size and x-values

    :param g1: numerator graph
    :type g1: :class:`ROOT.TGraph`
    :param g1: denominator graph
    :type g1: :class:`ROOT.TGraph`
    """
    if g1.GetN() != g2.GetN(): return False
    for i in xrange(g1.GetN()):
        if g1.GetX()[i] != g2.GetX()[i]: return False
    return True


#______________________________________________________________________________=buf=
def get_graph_point(g, x):
    """Retrun index of graph (*g*) point with *x* value, None if no match.

    :param g: numerator graph
    :type g: :class:`ROOT.TGraph`
    :param x: x value
    :type x: float
    :rtype: int
    """
    for i in xrange(g.GetN()):
        if g.GetX()[i] == x: return i
    return None


# ______________________________________________________________________________=buf=
def divide_graphs(g_num, g_den, name=None):
    """Return ratio of two graphs

    :param g_num: numerator graph
    :type g_num: :class:`ROOT.TGraph`
    :param g_den: denominator graph
    :type g_den: :class:`ROOT.TGraph`
    :param name: name for new graph
    :type name: str
    :rtype: :class:`ROOT.TGraph`
    """
    # check graph compatibility
    if not graphs_compatible(g_num, g_den):
        print "Trying to divide incompatible graphs: %s, %s" % (g_num.GetName(), g_den.GetName())

    # calcualte ratio points
    x_arr = array('d', [])
    y_arr = array('d', [])
    for i in xrange(g_num.GetN()):
        x = g_num.GetX()[i]
        if get_graph_point(g_den, x) is None: continue
        y_num = g_num.GetY()[i]
        y_den = g_den.Eval(x)
        y = y_num / y_den if y_den else 0.0
        x_arr.append(x)
        y_arr.append(y)

    # construct and return graph
    g = TGraph(len(x_arr), x_arr, y_arr)
    g.SetName(name or 'g_ratio')
    g.GetXaxis().SetTitle(g_num.GetXaxis().GetTitle())
    g.GetYaxis().SetTitle('Ratio')
    return g





def dump_hepvals_arr(f, arr, arr_e=None, arr_up=None, arr_dn=None, arr_up2=None, arr_dn2=None,
                     title=None, units=None, cme=None, lumi=None, qualifiers=None):
    """

    :param g:
    :return:
    """
    if title is None: title = "Undefined variable"
    if units:
        f.write("- header: {name: '%s', units: %s}\n" % (title, units))
    else:
        print f.write("- header: {name: '%s'}\n" % (title))
    if cme or lumi or qualifiers:
        f.write("  qualifiers:\n")
        #if cme:  f.write("  - {name: SQRT(S), units: GeV, value: %.1f}\n" % (cme))
        #if lumi: f.write("  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: %.1f}\n" % (lumi))
        if qualifiers:
            for (k,v) in qualifiers:
                if isinstance(v, dict):
                    f.write("  - {name: %s, %s}\n" % (k, ", ".join(["%s: %s"%(a,b) for (a,b) in v.items()])))
                else:
                    f.write("  - {name: %s, value: %s}\n" % (k,v))
    f.write("  values:\n")
    if arr_e:
        for (v,e) in zip(arr,arr_e):
            f.write("  - value: %f\n" % (v))
            f.write("    errors: \n")
            f.write("    - {symerror: %.4g}\n" % (e))
    elif arr_up and arr_dn and arr_up2 and arr_dn2:
        for(v,eup,edn,eup2,edn2) in zip(arr,arr_up,arr_dn,arr_up2,arr_dn2):
            f.write("  - value: %f\n" % (v))
            f.write("    errors: \n")
            f.write("    - {asymerror: {plus: %.4g, minus: %.4g}, label: '1 sigma'}\n" % (eup, edn))
            f.write("    - {asymerror: {plus: %.4g, minus: %.4g}, label: '2 sigma'}\n" % (eup2, edn2))
    elif arr_up and arr_dn:
        for(v,eup,edn) in zip(arr,arr_up,arr_dn):
            f.write("  - value: %f\n" % (v))
            f.write("    errors: \n")
            f.write("    - {asymerror: {plus: %.4g, minus: %.4g}}\n" % (eup, edn))
    else:
        for v in arr:
            f.write("  - {value: %f}\n" % (v))


# ______________________________________________________________________________=buf=
def dump_hepdist(h_data, h_mc, h_mc_up=None, h_mc_dn=None, sigs=None,
                 cme=None, lumi=None, qualifiers=None,
                 fname="hepdata.yaml"):
    """Dump Observed and Expected distribution to YAML HepData format

    Asymmetric uncertainties can be provided via *h_mc_up* and *h_mc_dn*

    :param h_data: data distribution
    :param h_mc: expected distribution
    :param h_mc_up: expected distribution +1 sigma
    :param h_mc_dn: expected distribution -1 sigma
    :param fname:
    :return:
    """
    if not qualifiers: qualifiers = []

    f = open(fname, 'w')

    ## bin edges
    f.write("independent_variables:\n")
    f.write("- header: {name: mTtot, units: GeV}\n")
    f.write("  values:\n")
    for i in xrange(1, h_data.GetNbinsX() + 1):
        low = h_data.GetBinLowEdge(i)
        high = h_data.GetBinLowEdge(i + 1)
        f.write("  - {low: %.0f, high: %.0f}\n" % (low, high))

    ## bin contents
    f.write("dependent_variables:\n")
    # data
    a_data = [h_data.GetBinContent(i) for i in xrange(1, h_data.GetNbinsX() + 1)]
    dump_hepvals_arr(f, a_data, title='NUM EVENTS', qualifiers=qualifiers + [['Process','Data']])

    # mc
    a_mc = [h_mc.GetBinContent(i) for i in xrange(1, h_mc.GetNbinsX() + 1)]
    if h_mc_up and h_mc_dn:
        a_mc_up = [h_mc_up.GetBinContent(i) - h_mc.GetBinContent(i) for i in xrange(1, h_mc.GetNbinsX() + 1)]
        a_mc_dn = [h_mc_dn.GetBinContent(i) - h_mc.GetBinContent(i) for i in xrange(1, h_mc.GetNbinsX() + 1)]
        dump_hepvals_arr(f, a_mc, arr_up=a_mc_up, arr_dn=a_mc_dn,
                     title='NUM EVENTS', qualifiers=qualifiers + [['Process','SM']])
    else:
        a_mc_e = [h_mc.GetBinError(i) for i in xrange(1, h_mc.GetNbinsX() + 1)]
        dump_hepvals_arr(f, a_mc, arr_e=a_mc_e,
                     title='NUM EVENTS', qualifiers=qualifiers + [['Process','SM']])

    # sigs
    if sigs:
        for h_sig in sigs:
            a_sig = [h_sig.GetBinContent(i) for i in xrange(1, h_sig.GetNbinsX() + 1)]
            dump_hepvals_arr(f, a_sig, title='NUM EVENTS', qualifiers=qualifiers + [['Process', h_sig.GetName()]])


    f.close()




#EOF
