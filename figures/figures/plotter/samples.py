# encoding: utf-8
'''
  file:    samples.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
import ROOT

# ________________________________________________________________________________________________
class Sample(object):
    # ________________________________________________________________________________________________
    def __init__(self, _name,
                 _line_color=None,
                 _line_style=None,
                 _line_width=None,
                 _fill_color=None,
                 _fill_style=None,
                 _marker_color=None,
                 _marker_style=None,
                 _marker_size=None,
                 ):
        self.name = _name
        self.tlatex = _name
        self.line_color = _line_color
        self.line_style = _line_style
        self.line_width = _line_width
        self.fill_color = _fill_color
        self.fill_style = _fill_style
        self.marker_color = _marker_color
        self.marker_style = _marker_style
        self.marker_size = _marker_size

    # ________________________________________________________________________________________________
    def style_hist(self, hist):
        if self.line_color != None: hist.SetLineColor(self.line_color)
        if self.line_style != None: hist.SetLineStyle(self.line_style)
        if self.line_width != None: hist.SetLineWidth(self.line_width)
        if self.fill_color != None: hist.SetFillColor(self.fill_color)
        if self.fill_style != None: hist.SetFillStyle(self.fill_style)
        if self.marker_color != None: hist.SetMarkerColor(self.marker_color)
        if self.marker_style != None: hist.SetMarkerStyle(self.marker_style)
        # if self.marker_size  !=None: hist.SetMarkerSize( self.marker_size )
        hist.tlatex = self.tlatex


# ________________________________________________________________________________________________
def style_hist(h, name=None):
    if name is None: name = h.GetName()
    s = globals()[name]
    s.style_hist(h)





## Data
data = Sample('Data',
    _marker_color=ROOT.kBlack,
    _marker_style=20,
    _marker_size=1.5)


## Backgrounds
ztautau = Sample("#it{Z}/#it{#gamma}*#rightarrow#it{#tau#tau}",
                 _line_color = ROOT.kBlack,
                 _fill_color = ROOT. kBlue-9,
                 _fill_style = 1000)

zmumu = Sample('#it{Z}#rightarrow#it{#mu#mu}',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT.kOrange,
    _fill_style = 1000)

zee = Sample('#it{Z}#rightarrow#it{ee}',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kGreen+4,
    _fill_style = 1000)

zll = Sample('#it{Z}/#it{#gamma}*#rightarrow#it{ll}',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kGreen -9,
    _fill_style = 1000)

wmunu = Sample('#it{W}#rightarrow#it{#mu#nu}',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT.kOrange,
    _fill_style = 1000)

wtaunu = Sample('#it{W}#rightarrow#it{#tau#nu}',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kRed -7,
    _fill_style = 1000)

wjets = Sample('#it{W}+jets',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kYellow -10,
    _fill_style = 1000)

wzjets = Sample('#it{W}/#it{Z}/#it{#gamma}*+jets',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kYellow -10,
    _fill_style = 1000)

taufakes = Sample('Jet#rightarrow#it{#tau} fake',
    _line_color = ROOT.kBlack,
    #_fill_color = ROOT. kRed -3,
    _fill_color = ROOT.TColor.GetColor(0.75 ,0.45 ,0.85),
    _fill_style = 1000)
lepfakes = Sample('Multijet',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT.TColor.GetColor(0.95 ,0.95 ,0.95),
    _fill_style = 1000)

ttbar = Sample('#it{t#bar{t}}',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kOrange +8,
    _fill_style = 1000)

#'#it{t#bar{t}}+single top',
top = Sample('Top quarks',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kYellow -10,
    _fill_style = 1000)

multijet = Sample('Multijet',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT.TColor.GetColor(0.95 ,0.95 ,0.95),
    _fill_style = 1000)

others = Sample('Others',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kYellow -8,
    _fill_style = 1000)

bkg = Sample('Background',
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kRed + 3,
    _fill_style=1000)


# Signals
H1000 = Sample('H1000',
    _line_width=2,
    _line_color=ROOT.kMagenta,
    _fill_color=0,
    _fill_style=0)

H600 = Sample('H600',
    _line_width=2,
    _line_color=ROOT.kGreen,
    _fill_color=0,
    _fill_style=0)

H400 = Sample('H400',
    _line_width=2,
    _line_color=ROOT.kBlue,
    _fill_color=0,
    _fill_style=0)
## Auxiliary
error = Sample('Uncertainty',
    _fill_color=ROOT.kBlack,
    _fill_style=3254,
    _line_color=0,
    _marker_style=0)

error_stat = Sample('Uncertainty (stat.)',
    _fill_color=ROOT.kBlack,
    _fill_style=3245,
    _line_color=0,
    _marker_style=0)


ratio = Sample('ratio',
    _fill_color=ROOT.kRed-3,
    _fill_style=1000,
    _marker_color=ROOT.kBlack,
    _marker_style=20)

ratio_stat = Sample('ratio_stat',
    _fill_color=ROOT.kYellow-10,
    _fill_style=1000,
    _marker_color=ROOT.kBlack,
    _marker_style=20)


ratio_line = Sample('ratio_line',
    _line_color=ROOT.kRed-3,
    _line_width=2)

significance = Sample('significance',
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kRed,
    _fill_style=1000)





