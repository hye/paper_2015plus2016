# encoding: utf-8
'''
  file:    histutils.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
# imports
from array import array
from math import log10

from ROOT import TH1F, TFile, TLatex, TLegend, TLine, TCanvas, TPad, TColor, \
    gROOT, gPad, gStyle, kBlack

atlasStyle = None

class Plot(object):
    """
    TODO:
    """
    # ________________________________________________________________________________________________
    def __init__(self, index, name=None, func=None, approved=None, **kwargs):
        self.index = index
        self.name = name or ""
        self.func = func
        self.approved = approved
        self.kwargs = kwargs

        assert index is not None, "Must provide Plot index"
        assert func is not None, "Must provide Plot func"

    # ______________________________________________________________________________
    def cname(self):
        return "c%s_%s" % (self.index, self.name)

    # ________________________________________________________________________________________________
    def plot(self, exts=None, gray=None):
        """Create and Save plot"""
        if not exts: exts = ["pdf"]
        (c, locs) = self.func(**self.kwargs)
        cname = self.cname()
        if not gray:
            for ext in exts: 
	      c.SaveAs("%s.%s" % (cname, ext))
	    c.SaveAs("%s.%s" % (cname, 'eps'))
	    c.SaveAs("%s.%s" % (cname, 'png'))
            f = TFile("%s.%s" % (cname, 'root'),'RECREATE')
            c.Write()
            f.Close()
        else:
            c.SetGrayscale()
            for ext in exts: 
              c.SaveAs("%s_gray.%s" % (cname, ext))
            c.SaveAs("%s_gray.%s" % (cname, 'eps'))
            c.SaveAs("%s_gray.%s" % (cname, 'png'))
            f = TFile("%s_gray.%s" % (cname, 'root'),'RECREATE')
	    c.Write()
            f.Close()

# ------------------------------------------------------------------------------
# RatioCanvas Class
# ------------------------------------------------------------------------------
class RatioCanvas(object):
    # ________________________________________________________________________________________________
    def __init__(self, name, x, y, r):
        c = TCanvas(name, name, x, y)
        pad1 = TPad("pad1", "top pad", 0.0, r-0.02, 1., 1.)
        pad1.SetBottomMargin(0.040)
        pad1.Draw()
        pad1.SetTopMargin(0.040)
        pad2 = TPad("pad2", "bottom pad", 0, 0, 1, r*(1-0.02))
        pad2.SetTopMargin(0.040)
        pad2.SetBottomMargin(0.40)
        pad2.Draw()
        pad1.cd()

        self.c = c
        self.pad1 = pad1
        self.pad2 = pad2
        self.ratio_split = r
        # self.label_size = _label_size

    # ________________________________________________________________________________________________
    def draw_frame1(self, x1, y1, x2, y2, title=''):
        self.pad1.cd()
        fr1 = self.pad1.DrawFrame(x1, y1, x2, y2, title)
        self.fr1 = fr1
        self.__reset_frame1_text__()

    # ________________________________________________________________________________________________
    def draw_frame2(self, x1, y1, x2, y2, title=''):
        self.pad2.cd()
        fr2 = self.pad2.DrawFrame(x1, y1, x2, y2, title)
        fr2.GetYaxis().SetNdivisions(505)
        fr2.GetXaxis().SetNdivisions(505)
        self.fr2 = fr2
        self.__reset_frame2_text__()

    # ________________________________________________________________________________________________
    def scale_text(self, scale):
        self.__scale_frame_text__(self.fr1, scale)
        self.__scale_frame_text__(self.fr2, scale)

    # ________________________________________________________________________________________________
    def __reset_frame_text__(self, fr):
        xaxis = fr.GetXaxis()
        yaxis = fr.GetYaxis()
        gs = gStyle
        yaxis.SetTitleSize(gs.GetTitleSize('Y'))
        yaxis.SetLabelSize(gs.GetLabelSize('Y'))
        yaxis.SetTitleOffset(gs.GetTitleOffset('Y'))
        yaxis.SetLabelOffset(gs.GetLabelOffset('Y'))
        xaxis.SetTitleSize(gs.GetTitleSize('X'))
        xaxis.SetLabelSize(gs.GetLabelSize('X'))
        xaxis.SetTickLength(gs.GetTickLength('X'))

    # ________________________________________________________________________________________________
    def __reset_frame1_text__(self):
        self.__reset_frame_text__(self.fr1)
        self.fr1.GetXaxis().SetTitleSize(0)
        self.fr1.GetXaxis().SetLabelSize(0)
        # scale = 1.
        scale = 1. / (1. - self.ratio_split)
        self.__scale_frame_text__(self.fr1, scale)

    # ________________________________________________________________________________________________
    def __reset_frame2_text__(self):
        self.__reset_frame_text__(self.fr2)
        # scale = (1.-self.ratio_split)/self.ratio_split
        scale = 1. / self.ratio_split
        self.__scale_frame_text__(self.fr2, scale)

    # ________________________________________________________________________________________________
    def __scale_frame_text__(self, fr, scale):
        xaxis = fr.GetXaxis()
        yaxis = fr.GetYaxis()
        yaxis.SetTitleSize(yaxis.GetTitleSize() * scale)
        yaxis.SetLabelSize(yaxis.GetLabelSize() * scale)
        yaxis.SetTitleOffset(1.0 * yaxis.GetTitleOffset() / scale)
        #yaxis.SetTitleOffset(1.1 * yaxis.GetTitleOffset() / scale)
        yaxis.SetLabelOffset(yaxis.GetLabelOffset() * scale)
        xaxis.SetTitleSize(xaxis.GetTitleSize() * scale)
        xaxis.SetLabelSize(xaxis.GetLabelSize() * scale)
        xaxis.SetTickLength(xaxis.GetTickLength() * scale)
        xaxis.SetTitleOffset(1.6 * xaxis.GetTitleOffset() / scale)
        xaxis.SetLabelOffset(2.5 * xaxis.GetLabelOffset() / scale)


# ------------------------------------------------------------------------------
# SingleCanvas Class
# ------------------------------------------------------------------------------
class SingleCanvas(object):
    # ________________________________________________________________________________________________
    def __init__(self, name, x, y):
        c = TCanvas(name, name, x, y)
        c.SetTopMargin(0.040)
        self.c = c
        # self.label_size = _label_size

    # ________________________________________________________________________________________________
    def draw_frame(self, x1, y1, x2, y2, title=''):
        self.c.cd()
        fr = self.c.DrawFrame(x1, y1, x2, y2, title)
        fr.GetYaxis().SetNdivisions(505)
        fr.GetXaxis().SetNdivisions(505)
        self.fr = fr
        self.__reset_frame_text__()

    # ________________________________________________________________________________________________
    def scale_text(self, scale):
        fr = self.fr
        xaxis = fr.GetXaxis()
        yaxis = fr.GetYaxis()
        yaxis.SetTitleSize(yaxis.GetTitleSize() * scale)
        yaxis.SetLabelSize(yaxis.GetLabelSize() * scale)
        yaxis.SetTitleOffset(1.1 * yaxis.GetTitleOffset() / scale)
        yaxis.SetLabelOffset(yaxis.GetLabelOffset() * scale)
        xaxis.SetTitleSize(xaxis.GetTitleSize() * scale)
        xaxis.SetLabelSize(xaxis.GetLabelSize() * scale)
        xaxis.SetTickLength(xaxis.GetTickLength() * scale)
        xaxis.SetTitleOffset(1.5 * xaxis.GetTitleOffset() / scale)
        xaxis.SetLabelOffset(2.5 * xaxis.GetLabelOffset() / scale)

    # ________________________________________________________________________________________________
    def __reset_frame_text__(self):
        fr = self.fr
        xaxis = fr.GetXaxis()
        yaxis = fr.GetYaxis()
        gs = gStyle
        yaxis.SetTitleSize(gs.GetTitleSize('Y'))
        yaxis.SetLabelSize(gs.GetLabelSize('Y'))
        yaxis.SetTitleOffset(1.15*gs.GetTitleOffset('Y'))
        yaxis.SetLabelOffset(gs.GetLabelOffset('Y'))
        xaxis.SetTitleSize(gs.GetTitleSize('X'))
        xaxis.SetLabelSize(gs.GetLabelSize('X'))
        xaxis.SetTickLength(gs.GetTickLength('X'))


# ------------------------------------------------------------------------------
# MetaLegend Class
# ------------------------------------------------------------------------------
class MetaLegend(object):
    """
    A better TLegend class that increases in height as you call AddEntry.
    """

    # ______________________________________________________________________________
    def __init__(self, width=0.15, height=0.05,
                 x1=None, y1=None,
                 x2=None, y2=None,
                 border=0, fill_color=0, fill_style=0):
        #        assert (x1 == y1 == None) or (x2 == y2 == None)
        #        assert (x1 != None and y1 != None) or (x2 != None and y2 != None)

        if x1 == x2 == None:
            x2 = 0.93
            x1 = x2 - width
        elif x1 == None:
            x1 = x2 - width
        elif x2 == None:
            x2 = x1 + width

        if y1 == y2 == None:
            y2 = 0.93
            y1 = y2 - width
        elif y1 == None:
            y1 = y2 - width
        elif y2 == None:
            y2 = y1 + width
        self.tlegend = TLegend(x1, y1, x2, y2)
        self.tlegend.SetBorderSize(border)
        self.tlegend.SetFillColor(fill_color)
        self.tlegend.SetFillStyle(fill_style)
        self.tlegend.SetTextFont(42)
        self.width = width
        self.height = height  # per entry
        self._nentries = 0
        self._has_drawn = False

    # ______________________________________________________________________________
    def AddEntry(self, obj, label='', option='P'):
        self._nentries += 1
        self.resize()
        self.tlegend.AddEntry(obj, label, option)

    # ______________________________________________________________________________
    def Draw(self):
        self.resize()
        self.tlegend.Draw()
        self._has_drawn = True

    # ______________________________________________________________________________
    def resize(self):
        if self._has_drawn:
            y2 = self.tlegend.GetY2NDC()
            self.tlegend.SetY1NDC(y2 - (self.height * self._nentries) - 0.01)
        else:
            y2 = self.tlegend.GetY2()
            self.tlegend.SetY1(y2 - (self.height * self._nentries) - 0.01)
        pass




# global functions
# ______________________________________________________________________________=buf=
def import_module(fname):
    """Dynamically import python module"""
    from os import path
    import imp
    mod_name, file_ext = path.splitext(path.split(fname)[-1])
    mod = imp.load_source(mod_name, fname)
    #if mod:
    #    print "Imported module {0}".format(fname)
    return mod


## Style, Text, Legends
#______________________________________________________________________________
def SetAtlasStyle():
    """Set the official ATLAS style"""
    gROOT.LoadMacro("atlasstyle/AtlasStyle.C")
    gROOT.LoadMacro("atlasstyle/AtlasUtils.C")
    gROOT.LoadMacro("atlasstyle/AtlasLabels.C")
    import ROOT
    #ROOT.SetAtlasStyle()
    # Do it ourselves to avoid crappy std::out
    global atlasStyle
    if atlasStyle is None:
        atlasStyle = ROOT.AtlasStyle()
    gROOT.SetStyle("ATLAS")
    gROOT.ForceStyle()

#______________________________________________________________________________
def set_palette(name=None, ncontours=200):
    """Set a color palette from a given RGB list

    stops, red, green and blue should all be lists of the same length
    see set_decent_colors for an example

    (may want this for migration matrices)

    :param name: name of palette style
    :param ncontours: number of contours to use in palette
    """

    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    elif name == "light":
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.20, 0.00, 0.80, 1.00]
        green = [1.00, 0.10, 0.00, 0.10, 0.00]
        blue  = [1.00, 1.00, 0.50, 0.30, 0.00]

    elif name == "limit":
        # default palette, looks cool
        #stops = [0.00, 0.03333, 1.00]
        stops = [0.00, 0.5076, 1.00]
        red   = [1.00, 1.00, 0.00]
        green = [1.00, 0.50, 0.00]
        blue  = [1.00, 0.50, 0.50]
    elif name == "5050":
        # default palette, looks cool
        #stops = [0.00, 0.03333, 1.00]
        stops = [0.00, 0.5   , 1.00]
        red   = [1.00, 1.00, 0.00]
        green = [1.00, 0.50, 0.00]
        blue  = [1.00, 0.50, 0.50]
    elif name == "green":
        # default palette, looks cool
        stops = [0.00, 1.00]
        red   = [1.00, 0.00]
        green = [1.00, 1.00]
        blue  = [1.00, 0.00]
    else:
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]

    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)

    TColor.CreateGradientColorTable(len(s), s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)


#________________________________________________________________________________________________
def draw_standard_text(lumi = None, lumix = None, atlasx = None, atlasy = None, ratio = True,
                       under = None, status = None, compact=None):
    """Draw standard text for plots including data

    :param lumi: integrated luminosity in fb (float)
    :param lumix: x-position for lumi label
    :param atlasx: x-position for ATLAS label
    :param atlasy: y-position for ATLAS label
    :param ratio: set true if this is a ratio canvas
    :param under: set true to place status label (Internal, Prelim etc) under ATLAS label
    :param index:
    :return:
    """
    if status is None: status = "Internal"
    if under is None: under = False

    x = 0.20
    if atlasx!=None: x = atlasx
    x2 = 0.20
    if lumix!=None: x2 = lumix
    y = 0.88
    if not ratio: y = 0.905
    if atlasy != None: y = atlasy
    h = 0.06
    text_size = 0.040 / gPad.GetHNDC()
    atlas_width = text_size * 3.3 * gPad.GetHNDC()/gPad.GetWNDC()
    lumi_width  = text_size * 5.3 * gPad.GetHNDC()/gPad.GetWNDC()
    if lumi:
        lumi_text = '#sqrt{s} = 13 TeV, '
        if isinstance(lumi, list):
            #lumi_text += '%.1f - %.1f fb^{-1}' % (lumi[0], lumi[1])
            lumi_text += '%d - %d fb^{-1}' % (lumi[0], lumi[1])
        else:
            #lumi_text += '%.1f fb^{-1}' % (lumi)
            lumi_text += '%d fb^{-1}' % (lumi)


    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(text_size)
    text = "#it{#bf{ATLAS}}"

    if status != "": text += " " + status
    if compact:
        if lumi: text += " " + lumi_text
        latex.DrawLatex(x,y,text)
    else:
        latex.DrawLatex(x,y,text)
        if lumi: latex.DrawLatex(x,y-h, lumi_text)


    """
    latex.SetTextFont(72)
    latex.DrawLatex(x,y, 'ATLAS' )
    latex.SetTextFont(42)
    if status == "":
        pass
    else:
        if under:
          latex.DrawLatex(x+0.02,y-0.05,status)
        else:
          latex.DrawLatex(x+atlas_width,y,status)
    #latex.SetTextAlign(31)
    latex.SetTextAlign(11)
    if lumi:
        if isinstance(lumi,list):
            lumi_text = '%.1f - %.1f fb^{-1}'%(lumi[0],lumi[1])
        else:
            lumi_text = '%.1f fb^{-1}'%(lumi)
        com_text = '#sqrt{s} = 13 TeV'
        #latex.DrawLatex(x2,y,"%s,  %s"% (lumi_text,com_text) )
        #latex.DrawLatex(x2,y,"%s, %s"% (com_text,lumi_text) )
        latex.DrawLatex(x2, y-0.06, "%s, %s" % (com_text, lumi_text))
    """


#______________________________________________________________________________
def draw_channel_text(x=None,y=None,channel=None,ratio=True):
    """Draw name of channel on plot

    :param x: x-pos (NDC)
    :param y: y-pos (NDC)
    :param channel: channel name
    """
    if y is None:
        y = 0.76 
        if not ratio: y = 0.785
    if x is None: x = 0.20

    latex = TLatex()
    latex.SetNDC()
    #latex.SetTextFont(43)
    latex.SetTextFont(42)
    text_size = 0.040 / gPad.GetHNDC()
    latex.SetTextSize(text_size)
    latex.DrawLatex(x,y,channel)


#______________________________________________________________________________
def get_ytitle_events(bin_width = None, xunit=None, scale=None, ytitle = None, wcorr =None):
    """Return y-axis title for histograms

    :param bin_width: width of xbins
    :param xunit: x-axis unit
    :param scale: multiplicative factor applied to axis
    :param ytitle: base title for y-axis (default: "Events")
    :return: y-axis title
    """
    if not ytitle: ytitle = "Events"
    if scale: ytitle += " #times 10^{%.0f}"%log10(scale)
    if wcorr:
        ytitle += " / "
        #if scale: ytitle += "10^{%.0f} "%log10(scale)
        ytitle += xunit or "unit"
    else:
        if bin_width:
            if bin_width != 1.0 or scale is not None:
                w = float(bin_width)
                if scale: w*=scale
                if w  >= 10.0: ytitle += " / %.0f"%(w)
                else:          ytitle += " / %.2g"%(w)
            if xunit:             ytitle += " %s" % (xunit)
    return ytitle


#______________________________________________________________________________
def get_ytitle_unity(bin_width=None, xunit=None):
    """Return y-axis title for normalized histogram"""
    return get_ytitle_events(bin_width, xunit=xunit, ytitle="Event fraction")


#______________________________________________________________________________
def make_legend(entries, width=0.15, height=0.05, x1=None, y1=None, x2=None, y2=None):
    """
    Creates a legend from a list of hists (or graphs).
    """
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    hists = [o[0] for o in entries]
    labels = [o[1] for o in entries]
    draw_options = [o[2] for o in entries]
    leg = MetaLegend(width=width, height=height, x1=x1, y1=y1, x2=x2, y2=y2)

    for h, lab, opt in zip(hists, labels, draw_options):
        #if not opt in ('P', 'F', 'L', 'PL', 'LP', 'FL', 'LF'):
        if not opt:
            ## assume opt is of the same format as draw_options used with Draw
            if opt.count('P'):
                if opt.count('E'):
                    opt = 'PL'
                else:
                    opt = 'P'
            else: # '', 'HIST', etc.
                opt = 'F'
        leg.AddEntry(h, label=lab, option=opt)
    return leg


#________________________________________________________________________________________________
def build_leg(data, bkgs = None, sigs = None, err = None, lumi=None, leg_xpos=None, leg_ypos=None, nolabels=False):
  """
  may want to keep this function for tag and probe plots

  ##TODO:
  * split samples into sig, bkg
  * make sigs / bkgs named


  """
  # make legend
  opts = []
  # data
  if not lumi:     opts += [[data, 'Data', 'P']]
  elif lumi < 0.5: opts += [[data, "Data (13 TeV, %.0f pb^{-1})" % (lumi * 1000.0), 'P']]
  else:            opts += [[data, "Data (13 TeV, %.1f fb^{-1})" % (lumi), 'P']]
  # bkg and sig
  for h in bkgs:   opts += [[h, h.tlatex, "F"]]
  for h in sigs:   opts += [[h, h.tlatex, "L"]]
  if err: opts += [[err, "Uncertainty", "F"]]

  #leg = make_legend( hists, names, styles, width = 0.30, x2=0.92, y2=0.92, height=0.07 ) ## good for SingleCanvas
  x2 = leg_xpos or 0.93
  y2 = leg_ypos or 0.83
  #leg = make_legend( hists, names, styles, width = 0.35, x2=x2, y2=0.93, height=0.05 )
  if nolabels:
      for opt in opts: opt[1]=""

  leg = make_legend(opts, width=0.20, x2=x2, y2=y2, height=0.04)

  return leg

def build_leg_bkg(data, bkgs = None, err = None, lumi=None, leg_xpos=None, leg_ypos=None, nolabels=False):
  """
  may want to keep this function for tag and probe plots

  ##TODO:
  * split samples into sig, bkg
  * make sigs / bkgs named


  """
  # make legend
  opts = []
  # data
  if not lumi:     opts += [[data, 'Data', 'P']]
  elif lumi < 0.5: opts += [[data, "Data (13 TeV, %.0f pb^{-1})" % (lumi * 1000.0), 'P']]
  else:            opts += [[data, "Data (13 TeV, %.1f fb^{-1})" % (lumi), 'P']]
  # bkg and sig
  for h in bkgs:   opts += [[h, h.tlatex, "F"]]
  if err: opts += [[err, "Uncertainty", "F"]]

  #leg = make_legend( hists, names, styles, width = 0.30, x2=0.92, y2=0.92, height=0.07 ) ## good for SingleCanvas
  x2 = leg_xpos or 0.93
  y2 = leg_ypos or 0.83
  #leg = make_legend( hists, names, styles, width = 0.35, x2=x2, y2=0.93, height=0.05 )
  if nolabels:
      for opt in opts: opt[1]=""

  leg = make_legend(opts, width=0.20, x2=x2, y2=y2, height=0.04)

  return leg

def build_leg_bkg2(data, bkgs = None, err = None, lumi=None, leg_xpos=None, leg_ypos=None, nolabels=False):
  """
  may want to keep this function for tag and probe plots

  ##TODO:
  * split samples into sig, bkg
  * make sigs / bkgs named


  """
  # make legend
  opts = []
  # data
  #if not lumi:     opts += [[data, 'Data', 'P']]
  #elif lumi < 0.5: opts += [[data, "Data (13 TeV, %.0f pb^{-1})" % (lumi * 1000.0), 'P']]
  #else:            opts += [[data, "Data (13 TeV, %.1f fb^{-1})" % (lumi), 'P']]
  # bkg and sig
  for h in bkgs:   opts += [[h, h.tlatex, "F"]]
  if err: opts += [[err, "Uncertainty", "F"]]

  #leg = make_legend( hists, names, styles, width = 0.30, x2=0.92, y2=0.92, height=0.07 ) ## good for SingleCanvas
  x2 = leg_xpos or 0.93
  y2 = leg_ypos or 0.83
  #leg = make_legend( hists, names, styles, width = 0.35, x2=x2, y2=0.93, height=0.05 )
  if nolabels:
      for opt in opts: opt[1]=""

  leg = make_legend(opts, width=0.20, x2=x2, y2=y2, height=0.04)

  return leg
def build_leg_sig(data, sigs = None, leg_xpos=None, leg_ypos=None, nolabels=False):
  """
  may want to keep this function for tag and probe plots

  ##TODO:
  * split samples into sig, bkg
  * make sigs / bkgs named


  """
  # make legend
  opts = []
  # bkg and sig
  for h in sigs:   
    if '1500' in h.tlatex:
      h.tlatex += ', tan #beta=25'   
    elif '1000' in h.tlatex:
      h.tlatex += ', tan #beta=12'   
    else:
      h.tlatex += ', tan #beta=6'   
    opts += [[h, h.tlatex, "L"]]

  #leg = make_legend( hists, names, styles, width = 0.30, x2=0.92, y2=0.92, height=0.07 ) ## good for SingleCanvas
  x2 = leg_xpos or 0.93
  y2 = leg_ypos or 0.79
  #leg = make_legend( hists, names, styles, width = 0.35, x2=x2, y2=0.93, height=0.05 )
  if nolabels:
      for opt in opts: opt[1]=""

  leg = make_legend(opts, width=0.30, x2=x2, y2=y2, height=0.04)

  return leg

#______________________________________________________________________________
def make_ratio_leg(gstat,gsys=None):
    ## should make a function for this
    h = 0.15
    w = 0.3
    x1 = 0.20
    y1 = 0.800
    leg_ratio = TLegend(x1,y1,x1+w,y1+h)
    leg_ratio.SetBorderSize(0)
    #leg_ratio.SetFillStyle(0)
    leg_ratio.SetFillColor(0)
    leg_ratio.SetNColumns(2)
    leg_ratio.SetColumnSeparation(-0.25)
    leg_ratio.SetTextFont(42)
    # leg_ratio.AddEntry(gstat,    "stat.","F")
    # if gsys: leg_ratio.AddEntry(gsys,"sys." ,"F")
    hstat_border = TH1F()
    hstat_border.SetLineColor(1)
    hstat_border.SetFillColor(gstat.GetFillColor())
    leg_ratio.AddEntry(hstat_border, "stat.", "F")
    if gsys:
        hsys_border = TH1F()
        hsys_border.SetLineColor(1)
        hsys_border.SetFillColor(gsys.GetFillColor())
        leg_ratio.AddEntry(hsys_border, "sys.", "F")
    leg_ratio.Draw()
    return leg_ratio, hstat_border, hsys_border


#______________________________________________________________________________
def draw_blind(val, xmin, xmax, ymin, ymax, logy=False):
    print "Drawing blind at ", val
    line = TLine(val, ymin, val, ymax)
    line.SetLineStyle(2)
    line.SetLineWidth(2)
    line.SetLineColor(kBlack)
    line.Draw()
    latex = TLatex()
    latex.SetTextFont(42)
    latex.SetTextAngle(90)
    latex.SetTextAlign(33)
    tyblind = ymax - 0.01*(ymax-ymin)
    if logy: tyblind = ymax - 0.3*(ymax-ymin)
    txblind = val+0.01*(xmax-xmin)
    latex.DrawLatex(txblind, tyblind, "blind")
    return line

#EOF
