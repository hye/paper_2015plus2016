=======
figures
=======

A package to help make plots for ATLAS publications.

The idea is that you use ``figures`` to create plotting templates within
your note workarea. The note-specific configuration should be checked
into your note Version Control System (eg. SVN or Git). The templates
are accompanied by scripts to handle checkout of third-party libraries
and syncing of your analysers inputs, to avoid overloading your VCS
with such data.

The package provides:

* Makefile build of figures for your note
* Syncing of analyser input data
* Common tools for histogram grabbing and manipulation
* Common tools for plot styling


Install
=======


Install figures package
-----------------------

Install the *figures* package into a generic location on your computer.
This should NOT be within any of the notes you are working on! This only
needs to be done one time as we'll use this for all your papers.

::

    git clone ssh://git@gitlab.cern.ch:7999/wdavey/figures.git

To setup the figures package, call::

    cd figures
    source setup.sh


Initialize note figures area
----------------------------

We will now initialize a figures area for a particular note you are working on.

With the figures package setup (as in the last step), go to your new note
working area and initialize a figures template::

    cd <path/to/my/note>
    mkdir -p figures
    cd figures
    initfigs


Modify the `Makefile configuration`_ to your needs.

Then add your new templates to your note's VCS, eg::

    cd ..
    svn add figures
    svn ci -m "Adding figures templates"

The figures area is now setup for you and your colleagues to use.


Makefile configuration
----------------------

You can customize the configuration of your figures build via the Makefile.
The following options are available.

=========  ===========================================================================
Variable   Description
=========  ===========================================================================
INPUTS     remote location of your analyser's inputs (to be synced)
PLOTCFG    path to your plot config file
DEPS       dependencies for your plots (typically include all code and input files)
FIGEXTS    graphic extensions (can contain .pdf .png .eps .root)
=========  ===========================================================================



Setup note area
---------------
Enter a fresh shell (so that your generic figures setup is no longer configured),
go to your note's figures template area and setup::

    cd <path/to/my/note>/figures
    source setup.sh

This will check out all dependent packages (including a copy of the figures package
that will be used for the note) and will set environment variables
so you can use the plotting tools. Do this each time you enter a new shell.


To sync your analyser's inputs from a remote location to your local note area, do::

    make sync


Make plots
----------
To build the plots defined by your setup, issue::

    make

If you didn't make any of your own modifications, this will use the default
configuration defined in ``myconfig/plots.py``.



Customizing your setup
======================

To create a new plot:

1. write a function to construct the canvas (look at the example function ``plot_stack`` in ``myconfig/simple.py``)
2. add an entry to the ``plots`` list in ``myconfig/plots.py``, referencing your function and providing key-word args

The setup for making plots is relatively 'plug-and-play'. You can make use of the tools
in the ``figures/plotter`` sub-package. Below we list the modules in this sub-package
with just a few examples of the contents.

``figures.plotter`` modules (at your disposal):

* `histutils.py <plotter/histutils.py>`_:

 * HistGrabber - easily grab histograms from input files
 * sum_hists - sum histograms
 * build_stack - build THStack
 * build_total_stat_plus_sys - build total background plus systematic uncertainties
 * apply_blind - apply a blind to a histogram
 * ...

* `plotutils.py <plotter/plotutils.py>`_:

 * SingleCanvas - Canvas with single pannel
 * RatioCanvas - Canvas with top and bottom pannel
 * draw_standard_text - draw standard ATLAS labels and text
 * make_legend - make a legend
 * ...

Please check the source code for a more thorough description of the available tools.


Examples from ATLAS notes
-------------------------

TODO


