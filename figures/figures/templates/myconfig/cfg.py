# encoding: utf-8
"""
cfg.py
~~~~~~

this is an auto-generated example config file

define any general config items here that you might want
to use in your plotting scripts.
"""

## anaysis info
lumi = 13.3  # fb
unblinded = True
approved = False

## plot layout
c_w = 700  # canvas width
c_h = 700  # canvas height (square)
# c_h           = 500 # canvas height (landscape)
c_ratio_split = 0.3
t_size = 1.
label_size = 0.04

## write out types (can import and modify)
from plotter import plotutils
plotutils.do_eps = False
plotutils.do_pdf = True
plotutils.do_png = True
plotutils.do_greyscale = True

## common tlatex expressions can go here
mTtot = '#it{m}_{T}^{tot}'
tauh = '#it{#tau}_{had}'
tauhvis = '#it{#tau}_{had-vis}'
taue = '#it{#tau_{e}}'
taumu = '#it{#tau_{#mu}}'
taulep = '#it{#tau}_{lep}'
tauhad = tauh
tauhadvis = tauhvis
met = "#it{E}_{T}^{miss}"

# process names
ztautau = '#it{Z}/#it{#gamma}*#rightarrow#it{#tau#tau}'


# add in some analysis specific samples 
from plotter import samples
import ROOT
samples.bkg1 = samples.Sample('Background 1',
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kYellow - 10,
    _fill_style=1000)

samples.bkg2 = samples.Sample('Background 2',
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kBlue - 9,
    _fill_style=1000)

samples.sig = samples.Sample('Signal',
    _line_color=ROOT.kMagenta,
    _fill_color=ROOT.kWhite,
    _fill_style=0)


