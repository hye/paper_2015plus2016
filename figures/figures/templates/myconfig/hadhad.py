# encoding: utf-8
'''
  file:    hadhad.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
import re

import ROOT

import cfg
from plotter.histutils import HistGrabber, log_bins, apply_blind, build_stack, \
    get_poissonized_graph, get_poissonized_ratio_graph, get_mc_ratio_band, get_significance_ratio, \
    build_total_stat_plus_sys
from plotter.plotutils import RatioCanvas, build_leg, draw_standard_text, draw_blind, \
    get_ytitle_events
from plotter.samples import style_hist

systematics = [# Tau
               'TAUS_TRUEHADTAU_EFF_JETID_TOTAL_',
               'TAUS_TRUEHADTAU_SME_TES_DETECTOR_',
               'TAUS_TRUEHADTAU_SME_TES_MODEL_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016_',
               'TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_',
               'TAUS_TRUEHADTAU_SME_TES_INSITU_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015_',
               'TAUS_TRUEHADTAU_EFF_RECO_TOTAL_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015_',
               'TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015_',
               # Fakes
               'HHFAKERATE_',
               'QCDFF_',
               # Flavour Tagging
               'FT_EFF_extrapolation_from_charm_',
               'FT_EFF_Eigen_C_2_',
               'FT_EFF_extrapolation_',
               'FT_EFF_Eigen_C_0_',
               'FT_EFF_Eigen_B_1_',
               'FT_EFF_Eigen_Light_0_',
               'FT_EFF_Eigen_Light_2_',
               'FT_EFF_Eigen_C_1_',
               'FT_EFF_Eigen_Light_1_',
               'FT_EFF_Eigen_B_0_',
               'FT_EFF_Eigen_Light_3_',
               # Jets
               'JET_JvtEfficiency_',
               'JET_JER_SINGLE_NP_',
               'JET_TILECORR_Uncertainty_',
               ]


# ________________________________________________________________________________________________
def plot_stack(cat=None, var=None):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## defaults
    if var is None: var = "MTtot"
    if cat is None: cat = "0tag0jet_0ptv"

    ## configuration (much will be overwritten by variable-specific config)
    x1 = x2 = y1 = y2 = None  # canvas boundaries
    logx = logy = False       # use logarithmic axis scales
    rebin = None              # fixed-width rebin factor
    bins = None               # new binning
    wcorr = None              # apply bin width correction
    checkbins = True          # check for bin misalignment on rebin
    xtitle = None             #
    xunit = None              #
    ndivx = None              # x-axis tick divisions
    blind = None              # position of blind
    ratio_opt = "ratio"          # options: ratio, sig
    sig_scale = 1. / 4.      # signal scale factor
    use_sys = True            # include systematics in uncertainty

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.05                # standard text height
    l_x2 = 0.93               # leg xpos
    leg_left = True           # place legend on LHS
    atlasx = None             # atlas x pos

    # samples
    filestr = "inputs/hadhad/limit_inputs_feb23.root"
    histstr = "{samp}_{cat}_HadHad_{var}"
    histstr_up = "{sys}_1up/{samp}_{cat}_HadHad_{var}_{sys}_1up"
    histstr_dn = "{sys}_1down/{samp}_{cat}_HadHad_{var}_{sys}_1down"
    bkgnames = ["Multijet", "Ztautau", "Wtaunu", "Top", "Others"]
    sigmasses = [600]


    ## plot-dependent configuration
    if var in [None, "MTtot"]:
        bins = log_bins(22, 150., 1000.)
        checkbins = False
        wcorr = True
        logx = True
        logy = True
        xtitle = "{0}({1}, {1}, {2}) [GeV]".format(cfg.mTtot, cfg.tauhadvis, cfg.met)
        blind = 255

    ## auto-configuration
    if logy: y1 = 0.01
    # ratio y-axis range
    if ratio_opt == "sig":
        ry1 = -7.0
        ry2 = +7.0
    elif ratio_opt == "ratio":
        ry1 = 0.41
        ry2 = 1.59
    if xunit == None:
        s = re.search(".*\[(.*)\]", xtitle)
        if s: xunit = s.group(1)



    ## Histogram getting and manipulation
    ##===========================================================================
    # read in nominal histograms
    print "Getting nominal histograms..."
    grabber = HistGrabber(filestr=filestr, histstr=histstr, cat=cat, var=var, bins=bins, rebin=rebin, checkbins=checkbins)
    bkgs   = [grabber.hist(samp=s, name=s.lower(), wcorr=wcorr) for s in bkgnames]
    sigs   = [grabber.histsum(name="H%d"%m, samps=["bbH%d"%m, "ggH%d"%m], wcorr=wcorr) for m in sigmasses]
    h_data = grabber.hist(samp="Data", name="data", wcorr=wcorr)
    # post-process
    if sig_scale is not None:
        for s in sigs: s.Scale(sig_scale)
    if not cfg.unblinded: apply_blind(h_data, blind)
    g_data = get_poissonized_graph(h_data)
    for h in [g_data] + bkgs + sigs: style_hist(h)
    h_stack = build_stack(bkgs)

    # get versions without bin-width correction (for ratio band calculations)
    if wcorr:
        print "Getting nominal histograms without bin-width correction..."
        #bkgs_uncorr = [grabber.hist(samp=s, name="", wcorr=False) for s in bkgnames]
        sigs_uncorr = [grabber.histsum(name="", samps=["bbH%d"%m, "ggH%d"%m], wcorr=False) for m in sigmasses]
        h_data_uncorr = grabber.hist(samp="Data", name="", wcorr=False)
        # post-process
        if sig_scale is not None:
            for s in sigs_uncorr: s.Scale(sig_scale)
        if not cfg.unblinded: apply_blind(h_data_uncorr, blind)
    else:
        #bkgs_uncorr = bkgs
        sigs_uncorr = sigs
        h_data_uncorr = h_data



    ## get bkg total + error
    print "Getting background sum and errors..."
    # stat only
    h_bkg_tot_stat = grabber.histsum(samps=bkgnames, name="error_stat", wcorr=wcorr)
    if wcorr:
        h_bkg_tot_stat_uncorr = grabber.histsum(samps=bkgnames, name="error_stat_uncorr", wcorr=False)
    else:
        h_bkg_tot_stat_uncorr = h_bkg_tot_stat
    # include systematics
    grabber_up = HistGrabber(filestr=filestr, histstr=histstr_up, cat=cat, var=var,
                             bins=bins, rebin=rebin, scale=4.0, checkbins=checkbins)
    grabber_dn = HistGrabber(filestr=filestr, histstr=histstr_dn, cat=cat, var=var,
                             bins=bins, rebin=rebin, scale=4.0, checkbins=checkbins)
    if use_sys:
        h_bkg_tot = build_total_stat_plus_sys( \
            name="error", grabber=grabber, grabber_up=grabber_up,
            grabber_dn=grabber_dn, systematics=systematics,
            samps=bkgnames, wcorr=wcorr)
        if wcorr:
            h_bkg_tot_uncorr = build_total_stat_plus_sys( \
                name="error_uncorr", grabber=grabber, grabber_up=grabber_up,
                grabber_dn=grabber_dn, systematics=systematics,
                samps=bkgnames, wcorr=False)
        else:
            h_bkg_tot_uncorr = h_bkg_tot
    # stat uncertainty only
    else:
        h_bkg_tot = h_bkg_tot_stat
        h_bkg_tot_uncorr = h_bkg_tot_stat_uncorr
        h_bkg_tot.SetName("error")
        h_bkg_tot_uncorr.SetName("error_uncorr")
    style_hist(h_bkg_tot)
    if use_sys: style_hist(h_bkg_tot_stat)

    ## determine axis ranges, titles
    if x1 == None: x1 = h_data.GetBinLowEdge(1)
    if x2 == None: x2 = h_data.GetBinLowEdge(h_data.GetNbinsX() + 1)
    if y1 == None:
        if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum(), h_data.GetMinimum()])
        else:      y1 = 0.0
    if y2 == None:
        if logy:   y2 = 10. * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
        else:      y2 = 1.2 * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
    if bins: bin_width = None
    else:    bin_width = h_data.GetBinWidth(2)
    ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=wcorr)

    ## additional inputs for significance band
    ratios = []
    if ratio_opt == "ratio":
        # data ratio
        h_ratio_data = get_poissonized_ratio_graph(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_data.drawopt = "P"
        style_hist(h_ratio_data, "data")
        # mc ratio
        h_ratio_mc = get_mc_ratio_band(h_bkg_tot_uncorr)
        h_ratio_mc.drawopt = "E2"
        style_hist(h_ratio_mc, "ratio")
        h_ratio_mc_stat = get_mc_ratio_band(h_bkg_tot_stat_uncorr)
        h_ratio_mc_stat.drawopt = "E2"
        style_hist(h_ratio_mc_stat, "ratio_stat")
        # line
        #line = ROOT.TLine(x1, 1., x2, 1.)
        #style_hist(line, "ratio_line")
        #line.drawopt = ""
        if use_sys: ratios = [h_ratio_mc, h_ratio_mc_stat, h_ratio_data]
        else:       ratios = [h_ratio_mc, h_ratio_data]

    elif ratio_opt == "sig":
        h_ratio_obs = get_significance_ratio(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_obs.drawopt = "HIST"
        style_hist(h_ratio_obs, "significance")
        h_ratio_sigs = []
        for (i, s) in enumerate(sigs_uncorr):
            h = get_significance_ratio(s, h_bkg_tot_uncorr, ismc=True)
            style_hist(h, sigs[i].GetName())
            h.drawopt = "HIST"
            h_ratio_sigs.append(h)
        ratios = [h_ratio_obs] + h_ratio_sigs
    else:
        print "No ratio configured!"

    ## build components
    leg = build_leg(h_data, bkgs, sigs, h_bkg_tot)
    if use_sys: leg.AddEntry(h_bkg_tot_stat, h_bkg_tot_stat.tlatex, 'F')



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = RatioCanvas('rc', cfg.c_w, cfg.c_h, cfg.c_ratio_split)
    #h_total.GetXaxis().SetRangeUser(x1, x2)
    rc.draw_frame1(x1, y1, x2, y2, ';;%s' % (ytitle))
    if ratio_opt == "ratio":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Obs. / exp.' % (xtitle))
    elif ratio_opt == "sig":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Significance' % (xtitle))
    rc.scale_text(cfg.t_size)
    if ndivx is not None:
        rc.fr1.GetXaxis().SetNdivisions(ndivx)
        rc.fr2.GetXaxis().SetNdivisions(ndivx)

    # draw pad1
    rc.pad1.cd()
    rc.pad1.SetLogy(logy)
    rc.pad1.SetLogx(logx)
    rc.fr2.GetXaxis().SetMoreLogLabels()
    rc.fr2.GetXaxis().SetNoExponent()
    h_stack.Draw("SAME,HIST")
    g_data.Draw("SAME,PZ")
    h_bkg_tot.Draw("SAME,E2")
    if use_sys: h_bkg_tot_stat.Draw("SAME,E2")
    for s in sigs:
        s.Draw("SAME,HIST")
    draw_standard_text(atlasx=atlasx, lumi=cfg.lumi)
    leg.Draw()
    if not cfg.unblinded:
        line = draw_blind(blind, x1, x2, y1, y2, logy=logy)
    ROOT.gPad.RedrawAxis()
    rc.pad1.Update()

    # draw pad 2
    rc.pad2.cd()
    rc.pad2.SetLogx(logx)
    for r in ratios:
        r.Draw("SAME,{}".format(r.drawopt))

    ROOT.gPad.RedrawAxis()
    rc.pad2.Update()


    return (rc.c, locals())




