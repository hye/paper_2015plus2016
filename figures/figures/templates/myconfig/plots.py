# encoding: utf-8
'''
  file:    plots.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012 

  description:
    This macro produces all the plots for the Z'tautau analysis. 
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs. 

    To run the macro, you need to setup a ROOT release compiled 
    against PyROOT (any recent release is probaly fine). 
    Then simply execute this macro:

      python plots.py

'''

## modules
from plotter.plotutils import Plot
from myconfig.simple import placeholder, plot_stack, generate_dummy_inputs 

generate_dummy_inputs()

## define all plots
plots = [
    Plot(1, name="placeholder", func=placeholder),
    Plot(2, name="stack", func=plot_stack),
]
