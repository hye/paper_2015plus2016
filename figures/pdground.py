#!/bin/env python

# This class implements the pdg rounding rules indicated in
# section 5.3 of doi:10.1088/0954-3899/33/1/001
#
# Note: because it uses round (and in general floats), it is affected
# by the limitations described in
# http://docs.python.org/library/functions.html#round
#  and
# http://dx.doi.org/10.1145/103162.103163
#
# davide.gerbaudo@gmail.com
# September 2012

def pdgRound(value, error) :
    "Given a value and an error, round and format them according to the PDG rules for significant digits"
    def threeDigits(value) :
        "extract the three most significant digits and return them as an int"
        return int(("%.2e"%float(error)).split('e')[0].replace('.','').replace('+','').replace('-',''))
    def nSignificantDigits(threeDigits) :
        assert threeDigits<1000,"three digits (%d) cannot be larger than 10^3"%threeDigits
        if threeDigits<101 : return 2 # not sure
        elif threeDigits<356 : return 2
        elif threeDigits<950 : return 1
        else : return 2
    def frexp10(value) :
        "convert to mantissa+exp representation (same as frex, but in base 10)"
        valueStr = ("%e"%float(value)).split('e')
        return float(valueStr[0]), int(valueStr[1])
    def nDigitsValue(expVal, expErr, nDigitsErr) :
        "compute the number of digits we want for the value, assuming we keep nDigitsErr for the error"
        return expVal-expErr+nDigitsErr
    def formatValue(value, exponent, nDigits, extraRound=0) :
        "Format the value; extraRound is meant for the special case of threeDigits>950"
        roundAt = nDigits-1-exponent - extraRound
        nDec = roundAt if exponent<nDigits else 0
        nDec = max([nDec, 0])
        return ('%.'+str(nDec)+'f')%round(value,roundAt)
    tD = threeDigits(error)
    nD = nSignificantDigits(tD)
    expVal, expErr = frexp10(value)[1], frexp10(error)[1]
    extraRound = 1 if tD>=950 else 0
    return (formatValue(value, expVal, nDigitsValue(expVal, expErr, nD), extraRound),
            formatValue(error,expErr, nD, extraRound))

def test(valueError=(0., 0.)) :
    val, err = valueError
    print val,' +/- ',err,' --> ',
    val, err = pdgRound(val, err)
    print ' ',val,' +/- ',err

if __name__=='__main__' :
    for x in [
#(0.827, 0.119121212)
#              ,(0.827, 0.3676565)
#              ,(0.827, 0.952)
#              ,(1.2345e7, 67890.1e2)
#              ,(1.2345e7, 54321.1e2)
#              ,(1.2345e7, 32100.1e2)
#              ,(0.00827, 0.0000123)
#              ,(0.00827, 0.0000456)
#              ,(0.00827, 0.0000952)
#              ,(225.651, 96.1938)
#              ,(555.999, 23.777)
# TNV: Lep-had numbers from Lei
#b-tag
(6719.05380019,125.749127493),#   Total Bkg
 (3251.53322793, 158.642774963),#   Fakes
 (91.4372364438, 17.1732298278),#   DYZ
 (694.320253912, 71.5261170265),#   ZplusJets
 (2681.04714063, 82.6959144982),#   Top
 (6.93795405568, 1.54400660506),#   Diboson
#
#b-veto                                                                                                                                                                                
(198454.671107, 1940.63819106),#   Total Bkg
 (1092.74230816, 125.835676448),#   Top
 (16226.5721373, 746.651571799),#   DYZ
 (916.377098661, 66.028989787),#    Diboson
 (96459.1922392, 1616.21580873),#   ZplusJets
 (83791.1779194, 793.199124696)#   Fakes
              ] : test(x)
