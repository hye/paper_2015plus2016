import pickle
from pdground import pdgRound
import ROOT

def get_mssm_xsecs():
    g_gg = ROOT.TGraph2D()
    g_bb = ROOT.TGraph2D()
    with open("mssm_xsecs.txt") as f:
        for l in f:
            (m, tb, gg, bb) = l.split(",")
            m = int(m)
            tb = int(tb)
            gg = float(gg)
            bb = float(bb)
            g_gg.SetPoint(g_gg.GetN(), m, tb, gg)
            g_bb.SetPoint(g_bb.GetN(), m, tb, bb)

    return (g_gg, g_bb)

def get_inputs(fname):
    d = dict()
    with open(fname) as f: 
        for l in f.readlines():
            (_, proc, nums) = l.split(":")
            (mass, prod) = proc.strip().split(" ")
            (n, en) = nums.strip().split("+-")
            
            mass = int(mass)
            n = float(n)
            en = float(en)

            if mass not in d: d[mass] = dict()
            d[mass][prod] = [n, en]

    return d


header = r"{:40s} & {:24s} & {:24s} \\".format("Process", r"\bveto", r"\btag")
line   = r"{:40s} & {:10s} +- {:10s} & {:10s} +- {:10s} \\"


fin_hh_btag = "inputs/yields/hadhad_btag.txt"
fin_hh_bveto = "inputs/yields/hadhad_bveto.txt"
fin_lh_btag = "inputs/yields/lephad_btag.txt"
fin_lh_bveto = "inputs/yields/lephad_bveto.txt"

d_lh_btag = get_inputs(fin_lh_btag)
d_lh_bveto = get_inputs(fin_lh_bveto)
d_hh_btag = get_inputs(fin_hh_btag)
d_hh_bveto = get_inputs(fin_hh_bveto)

lumi = 36100.


print ""
print "Lep-had" 

masses = [300, 400, 500, 800, 1000, 1200, 1500, 2000]

for mass in masses: 
    name = 'H{}'.format(mass)
    
    ## btag 
    d = d_lh_btag[mass]
    nbbH_btag = d['bbH'][0]
    nggH_btag = d['ggH'][0]

    ## bveto 
    d = d_lh_bveto[mass]
    nbbH_bveto = d['bbH'][0]
    nggH_bveto = d['ggH'][0]

    nbbH = nbbH_btag + nbbH_bveto
    nggH = nggH_btag + nggH_bveto
    
    print "ggH({}), n: {}, a*e: {}".format(mass, nggH, nggH/lumi)
    print "bbH({}), n: {}, a*e: {}".format(mass, nbbH, nbbH/lumi)

print ""
print "Had-had" 

for mass in masses: 
    name = 'H{}'.format(mass)
    
    ## btag 
    d = d_hh_btag[mass]
    nbbH_btag = d['bbH'][0]
    nggH_btag = d['ggH'][0]

    ## bveto 
    d = d_hh_bveto[mass]
    nbbH_bveto = d['bbH'][0]
    nggH_bveto = d['ggH'][0]

    nbbH = nbbH_btag + nbbH_bveto
    nggH = nggH_btag + nggH_bveto
    
    print "ggH({}), n: {}, a*e: {}".format(mass, nggH, nggH/lumi)
    print "bbH({}), n: {}, a*e: {}".format(mass, nbbH, nbbH/lumi)
















