# encoding: utf-8
'''
  file:    lephad.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
import re, os

import ROOT

import cfg
from plotter.histutils import HistGrabber, log_bins, apply_blind, build_stack, \
    get_poissonized_graph, get_poissonized_ratio_graph, get_mc_ratio_band, get_significance_ratio, get_significance_ratio_diego, \
    build_total_stat_plus_sys, build_error_hist, set_bin_edges, set_sig_bin_width_corr, make_band, kill_below, dump_hepdist, dump_hepvals_arr
from plotter.plotutils import RatioCanvas, SingleCanvas, build_leg, draw_standard_text, draw_blind, \
    get_ytitle_events, draw_channel_text, make_legend, build_leg_bkg, build_leg_sig, build_leg_bkg2
from plotter.samples import style_hist
from math import sqrt, pi
from ZprimeXsecProvider import ZprimeXsecProvider



sys_asym = [
    "ATLAS_EGRESO_",
    "ATLAS_EGSCALE_ALLCORR_",
    "ATLAS_EGSCALE_E4SCINTILLATOR_",
    "ATLAS_EGSCALE_LARCALIB_EXTRA2015PRE_",
    "ATLAS_EGSCALE_LARTEMPERATURE_EXTRA2015PRE_",
    "ATLAS_EGSCALE_LARTEMPERATURE_EXTRA2016PRE_",
    "ATLAS_ELEID_",
    "ATLAS_ELEISO_",
    "ATLAS_ELEReco_",
    "ATLAS_ELETRIGEFF_",
    "ATLAS_Fakes_Dphi_QCD_",
    "ATLAS_Fakes_Dphi_Wjets_",
    "ATLAS_Fakes_shape_QCD_",
    "ATLAS_Fakes_shape_WjetsBtag_",
    "ATLAS_Fakes_shape_Wjets_",
    "ATLAS_Fakes_shape_rQCDehad_",
    "ATLAS_Fakes_shape_rQCDmuhad_",
    "ATLAS_JETEtaNonClosure_",
    "ATLAS_JETNP1_",
    "ATLAS_JETNP2_",
    "ATLAS_JETNP3_",
    "ATLAS_JETTileCorrUnc_",
    "ATLAS_JvtEffSf_",
    "ATLAS_LPXALPHAS_",
    "ATLAS_LPXPDFEW_",
    "ATLAS_LPXPDF_",
    "ATLAS_LPXPI_",
    "ATLAS_LPXSCALEZ_",
    "ATLAS_METSoftTrkScale_",
    "ATLAS_MUONEFFSTAT_",
    "ATLAS_MUONEFFSYS_",
    "ATLAS_MUONID_",
    "ATLAS_MUONISOSTAT_",
    "ATLAS_MUONISOSYS_",
    "ATLAS_MUONLOWPTEFFSTAT_",
    "ATLAS_MUONLOWPTEFFSYS_",
    "ATLAS_MUONMS_",
    "ATLAS_MUONSAGITTARESBIAS_",
    "ATLAS_MUONSAGITTARHO_",
    "ATLAS_MUONSCALE_",
    "ATLAS_MUONTRIGEFFSTAT_",
    "ATLAS_MUONTRIGEFFSYS_",
    "ATLAS_MUONTTVASTAT_",
    "ATLAS_MUONTTVASYS_",
    "ATLAS_PRW_",
    "ATLAS_TAUIDHighPt_",
    "ATLAS_TAUIDTotal_",
    "ATLAS_TAURECOEleEleOLR_",
    "ATLAS_TAURECOHighPt_",
    "ATLAS_TAURECOTauEleOLR_",
    "ATLAS_TAURECOTotal_",
    "ATLAS_TESDETECTOR_",
    "ATLAS_TESINSITU_",
    "ATLAS_TESMODEL_",
    "ATLAS_TTBAR_RADIATION_",
    "ATLAS_TTBAR_SHOWERGEN_",
    "ATLAS_btagEffSfEigen_B_0_",
    "ATLAS_btagEffSfEigen_B_1_",
    "ATLAS_btagEffSfEigen_B_2_",
    "ATLAS_btagEffSfEigen_C_0_",
    "ATLAS_btagEffSfEigen_C_1_",
    "ATLAS_btagEffSfEigen_C_2_",
    "ATLAS_btagEffSfEigen_C_3_",
    "ATLAS_btagEffSfEigen_Light_0_",
    "ATLAS_btagEffSfEigen_Light_1_",
    "ATLAS_btagEffSfEigen_Light_2_",
    "ATLAS_btagEffSfEigen_Light_3_",
    "ATLAS_btagEffSfEigen_Light_4_",
    "ATLAS_btagEffSfextrapolation_",
    "ATLAS_btagEffSfextrapolation_charm_",
    ]

sys_sym = [
    "ATLAS_JERNP1__1up",
    "ATLAS_LPXCHOICEHERAPDF20__1up",
    "ATLAS_LPXCHOICENNPDF30__1up",
    "ATLAS_LPXPDFEV1__1up",
    "ATLAS_LPXPDFEV2__1up",
    "ATLAS_LPXPDFEV3__1up",
    "ATLAS_LPXPDFEV4__1up",
    "ATLAS_LPXPDFEV5__1up",
    "ATLAS_LPXPDFEV6__1up",
    "ATLAS_LPXPDFEV7__1up",
    "ATLAS_LPXREDCHOICENNPDF30__1up",
    "ATLAS_METSoftTrkResoPara__1up",
    "ATLAS_METSoftTrkResoPerp__1up",
    ]




# ________________________________________________________________________________________________
def plot_stack(cat=None, var=None, chan=None):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## defaults
    if var is None: var = "MTTOT"
    if cat is None: cat = "0tag0jet_0ptv"
    if chan is None: chan = "MuHad"

    ## configuration (much will be overwritten by variable-specific config)
    x1 = x2 = y1 = y2 = None  # canvas boundaries
    logx = logy = False       # use logarithmic axis scales
    rebin = None              # fixed-width rebin factor
    bins = None               # new binning
    wcorr = None              # apply bin width correction
    checkbins = True          # check for bin misalignment on rebin
    xtitle = None             #
    xunit = None              #
    ndivx = None              # x-axis tick divisions
    blind = None              # position of blind
    ratio_opt = "sig"         # options: ratio, sig
    scale = None              # overall scale for events
    sig_scale = 1. / 4.       # signal scale factor
    use_sys = True            # include systematics in uncertainty

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.05                # standard text height
    l_x2 = None               # leg xpos
    l_y2 = None               # leg xpos
    atlasx = None             # atlas x pos

    # samples
    filestr = "inputs/lephad/LimitHistograms.httlh.13TeV.CUT.Freiburg.MTTOT.V140.root"
    histstr = "{samp}_{cat}_{chan}_{var}"
    histstr_up = "{sys}_1up/{samp}_{cat}_{chan}_{var}_{sys}_1up"
    histstr_dn = "{sys}_1down/{samp}_{cat}_{chan}_{var}_{sys}_1down"
    histstr_sym = "{sys}/{samp}_{cat}_{chan}_{var}_{sys}"
    bkgdefs = [
        ("fakes",   "Fakes"),
        ("ztautau", "ZplusJets"),
        ("zll",     "DYZ"),
        ("top",     "Top"),
        ("diboson", "Diboson"),
    ]
    bkgnames = [s[1] for s in bkgdefs]
    sigdefs = ("H600", ["bbHlh600", "ggHlh600"])

    # cat config
    if cat == "0tag0jet_0ptv":
        catname = cfg.bveto
    else: 
        catname = cfg.btag

    ## plot-dependent configuration
    if var in [None, "MTTOT3"]:
        #bins = log_bins(22, 150., 1000.)
        #checkbins = False
        #wcorr = True
        #logx = True
        logy = True
        #xtitle = "{0}({1}, {2}, {3}) [GeV]".format(cfg.mTtot, cfg.taulep, cfg.tauhadvis, cfg.met)
        xtitle = "{0} [GeV]".format(cfg.mTtot)
        blind = 255
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
    elif var  == "MTTOT2":
        #bins = log_bins(22, 150., 1000.)
        #checkbins = False
        #wcorr = True
        #logx = True
        logy = True
        #xtitle = "{0}({1}, {2}, {3}) [GeV]".format(cfg.mTtot, cfg.taulep, cfg.tauhadvis, cfg.met)
        xtitle = "{0} [GeV]".format(cfg.mTtot)
        blind = 255
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
    elif var  == "MTTOT":
        bins = log_bins(22, 150., 1000.)
        checkbins = False
        wcorr = True
        logx = True
        logy = True
        #xtitle = "{0}({1}, {2}, {3}) [GeV]".format(cfg.mTtot, cfg.taulep, cfg.tauhadvis, cfg.met)
        xtitle = "{0} [GeV]".format(cfg.mTtot)
        blind = 255
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
    elif var  == "TauPt":
        logx = True
        logy = True
        wcorr=True
        xtitle = "Tau #it{p}_{T} [GeV]"
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
    elif var  == "LepPt":
        logx = True
        logy = True
        wcorr = True
        xtitle = "Lepton #it{p}_{T} [GeV]"
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
    elif var  == "mvis2":
        #bins = log_bins(22, 150., 1000.)
        #checkbins = False
        wcorr = True
        logx = True
        logy = True
        x1 = 50
        y1 = 1.
        xtitle = "{0}({1}, {2}) [GeV]".format(cfg.mvis, cfg.taulep, cfg.tauhadvis)
        #blind = 255
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
        pass
    elif var == "dPhiLepTau":
        xtitle = "{0}({1}, {2}) [rad]".format(cfg.dphi, cfg.taulep, cfg.tauhadvis)
        #wcorr=True
        l_x2 = 0.40
        l_y2 = 0.70
        scale = 1.e-3
    elif var == "dPhiMETLep":
        xtitle = "{0}({1}, {2}) [rad]".format(cfg.dphi, cfg.met, cfg.taulep)
        wcorr = True
        scale = 1.e-3
    elif var == "dPhiMETTau":
        xtitle = "{0}({1}, {2}) [rad]".format(cfg.dphi, cfg.met, cfg.tauhadvis)
        wcorr = True
        l_x2 = 0.40
        l_y2 = 0.70
        scale = 1.e-3
    else:
        #bins = log_bins(22, 150., 1000.)
        #checkbins = False
        #wcorr = True
        #logx = True
        logy = True
        xtitle = var
        #blind = 255
        if cat == "1tag0jet_0ptv":
            #logy = False
            pass
        pass



    ## auto-configuration
    #if logy and not y1: y1 = 0.01
    # ratio y-axis range
    if ratio_opt == "sig":
        ry1 = -7.0
        ry2 = +7.0
    elif ratio_opt == "ratio":
        ry1 = 0.71
        ry2 = 1.29
    if xunit == None:
        s = re.search(".*\[(.*)\]", xtitle)
        if s: xunit = s.group(1)


    ## Histogram getting and manipulation
    ##===========================================================================
    # read in nominal histograms
    print "Getting nominal histograms..."
    grabber = HistGrabber(filestr=filestr, histstr=histstr, cat=cat, var=var,
                          chan=chan, bins=bins, rebin=rebin, checkbins=checkbins)
    bkgs   = [grabber.hist(name=n, wcorr=wcorr, scale=scale, samp=s) for (n,s) in bkgdefs]
    sigs   = [grabber.hist(name=sigdefs[0], wcorr=wcorr, scale=scale, samp=sigdefs[1])]
    h_data = grabber.hist(name="data", wcorr=wcorr, scale=scale, samp="data")
    # post-process
    if sig_scale is not None:
        for s in sigs: s.Scale(sig_scale)
    if not cfg.unblinded: apply_blind(h_data, blind)
    g_data = get_poissonized_graph(h_data)
    for h in [g_data] + bkgs + sigs: style_hist(h)
    h_stack = build_stack(bkgs)

    # get versions without bin-width correction or scale (for ratio band calculations)
    if wcorr or scale:
        print "Getting nominal histograms without bin-width correction..."
        sigs_uncorr = [grabber.hist(name=sigdefs[0], wcorr=False, scale=None, samp=sigdefs[1])]
        h_data_uncorr = grabber.hist(name="data", wcorr=False, scale=None, samp="data")
        # post-process
        if sig_scale is not None:
            for s in sigs_uncorr: s.Scale(sig_scale)
        if not cfg.unblinded: apply_blind(h_data_uncorr, blind)
    else:
        #bkgs_uncorr = bkgs
        sigs_uncorr = sigs
        h_data_uncorr = h_data



    ## get bkg total + error
    print "Getting background sum and errors..."
    # stat only
    h_bkg_tot_stat = grabber.hist(name="error_stat", wcorr=wcorr, scale=scale, samp=bkgnames)
    if wcorr or scale:
        h_bkg_tot_stat_uncorr = grabber.hist(name="error_stat_uncorr", wcorr=False, scale=None, samp=bkgnames)
    else:
        h_bkg_tot_stat_uncorr = h_bkg_tot_stat
    # include systematics
    grabber_up = HistGrabber(filestr=filestr, histstr=histstr_up, cat=cat, var=var, chan=chan,
                             bins=bins, rebin=rebin, scale=scale, checkbins=checkbins, fallback=grabber)
    grabber_dn = HistGrabber(filestr=filestr, histstr=histstr_dn, cat=cat, var=var, chan=chan,
                             bins=bins, rebin=rebin, scale=scale, checkbins=checkbins, fallback=grabber)
    grabber_sym = HistGrabber(filestr=filestr, histstr=histstr_sym, cat=cat, var=var, chan=chan,
                             bins=bins, rebin=rebin, scale=scale, checkbins=checkbins, fallback=grabber)
    if use_sys:
        h_bkg_tot = build_total_stat_plus_sys( \
            name="error", grabber=grabber, grabber_up=grabber_up, grabber_sym=grabber_sym,
            grabber_dn=grabber_dn, sys_asym=sys_asym, sys_sym=sys_sym,
            wcorr=wcorr, scale=scale, samp=bkgnames)
        if wcorr or scale:
            h_bkg_tot_uncorr = build_total_stat_plus_sys( \
                name="error_uncorr", grabber=grabber, grabber_up=grabber_up, grabber_sym=grabber_sym,
                grabber_dn=grabber_dn, sys_asym=sys_asym, sys_sym=sys_sym,
                wcorr=False, scale=None, samp=bkgnames)
        else:
            h_bkg_tot_uncorr = h_bkg_tot
    # stat uncertainty only
    else:
        h_bkg_tot = h_bkg_tot_stat
        h_bkg_tot.SetName("error")
        h_bkg_tot_uncorr = h_bkg_tot_stat_uncorr
        h_bkg_tot_uncorr.SetName("error_uncorr")
    style_hist(h_bkg_tot, "error")
    if use_sys: style_hist(h_bkg_tot_stat, "error_stat")

    ## determine axis ranges, titles
    if x1 == None: x1 = h_data.GetBinLowEdge(1)
    if x2 == None: x2 = h_data.GetBinLowEdge(h_data.GetNbinsX() + 1)
    if y1 == None:
        #if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum(), h_data.GetMinimum()])
        if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum()])
        else:      y1 = 0.0
    if y2 == None:
        if logy:   y2 = 100. * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
        else:      y2 = 1.4 * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
    if bins: bin_width = None
    else:    bin_width = h_data.GetBinWidth(2)
    ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=wcorr, scale=scale)

    ## additional inputs for significance band
    ratios = []
    if ratio_opt == "ratio":
        # data ratio
        h_ratio_data = get_poissonized_ratio_graph(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_data.drawopt = "P"
        style_hist(h_ratio_data, "data")
        # mc ratio
        h_ratio_mc = get_mc_ratio_band(h_bkg_tot_uncorr)
        h_ratio_mc.drawopt = "E2"
        style_hist(h_ratio_mc, "ratio")
        h_ratio_mc_stat = get_mc_ratio_band(h_bkg_tot_stat_uncorr)
        h_ratio_mc_stat.drawopt = "E2"
        style_hist(h_ratio_mc_stat, "ratio_stat")
        # line
        #line = ROOT.TLine(x1, 1., x2, 1.)
        #style_hist(line, "ratio_line")
        #line.drawopt = ""
        if use_sys: ratios = [h_ratio_mc, h_ratio_mc_stat, h_ratio_data]
        else:       ratios = [h_ratio_mc, h_ratio_data]

    elif ratio_opt == "sig":
        h_ratio_obs = get_significance_ratio(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_obs.drawopt = "HIST"
        style_hist(h_ratio_obs, "significance")
        h_ratio_sigs = []
        for (i, s) in enumerate(sigs_uncorr):
            h = get_significance_ratio(s, h_bkg_tot_uncorr, ismc=True)
            style_hist(h, sigs[i].GetName())
            h.drawopt = "HIST"
            h_ratio_sigs.append(h)
        ratios = [h_ratio_obs] + h_ratio_sigs
    else:
        print "No ratio configured!"

    ## build components
    leg = build_leg(h_data, bkgs, sigs, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2)
    #if use_sys: leg.AddEntry(h_bkg_tot_stat, h_bkg_tot_stat.tlatex, 'F')



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = RatioCanvas('rc', cfg.c_w, cfg.c_h, cfg.c_ratio_split)
    #h_total.GetXaxis().SetRangeUser(x1, x2)
    rc.draw_frame1(x1, y1, x2, y2, ';;%s' % (ytitle))
    if ratio_opt == "ratio":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Obs. / exp.' % (xtitle))
    elif ratio_opt == "sig":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Significance' % (xtitle))
    rc.scale_text(cfg.t_size)
    if ndivx is not None:
        rc.fr1.GetXaxis().SetNdivisions(ndivx)
        rc.fr2.GetXaxis().SetNdivisions(ndivx)

    # draw pad1
    rc.pad1.cd()
    rc.pad1.SetLogy(logy)
    rc.pad1.SetLogx(logx)
    rc.fr2.GetXaxis().SetMoreLogLabels()
    rc.fr2.GetXaxis().SetNoExponent()
    h_stack.Draw("SAME,HIST")
    g_data.Draw("SAME,PZ")
    h_bkg_tot.Draw("SAME,E2")
    #if use_sys: h_bkg_tot_stat.Draw("SAME,E2")
    for s in sigs:
        s.Draw("SAME,HIST")
    draw_standard_text(atlasx=atlasx, lumi=cfg.lumi, status=cfg.status, compact=True)
    draw_channel_text(channel="{} {}".format(cfg.lephad, catname))
    leg.Draw()
    if not cfg.unblinded:
        line = draw_blind(blind, x1, x2, y1, y2, logy=logy)
    ROOT.gPad.RedrawAxis()
    rc.pad1.Update()

    # draw pad 2
    rc.pad2.cd()
    rc.pad2.SetLogx(logx)
    for r in ratios:
        r.Draw("SAME,{}".format(r.drawopt))

    #ar1 = ROOT.TArrow(626, 0.9, 626, 1.1, 0.04, "|>");
    #ar1.SetLineColor(ROOT.kMagenta+2)
    #ar1.SetFillColor(ROOT.kMagenta+2)
    #ar1.SetAngle(40)
    #ar1.Draw();

    ROOT.gPad.RedrawAxis()
    rc.pad2.Update()


    return (rc.c, locals())


def get_mssm_xsecs():
    g_gg = ROOT.TGraph2D()
    g_bb = ROOT.TGraph2D()
    #with open("mssm_xsecs.txt") as f:
    with open("mh125_xsecs.txt") as f:
        for l in f:
            (m, tb, gg, bb) = l.split(",")
            m = int(m)
            tb = int(tb)
            gg = float(gg)
            bb = float(bb)
            g_gg.SetPoint(g_gg.GetN(), m, tb, gg)
            g_bb.SetPoint(g_bb.GetN(), m, tb, bb)

    return (g_gg, g_bb)


# ________________________________________________________________________________________________
def plot_stack_postfit(cat=None, var=None, chan=None, ratio_opt="sig"):
    """
      make stack plots for had-had (postfit)
    """
    ## Configuration
    ##===========================================================================

    ## defaults
    if var is None: var = "SumOfPt"
    if cat is None: cat = "T1"
    if chan is None: chan = "LepHad"

    ## configuration (much will be overwritten by variable-specific config)
    x1 = x2 = y1 = y2 = None  # canvas boundaries
    logx = logy = False       # use logarithmic axis scales
    #rebin = None              # fixed-width rebin factor
    #bins = None               # new binning
    #wcorr = None              # apply bin width correction
    #checkbins = True          # check for bin misalignment on rebin
    actualbins = None         # actual bin edges (since postfit plots have fake binning)
    xtitle = None             #
    xunit = None              #
    ndivx = None              # x-axis tick divisions
    blind = None              # position of blind
    #ratio_opt = "sig"         # options: ratio, sig
    #ratio_opt = "ratio"  # options: ratio, sig
    #scale = None              # overall scale for events
    sig_scale = None          # signal scale factor
    use_sys = True            # include systematics in uncertainty
    sig_ontop = False
    ticks = [] # extra x-axis label points
    scale = None

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.05                # standard text height
    l_x2 = None               # leg xpos
    l_y2 = None               # leg xpos
    atlasx = None             # atlas x pos

    # samples
    filestr = "inputs/postfit/topdecor_100/total_0ptv_{cat}_{chan}_{var}.root"
    if cat == "T5":
        #filestr = "inputs/lephad/postfitInput/v4_rebin/total_Region_BMin0_J0_{cat}_isMVA0_L1_Y2015_dist{var}_D{chan}.root"
        filestr = "inputs/postfit/2017-08-14/total_Region_BMin0_J0_{cat}_isMVA0_L1_Y2015_dist{var}_D{chan}.root"

    histstr = "{samp}"
    if cat == "T0":
      bkgdefs = [
                 ("taufakes",   "WJETSFakes"),
                 ("ztautau", "ZplusJets"),
                 ("lepfakes",   "QCDFakes"),
                 ("zll",     "DYZ"),
                 ("top",     "Top"),
                 ("others", "Diboson"),
                ]
    elif cat == "T1" or cat == "T2":
      bkgdefs = [
                 ("top",     "Top"),
                 ("taufakes",   "JETSFakes"),
                 ("ztautau", "ZplusJets"),
                 ("wjets", "WplusJets"),
                 ("lepfakes",   "QCDFakes"),
                 ("zll",     "DYZ"),
                 ("others", "Diboson"),
                ]
    bkgnames = [s[1] for s in bkgdefs]
    #sigmasses = [400, 1000, 1500]
    #sigmasses = [500]
    #sigdefs = ("H500", ["bbH500", "ggH500"])
    #tanb = 15.0
    #tanb = 6.0

    # cat config
    if cat == "T0":   catname = cfg.bveto
    elif cat == "T1": catname = cfg.btag
    elif cat == "T2":
        catname = cfg.tcr
        #top = bkgdefs.pop(3)
        #bkgdefs = [top] + bkgdefs
        #bkgnames = [s[1] for s in bkgdefs]
    elif cat == "T5":
        catname = cfg.bincl
        #sigmasses = [1000, 2000, 3000]
        sigmasses = [1500, 2000, 2500]
    else:
        print "Invalid category", cat
        exit(1)

    # chan config
    if chan == "ElHad": channame = cfg.ehad
    elif chan == "MuHad": channame = cfg.muhad
    elif chan == "LepHad": channame = cfg.lephad
    else:
        print "Invalid channel ", chan
        exit(1)

    # standard log-binning

    ## plot-dependent configuration
    if var in [None, "SumOfPt"]:
        logy = False
        logx = True
        #xtitle = "{0}({1}, {2}, {3}) [GeV]".format(cfg.mTtot, cfg.taulep, cfg.tauhadvis, cfg.met)
        xtitle = "{0} [GeV]".format(cfg.mTtot)

        ## NOTE: changed last bin from 5000 to something just above second last (ie its an "overflow" bin)
        ##       otherwise the plot looks shite
        #if   chan == "ElHad" and cat == "T0": # ehad, bveto
        #    actualbins = [50, 70, 80, 90, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300,
        #                  320, 340, 360, 400, 500, 630]#5000]
        #elif chan == "ElHad" and cat == "T1": # ehad, btag
        #    actualbins = [50, 100, 150, 200, 250, 300, 350, 400, 500, 630]#5000]
        #elif chan == "MuHad" and cat == "T0": # muhad, bveto
        #    actualbins = [50,70,80,90,100,120,140,160,180,200,220,240,260,280,300,500,850]#5000]
        #elif chan == "MuHad" and cat == "T1": # muhad, btag
        #    actualbins = [50, 100, 150, 200, 250, 300, 400, 550]#5000]


        if cat == "T0":
            #actualbins = [50, 60, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,1500]
            #actualbins = [50, 60, 62, 65, 67, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,1500]
            actualbins = [50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,1500]
            x2 = 1499.99
            ticks = [50,1500]
            #y2 = 4.e6
            #y2 = 4.e8 #withoutBinWidthCorr
            #y1 = 7.e-4
            y1 = 0.
            y2 = 10.
            x1 = 400.
        elif cat == "T5":
            actualbins = [60, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500, 600, 700, 800, 1000]
            x2 = 999.99
            ticks = [500, 800]
            y2 = 4.e5
        elif cat == "T1":
            if chan == "ElHad":
              actualbins = [50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 1500]
	      ticks = [50,1500]
	      y2 = 7.e5
	      #y2 = 7.e7 #withoutBinWidthCorr
	      y1 = 7.e-4
              x2 = 1499.99
            elif chan == "MuHad":
              actualbins = [50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 1500]
	      ticks = [50,1500]
	      y2 = 7.e5
	      #y2 = 7.e7 #withoutBinWidthCorr
	      y1 = 7.e-4
              x2 = 1499.99
            elif chan == "LepHad":
              #actualbins = [50, 80, 100, 150, 200, 250, 300, 350, 400, 500, 630,700,1500]
              actualbins = [50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 1500]
	      ticks = [50,1500]
	      #y2 = 7.e4
	      #y2 = 7.e6 #withoutBinWidthCorr
	      #y1 = 7.e-4
	      y1 = 0.
              y2 = 2.
              x1 = 400.
              x2 = 1499.99
            elif "LepHadVR" in chan:
              #actualbins = [50, 80, 100, 150, 200, 250, 300, 350, 400, 500, 630,700,1500]
              actualbins = [50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 1500]
	      ticks = [50,1500]
	      #y2 = 7.e4
	      #y2 = 7.e6 #withoutBinWidthCorr
	      #y1 = 7.e-4
	      y1 = 0.
              y2 = 2.
              x1 = 400.
              x2 = 1499.99
        elif cat == "T2":
            actualbins = [100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 1500]
            ticks = [100,1500]
            y2 = 4.e4
            y1 = 7.e-3
            if chan == "ElHad" or chan == "MuHad":
              y2 = 4.e5
              y1 = 7.e-2

    elif var == "TauPt":
        xtitle = "%s #it{p}_{T} [GeV]" % (cfg.tauhadvis)
        logx=True
        logy=True
        #y1 = 1.e-3
        if cat == "T0":
            y1 = 7.e-1
            y2 = 2.e6
            x1 = 25
            x2 = 299.
            #actualbins = [25,30,35,40,46,54,64,76,90,106,124,144,170,200,300]
            #actualbins = [25,30,40,46,54,64,76,90,106,124,144,170,200,300]
            actualbins = [25,30,36,42,54,66,78,90,102,114,126,138,150,200,300]
            ticks = [25,300]
        elif cat == "T1" or cat == "T2":
            y1 = 7.e-2
            y2 = 9.e4
            x1 = 25
            x2 = 299.
            #actualbins = [20,25,30,40,50,60,71,83,96,112,130,150,200,300]
            actualbins = [20,25,30,36,42,54,66,78,90,102,114,126,138,150,200,300]
            ticks = [25, 300]

    elif var == "LeptonPt":
        xtitle = "Lepton #it{p}_{T} [GeV]"
        logx=True
        logy=True
        if cat == "T0":
            y1 = 7.e-1
	    y2 = 2.e8
            x1 = 30
            x2 = 299.
            actualbins = [30,35,40,48,58,70,84,100,118,138,160,220,300]
            ticks = [30, 300]
        elif cat == "T1" or cat == "T2":
            y1 = 7.e-2
         
            x1 = 30
            x2 = 299.
            actualbins = [30,40,50,60,70,82,96,112,130,150,200,300]
            ticks = [30, 300]
	    if cat == "T2":
                y2 = 9.e6
            else:
                y2 = 9.e5

    elif var == "MET":
        xtitle = "{} [GeV]".format(cfg.met)
        logx=True
        logy=True
        y1 = 2.e-3
        x1 = 20
        #x1 = 50
        x2 = 999
        actualbins = [20, 25, 30, 36, 44, 52, 63, 76, 91, 109, 132, 158, 190, 229, 275, 331, 398, 478, 575, 692, 832, 1000]
        ticks = [60, 400, 700]
        #if cat == "T0":
        #    actualbins = [50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,5000]
        #else:
        #    actualbins = [50, 100, 150, 200, 250, 300, 350, 400, 500, 5000]
    elif var == "TauMETDphi":
        xtitle = "#Delta#phi({}, {}) [rad]".format(cfg.met, cfg.tauhadvis)
        logx=False
        logy=True
        if cat == "T0":
            y1 = 2.e2
            y2 = 2.e7
        elif cat == "T1" or cat == "T2":
            y1 = 8.e-1
            y2 = 4.e6
        x1 = 0.001
        x2 = 3.25
        actualbins = [0, 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5, 2.75, 3., 3.25]
        ticks = [0]
    elif var == "LepMETDphi":
        xtitle = "#Delta#phi({}, {}) [rad]".format(cfg.met, cfg.lep)
        logx=False
        logy=True
	if cat == "T0":
          y1 = 8.e0
          y2 = 2.e11
        elif cat == "T1" or cat == "T2":
          y1 = 8.e-1
          y2 = 2.e10
        x1 = 0.001
        x2 = 3.25
        actualbins = [0, 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5, 2.75, 3., 3.25]
        ticks = [0]
    else:
        logy = True
        xtitle = var

    ## auto-configuration
    #if logy and not y1: y1 = 0.01
    # ratio y-axis range
    if ratio_opt == "sig":
        #ry1 = -7.0
        #ry2 = +7.0
        ry1 = -3.5
        ry2 = +3.5
    elif ratio_opt == "ratio":
        if cat == 'T1' and var == 'TauMETDphi':
            ry1 = 0.45
            ry2 = 1.55
	elif cat == 'T0':
            ry1 = 0.81
            ry2 = 1.19
            if 'prefit' in filestr:
                ry1 = 0.49
                ry2 = 1.51
        else:
            ry1 = 0.81
            ry2 = 1.19
            if 'prefit' in filestr:
                ry1 = 0.49
                ry2 = 1.51
    if xunit == None:
        s = re.search(".*\[(.*)\]", xtitle)
        if s: xunit = s.group(1)


    ## Histogram getting and manipulation
    ##===========================================================================
    # read in nominal histograms
    print "Getting nominal histograms..."
    grabber = HistGrabber(filestr=filestr, histstr=histstr, cat=cat, var=var, chan=chan)
    bkgs    = [grabber.hist(name=n, samp=s) for (n,s) in bkgdefs]
    h_data  = grabber.hist(name="data", samp="Data")
    ## get bkg total + error
    print "Getting background sum and errors..."
    h_bkg_tot_nom = grabber.hist(name="tot", samp="Total_prediction")
    h_bkg_tot_up = grabber.hist(name="tot_up", samp="Total_prediction_up")
    h_bkg_tot_dn = grabber.hist(name="tot_dn", samp="Total_prediction_do")
    h_bkg_tot = build_error_hist(h_bkg_tot_nom, h_bkg_tot_up, h_bkg_tot_dn, "error")

    print "data bin 1: ", h_data.GetBinContent(1)


    ## get signals
    '''
    (g_xsec_gg, g_xsec_bb) = get_mssm_xsecs()
    sigs = []
    sigs_forsig = []
    #if mass == 1000:
    #  tanb = 10
    for mass in sigmasses:
        if mass == 1500:
          tanb = 25
        elif mass == 1000:
          tanb = 12
        if cat == "T5":
            sname = "SSMZprime{}".format(mass)
            s =  grabber.hist(name=sname, samp=sname)
            sigs.append(s)
        else:
            tags = ''
            if cat == 'T0':
              tags = '0tag'
            elif cat == 'T1' or cat == 'T2':
              tags = '1tag'
            sname_gg = "ggHlh{:s}{}".format(tags,mass)
            ggH =  grabber.hist(name=sname_gg, samp=sname_gg)
            ggH_forsig =  grabber.hist(name=sname_gg, samp=sname_gg)
            xsec_gg = g_xsec_gg.Interpolate(mass, tanb)
            if mass < 1000:
              ggH.Scale(xsec_gg)
            else:
              ggH.Scale(xsec_gg*100)
            #ggH.Scale(xsec_gg)
            ggH_forsig.Scale(xsec_gg)

            sname_bb = "bbHlh{:s}{}".format(tags,mass)
            bbH =  grabber.hist(name=sname_bb, samp=sname_bb)
            bbH_forsig =  grabber.hist(name=sname_bb, samp=sname_bb)
            xsec_bb = g_xsec_bb.Interpolate(mass, tanb)
            if mass < 1000:
              bbH.Scale(xsec_bb)
            else:
              bbH.Scale(xsec_bb*100)
            #bbH.Scale(xsec_bb)
            bbH_forsig.Scale(xsec_bb)

            s = ggH.Clone("H{}".format(mass))
            #s = bbH.Clone("H{}".format(mass))
            s.Add(bbH)
            sigs.append(s)

            ss = ggH_forsig.Clone("H{}".format(mass))
            #ss = bbH_forsig.Clone("H{}".format(mass))
            ss.Add(bbH_forsig)
            sigs_forsig.append(ss)

            print "xsec (gg): {}".format(xsec_gg)
            print "xsec (bb): {}".format(xsec_bb)
    for s in sigs: kill_below(s, 0.0)
    for s in sigs_forsig: kill_below(s, 0.0)
    '''


    # reimplement actual bin edges
    if actualbins:
        wcorr = True
        #wcorr = False
        bkgs = [set_bin_edges(h, actualbins, wcorr=wcorr) for h in bkgs]
        #sigs_uncorr      = [set_bin_edges(h, actualbins, wcorr=False, name=h.GetName()+"_uncorr") for h in sigs]
        #sigs             = [set_bin_edges(h, actualbins, wcorr=wcorr) for h in sigs]
        #sigs_forsig_uncorr      = [set_bin_edges(h, actualbins, wcorr=False, name=h.GetName()+"_uncorr") for h in sigs_forsig]
        #sigs_forsig             = [set_bin_edges(h, actualbins, wcorr=wcorr) for h in sigs_forsig]
        h_data_uncorr    = set_bin_edges(h_data, actualbins, wcorr=False, name="data_uncorr")
        h_data           = set_bin_edges(h_data, actualbins, wcorr=wcorr)
        h_bkg_tot_uncorr = set_bin_edges(h_bkg_tot, actualbins, wcorr=False, name="error_uncorr")
        h_bkg_tot        = set_bin_edges(h_bkg_tot, actualbins, wcorr=wcorr)
    else:
        #sigs_uncorr = sigs
        h_data_uncorr = h_data
        h_bkg_tot_uncorr = h_bkg_tot

    #if sig_ontop:
        #for s in sigs: s.Add(h_bkg_tot)
        #for s in sigs_forsig: s.Add(h_bkg_tot)

    # post-process
    if scale is not None:
        for h in [h_data, h_bkg_tot] + bkgs + sigs: h.Scale(scale)
    #if sig_scale is not None:
    #    for s in sigs: s.Scale(sig_scale)
    if not cfg.unblinded: apply_blind(h_data, blind)
    g_data = get_poissonized_graph(h_data)
    for h in [g_data] + bkgs + sigs + [h_bkg_tot]: style_hist(h)
    h_stack = build_stack(bkgs)

    ## determine axis ranges, titles
    if x1 == None: x1 = h_data.GetBinLowEdge(1)
    if x2 == None: x2 = h_data.GetBinLowEdge(h_data.GetNbinsX() + 1)
    if y1 == None:
        #if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum(), h_data.GetMinimum()])
        if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum()])
        else:      y1 = 0.0
    if y2 == None:
        if logy:   y2 = 1000. * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
        else:      y2 = 1.4 * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
    if actualbins: bin_width = None
    else:    bin_width = h_data.GetBinWidth(2)
    ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=bool(actualbins), scale=scale)
    #ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=False, scale=scale)

    ## additional inputs for significance band
    ratios = []
    if ratio_opt == "ratio":
        # data ratio
        h_ratio_data = get_poissonized_ratio_graph(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_data.drawopt = "P0"
        style_hist(h_ratio_data, "data")
        # mc ratio
        h_ratio_mc = get_mc_ratio_band(h_bkg_tot_uncorr)
        h_ratio_mc.drawopt = "E2"
        style_hist(h_ratio_mc, "error")
        #h_ratio_mc_stat = get_mc_ratio_band(h_bkg_tot_stat_uncorr)
        #h_ratio_mc_stat.drawopt = "E2"
        #style_hist(h_ratio_mc_stat, "ratio_stat")
        # line
        #line = ROOT.TLine(x1, 1., x2, 1.)
        #style_hist(line, "ratio_line")
        #line.drawopt = ""
        #if use_sys: ratios = [h_ratio_mc, h_ratio_mc_stat, h_ratio_data]
        #else:       ratios = [h_ratio_mc, h_ratio_data]
        ratios = [h_ratio_mc, h_ratio_data]

    elif ratio_opt == "sig":
        #h_ratio_obs = get_significance_ratio(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_obs = get_significance_ratio_diego(h_data_uncorr, h_bkg_tot_uncorr)
        print "Observed significances: ", [h_ratio_obs.GetBinContent(i) for i in xrange(1, h_ratio_obs.GetNbinsX() + 1)]
        #h_ratio_obs.drawopt = "HIST"
        #style_hist(h_ratio_obs, "significance")
        h_ratio_obs.drawopt = "PZ"
        style_hist(h_ratio_obs, "data")
        line = ROOT.TLine(x1, 0., x2, 0.)
        line.drawopt = ""
        h_ratio_sigs = []
        for (i, s) in enumerate(sigs_forsig_uncorr):
            #h = get_significance_ratio(s, h_bkg_tot_uncorr, ismc=True)
            h = get_significance_ratio_diego(s, h_bkg_tot_uncorr, ismc=True)
            #kill_below(h, x1)
            style_hist(h, sigs_uncorr[i].GetName().replace("_uncorr",""))
            #h.drawopt = "HIST]["
            h.drawopt = "HIST]["
            h.line = h.Clone("{}_line".format(h.GetName()))
            h.line.SetLineWidth(3)
            h.line.SetLineColor(h.GetLineColor() - 12)
            h.line.SetLineStyle(2)
            h_ratio_sigs.append(h)
        ratios = h_ratio_sigs + [line] + [h_ratio_obs]
    else:
        print "No ratio configured!"


    for g in sigs:
        g.line = g.Clone("{}_line".format(g.GetName()))
        g.line.tlatex = g.tlatex
        g.line.SetLineWidth(3)
        g.line.SetLineColor(g.GetLineColor()-12)
        g.line.SetLineStyle(2)



    ## build components
    #leg = build_leg(h_data, bkgs, sigs, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2)
    ##if use_sys: leg.AddEntry(h_bkg_tot_stat, h_bkg_tot_stat.tlatex, 'F')
    #leg2 = build_leg(h_data, bkgs, [s.line for s in sigs], h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2, nolabels=True)

    leg = build_leg_bkg(h_data, bkgs, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2)
    leg2 = build_leg_bkg(h_data, bkgs,h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2, nolabels=True)
    leg3 = build_leg_sig(h_data, sigs, leg_xpos=0.73, leg_ypos=l_y2)
    leg4 = build_leg_sig(h_data, [s.line for s in sigs], leg_xpos=0.73, leg_ypos=l_y2, nolabels=True)
    if var == "TauMETDphi":
        bkg_list1 = []
        bkg_list2 = []
        for i in range(0,len(bkgs)):
            if i <= ((len(bkgs)-1)/2):
                bkg_list1.append(bkgs[i])
            else:
                bkg_list2.append(bkgs[i])
           
        leg = build_leg_bkg(h_data, bkg_list1, err=None, leg_xpos=0.73, leg_ypos=l_y2)
        leg2 = build_leg_bkg(h_data, bkg_list1,err=None, leg_xpos=0.73, leg_ypos=l_y2, nolabels=True)
        leg3 = build_leg_sig(h_data, sigs, leg_xpos=0.53, leg_ypos=l_y2)
        leg4 = build_leg_sig(h_data, [s.line for s in sigs], leg_xpos=0.53, leg_ypos=l_y2, nolabels=True)
        leg5 = build_leg_bkg2(h_data, bkg_list2, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2)
        leg6 = build_leg_bkg2(h_data, bkg_list2, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2, nolabels=True)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = RatioCanvas('rc', cfg.c_w, cfg.c_h, cfg.c_ratio_split)
    #h_total.GetXaxis().SetRangeUser(x1, x2)
    rc.draw_frame1(x1, y1, x2, y2, ';;%s' % (ytitle))
    if ratio_opt == "ratio":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Obs. / exp.' % (xtitle))
    elif ratio_opt == "sig":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Significance' % (xtitle))
        rc.pad2.SetGridy()
    rc.scale_text(cfg.t_size)
    if ndivx is not None:
        rc.fr1.GetXaxis().SetNdivisions(ndivx)
        rc.fr2.GetXaxis().SetNdivisions(ndivx)

    # draw pad1
    rc.pad1.cd()
    rc.pad1.SetLogy(logy)
    rc.pad1.SetLogx(logx)
    if sig_ontop:
        for s in sigs: s.Draw("SAME,HIST")
    h_stack.Draw("SAME,HIST")
    g_data.Draw("SAME,PZ")
    h_bkg_tot.Draw("SAME,E2")
    #if use_sys: h_bkg_tot_stat.Draw("SAME,E2")
    if not sig_ontop:
        for s in sigs:
            s.Draw("SAME,HIST")
            s.line.Draw("SAME,HIST")

    draw_standard_text(atlasx=atlasx, lumix=atlasx, lumi=cfg.lumi, status=cfg.status, compact=True)
    draw_channel_text(x=atlasx, y=0.82, channel="{} {}".format(channame, catname))
    leg.Draw()
    leg2.Draw()
    leg3.Draw()
    leg4.Draw()
    if var == "TauMETDphi":
        leg5.Draw()
        leg6.Draw()
    if not cfg.unblinded:
        line = draw_blind(blind, x1, x2, y1, y2, logy=logy)
    ROOT.gPad.RedrawAxis()
    rc.pad1.Update()

    # draw pad 2
    rc.pad2.cd()
    rc.pad2.SetLogx(logx)
    rc.fr2.GetXaxis().SetMoreLogLabels()
    rc.fr2.GetXaxis().SetNoExponent()
    for r in ratios:
        r.Draw("SAME,{}".format(r.drawopt))
        if hasattr(r, "line"):
            r.line.Draw("SAME,{}".format(r.drawopt))


    ## drawing extra ticks
    tl = ROOT.TLatex()
    tl.SetTextFont(42)
    tl.SetTextSize(rc.fr2.GetXaxis().GetLabelSize())
    tl.SetTextAlign(23)
    #tick_offset = -7.9
    tick_offset = -3.95
    if ratio_opt == "ratio":
        #tick_offset = 0.36
        #tick_offset = 0.34
        if cat == 'T1' and var == 'TauMETDphi':
            tick_offset = 0.36
        elif cat == 'T1' or cat == 'T2':
            tick_offset = 0.787
            if 'prefit' in filestr:
                tick_offset = 0.43
        else:
            tick_offset = 0.787
            if 'prefit' in filestr:
                tick_offset = 0.43
    for tick in ticks:
        tl.DrawLatex(tick, tick_offset, str(tick))


    ROOT.gPad.RedrawAxis()
    rc.pad2.Update()


    return (rc.c, locals())


def get_qcd_ffs(fname, prong, cat, year):
    f = ROOT.TFile.Open(fname)

    hstr = "h_ff_qcdCR_after_{cat}_antiIso_{prong}prong_{year}_{sys}"

    h_nom = f.Get(hstr.format(cat=cat, prong=prong, year=year, sys="nom"))
    h_up  = f.Get(hstr.format(cat=cat, prong=prong, year=year, sys="simUp"))
    h_dn = f.Get(hstr.format(cat=cat, prong=prong, year=year, sys="simDown"))

    g = ROOT.TGraphErrors(h_nom)

    g_sys = ROOT.TGraphAsymmErrors()
    for i in xrange(1, h_nom.GetNbinsX()+1):
        x = h_nom.GetBinCenter(i)
        xup = h_nom.GetBinLowEdge(i+1)
        xdn = h_nom.GetBinLowEdge(i)
        n = h_nom.GetBinContent(i)
        e = h_nom.GetBinError(i)
        up = h_up.GetBinContent(i)
        dn = h_dn.GetBinContent(i)
        eup = sqrt(pow(e, 2) + pow(up - n, 2))
        edn = sqrt(pow(e, 2) + pow(dn - n, 2))
        g_sys.SetPoint(i-1, x, n)
        #g_sys.SetPointError(i-1, x-xdn, xup-x, edn, eup)
        g_sys.SetPointError(i - 1, x - xdn, xup - x, edn, eup)

    #g_sys = make_band(ROOT.TGraph(h_up), ROOT.TGraph(h_dn))

    f.Close()

    g.RemovePoint(0)
    g_sys.RemovePoint(0)
    return (g, g_sys)


def get_wcr_ffs(fname, prong, cat):
    f = ROOT.TFile.Open(fname)

    h_nom = f.Get("WFF_bveto_{}prong".format(prong))
    h_up  = f.Get("combined_{}prong_up".format(prong))
    h_dn = f.Get("combined_{}prong_down".format(prong))

    if cat == "btag":
        if prong == 1:
            for h in [h_nom, h_up, h_dn]: h.Scale(0.8)
        else:
            for h in [h_nom, h_up, h_dn]: h.Scale(0.66)

    g = ROOT.TGraphErrors(h_nom)

    g_sys = ROOT.TGraphAsymmErrors()
    for i in xrange(1, h_nom.GetNbinsX()+1):
        x = h_nom.GetBinCenter(i)
        xup = h_nom.GetBinLowEdge(i+1)
        xdn = h_nom.GetBinLowEdge(i)
        n = h_nom.GetBinContent(i)
        e = h_nom.GetBinError(i)
        up = h_up.GetBinContent(i)
        dn = h_dn.GetBinContent(i)
        eup = sqrt(pow(e, 2) + pow(up - n, 2))
        edn = sqrt(pow(e, 2) + pow(dn - n, 2))
        #print "x: {:5.2f}, n: {:.4f}, up: {:.4f}, dn: {:.4f}, eup: {:.4f}, edn: {:.4f}".format(x, n, up, dn, eup, edn)
        if cat == "btag":
            if prong == 1:
                eup = sqrt(pow(eup,2) + pow(0.1/0.8*n,2))
                edn = sqrt(pow(edn,2) + pow(0.1/0.8*n,2))
            else:
                eup = sqrt(pow(eup,2) + pow(0.17/0.66*n,2))
                edn = sqrt(pow(edn,2) + pow(0.17/0.66*n,2))

        g_sys.SetPoint(i-1, x, n)
        g_sys.SetPointError(i-1, x-xdn, xup-x, edn, eup)

    #g_sys = make_band(ROOT.TGraph(h_up), ROOT.TGraph(h_dn))

    f.Close()

    return (g, g_sys)

def get_qcd_dphicorr(fname):
    f = ROOT.TFile.Open(fname)
    h = f.Get("bveto_low_tauPt_inclusive_ratio")
    g = ROOT.TGraphAsymmErrors()
    for i in xrange(1, h.GetNbinsX()+1):
        x = h.GetBinCenter(i)
        exup = abs(x - min(h.GetBinLowEdge(i+1), pi))
        exdn = abs(x - h.GetBinLowEdge(i))
        y = h.GetBinContent(i)
        ey = 0.5 * abs(1. - y)
        g.SetPoint(i-1, x, y)
        g.SetPointError(i-1, exdn, exup, ey, ey)
    f.Close()
    return g

def get_wcr_dphicorr(fname):
    f = ROOT.TFile.Open(fname)
    h = f.Get("Combined")
    hup = f.Get("Combined_1up")
    hdn = f.Get("Combined_1down")
    g = ROOT.TGraphAsymmErrors()
    for i in xrange(1, h.GetNbinsX()+1):
        x = h.GetBinCenter(i)
        exup = abs(x - h.GetBinLowEdge(i+1))
        exdn = abs(x - h.GetBinLowEdge(i))
        y = h.GetBinContent(i)
        # NOTE: looks like these are just stat, and dont include the 30% sys
        #eyup = abs(hup.GetBinContent(i) - y)
        #eydn = abs(hdn.GetBinContent(i) - y)
        eyup = eydn = 0.3 * abs(1. - y)

        g.SetPoint(i-1, x, y)
        g.SetPointError(i-1, exdn, exup, eydn, eyup)
    f.Close()
    return g




# ________________________________________________________________________________________________
def plot_fakefactors(prong):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 25
    x2 = 200
    y1 = 0.0
    y2 = 0.35
    logy = False
    logx = True
    xtitle = "m_{A} [GeV]"
    ytitle = "Fake-factor"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/limit-scans/limit-scan-tautau-lh_combined.18April.txt"
    filestr_wcr = "inputs/lephad/LepHadFakefactor/Wjets_fake_factors_Mar03_tag-26-05.root"
    filestr_qcd = "inputs/lephad/LepHadFakefactor/hist_fakefactors_qcdCR_yearSplit_data.root"

    assert prong in [1,3], "Invalid prong {}".format(prong)
    if prong == 1:
        prong_text = "1-prong"
    elif prong == 3:
        prong_text = "3-prong"
        y2 = 0.1

    ## Graph getting and manipulation
    ##===========================================================================
    (g_qcd_bveto, g_qcd_bveto_sys) = get_qcd_ffs(filestr_qcd, prong, "bveto", 2016)
    (g_qcd_btag, g_qcd_btag_sys) = get_qcd_ffs(filestr_qcd, prong, "btag", 2016)
    (g_wcr_bveto, g_wcr_bveto_sys) = get_wcr_ffs(filestr_wcr, prong, "bveto")
    (g_wcr_btag, g_wcr_btag_sys) = get_wcr_ffs(filestr_wcr, prong, "btag")


    #graphs_sys = [g_qcd_bveto_sys, g_qcd_btag_sys, g_wcr_bveto_sys, g_wcr_btag_sys]
    #for g in graphs_sys:
    #    #style_hist(g, "stat")
    #    g.SetFillColorAlpha(ROOT.kRed, 0.5)


    # qcd bveto
    g_qcd_bveto_sys.SetMarkerStyle(20)
    g_qcd_bveto_sys.SetMarkerColor(ROOT.kRed)
    g_qcd_bveto_sys.SetLineStyle(1)
    g_qcd_bveto_sys.SetLineColor(ROOT.kRed)
    g_qcd_bveto_sys.SetFillColorAlpha(ROOT.kRed, 0.5)

    # qcd btag
    g_qcd_btag_sys.SetMarkerStyle(24)
    g_qcd_btag_sys.SetMarkerColor(ROOT.kOrange+2)
    g_qcd_btag_sys.SetLineStyle(7)
    g_qcd_btag_sys.SetLineColor(ROOT.kOrange+2)
    g_qcd_btag_sys.SetFillColorAlpha(ROOT.kOrange+2, 0.5)

    # wcr bveto
    g_wcr_bveto_sys.SetMarkerStyle(21)
    g_wcr_bveto_sys.SetMarkerColor(ROOT.kBlue)
    g_wcr_bveto_sys.SetLineStyle(1)
    g_wcr_bveto_sys.SetLineColor(ROOT.kBlue)
    g_wcr_bveto_sys.SetFillColorAlpha(ROOT.kBlue, 0.5)

    # wcr btag
    g_wcr_btag_sys.SetMarkerStyle(25)
    g_wcr_btag_sys.SetMarkerColor(ROOT.kAzure+2)
    g_wcr_btag_sys.SetLineStyle(7)
    g_wcr_btag_sys.SetLineColor(ROOT.kAzure+2)
    g_wcr_btag_sys.SetFillColorAlpha(ROOT.kAzure+2, 0.5)


    ## build components
    entries = [
        [g_wcr_bveto_sys, "#it{f}_{W} #it{b}-veto", "FP"],
        [g_wcr_btag_sys, "#it{f}_{W} #it{b}-tag", "FP"],
        [g_qcd_bveto_sys, "#it{f}_{MJ} #it{b}-veto", "FP"],
        [g_qcd_btag_sys, "#it{f}_{MJ} #it{b}-tag", "FP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    if logx: rc.c.SetLogx()
    rc.c.cd()

    g_qcd_bveto_sys.Draw("SAME,5")
    #g_qcd_bveto.Draw("SAME,3")
    g_qcd_bveto_sys.Draw("SAME,PX")

    g_qcd_btag_sys.Draw("SAME,5")
    #g_qcd_btag.Draw("SAME,3")
    g_qcd_btag_sys.Draw("SAME,PX")

    g_wcr_bveto_sys.Draw("SAME,5")
    #g_wcr_bveto.Draw("SAME,3")
    g_wcr_bveto_sys.Draw("SAME,PX")

    g_wcr_btag_sys.Draw("SAME,5")
    #g_wcr_btag.Draw("SAME,3")
    g_wcr_btag_sys.Draw("SAME,PX")



    """
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy-2.*t_h, prong_text)
    #latex.DrawLatex(atlasx, atlasy-3.*t_h, "H/A #rightarrow #tau#tau 95% CL limits")


    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())


# ________________________________________________________________________________________________
def plot_fakefactors_split():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 25
    x2 = 199.99
    y1 = 0.0001
    #y2_1p = 0.3499
    y2_1p = 0.2999
    y2_3p = 0.0799
    logy = False
    logx = False
    xtitle = "Tau #it{p}_{T} [GeV]"
    #ytitle = "{} fake-factor".format(cfg.tauhadvis)
    ytitle = "Tau fake-factor"
    fsplit = 0.21
    tgap = 0.02

    # decoration placement
    t_x = 0.92                # standard text x
    t_y = 0.80                # standard text y
    t_h = 0.15                # standard text height
    l_x1 = 0.74                # leg xpos right-hand side
    l_y2 = 0.80               # leg ypos top
    l_h  = 0.10               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.8              # atlas y pos

    #filestr = "inputs/limit-scans/limit-scan-tautau-lh_combined.18April.txt"
    filestr_wcr = "inputs/lephad/LepHadFakefactor/Wjets_fake_factors_Mar03_tag-26-05.root"
    filestr_qcd = "inputs/lephad/LepHadFakefactor/hist_fakefactors_qcdCR_yearSplit_data.root"


    ## Graph getting and manipulation
    ##===========================================================================
    (g_qcd_bveto_1p, g_qcd_bveto_sys_1p) = get_qcd_ffs(filestr_qcd, 1, "bveto", 2016)
    (g_qcd_btag_1p, g_qcd_btag_sys_1p) = get_qcd_ffs(filestr_qcd, 1, "btag", 2016)
    (g_wcr_bveto_1p, g_wcr_bveto_sys_1p) = get_wcr_ffs(filestr_wcr, 1, "bveto")
    (g_wcr_btag_1p, g_wcr_btag_sys_1p) = get_wcr_ffs(filestr_wcr, 1, "btag")

    (g_qcd_bveto_3p, g_qcd_bveto_sys_3p) = get_qcd_ffs(filestr_qcd, 3, "bveto", 2016)
    (g_qcd_btag_3p, g_qcd_btag_sys_3p) = get_qcd_ffs(filestr_qcd, 3, "btag", 2016)
    (g_wcr_bveto_3p, g_wcr_bveto_sys_3p) = get_wcr_ffs(filestr_wcr, 3, "bveto")
    (g_wcr_btag_3p, g_wcr_btag_sys_3p) = get_wcr_ffs(filestr_wcr, 3, "btag")

    graphs_qcd = [g_qcd_bveto_sys_1p, g_qcd_btag_sys_1p, g_qcd_bveto_sys_3p, g_qcd_btag_sys_3p]
    graphs_wcr = [g_wcr_bveto_sys_1p, g_wcr_btag_sys_1p, g_wcr_bveto_sys_3p, g_wcr_btag_sys_3p]

    def style_qcd(g):
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kRed)
        g.SetFillColorAlpha(ROOT.kRed, 0.5)

    def style_wcr(g):
        g.SetMarkerStyle(21)
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kBlue)
        g.SetFillColorAlpha(ROOT.kBlue, 0.5)

    for g in graphs_qcd: style_qcd(g)
    for g in graphs_wcr: style_wcr(g)

    ## build components
    entries = [
        #[g_qcd_bveto_sys_1p, "#it{f}_{W}", "FP"],
        #[g_wcr_bveto_sys_1p, "#it{f}_{MJ}", "FP"],
        [g_wcr_bveto_sys_1p, "#it{W}+jets", "FP"],
        [g_qcd_bveto_sys_1p, "Multijet", "FP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    c = ROOT.TCanvas('c', 'c', cfg.c_w, cfg.c_h)

    c.p1 = ROOT.TPad("p1", "p1", 0.0, 1.0 - 1. * fsplit - tgap, 1.0, 1.0-tgap)
    c.p2 = ROOT.TPad("p2", "p2", 0.0, 1.0 - 2. * fsplit - tgap, 1.0, 1.0 - 1. * fsplit - tgap)
    c.p3 = ROOT.TPad("p3", "p3", 0.0, 1.0 - 3. * fsplit - tgap, 1.0, 1.0 - 2. * fsplit - tgap)
    c.p4 = ROOT.TPad("p4", "p4", 0.0, 1.0 - 3. * fsplit - tgap, 1.0, 0.)
    c.py = ROOT.TPad("py", "py", 0.0, 0.0, 0.05, 1.0 - tgap)
    for p in [c.p1, c.p2, c.p3]: p.SetBottomMargin(0.005)
    for p in [c.p1, c.p2, c.p3, c.p4]:
        p.SetTopMargin(0.005)
        if logx: p.SetLogx()

    h4 = 1.0 - 3.*fsplit - tgap
    scale = h4 / fsplit
    c.p4.SetBottomMargin((h4-fsplit)/h4)
    c.p4.SetTopMargin(0.005 / scale)
    c.p1.Draw()
    c.p2.Draw()
    c.p3.Draw()
    c.p4.Draw()
    c.py.Draw()

    c.p1.cd()
    c.fr1 = c.p1.DrawFrame(x1, y1, x2, y2_1p, "")
    g_qcd_bveto_sys_1p.Draw("SAME,5")
    g_qcd_bveto_sys_1p.Draw("SAME,PX")
    g_wcr_bveto_sys_1p.Draw("SAME,5")
    g_wcr_bveto_sys_1p.Draw("SAME,PX")

    c.p2.cd()
    c.fr2 = c.p2.DrawFrame(x1, y1, x2, y2_1p, "")
    g_qcd_btag_sys_1p.Draw("SAME,5")
    g_qcd_btag_sys_1p.Draw("SAME,PX")
    g_wcr_btag_sys_1p.Draw("SAME,5")
    g_wcr_btag_sys_1p.Draw("SAME,PX")

    c.p3.cd()
    c.fr3 = c.p3.DrawFrame(x1, y1, x2, y2_3p, "")
    g_qcd_bveto_sys_3p.Draw("SAME,5")
    g_qcd_bveto_sys_3p.Draw("SAME,PX")
    g_wcr_bveto_sys_3p.Draw("SAME,5")
    g_wcr_bveto_sys_3p.Draw("SAME,PX")

    c.p4.cd()
    c.fr4 = c.p4.DrawFrame(x1, y1, x2, y2_3p, ";%s"%xtitle)
    g_qcd_btag_sys_3p.Draw("SAME,5")
    g_qcd_btag_sys_3p.Draw("SAME,PX")
    g_wcr_btag_sys_3p.Draw("SAME,5")
    g_wcr_btag_sys_3p.Draw("SAME,PX")


    title_size = 33
    label_size = 30
    tick_size_x = 0.1
    tick_size_y = 0.015
    for fr in [c.fr1, c.fr2, c.fr3, c.fr4]:
        fr.GetXaxis().SetTitleFont(43)
        fr.GetXaxis().SetTitleSize(0)
        fr.GetXaxis().SetLabelFont(43)
        fr.GetXaxis().SetLabelSize(0)

        fr.GetYaxis().SetTitleFont(43)
        fr.GetYaxis().SetTitleSize(title_size)
        fr.GetYaxis().SetLabelFont(43)
        fr.GetYaxis().SetLabelSize(label_size)

        fr.GetYaxis().SetNdivisions(505)

        fr.GetXaxis().SetTickSize(tick_size_x)
        fr.GetYaxis().SetTickSize(tick_size_y)


    c.fr4.GetXaxis().SetTitleSize(title_size)
    c.fr4.GetXaxis().SetLabelSize(label_size)
    c.fr4.GetXaxis().SetTickSize(tick_size_x / scale)
    c.fr4.GetYaxis().SetTickSize(tick_size_y * scale)
    c.fr4.GetXaxis().SetTitleOffset(4)
    c.fr4.GetXaxis().SetLabelOffset(0.02)
    if logx: c.fr4.GetXaxis().SetMoreLogLabels()


    """
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """


    t_y_4 = (h4-fsplit)/h4 + t_y*fsplit/h4
    # standard atlas text
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(20)
    latex.SetTextAlign(11)
    c.p3.cd()
    if not cfg.status:
        latex.DrawLatex(atlasx, t_y, "#it{#bf{ATLAS}} Internal")
    else:
        latex.DrawLatex(atlasx, t_y, "#it{#bf{ATLAS}} %s"%(cfg.status))
    latex.DrawLatex(atlasx, t_y-t_h, "#sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    c.p4.cd()
    latex.DrawLatex(atlasx, t_y_4, "{} channel".format(cfg.lephad))

    # pane labels
    latex.SetTextAlign(31)
    c.p1.cd()
    latex.DrawLatex(t_x, t_y, "1-prong, #it{b}-veto")
    c.p2.cd()
    latex.DrawLatex(t_x, t_y, "1-prong, #it{b}-tag")
    c.p3.cd()
    latex.DrawLatex(t_x, t_y, "3-prong, #it{b}-veto")
    c.p4.cd()
    latex.DrawLatex(t_x, t_y_4, "3-prong, #it{b}-tag")

    leg.Draw()

    c.py.cd()
    latex.SetTextSize(title_size)
    latex.SetTextAngle(90)
    latex.SetTextAlign(32)
    latex.DrawLatex(0.5, 0.985, ytitle)

    #ROOT.gPad.RedrawAxis()
    c.Update()


    #rc.c.SaveAs("test.pdf")
    return (c, locals())




# ________________________________________________________________________________________________
def plot_fakefactors_split_wcorr():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 25
    x2 = 199.99
    x1_2 = 0.0
    x2_2 = pi
    y1 = 0.0001
    #y2_1p = 0.3499
    y2_1p = 0.2999
    y2_3p = 0.0799
    y1_2 = 0.61
    y2_2 = 1.49
    logy = False
    logx = False
    xtitle = "Tau #it{p}_{T} [GeV]"
    #ytitle = "{} fake-factor".format(cfg.tauhadvis)
    ytitle = "Tau fake-factor"
    xtitle2 = "#it{#Delta#phi}(%s, %s) [rad]" % (cfg.tauhadvis, cfg.met)
    ytitle2 = "Correction"
    fsplit = 0.15
    tgap = 0.02
    fsplit2 = (1.0 - tgap - 3. * fsplit) / 2.0

    print "fsplit: ", fsplit
    print "fsplit2: ", fsplit2

    # decoration placement
    t_x = 0.92                # standard text x
    t_y = 0.75                # standard text y
    t_h = 0.11                # standard text height
    l_x1 = 0.74                # leg xpos right-hand side
    l_y2 = 0.80               # leg ypos top
    l_h  = 0.10               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.8              # atlas y pos

    #filestr = "inputs/limit-scans/limit-scan-tautau-lh_combined.18April.txt"
    filestr_wcr = "inputs/lephad/LepHadFakefactor/Wjets_fake_factors_Mar03_tag-26-05.root"
    filestr_qcd = "inputs/lephad/LepHadFakefactor/hist_fakefactors_qcdCR_yearSplit_data.root"
    filestr_qcd_dphi = "inputs/lephad/LepHadFakefactor/dphi_correction_qcd_bveto_jbp70.root"
    filestr_wcr_dphi = "inputs/lephad/LepHadFakefactor/MyDphiCorrectionAll.root"


    ## Graph getting and manipulation
    ##===========================================================================
    (g_qcd_bveto_1p, g_qcd_bveto_sys_1p) = get_qcd_ffs(filestr_qcd, 1, "bveto", 2016)
    (g_qcd_btag_1p, g_qcd_btag_sys_1p) = get_qcd_ffs(filestr_qcd, 1, "btag", 2016)
    (g_wcr_bveto_1p, g_wcr_bveto_sys_1p) = get_wcr_ffs(filestr_wcr, 1, "bveto")
    (g_wcr_btag_1p, g_wcr_btag_sys_1p) = get_wcr_ffs(filestr_wcr, 1, "btag")

    (g_qcd_bveto_3p, g_qcd_bveto_sys_3p) = get_qcd_ffs(filestr_qcd, 3, "bveto", 2016)
    (g_qcd_btag_3p, g_qcd_btag_sys_3p) = get_qcd_ffs(filestr_qcd, 3, "btag", 2016)
    (g_wcr_bveto_3p, g_wcr_bveto_sys_3p) = get_wcr_ffs(filestr_wcr, 3, "bveto")
    (g_wcr_btag_3p, g_wcr_btag_sys_3p) = get_wcr_ffs(filestr_wcr, 3, "btag")

    g_qcd_dphi = get_qcd_dphicorr(filestr_qcd_dphi)
    g_wcr_dphi = get_wcr_dphicorr(filestr_wcr_dphi)

    graphs_qcd = [g_qcd_bveto_sys_1p, g_qcd_btag_sys_1p, g_qcd_bveto_sys_3p, g_qcd_btag_sys_3p, g_qcd_dphi]
    graphs_wcr = [g_wcr_bveto_sys_1p, g_wcr_btag_sys_1p, g_wcr_bveto_sys_3p, g_wcr_btag_sys_3p, g_wcr_dphi]



    def style_qcd(g):
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kRed)
        g.SetFillColorAlpha(ROOT.kRed, 0.5)

    def style_wcr(g):
        g.SetMarkerStyle(21)
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kBlue)
        g.SetFillColorAlpha(ROOT.kBlue, 0.5)

    for g in graphs_qcd: style_qcd(g)
    for g in graphs_wcr: style_wcr(g)

    ## build components
    entries = [
        #[g_qcd_bveto_sys_1p, "#it{f}_{W}", "FP"],
        #[g_wcr_bveto_sys_1p, "#it{f}_{MJ}", "FP"],
        [g_wcr_bveto_sys_1p, "#it{W}+jets", "FP"],
        [g_qcd_bveto_sys_1p, "Multijet", "FP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    c = ROOT.TCanvas('c', 'c', cfg.c_w, cfg.c_h)

    c.p1 = ROOT.TPad("p1", "p1", 0.0, 1.0 - 1. * fsplit - tgap, 1.0, 1.0-tgap)
    c.p2 = ROOT.TPad("p2", "p2", 0.0, 1.0 - 2. * fsplit - tgap, 1.0, 1.0 - 1. * fsplit - tgap)
    c.p3 = ROOT.TPad("p3", "p3", 0.0, 1.0 - 3. * fsplit - tgap, 1.0, 1.0 - 2. * fsplit - tgap)
    c.p4 = ROOT.TPad("p4", "p4", 0.0, fsplit2, 1.0, 2. * fsplit2)
    c.p5 = ROOT.TPad("p5", "p5", 0.0, 0.0, 1.0, fsplit2)
    c.py = ROOT.TPad("py", "py", 0.0, fsplit2, 0.05, 1.0 - tgap)
    c.py2 = ROOT.TPad("py", "py", 0.0, 0.0, 0.05, fsplit2)
    for p in [c.p1, c.p2, c.p3]: p.SetBottomMargin(0.005)
    for p in [c.p1, c.p2, c.p3, c.p4]:
        p.SetTopMargin(0.005)
        if logx: p.SetLogx()
    c.p5.SetTopMargin(0.01)

    scale = fsplit2 / fsplit
    c.p4.SetBottomMargin(1. - fsplit / fsplit2 )
    c.p5.SetBottomMargin(1. - fsplit / fsplit2)
    #c.p4.SetBottomMargin(0.1)
    c.p4.SetTopMargin(0.005 / scale)
    c.p1.Draw()
    c.p2.Draw()
    c.p3.Draw()
    c.p4.Draw()
    c.p5.Draw()
    c.py.Draw()
    c.py2.Draw()

    c.p1.cd()
    c.fr1 = c.p1.DrawFrame(x1, y1, x2, y2_1p, "")
    g_qcd_bveto_sys_1p.Draw("SAME,5")
    g_qcd_bveto_sys_1p.Draw("SAME,PX")
    g_wcr_bveto_sys_1p.Draw("SAME,5")
    g_wcr_bveto_sys_1p.Draw("SAME,PX")

    c.p2.cd()
    c.fr2 = c.p2.DrawFrame(x1, y1, x2, y2_1p, "")
    g_qcd_btag_sys_1p.Draw("SAME,5")
    g_qcd_btag_sys_1p.Draw("SAME,PX")
    g_wcr_btag_sys_1p.Draw("SAME,5")
    g_wcr_btag_sys_1p.Draw("SAME,PX")

    c.p3.cd()
    c.fr3 = c.p3.DrawFrame(x1, y1, x2, y2_3p, "")
    g_qcd_bveto_sys_3p.Draw("SAME,5")
    g_qcd_bveto_sys_3p.Draw("SAME,PX")
    g_wcr_bveto_sys_3p.Draw("SAME,5")
    g_wcr_bveto_sys_3p.Draw("SAME,PX")

    c.p4.cd()
    c.fr4 = c.p4.DrawFrame(x1, y1, x2, y2_3p, ";%s"%xtitle)
    g_qcd_btag_sys_3p.Draw("SAME,5")
    g_qcd_btag_sys_3p.Draw("SAME,PX")
    g_wcr_btag_sys_3p.Draw("SAME,5")
    g_wcr_btag_sys_3p.Draw("SAME,PX")

    c.p5.cd()
    c.fr5 = c.p5.DrawFrame(x1_2, y1_2, x2_2, y2_2, ";%s"%(xtitle2))
    g_qcd_dphi.Draw("SAME, 5")
    g_qcd_dphi.Draw("SAME, PX")
    g_wcr_dphi.Draw("SAME, 5")
    g_wcr_dphi.Draw("SAME, PX")


    title_size = 33
    label_size = 30
    tick_size_x = 0.1
    tick_size_y = 0.015
    for fr in [c.fr1, c.fr2, c.fr3, c.fr4, c.fr5]:
        fr.GetXaxis().SetTitleFont(43)
        fr.GetXaxis().SetTitleSize(0)
        fr.GetXaxis().SetLabelFont(43)
        fr.GetXaxis().SetLabelSize(0)

        fr.GetYaxis().SetTitleFont(43)
        fr.GetYaxis().SetTitleSize(title_size)
        fr.GetYaxis().SetLabelFont(43)
        fr.GetYaxis().SetLabelSize(label_size)

        fr.GetYaxis().SetNdivisions(505)

        fr.GetXaxis().SetTickSize(tick_size_x)
        fr.GetYaxis().SetTickSize(tick_size_y)

    for fr in [c.fr4, c.fr5]:
        fr.GetXaxis().SetTitleSize(title_size)
        fr.GetXaxis().SetLabelSize(label_size)
        fr.GetXaxis().SetTickSize(tick_size_x / scale)
        fr.GetYaxis().SetTickSize(tick_size_y * scale)
        fr.GetXaxis().SetTitleOffset(3.9)
        fr.GetXaxis().SetLabelOffset(0.02)
    if logx: c.fr4.GetXaxis().SetMoreLogLabels()

    c.fr5.GetYaxis().SetTitleOffset(2)

    """
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """


    t_y_4 = (fsplit2-fsplit)/fsplit2 + t_y*fsplit/fsplit2
    # standard atlas text
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(20)
    latex.SetTextAlign(11)
    #c.p3.cd()
    c.p4.cd()
    #latex.DrawLatex(atlasx, t_y_4, "#it{#bf{ATLAS}} Internal")
    #latex.DrawLatex(atlasx, t_y_4-t_h, "#sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    if not cfg.status:
        latex.DrawLatex(atlasx, t_y_4, "#it{#bf{ATLAS}} Internal, #sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    else:
        latex.DrawLatex(atlasx, t_y_4, "#it{#bf{ATLAS}} %s, #sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.status, cfg.lumi))
    latex.DrawLatex(atlasx, t_y_4-t_h, "{} channel".format(cfg.lephad))

    # pane labels
    latex.SetTextAlign(31)
    c.p1.cd()
    latex.DrawLatex(t_x, t_y, "1-prong, #it{b}-veto")
    c.p2.cd()
    latex.DrawLatex(t_x, t_y, "1-prong, #it{b}-tag")
    c.p3.cd()
    latex.DrawLatex(t_x, t_y, "3-prong, #it{b}-veto")
    c.p4.cd()
    latex.DrawLatex(t_x, t_y_4, "3-prong, #it{b}-tag")
    leg.Draw()
    c.p5.cd()
    latex.DrawLatex(t_x, t_y_4, "all-prong, #it{b}-veto")



    c.py.cd()
    latex.SetTextSize(title_size)
    latex.SetTextAngle(90)
    latex.SetTextAlign(32)
    latex.DrawLatex(0.5, 0.985, ytitle)

    c.py2.cd()
    latex.DrawLatex(0.5, 0.95, ytitle2)

    #ROOT.gPad.RedrawAxis()
    c.Update()


    #rc.c.SaveAs("test.pdf")
    return (c, locals())




# ________________________________________________________________________________________________
def plot_fakefactors_split_2x2():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 25
    x2 = 199.99
    y1 = 0.0001
    #y2_1p = 0.3499
    y2_1p = 0.2999
    y2_3p = 0.0799
    logy = False
    logx = False
    #xtitle = "Tau #it{p}_{T} [GeV]"
    xtitle = "%s #it{p}_{T} [GeV]" % (cfg.tauhadvis)
    #ytitle = "{} fake-factor".format(cfg.tauhadvis)
    #ytitle = "Tau fake-factor"
    ytitle = "%s fake-factor" % (cfg.tauhadvis)
    fsplit = 0.4015
    hsplit = 0.405
    tgap = 0.037
    rgap = 0.030

    # decoration placement
    t_x = 0.92                # standard text x
    t_y = 0.90                # standard text y
    t_h = 0.08                # standard text height
    l_x1 = 0.50                # leg xpos right-hand side
    l_y2 = 0.80               # leg ypos top
    l_h  = 0.10               # leg text heigh
    l_w  = 0.50               # leg width
    atlasx = 0.35              # atlas x pos
    atlasy = 0.17              # atlas y pos

    #filestr = "inputs/limit-scans/limit-scan-tautau-lh_combined.18April.txt"
    filestr_wcr = "inputs/lephad/LepHadFakefactor/Wjets_fake_factors_Mar03_tag-26-05.root"
    filestr_qcd = "inputs/lephad/LepHadFakefactor/hist_fakefactors_qcdCR_yearSplit_data.root"


    ## Graph getting and manipulation
    ##===========================================================================
    (g_qcd_bveto_1p, g_qcd_bveto_sys_1p) = get_qcd_ffs(filestr_qcd, 1, "bveto", 2016)
    (g_qcd_btag_1p, g_qcd_btag_sys_1p) = get_qcd_ffs(filestr_qcd, 1, "btag", 2016)
    (g_wcr_bveto_1p, g_wcr_bveto_sys_1p) = get_wcr_ffs(filestr_wcr, 1, "bveto")
    (g_wcr_btag_1p, g_wcr_btag_sys_1p) = get_wcr_ffs(filestr_wcr, 1, "btag")

    (g_qcd_bveto_3p, g_qcd_bveto_sys_3p) = get_qcd_ffs(filestr_qcd, 3, "bveto", 2016)
    (g_qcd_btag_3p, g_qcd_btag_sys_3p) = get_qcd_ffs(filestr_qcd, 3, "btag", 2016)
    (g_wcr_bveto_3p, g_wcr_bveto_sys_3p) = get_wcr_ffs(filestr_wcr, 3, "bveto")
    (g_wcr_btag_3p, g_wcr_btag_sys_3p) = get_wcr_ffs(filestr_wcr, 3, "btag")

    graphs_qcd = [g_qcd_bveto_sys_1p, g_qcd_btag_sys_1p, g_qcd_bveto_sys_3p, g_qcd_btag_sys_3p]
    graphs_wcr = [g_wcr_bveto_sys_1p, g_wcr_btag_sys_1p, g_wcr_bveto_sys_3p, g_wcr_btag_sys_3p]

    def style_qcd(g):
        """
        ## TRANSPARENT FILL STYLE
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kRed)
        g.SetFillColorAlpha(ROOT.kRed, 0.5)
        """
        ## HATCHED FILL STYLE
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetFillColor(ROOT.kRed)
        #g.SetFillStyle(3545)
        g.SetFillStyle(3005)
        g.SetLineColor(ROOT.kRed)
        g.SetLineStyle(1)
        g.SetLineWidth(2)
        g.outline = g.Clone("{}_outline".format(g.GetName()))
        g.outline.SetFillStyle(0)



    def style_wcr(g):
        """
        ## TRANSPARENT FILL STYLE
        g.SetMarkerStyle(21)
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kBlue)
        g.SetFillColorAlpha(ROOT.kBlue, 0.5)
        """
        ## HATCHED FILL STYLE
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetFillColor(ROOT.kBlue)
        #g.SetFillStyle(3554)
        g.SetFillStyle(3004)
        g.SetLineColor(ROOT.kBlue)
        g.SetLineStyle(1)
        g.SetLineWidth(2)
        g.outline = g.Clone("{}_outline".format(g.GetName()))
        g.outline.SetFillStyle(0)


    for g in graphs_qcd: style_qcd(g)
    for g in graphs_wcr: style_wcr(g)

    ## build components
    entries = [
        #[g_qcd_bveto_sys_1p, "#it{f}_{W}", "FP"],
        #[g_wcr_bveto_sys_1p, "#it{f}_{MJ}", "FP"],
        [g_wcr_bveto_sys_1p, "#it{W}+jets", "FP"],
        [g_qcd_bveto_sys_1p, "Multijet", "FP"],
        #[g_wcr_bveto_sys_1p, "1-fake (#it{f}_{1})", "FP"],
        #[g_qcd_bveto_sys_1p, "2-fake (#it{f}_{2a})", "FP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    c = ROOT.TCanvas('c', 'c', cfg.c_w, cfg.c_h)

    c.p1 = ROOT.TPad("p1", "p1", 0.0, 1.0 - 1. * fsplit - tgap, 1.0-hsplit-rgap, 1.0-tgap)
    c.p2 = ROOT.TPad("p2", "p2", 1.-hsplit-rgap, 1.0 - 1. * fsplit - tgap, 1.0-rgap, 1.0 - tgap)
    c.p3 = ROOT.TPad("p3", "p3", 0.0, 0.0, 1.-hsplit-rgap, 1.0 - 1. * fsplit - tgap)
    c.p4 = ROOT.TPad("p4", "p4", 1.-hsplit-rgap, 0.0, 1.0-rgap, 1.0 - 1. * fsplit - tgap)
    #c.py = ROOT.TPad("py", "py", 0.0, 0.0, 0.05, 1.0 - tgap)
    for p in [c.p1, c.p2]:
        if logx: p.SetLogx()
        p.SetBottomMargin(0.005)
        p.SetTopMargin(0.005)
    h4 = 1.0 - 1.*fsplit - tgap
    w4 = 1.0 - 1. * hsplit - rgap
    scale = h4 / fsplit
    for p in [c.p3, c.p4]:
        if logx: p.SetLogx()
        p.SetBottomMargin((h4-fsplit)/h4)
        p.SetTopMargin(0.005 / scale)
    for p in [c.p1, c.p3]:
        p.SetLeftMargin(1. - hsplit / (1.-hsplit-rgap))
        p.SetRightMargin(0.005)
    for p in [c.p2, c.p4]:
        p.SetLeftMargin(0.005)

    c.p1.Draw()
    c.p2.Draw()
    c.p3.Draw()
    c.p4.Draw()
    #c.py.Draw()

    c.p1.cd()
    c.fr1 = c.p1.DrawFrame(x1, y1, x2, y2_1p, ";;%s"%ytitle)
    g_qcd_bveto_sys_1p.Draw("SAME,5")
    g_qcd_bveto_sys_1p.outline.Draw("SAME,5") # When using hatching
    g_qcd_bveto_sys_1p.Draw("SAME,PX")
    g_wcr_bveto_sys_1p.Draw("SAME,5")
    g_wcr_bveto_sys_1p.outline.Draw("SAME,5") # When using hatching
    g_wcr_bveto_sys_1p.Draw("SAME,PX")

    c.p2.cd()
    c.fr2 = c.p2.DrawFrame(x1, y1, x2, y2_1p, "")
    g_qcd_btag_sys_1p.Draw("SAME,5")
    g_qcd_btag_sys_1p.outline.Draw("SAME,5") # When using hatching
    g_qcd_btag_sys_1p.Draw("SAME,PX")
    g_wcr_btag_sys_1p.Draw("SAME,5")
    g_wcr_btag_sys_1p.outline.Draw("SAME,5") # When using hatching
    g_wcr_btag_sys_1p.Draw("SAME,PX")

    c.p3.cd()
    c.fr3 = c.p3.DrawFrame(x1, y1, x2, y2_3p, "")
    g_qcd_bveto_sys_3p.Draw("SAME,5")
    g_qcd_bveto_sys_3p.outline.Draw("SAME,5") # When using hatching
    g_qcd_bveto_sys_3p.Draw("SAME,PX")
    g_wcr_bveto_sys_3p.Draw("SAME,5")
    g_wcr_bveto_sys_3p.outline.Draw("SAME,5") # When using hatching
    g_wcr_bveto_sys_3p.Draw("SAME,PX")

    c.p4.cd()
    c.fr4 = c.p4.DrawFrame(x1, y1, x2, y2_3p, ";%s"%xtitle)
    g_qcd_btag_sys_3p.Draw("SAME,5")
    g_qcd_btag_sys_3p.outline.Draw("SAME,5") # When using hatching
    g_qcd_btag_sys_3p.Draw("SAME,PX")
    g_wcr_btag_sys_3p.Draw("SAME,5")
    g_wcr_btag_sys_3p.outline.Draw("SAME,5") # When using hatching
    g_wcr_btag_sys_3p.Draw("SAME,PX")


    title_size = 33
    label_size = 30
    tick_size_x = 0.05
    tick_size_y = 0.025
    for fr in [c.fr1, c.fr2, c.fr3, c.fr4]:
        fr.GetXaxis().SetTitleFont(43)
        fr.GetXaxis().SetTitleSize(0)
        fr.GetXaxis().SetLabelFont(43)
        fr.GetXaxis().SetLabelSize(0)

        fr.GetYaxis().SetTitleFont(43)
        fr.GetYaxis().SetTitleSize(title_size)
        fr.GetYaxis().SetLabelFont(43)
        fr.GetYaxis().SetLabelSize(label_size)
        fr.GetYaxis().SetTitleOffset(3.0)

        fr.GetYaxis().SetNdivisions(505)
        fr.GetXaxis().SetNdivisions(505)

        fr.GetXaxis().SetTickSize(tick_size_x)
        fr.GetYaxis().SetTickSize(tick_size_y)

    for fr in [c.fr3, c.fr4]:
        fr.GetXaxis().SetTitleSize(title_size)
        fr.GetXaxis().SetLabelSize(label_size)
        fr.GetXaxis().SetTickSize(tick_size_x / scale)
        fr.GetYaxis().SetTickSize(tick_size_y * scale)
        fr.GetXaxis().SetTitleOffset(2.5)
        fr.GetXaxis().SetLabelOffset(0.02)
        if logx: fr.GetXaxis().SetMoreLogLabels()

    for fr in [c.fr2, c.fr4]:
        fr.GetYaxis().SetTitleSize(0)




    t_y_4 = (h4-fsplit)/h4 + t_y*fsplit/h4
    t_x_4 = t_x + 0.05
    # standard atlas text
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(20)


    # TEXT IN LOWER LEFT
    #latex.SetTextAlign(11)
    #c.p3.cd()
    #if not cfg.status:
    #    latex.DrawLatex(atlasx, t_y_4-2.*t_h, "#it{#bf{ATLAS}} Internal")
    #else:
    #    latex.DrawLatex(atlasx, t_y_4 - 2. * t_h, "#it{#bf{ATLAS}} %s"%(cfg.status))
    #latex.DrawLatex(atlasx, t_y_4-3.*t_h, "#sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    #latex.DrawLatex(atlasx, t_y_4-4.*t_h, "{} channel".format(cfg.lephad))

    # TEXT IN UPPER LEFT / RIGHT
    c.p1.cd()
    #if cfg.status is None:
    #    latex.DrawLatex(atlasx, atlasy, "#it{#bf{ATLAS}} Internal")
    #else:
    #    latex.DrawLatex(atlasx, atlasy, "#it{#bf{ATLAS}} %s"%(cfg.status))
    #latex.DrawLatex(atlasx, atlasy-t_h, "#sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    latex.DrawLatex(atlasx-0.03, atlasy-t_h, "#it{#bf{ATLAS}} #sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    c.p2.cd()
    latex.DrawLatex(atlasx-0.3, atlasy-t_h, "{} channel".format(cfg.lephad))



    # pane labels
    latex.SetTextAlign(31)
    c.p1.cd()
    latex.DrawLatex(t_x_4, t_y, "one-track, #it{b}-veto")
    c.p2.cd()
    latex.DrawLatex(t_x, t_y, "one-track, #it{b}-tag")
    c.p3.cd()
    latex.DrawLatex(t_x_4, t_y_4, "three-track, #it{b}-veto")
    c.p4.cd()
    latex.DrawLatex(t_x, t_y_4, "three-track, #it{b}-tag")

    leg.Draw()

    #ROOT.gPad.RedrawAxis()
    c.Update()


    #rc.c.SaveAs("test.pdf")
    return (c, locals())



# ________________________________________________________________________________________________
def plot_wcorr():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 0
    x2 = pi
    y1 = 0.61
    y2 = 1.49
    logy = False
    xtitle = "#it{#Delta#phi}(%s, %s) [rad]" % (cfg.tauhadvis, cfg.met)
    #ytitle = "Tau fake-factor correction"
    ytitle = "%s fake-factor correction" % (cfg.tauhadvis)

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    filestr_qcd_dphi = "inputs/lephad/LepHadFakefactor/dphi_correction_qcd_bveto_jbp70.root"
    filestr_wcr_dphi = "inputs/lephad/LepHadFakefactor/MyDphiCorrectionAll.root"

    ## Graph getting and manipulation
    ##===========================================================================
    g_qcd_dphi = get_qcd_dphicorr(filestr_qcd_dphi)
    g_wcr_dphi = get_wcr_dphicorr(filestr_wcr_dphi)

    def style_qcd(g):
        """
        ## TRANSPARENT FILL STYLE
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kRed)
        g.SetFillColorAlpha(ROOT.kRed, 0.5)
        """
        ## HATCHED FILL STYLE
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetFillColor(ROOT.kRed)
        #g.SetFillStyle(3545)
        g.SetFillStyle(3005)
        g.SetLineColor(ROOT.kRed)
        g.SetLineStyle(1)
        g.SetLineWidth(2)
        g.outline = g.Clone("{}_outline".format(g.GetName()))
        g.outline.SetFillStyle(0)



    def style_wcr(g):
        """
        ## TRANSPARENT FILL STYLE
        g.SetMarkerStyle(21)
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kBlue)
        g.SetFillColorAlpha(ROOT.kBlue, 0.5)
        """
        ## HATCHED FILL STYLE
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetFillColor(ROOT.kBlue)
        #g.SetFillStyle(3554)
        g.SetFillStyle(3004)
        g.SetLineColor(ROOT.kBlue)
        g.SetLineStyle(1)
        g.SetLineWidth(2)
        g.outline = g.Clone("{}_outline".format(g.GetName()))
        g.outline.SetFillStyle(0)




    style_qcd(g_qcd_dphi)
    style_wcr(g_wcr_dphi)

    ## build components
    entries = [
        [g_wcr_dphi, "#it{W}+jets", "FP"],
        [g_qcd_dphi, "Multijet", "FP"],
        #[g_wcr_dphi, "1-fake (#it{f}_{1})", "FP"],
        #[g_qcd_dphi, "2-fake (#it{f}_{2a})", "FP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)


    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()

    g_qcd_dphi.Draw("SAME,5")
    g_qcd_dphi.outline.Draw("SAME,5")
    g_qcd_dphi.Draw("SAME,PX")
    g_wcr_dphi.Draw("SAME,5")
    g_wcr_dphi.outline.Draw("SAME,5")
    g_wcr_dphi.Draw("SAME,PX")

    # standard text + legend
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status = cfg.status)
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status, lumi=cfg.lumi, compact=True)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx, atlasy - t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy - 2.*t_h, "{} channel".format(cfg.lephad))
    #latex.DrawLatex(atlasx, atlasy - 1. * t_h, "{} channel".format(cfg.lephad))
    draw_channel_text(x=atlasx, y=0.85, channel="{} channel".format(cfg.lephad))
    leg.Draw()



    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())


def get_acc_points(prod,chan):
    """

    :param prod:
    :return:
    """
    import acceptances as acc
    br_muhad = 0.1741
    br_ehad = 0.1783
    br_hadhad = 1.0 - br_muhad - br_ehad
    br_lh = 2.0 * (br_muhad + br_ehad) * br_hadhad
    br_hh = br_hadhad * br_hadhad
    points = []
    if prod == "Zprime":
        if chan == "lh":
            points = [[m, a / 100.] for (m,a,_) in acc.ssm]
        elif chan == "hh":
            points = [[m, a / 100.] for (m, _, a) in acc.ssm]
    elif prod == "bbH":
        if chan == "lh":
            points = [(m, a / 100.0 * br_lh) for (m,a) in acc.bbH_lh]
        elif chan == "hh":
            points = [(m, a / 100.0 * br_lh) for (m, a) in acc.bbH_hh]
        points = [[m,a] for (m,a) in points if m<=2250]
    elif prod == "ggH":
        if chan == "lh":
            points = [(m, a / 100.0 * br_lh) for (m,a) in acc.ggH_lh]
        elif chan == "hh":
            points = [(m, a / 100.0 * br_lh) for (m, a) in acc.ggH_hh]
        points = [[m, a] for (m, a) in points if m <= 2250]
    else:
        print "ERROR - invalid prod: {}".format(prod)
        exit(1)

    return points

def get_acc_graphs(prod, chan):
    """

    :return:
    """
    g = ROOT.TGraph()
    for (m, a) in get_acc_points(prod, chan):
        g.SetPoint(g.GetN(),m,a)
    return g

def get_acceff_points(prod,chan):
    import make_tables_sig as mod
    lumi = 36100.
    if prod == "Zprime":
        zpxsec = ZprimeXsecProvider("SSM")
        if chan == "hh":
            points = [
                [200, 2525.876561],
                [300, 8697.050503],
                [400, 10760.404465],
                [500, 8502.806143],
                [600, 6022.305917],
                [700, 3870.437998],
                [800, 2568.801777],
                [900, 1695.846916],
                [1000, 1136.682953],
                [1250, 438.473498],
                [1500, 182.698968],
                [1750, 80.160096],
                [2000, 36.349139],
                [2250, 17.651782],
                [2500, 8.769309],
                [2750, 4.586581],
                [3000, 2.443738],
                [3250, 1.377051],
                [3500, 0.787049],
                [3750, 0.473102],
                [4000, 0.296300],
            ]
        elif chan == "lh":
            points_ehad = [
                [200, 39746.110151],
                [300, 24749.859857],
                [400, 11919.061181],
                [500, 6849.295221],
                [600, 3942.886993],
                [700, 2154.001002],
                [800, 1333.262153],
                [900, 797.911535],
                [1000, 521.006001],
                [1250, 181.359758],
                [1500, 74.525091],
                [1750, 31.041069],
                [2000, 13.501388],
                [2250, 6.209130],
                [2500, 3.017827],
                [2750, 1.632098],
                [3000, 0.864494],
                [3250, 0.497804],
                [3500, 0.284564],
                [3750, 0.180698],
                [4000, 0.118802],
                ]
            points_muhad = [
                [200, 54417.528631],
                [300, 27200.087945],
                [400, 12996.382393],
                [500, 6693.410483],
                [600, 3661.788554],
                [700, 2214.101676],
                [800, 1380.496075],
                [900, 845.731859],
                [1000, 559.974723],
                [1250, 213.166892],
                [1500, 82.940188],
                [1750, 35.846936],
                [2000, 16.729649],
                [2250, 8.117362],
                [2500, 3.981540],
                [2750, 2.116214],
                [3000, 1.114788],
                [3250, 0.657402],
                [3500, 0.373840],
                [3750, 0.229912],
                [4000, 0.150997],
            ]
            points = [[m, ae+am] for ((m,ae),(_,am)) in zip(points_ehad,points_muhad)]

        points = [[m, a/lumi/zpxsec.getXsec("%.1f"%(int(m)))] for (m,a) in points]

    else:
        if chan == "lh":
            dbtag = mod.d_lh_btag
            dbveto = mod.d_lh_bveto
        elif chan == "hh":
            dbtag = mod.d_hh_btag
            dbveto = mod.d_hh_bveto
        points = [(m,(dbtag[m][prod][0] + dbveto[m][prod][0])/lumi) for m in sorted(dbtag)]
    return points

def get_acceff_graphs(prod, chan):
    """

    :return:
    """
    g = ROOT.TGraph()
    for (m, a) in get_acceff_points(prod, chan):
        g.SetPoint(g.GetN(),m,a)
    return g


def get_btagfrac_points(prod,chan):
    import make_tables_sig as mod
    lumi = 36100.
    if chan == "lh":
        dbtag = mod.d_lh_btag
        dbveto = mod.d_lh_bveto
    elif chan == "hh":
        dbtag = mod.d_hh_btag
        dbveto = mod.d_hh_bveto
    points = [(m,dbtag[m][prod][0]/(dbtag[m][prod][0] + dbveto[m][prod][0])) for m in sorted(dbtag)]
    return points

def get_btagfrac_graphs(prod, chan):
    """

    :return:
    """
    g = ROOT.TGraph()
    for (m, a) in get_btagfrac_points(prod, chan):
        g.SetPoint(g.GetN(),m,a)
    return g



def plot_acc(prod):
    """

    :return:
    """


    ## Configuration
    ##===========================================================================
    ## configuration (much will be overwritten by variable-specific config)
    x1 = 150
    x2 = 2300
    y1 = 1.e-2
    y2 = 1.0
    logy = True
    xtitle = "#it{m}_{#it{#phi}} [GeV]"
    ytitle = "Acceptance ( #times efficiency )"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.3                # leg xpos right-hand side
    l_y2 = 0.37               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos


    if prod == "Zprime":
        x1 = 100
        x2 = 4100
        prod_text = "#it{Z'} #rightarrow #tau#tau"
        xtitle = "#it{m}_{#it{Z'}} [GeV]"
    elif prod == "bbH":
        prod_text = "#it{#phi} #rightarrow #tau#tau, #it{b}-associated production"
    elif prod == "ggH":
        prod_text = "#it{#phi} #rightarrow #tau#tau, gluon-gluon fusion"


    ## Graph getting and manipulation
    ##===========================================================================
    #g_ggH_lh = get_acc_graphs("ggH", "lh")
    #g_ggH_hh = get_acc_graphs("ggH", "hh")
    #g_bbH_lh = get_acc_graphs("bbH", "lh")
    #g_bbH_hh = get_acc_graphs("bbH", "hh")
    #g_zp_lh = get_acc_graphs("Zprime", "lh")
    #g_zp_hh = get_acc_graphs("Zprime", "hh")

    g_acc_lh = get_acc_graphs(prod, "lh")
    g_acc_hh = get_acc_graphs(prod, "hh")

    g_acceff_lh = get_acceff_graphs(prod, "lh")
    g_acceff_hh = get_acceff_graphs(prod, "hh")

    ## style this shit
    #g_acc_lh.SetMarkerStyle


    g_acceff_lh.SetMarkerColor(ROOT.kBlue)
    g_acceff_lh.SetMarkerStyle(22)
    g_acceff_lh.SetLineColor(ROOT.kBlue)
    g_acceff_lh.SetLineWidth(3)

    g_acc_lh.SetMarkerColor(ROOT.kBlue)
    g_acc_lh.SetMarkerStyle(26)
    g_acc_lh.SetLineColor(ROOT.kBlue)
    g_acc_lh.SetLineWidth(3)
    g_acc_lh.SetLineStyle(2)

    g_acceff_hh.SetMarkerColor(ROOT.kRed)
    g_acceff_hh.SetMarkerStyle(21)
    g_acceff_hh.SetLineColor(ROOT.kRed)
    g_acceff_hh.SetLineWidth(3)

    g_acc_hh.SetMarkerColor(ROOT.kRed)
    g_acc_hh.SetMarkerStyle(25)
    g_acc_hh.SetLineColor(ROOT.kRed)
    g_acc_hh.SetLineWidth(3)
    g_acc_hh.SetLineStyle(2)



    graphs = [g_acc_lh, g_acc_hh, g_acceff_lh, g_acceff_hh]
    # style em
    #for g in graphs: style_hist(g)

    ## build components
    #entries = [[g, g.tlatex, "LP"] for g in graphs]
    entries = [
        [g_acc_lh, "{} (#it{{A}})".format(cfg.lephad), "LP"],
        [g_acc_hh, "{} (#it{{A}})".format(cfg.hadhad), "LP"],
        [g_acceff_lh, "{} (#it{{A}}#scale[0.5]{{ }}#times#scale[0.5]{{ }}#it{{#varepsilon}})".format(cfg.lephad), "LP"],
        [g_acceff_hh, "{} (#it{{A}}#scale[0.5]{{ }}#times#scale[0.5]{{ }}#it{{#varepsilon}})".format(cfg.hadhad), "LP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()


    for g in graphs: g.Draw("LP")


    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    if cfg.status is None: status = "Simulation Internal"
    else: status = "Simulation {}".format(cfg.status)
    status = "Simulation #sqrt{s} = 13 TeV"
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    #lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy - t_h, com_text)
    latex.DrawLatex(atlasx, atlasy-1.*t_h, prod_text)
    #latex.DrawLatex(atlasx, atlasy-3.*t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")


    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    return (rc.c, locals())


def plot_btag_fraction():
    """

    :return:
    """


    ## Configuration
    ##===========================================================================
    ## configuration (much will be overwritten by variable-specific config)
    x1 = 150
    x2 = 2300
    y1 = 1.e-2
    y2 = 1.0
    logy = True
    xtitle = "#it{m}_{#it{#phi}} [GeV]"
    ytitle = "{} fraction".format(cfg.btag)

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.3                # leg xpos right-hand side
    l_y2 = 0.67               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.60               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    ## Graph getting and manipulation
    ##===========================================================================
    #g_ggH_lh = get_acc_graphs("ggH", "lh")
    #g_ggH_hh = get_acc_graphs("ggH", "hh")
    #g_bbH_lh = get_acc_graphs("bbH", "lh")
    #g_bbH_hh = get_acc_graphs("bbH", "hh")
    #g_zp_lh = get_acc_graphs("Zprime", "lh")
    #g_zp_hh = get_acc_graphs("Zprime", "hh")

    g_ggH_lh = get_btagfrac_graphs("ggH", "lh")
    g_ggH_hh = get_btagfrac_graphs("ggH", "hh")
    g_bbH_lh = get_btagfrac_graphs("bbH", "lh")
    g_bbH_hh = get_btagfrac_graphs("bbH", "hh")


    g_bbH_lh.SetMarkerColor(ROOT.kBlue)
    g_bbH_lh.SetMarkerStyle(22)
    g_bbH_lh.SetLineColor(ROOT.kBlue)
    g_bbH_lh.SetLineWidth(3)

    g_ggH_lh.SetMarkerColor(ROOT.kBlue)
    g_ggH_lh.SetMarkerStyle(26)
    g_ggH_lh.SetLineColor(ROOT.kBlue)
    g_ggH_lh.SetLineWidth(3)
    g_ggH_lh.SetLineStyle(2)

    g_bbH_hh.SetMarkerColor(ROOT.kRed)
    g_bbH_hh.SetMarkerStyle(21)
    g_bbH_hh.SetLineColor(ROOT.kRed)
    g_bbH_hh.SetLineWidth(3)

    g_ggH_hh.SetMarkerColor(ROOT.kRed)
    g_ggH_hh.SetMarkerStyle(25)
    g_ggH_hh.SetLineColor(ROOT.kRed)
    g_ggH_hh.SetLineWidth(3)
    g_ggH_hh.SetLineStyle(2)



    graphs = [g_bbH_lh, g_ggH_lh, g_bbH_hh, g_ggH_hh]
    # style em
    #for g in graphs: style_hist(g)

    ## build components
    #entries = [[g, g.tlatex, "LP"] for g in graphs]
    entries = [
        [g_bbH_lh, "{} #it{{b}}-associated production".format(cfg.lephad), "LP"],
        [g_bbH_hh, "{} #it{{b}}-associated production".format(cfg.lephad), "LP"],
        [g_ggH_lh, "{} gluon-gluon fusion".format(cfg.hadhad), "LP"],
        [g_ggH_hh, "{} gluon-gluon fusion".format(cfg.hadhad), "LP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()


    for g in graphs: g.Draw("LP")


    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    #if not cfg.status: status = "Simulation Internal"
    #else: status = "Simulation {}".format(cfg.status)
    status = "Simulation #sqrt{s} = 13 TeV"
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    #lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    #com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy - t_h, com_text)
    #latex.DrawLatex(atlasx, atlasy-2.*t_h, prod_text)
    #latex.DrawLatex(atlasx, atlasy-3.*t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")


    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    return (rc.c, locals())



def plot_btag_fraction_single(prod):
    """

    :return:
    """


    ## Configuration
    ##===========================================================================
    ## configuration (much will be overwritten by variable-specific config)
    x1 = 150
    x2 = 2300
    y1 = 0.0
    y2 = 0.6
    logy = False
    xtitle = "#it{m}_{#it{#phi}} [GeV]"
    ytitle = "{} fraction".format(cfg.btag)

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    #l_y2 = 0.94               # leg ypos top
    l_y2 = 0.30  # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    if prod == "bbH":
        prod_text = "#it{#phi} #rightarrow #tau#tau, #it{b}-associated production"
    elif prod == "ggH":
        prod_text = "#it{#phi} #rightarrow #tau#tau, gluon-gluon fusion"
        y2 = 0.05



    ## Graph getting and manipulation
    ##===========================================================================
    g_lh = get_btagfrac_graphs(prod, "lh")
    g_hh = get_btagfrac_graphs(prod, "hh")

    g_lh.SetMarkerColor(ROOT.kBlue)
    g_lh.SetMarkerStyle(22)
    g_lh.SetLineColor(ROOT.kBlue)
    g_lh.SetLineWidth(3)

    g_hh.SetMarkerColor(ROOT.kRed)
    g_hh.SetMarkerStyle(21)
    g_hh.SetLineColor(ROOT.kRed)
    g_hh.SetLineWidth(3)

    graphs = [g_lh, g_hh]
    # style em
    #for g in graphs: style_hist(g)

    ## build components
    #entries = [[g, g.tlatex, "LP"] for g in graphs]
    entries = [
        [g_lh, cfg.lephad, "LP"],
        [g_hh, cfg.hadhad, "LP"],
    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()


    for g in graphs: g.Draw("LP")


    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    #if not cfg.status: status = "Simulation Internal"
    #else: status = "Simulation {}".format(cfg.status)
    status = "Simulation #sqrt{s} = 13 TeV"
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    #lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    #com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy - t_h, com_text)
    latex.DrawLatex(atlasx, atlasy-1.*t_h, prod_text)
    #latex.DrawLatex(atlasx, atlasy-3.*t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")


    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    return (rc.c, locals())


def print_hepvals_graph(g, title=None, units=None, cme=13000.0, lumi=None, qualifiers=None):
    """

    :param g:
    :return:
    """
    if title is None: title = "Undefined variable"
    if units:
        print "- header: {name: '%s', units: %s}" % (title, units)
    else:
        print "- header: {name: '%s'}" % (title)
    print "  qualifiers:"
    if cme:  print "  - {name: SQRT(S), units: GeV, value: %.1f}" % (cme)
    if lumi: print "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: %.1f}" % (lumi)
    if qualifiers:
        for (k,v) in sorted(qualifiers.items()):
            print "  - {name: %s, value: %s}" % (k,v)
    print "  values:"
    for i in xrange(g.GetN()):
        print "  - {value: %f}" % (g.GetY()[i])


def print_acc_hepdata(prod):
    """

    :return:
    """
    g_acc_lh = get_acc_graphs(prod, "lh")
    g_acc_hh = get_acc_graphs(prod, "hh")
    g_acceff_lh = get_acceff_graphs(prod, "lh")
    g_acceff_hh = get_acceff_graphs(prod, "hh")
    masses = [g_acc_lh.GetX()[i] for i in xrange(g_acc_lh.GetN())]

    if prod in ["ggH", "bbH"]:
        g_bf_lh = get_btagfrac_graphs(prod, "lh")
        g_bf_hh = get_btagfrac_graphs(prod, "hh")

    # independent variables
    print "independent_variables:"
    print "- header: {name: Mass, units: GeV}"
    print "  values:"
    for m in masses:
        print "  - {value: %d}" % (m)

    # dependent variables
    print "dependent_variables:"
    print_hepvals_graph(g_acc_lh, title = "Acceptance", qualifiers={"Channel":"1l1tau_h"})
    print_hepvals_graph(g_acc_hh, title="Acceptance", qualifiers={"Channel": "2tau_h"})
    print_hepvals_graph(g_acceff_lh, title = "Acceptance x efficiency", qualifiers={"Channel":"1l1tau_h"})
    print_hepvals_graph(g_acceff_hh, title="Acceptance x efficiency", qualifiers={"Channel": "2tau_h"})
    if prod in ["ggH", "bbH"]:
        print_hepvals_graph(g_bf_lh, title="b-tag fraction", qualifiers={"Channel": "1l1tau_h"})
        print_hepvals_graph(g_bf_hh, title="b-tag fraction", qualifiers={"Channel": "2tau_h"})


def dump_acc_hepdata(prod):
    """

    :return:
    """
    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)
    fname = "hepdata_acc_{}.yaml".format(prod.lower())

    g_acc_lh = get_acc_graphs(prod, "lh")
    g_acc_hh = get_acc_graphs(prod, "hh")
    g_acceff_lh = get_acceff_graphs(prod, "lh")
    g_acceff_hh = get_acceff_graphs(prod, "hh")
    a_x = [g_acc_lh.GetX()[i] for i in xrange(g_acc_lh.GetN())]
    a_acc_lh = [g_acc_lh.GetY()[i] for i in xrange(g_acc_lh.GetN())]
    a_acc_hh = [g_acc_hh.GetY()[i] for i in xrange(g_acc_hh.GetN())]
    a_acceff_lh = [g_acceff_lh.GetY()[i] for i in xrange(g_acceff_lh.GetN())]
    a_acceff_hh = [g_acceff_hh.GetY()[i] for i in xrange(g_acceff_hh.GetN())]

    if prod in ["ggH", "bbH"]:
        g_bf_lh = get_btagfrac_graphs(prod, "lh")
        g_bf_hh = get_btagfrac_graphs(prod, "hh")
        a_bf_lh = [g_bf_lh.GetY()[i] for i in xrange(g_bf_lh.GetN())]
        a_bf_hh = [g_bf_hh.GetY()[i] for i in xrange(g_bf_hh.GetN())]
        if prod == 'ggH':
            prod_text = "'Higgs boson (gluon-gluon fusion)'"
        elif prod == 'bbH':
            prod_text = "'Higgs boson (b-associated production)'"
    else:
        prod_text = r"'$Z^{\prime}$ (Drell-Yan)'"
    quals += [['Production',prod_text]]


    print "Creating {}...".format(fname)
    f = open(fname, 'w')

    # independent variables
    f.write("independent_variables:\n")
    dump_hepvals_arr(f, a_x, title="Mass", units="GeV")

    # dependent variables
    f.write("dependent_variables:\n")
    dump_hepvals_arr(f, a_acc_lh, title = "Acceptance", qualifiers=quals+[["Channel","1l1tau_h"]])
    dump_hepvals_arr(f, a_acc_hh, title="Acceptance", qualifiers=quals+[["Channel","2tau_h"]])
    dump_hepvals_arr(f, a_acceff_lh, title = "Acceptance x efficiency", qualifiers=quals+[["Channel","1l1tau_h"]])
    dump_hepvals_arr(f, a_acceff_hh, title="Acceptance x efficiency", qualifiers=quals+[["Channel", "2tau_h"]])
    if prod in ["ggH", "bbH"]:
        dump_hepvals_arr(f, a_bf_lh, title="b-tag fraction", qualifiers=quals+[["Channel", "1l1tau_h"]])
        dump_hepvals_arr(f, a_bf_hh, title="b-tag fraction", qualifiers=quals+[["Channel", "2tau_h"]])

    f.close()


def dump_hepdata():
    """

    :return:
    """
    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)
    chan = '1l1tau_h'
    catdict = {'T0':'b-veto', 'T1':'b-tag', 'T5':'b-inclusive'}

    tanb = 10.0
    hmasses = [300, 500, 800]
    zmasses = [1500, 2000, 2500]

    filestr = "inputs/postfit/2017-07-14/total_Region_BMin0_J0_{cat}_isMVA0_L1_Y2015_dist{var}_D{chan}.root"
    grabber = HistGrabber(filestr=filestr, histstr="{samp}", var="MTTOT", chan="LepHad")
    for cat in ["T0", "T1", "T5"]:
        if cat in ["T0", "T5"]:
            actualbins = [60, 70, 80, 90, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 400,
                          500, 600, 700, 800, 1000]
        elif cat == "T1":
            actualbins = [60, 100, 150, 200, 250, 300, 350, 400, 500, 630]
        elif cat == "T2":
            actualbins = [110, 150, 200, 250, 300, 350, 400, 450, 500, 630]

        h_data  = grabber.hist(name="data", samp="Data", cat=cat)
        h_mc = grabber.hist(name="tot", samp="Total_prediction", cat=cat)
        h_mc_up = grabber.hist(name="tot_up", samp="Total_prediction_up", cat=cat)
        h_mc_dn = grabber.hist(name="tot_dn", samp="Total_prediction_do", cat=cat)
        # set bin error (since it is actually symmetric)
        for i in xrange(0,h_mc.GetNbinsX()+2):
            h_mc.SetBinError(i, abs(h_mc_up.GetBinContent(i) - h_mc.GetBinContent(i)))

        ## get signals
        (g_xsec_gg, g_xsec_bb) = get_mssm_xsecs()
        sigs = []
        sigmasses = zmasses if cat=='T5' else hmasses
        for mass in sigmasses:
            if cat == "T5":
                sname = "SSMZprime{}".format(mass)
                s = grabber.hist(name=sname, samp=sname, cat=cat)
                s.SetName("\'$Z^{\prime} (%d)$\'"%mass)
                sigs.append(s)
            else:
                sname_gg = "ggH{}".format(mass)
                ggH = grabber.hist(name=sname_gg, samp=sname_gg, cat=cat)
                xsec_gg = g_xsec_gg.Interpolate(mass, tanb)
                ggH.Scale(xsec_gg)

                sname_bb = "bbH{}".format(mass)
                bbH = grabber.hist(name=sname_bb, samp=sname_bb, cat=cat)
                xsec_bb = g_xsec_bb.Interpolate(mass, tanb)
                bbH.Scale(xsec_bb)

                s = ggH.Clone("H{}".format(mass))
                s.Add(bbH)
                s.SetName("\'$A/H (%d)$\'" % mass)

                sigs.append(s)
        for s in sigs: kill_below(s, 0.0)


        h_data = set_bin_edges(h_data, actualbins, wcorr=False)
        h_mc = set_bin_edges(h_mc, actualbins, wcorr=False)
        #h_mc_up = set_bin_edges(h_mc_up, actualbins, wcorr=False)
        #h_mc_dn = set_bin_edges(h_mc_dn, actualbins, wcorr=False)

        #dump_hepdist(h_data, h_mc, h_mc_up, h_mc_dn, fname="hepdata_lephad_{}.yaml".format(cat))
        dump_hepdist(h_data, h_mc, sigs=sigs, fname="hepdata_lephad_{}.yaml".format(cat),
                     qualifiers=quals + [['Channel',chan],['Category',catdict[cat]]])







