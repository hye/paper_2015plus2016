# encoding: utf-8
'''
  file:    hadhad.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
import re

import ROOT

import cfg
from plotter.histutils import HistGrabber, log_bins, apply_blind, build_stack, \
    get_poissonized_graph, get_poissonized_ratio_graph, get_mc_ratio_band, get_significance_ratio, get_significance_ratio_diego, \
    build_total_stat_plus_sys, build_error_hist, set_bin_edges, set_sig_bin_width_corr, make_band, kill_below, rebin_hist, dump_hepdist
from plotter.plotutils import RatioCanvas, build_leg, draw_standard_text, draw_blind, \
    get_ytitle_events, draw_channel_text, make_legend, build_leg_bkg, build_leg_sig
from plotter.samples import style_hist
from lephad import get_mssm_xsecs


"""
systematics = [# Tau
               'TAUS_TRUEHADTAU_EFF_JETID_TOTAL_',
               'TAUS_TRUEHADTAU_SME_TES_DETECTOR_',
               'TAUS_TRUEHADTAU_SME_TES_MODEL_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016_',
               'TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_',
               'TAUS_TRUEHADTAU_SME_TES_INSITU_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015_',
               'TAUS_TRUEHADTAU_EFF_RECO_TOTAL_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015_',
               'TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_',
               'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015_',
               # Fakes
               'HHFAKERATE_',
               'QCDFF_',
               # Flavour Tagging
               'FT_EFF_extrapolation_from_charm_',
               'FT_EFF_Eigen_C_2_',
               'FT_EFF_extrapolation_',
               'FT_EFF_Eigen_C_0_',
               'FT_EFF_Eigen_B_1_',
               'FT_EFF_Eigen_Light_0_',
               'FT_EFF_Eigen_Light_2_',
               'FT_EFF_Eigen_C_1_',
               'FT_EFF_Eigen_Light_1_',
               'FT_EFF_Eigen_B_0_',
               'FT_EFF_Eigen_Light_3_',
               # Jets
               'JET_JvtEfficiency_',
               'JET_JER_SINGLE_NP_',
               'JET_TILECORR_Uncertainty_',
               ]
"""

"""
sys_asym = [
    "ATLAS_EGRESO_",
    "ATLAS_EGSCALE_ALLCORR_",
    "ATLAS_JETTileCorrUnc_",
    "ATLAS_JvtEffSf_",
    "ATLAS_LPXKFACTORALPHAS_",
    "ATLAS_LPXKFACTORBEAMENERGY_",
    "ATLAS_LPXKFACTORPDFEW_",
    "ATLAS_LPXKFACTORPDF_",
    "ATLAS_LPXKFACTORPI_",
    #"ATLAS_LPXKFACTORSCALEW_",
    "ATLAS_LPXKFACTORSCALEZ_",
    "ATLAS_METSoftTrkScale_",
    "ATLAS_MUONID_",
    "ATLAS_MUONMS_",
    "ATLAS_MUONSAGITTARESBIAS_",
    "ATLAS_MUONSAGITTARHO_",
    "ATLAS_MUONSCALE_",
    "ATLAS_PRW_",
    "ATLAS_TAUIDHighPt_",
    "ATLAS_TAUIDTotal_",
    "ATLAS_TAURECOHighPt_",
    "ATLAS_TAURECOTotal_",
    "ATLAS_TAUTRIGEFFSTATDATA2015_",
    "ATLAS_TAUTRIGEFFSTATDATA2016_",
    "ATLAS_TAUTRIGEFFSTATMC2015_",
    "ATLAS_TAUTRIGEFFSTATMC2016_",
    "ATLAS_TAUTRIGEFFSYST2015_",
    "ATLAS_TAUTRIGEFFSYST2016_",
    "ATLAS_TESDETECTOR_",
    "ATLAS_TESINSITU_",
    "ATLAS_TESMODEL_",
    "ATLAS_btagEffSfEigen_B_0_",
    "ATLAS_btagEffSfEigen_B_1_",
    "ATLAS_btagEffSfEigen_C_0_",
    "ATLAS_btagEffSfEigen_C_1_",
    "ATLAS_btagEffSfEigen_C_2_",
    "ATLAS_btagEffSfEigen_Light_0_",
    "ATLAS_btagEffSfEigen_Light_1_",
    "ATLAS_btagEffSfEigen_Light_2_",
    "ATLAS_btagEffSfEigen_Light_3_",
    "ATLAS_btagEffSfextrapolation_",
    "ATLAS_btagEffSfextrapolation_charm_",
    "HHFAKERATE_",
    "QCDFF_",
    ]

sys_sym = [
    "ATLAS_METSoftTrkResoPara__1up",
    "ATLAS_METSoftTrkResoPerp__1up",
    "ATLAS_JERNP1__1up",
    "ATLAS_JETEtaNonClosure__1up",
    "ATLAS_JETNP1__1up",
    "ATLAS_JETNP2__1up",
    "ATLAS_JETNP3__1up",
    #"ATLAS_LPXKFACTORCHOICEHERAPDF20__1",
    #"ATLAS_LPXKFACTORPDFEV1__1",
    #"ATLAS_LPXKFACTORPDFEV2__1",
    #"ATLAS_LPXKFACTORPDFEV3__1",
    #"ATLAS_LPXKFACTORPDFEV4__1",
    #"ATLAS_LPXKFACTORPDFEV5__1",
    #"ATLAS_LPXKFACTORPDFEV6__1",
    #"ATLAS_LPXKFACTORPDFEV7__1",
    #"ATLAS_LPXKFACTORREDCHOICENNPDF30__1",
    #"LPX_KFACTOR_CHOICE_NNPDF30__1",
    ]
"""



def get_active_systematics(cfgfile):
    with open(cfgfile) as f: 
        sysset = set([l.strip('\n') for l in f])
    sys_asym = [] 
    sys_sym  = [] 
    while sysset:
        sys = sysset.pop()
        if sys.endswith("_1up"): 
            stub = sys[:-4]
            if stub+"_1down" in sysset: 
                sys_asym.append(stub)
                sysset.remove(stub+"_1down")
                continue
        elif sys.endswith("_1down"): 
            stub = sys[:-6]
            if stub+"_1up" in sysset: 
                sys_asym.append(stub)
                sysset.remove(stub+"_1up")
                continue
        sys_sym.append(sys)
    return (sys_asym, sys_sym)


# ________________________________________________________________________________________________
def plot_stack(cat=None, var=None):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## defaults
    if var is None: var = "MTTOT"
    if cat is None: cat = "0tag0jet_0ptv"

    ## configuration (much will be overwritten by variable-specific config)
    x1 = x2 = y1 = y2 = None  # canvas boundaries
    logx = logy = False       # use logarithmic axis scales
    rebin = None              # fixed-width rebin factor
    bins = None               # new binning
    wcorr = None              # apply bin width correction
    checkbins = True          # check for bin misalignment on rebin
    xtitle = None             #
    xunit = None              #
    ndivx = None              # x-axis tick divisions
    blind = None              # position of blind
    ratio_opt = "sig"         # options: ratio, sig
    sig_scale = 1. / 4.       # signal scale factor
    use_sys = True            # include systematics in uncertainty

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.05                # standard text height
    l_x2 = 0.93               # leg xpos
    leg_left = True           # place legend on LHS
    atlasx = None             # atlas x pos

    # samples
    #filestr = "inputs/hadhad/limit_inputs_feb23.root"
    #filestr = "inputs/hadhad/LimitHistograms.htthh.13TeV.CUT.Dresden.MTTOT.V013.root"
    filestr = "inputs/hadhad/LimitHistograms.htthh.13TeV.CUT.Dresden.MTTOT.V017_ICHEP_LUMI.root"
    sysfile = "inputs/hadhad/whitelist.V017.txt"
    histstr = "{samp}_{cat}_HadHad_{var}"
    histstr_up = "{sys}_1up/{samp}_{cat}_HadHad_{var}_{sys}_1up"
    histstr_dn = "{sys}_1down/{samp}_{cat}_HadHad_{var}_{sys}_1down"
    histstr_sym = "{sys}/{samp}_{cat}_HadHad_{var}_{sys}"
    bkgdefs = [
        ("multijet", "Multijet"),
        ("ztautau",  "Ztautau"),
        ("wtaunu",   "Wtaunu"),
        ("top",      "Top"),
        ("others",   "Others"),
        ]
    bkgnames = [s[1] for s in bkgdefs]
    sigdefs = ("H600", ["bbH600", "ggH600"])

    # cat config
    if cat == "0tag0jet_0ptv":
        catname = cfg.bveto
    else: 
        catname = cfg.btag


    ## plot-dependent configuration
    if var in [None, "MTTOT"]:
        bins = log_bins(22, 150., 1000.)
        checkbins = False
        wcorr = True
        logx = True
        logy = True
        xtitle = "{0}({1}, {1}, {2}) [GeV]".format(cfg.mTtot, cfg.tauhadvis, cfg.met)
        blind = 255
        #if cat == "1tag0jet_0ptv":
        #    logy = False


    ## auto-configuration
    #if logy: y1 = 0.01
    # ratio y-axis range
    if ratio_opt == "sig":
        ry1 = -7.0
        ry2 = +7.0
    elif ratio_opt == "ratio":
        ry1 = 0.41
        ry2 = 1.59
    if xunit == None:
        s = re.search(".*\[(.*)\]", xtitle)
        if s: xunit = s.group(1)

    ## Get active systematics 
    (sys_asym, sys_sym) = get_active_systematics(sysfile)
    #print "ASYM ERRORS: "
    #for l in sys_asym: 
    #    print "   ", l
    #print "SYM ERRORS: "
    #for l in sys_sym: 
    #    print "   ", l

    ## Histogram getting and manipulation
    ##===========================================================================
    # read in nominal histograms
    print "Getting nominal histograms..."
    grabber = HistGrabber(filestr=filestr, histstr=histstr, cat=cat, var=var, bins=bins, rebin=rebin, checkbins=checkbins)
    #bkgs   = [grabber.hist(samp=s, name=s.lower(), wcorr=wcorr) for s in bkgnames]
    #sigs   = [grabber.histsum(name="H%d"%m, samps=["bbH%d"%m, "ggH%d"%m], wcorr=wcorr) for m in sigmasses]
    #h_data = grabber.hist(samp="Data", name="data", wcorr=wcorr)
    bkgs   = [grabber.hist(name=n, wcorr=wcorr, samp=s) for (n,s) in bkgdefs]
    sigs   = [grabber.hist(name=sigdefs[0], wcorr=wcorr, samp=sigdefs[1])]
    h_data = grabber.hist(name="data", wcorr=wcorr, samp="Data")
    # post-process
    if sig_scale is not None:
        for s in sigs: s.Scale(sig_scale)
    if not cfg.unblinded: apply_blind(h_data, blind)
    g_data = get_poissonized_graph(h_data)
    for h in [g_data] + bkgs + sigs: style_hist(h)
    h_stack = build_stack(bkgs)

    # get versions without bin-width correction (for ratio band calculations)
    if wcorr:
        print "Getting nominal histograms without bin-width correction..."
        #sigs_uncorr = [grabber.histsum(name="", samps=["bbH%d"%m, "ggH%d"%m], wcorr=False) for m in sigmasses]
        #h_data_uncorr = grabber.hist(samp="Data", name="", wcorr=False)
        sigs_uncorr = [grabber.hist(name=sigdefs[0], wcorr=False, samp=sigdefs[1])]
        h_data_uncorr = grabber.hist(name="data", wcorr=False, samp="Data")
        # post-process
        if sig_scale is not None:
            for s in sigs_uncorr: s.Scale(sig_scale)
        if not cfg.unblinded: apply_blind(h_data_uncorr, blind)
    else:
        #bkgs_uncorr = bkgs
        sigs_uncorr = sigs
        h_data_uncorr = h_data



    ## get bkg total + error
    print "Getting background sum and errors..."
    # stat only
    h_bkg_tot_stat = grabber.hist(name="error_stat", wcorr=wcorr, samp=bkgnames)
    if wcorr:
        h_bkg_tot_stat_uncorr = grabber.hist(name="error_stat_uncorr", wcorr=False, samp=bkgnames)
    else:
        h_bkg_tot_stat_uncorr = h_bkg_tot_stat
    # include systematics
    grabber_up = HistGrabber(filestr=filestr, histstr=histstr_up, cat=cat, var=var,
                             bins=bins, rebin=rebin, scale=1.0, checkbins=checkbins, fallback=grabber)
    grabber_dn = HistGrabber(filestr=filestr, histstr=histstr_dn, cat=cat, var=var,
                             bins=bins, rebin=rebin, scale=1.0, checkbins=checkbins, fallback=grabber)
    grabber_sym = HistGrabber(filestr=filestr, histstr=histstr_sym, cat=cat, var=var,
                             bins=bins, rebin=rebin, scale=1.0, checkbins=checkbins, fallback=grabber)
    if use_sys:
        h_bkg_tot = build_total_stat_plus_sys( \
            name="error", grabber=grabber, grabber_up=grabber_up, grabber_sym=grabber_sym,
            grabber_dn=grabber_dn, sys_asym=sys_asym, sys_sym=sys_sym,
            wcorr=wcorr, samp=bkgnames)
        if wcorr:
            h_bkg_tot_uncorr = build_total_stat_plus_sys( \
                name="error_uncorr", grabber=grabber, grabber_up=grabber_up, grabber_sym=grabber_sym,
                grabber_dn=grabber_dn, sys_asym=sys_asym, sys_sym=sys_sym,
                wcorr=False, samp=bkgnames)
        else:
            h_bkg_tot_uncorr = h_bkg_tot
    # stat uncertainty only
    else:
        h_bkg_tot = h_bkg_tot_stat
        h_bkg_tot_uncorr = h_bkg_tot_stat_uncorr
        h_bkg_tot.SetName("error")
        h_bkg_tot_uncorr.SetName("error_uncorr")
    style_hist(h_bkg_tot)
    if use_sys: style_hist(h_bkg_tot_stat)

    ## determine axis ranges, titles
    if x1 == None: x1 = h_data.GetBinLowEdge(1)
    if x2 == None: x2 = h_data.GetBinLowEdge(h_data.GetNbinsX() + 1)
    if y1 == None:
        #if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum(), h_data.GetMinimum()])
        if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum()])
        else:      y1 = 0.0
    if y2 == None:
        if logy:   y2 = 40. * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
        else:      y2 = 1.4 * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
    if bins: bin_width = None
    else:    bin_width = h_data.GetBinWidth(2)
    ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=wcorr)

    ## additional inputs for significance band
    ratios = []
    if ratio_opt == "ratio":
        # data ratio
        h_ratio_data = get_poissonized_ratio_graph(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_data.drawopt = "P"
        style_hist(h_ratio_data, "data")
        # mc ratio
        h_ratio_mc = get_mc_ratio_band(h_bkg_tot_uncorr)
        h_ratio_mc.drawopt = "E2"
        style_hist(h_ratio_mc, "ratio")
        h_ratio_mc_stat = get_mc_ratio_band(h_bkg_tot_stat_uncorr)
        h_ratio_mc_stat.drawopt = "E2"
        style_hist(h_ratio_mc_stat, "ratio_stat")
        # line
        #line = ROOT.TLine(x1, 1., x2, 1.)
        #style_hist(line, "ratio_line")
        #line.drawopt = ""
        if use_sys: ratios = [h_ratio_mc, h_ratio_mc_stat, h_ratio_data]
        else:       ratios = [h_ratio_mc, h_ratio_data]

    elif ratio_opt == "sig":
        h_ratio_obs = get_significance_ratio(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_obs.drawopt = "HIST"
        style_hist(h_ratio_obs, "significance")
        h_ratio_sigs = []
        for (i, s) in enumerate(sigs_uncorr):
            h = get_significance_ratio(s, h_bkg_tot_uncorr, ismc=True)
            style_hist(h, sigs[i].GetName())
            h.drawopt = "HIST"
            h_ratio_sigs.append(h)
        ratios = [h_ratio_obs] + h_ratio_sigs
    else:
        print "No ratio configured!"

    ## build components
    leg = build_leg(h_data, bkgs, sigs, h_bkg_tot)
    #if use_sys: leg.AddEntry(h_bkg_tot_stat, h_bkg_tot_stat.tlatex, 'F')



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = RatioCanvas('rc', cfg.c_w, cfg.c_h, cfg.c_ratio_split)
    #h_total.GetXaxis().SetRangeUser(x1, x2)
    rc.draw_frame1(x1, y1, x2, y2, ';;%s' % (ytitle))
    if ratio_opt == "ratio":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Obs. / exp.' % (xtitle))
    elif ratio_opt == "sig":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Significance' % (xtitle))
    rc.scale_text(cfg.t_size)
    if ndivx is not None:
        rc.fr1.GetXaxis().SetNdivisions(ndivx)
        rc.fr2.GetXaxis().SetNdivisions(ndivx)

    # draw pad1
    rc.pad1.cd()
    rc.pad1.SetLogy(logy)
    rc.pad1.SetLogx(logx)
    rc.fr2.GetXaxis().SetMoreLogLabels()
    rc.fr2.GetXaxis().SetNoExponent()
    h_stack.Draw("SAME,HIST")
    g_data.Draw("SAME,PZ")
    h_bkg_tot.Draw("SAME,E2")
    #if use_sys: h_bkg_tot_stat.Draw("SAME,E2")
    for s in sigs:
        s.Draw("SAME,HIST")
    draw_standard_text(atlasx=atlasx, lumi=cfg.lumi, status = cfg.status)
    draw_channel_text(channel="{} {}".format(cfg.hadhad, catname))
    leg.Draw()
    if not cfg.unblinded:
        line = draw_blind(blind, x1, x2, y1, y2, logy=logy)
    ROOT.gPad.RedrawAxis()
    rc.pad1.Update()

    # draw pad 2
    rc.pad2.cd()
    rc.pad2.SetLogx(logx)
    for r in ratios:
        r.Draw("SAME,{}".format(r.drawopt))

    ROOT.gPad.RedrawAxis()
    rc.pad2.Update()


    return (rc.c, locals())


# ________________________________________________________________________________________________
def plot_stack_postfit(cat=None, var=None, ratio_opt="sig"):
    """
      make stack plots for had-had (postfit)
    """
    ## Configuration
    ##===========================================================================

    ## defaults
    if var is None: var = "MTTOT"
    if cat is None: cat = "T0"
    chan = "HadHad"

    ## configuration (much will be overwritten by variable-specific config)
    x1 = x2 = y1 = y2 = None  # canvas boundaries
    logx = logy = False       # use logarithmic axis scales
    #rebin = None              # fixed-width rebin factor
    #bins = None               # new binning
    #wcorr = None              # apply bin width correction
    #checkbins = True          # check for bin misalignment on rebin
    actualbins = None         # actual bin edges (since postfit plots have fake binning)
    actualbins_sig = None     # HACK to manually merge broken signal bins in lei's inputs
    xtitle = None             #
    xunit = None              #
    ndivx = None              # x-axis tick divisions
    blind = None              # position of blind
    #ratio_opt = "sig"         # options: ratio, sig
    #ratio_opt = "ratio"  # options: ratio, sig
    #scale = None              # overall scale for events
    sig_scale = None          # signal scale factor
    use_sys = True            # include systematics in uncertainty

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.05                # standard text height
    l_x2 = None               # leg xpos
    l_y2 = None               # leg xpos
    atlasx = None             # atlas x pos
    ticks = []

    # samples
    #filestr = "inputs/hadhad/postfitInputs/PostFitPlots_adhoc_hadhad/total_Region_BMin0_J0_{cat}_isMVA0_L0_Y2015_dist{var}_D{chan}.root"
    #filestr = "inputs/hadhad/postfitInputs/lephadrebin/total_Region_BMin0_J0_{cat}_isMVA0_L0_Y2015_dist{var}_D{chan}.root"
    #filestr = "inputs/postfit/tcr_tt_new/total_0ptv_{cat}_{chan}_{var}.root"
    #filestr = "inputs/postfit/tcr_tt_nomcstat/total_0ptv_{cat}_{chan}_{var}.root"
    #filestr = "inputs/postfit/tcr_tt_500/total_0ptv_{cat}_{chan}_{var}.root"
    #filestr = "inputs/postfit/aux_var/total_0ptv_{cat}_{chan}_{var}.root"
    #filestr = "inputs/postfit/aux_var_check/total_0ptv_{cat}_{chan}_{var}.root"
    #filestr = "inputs/postfit/prefit/total_0ptv_{cat}_{chan}_{var}.root"
    #filestr = "inputs/postfit/topdecor_1000/total_0ptv_{cat}_{chan}_{var}.root"
    filestr = "inputs/postfit/200416check/total_0ptv_{cat}_{chan}_{var}.root"
    if cat == "T5":
        filestr = "inputs/postfit/2017-08-14/total_Region_BMin0_J0_{cat}_isMVA0_L0_Y2015_dist{var}_D{chan}.root"


    histstr = "{samp}"
    if cat == "T0":
      bkgdefs = [
                 ("multijet", "Multijet"),
                 ("ztautau",  "Ztautau"),
                 ("wtaunu",   "Wtaunu"),
                 ("top",      "Top"),
                 ("others",   "Others"),
                ]
    elif cat == "T1":
      bkgdefs = [
                 ("top",      "Top"),
                 ("multijet", "Multijet"),
                 ("ztautau",  "Ztautau"),
                 ("wtaunu",   "Wtaunu"),
                 ("others",   "Others"),
                ]

    bkgnames = [s[1] for s in bkgdefs]
    sigmasses = [400, 1000, 1500]
    #sigmasses = [500]
    #sigdefs = ("H500", ["bbH500", "ggH500"])
    #tanb = 15.0
    tanb = 6.0

    # cat config
    if cat == "T0": catname = cfg.bveto
    elif cat == "T1": catname = cfg.btag
    elif cat == "T5":
        catname = cfg.bincl
        sigmasses = [1500, 2000, 2500]
    else:
        print "Invalid category ", cat
        exit(1)

    # chan config
    channame = cfg.hadhad

    # standard log-binning

    ## plot-dependent configuration
    if var in [None, "MTTOT"]:
        logy = False
        logx = True
        y1 = 2.e-2
      
        #xtitle = "{0}({1}, {2}, {3}) [GeV]".format(cfg.mTtot, cfg.tauhadvis, cfg.tauhadvis, cfg.met)
        xtitle = "{0} [GeV]".format(cfg.mTtot)
        if cat in ["T0", "T5"]:
            #actualbins = [150, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1500]
            #actualbins = [150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1150, 1500]
            actualbins = [150, 200, 250, 300, 350, 400, 450, 500, 1500]
            ticks = [150,1500]
            #y1 = 2.e-4
            #y2 = 4.e4
            #y2 = 4.e6 #withoutBinWidthCorr
            y1 = 0.
            y2 = 20.
	    x1 = 400.
            x2 = 1499.99
        elif cat == "T1":
            #actualbins = [150, 200, 250, 300, 350, 400, 450, 600, 700, 800, 1500]
            #actualbins = [150, 200, 250, 300, 350, 400, 450, 500, 550, 650, 1500]
            #actualbins = [150, 200, 250, 300, 350, 400, 450, 500, 550, 650, 1000]
            actualbins = [150, 200, 250, 300, 350, 400, 450, 500, 1000]
            #ticks = [150,1500]
            ticks = [150,1000]
            #y1 = 7.e-5
            #y2 = 3.e4
            #y2 = 1.e6 #withoutBinWidthCorr
            y1 = 0.
            y2 = 3.
            x1 = 400.
            x2 = 999.99
    elif var == "LEADTAUPT":
        xtitle = "Leading tau #it{p}_{T} [GeV]"
        logx=True
        logy=True
        if cat == "T0":
            y1 = 7.e-4
            y2 = 5.e5
	    x1 = 60.
            x2 = 999.
            #actualbins = [30,60,90,120,150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 1000]
            actualbins = [30,60,80,120,160, 200, 250, 300, 350, 400, 450, 500, 600, 800, 1000]
            ticks = [30,1000]
        elif cat == "T1":
            y1 = 7.e-5
            y2 = 5.e4
	    x1 = 60.
            x2 = 999.
            actualbins = [30,60,90,120,150, 200, 250, 300, 350, 400, 450, 500, 1000]
            ticks = [30,1000]
    elif var == "SUBLEADTAUPT":
        xtitle = "Sub-leading tau #it{p}_{T} [GeV]"
        logx=True
        logy=True
        if cat == "T0":
            y1 = 7.e-4
            y2 = 5.e4
            x1 = 60.
            x2 = 999.
            actualbins = [30,60,90,120,150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 1000]
            ticks = [30,1000]
        elif cat == "T1":
            y1 = 7.e-3
            y2 = 5.e2
	    x1 = 60.
            x2 = 999.
            actualbins = [30,60,90,120,150, 200, 250, 300, 400, 500, 1000]
            ticks = [30,1000]
    elif var == "MET":
        xtitle = "{} [GeV]".format(cfg.met)
        logx=True
        logy=True
        if cat == "T0":
            y1 = 7.e-2
            y2 = 5.e4
            x2 = 999.
            actualbins = [0,10,20,30,60,90,120,150,200, 250, 300, 350, 400, 500, 800, 1000]
            ticks = [1000]
        elif cat == "T1":
            y1 = 7.e-3
            y2 = 5.e2
            x2 = 999.
            actualbins = [0,10,20,30,60,90,120,150,200, 250, 300, 400, 500, 800, 1000]
            ticks = [1000]
    else:
        logy = True
        xtitle = var
    if actualbins_sig is None: actualbins_sig = actualbins

    ## auto-configuration
    #if logy and not y1: y1 = 0.01
    # ratio y-axis range
    if ratio_opt == "sig":
        #ry1 = -7.0
        #ry2 = +7.0
        ry1 = -3.5
        ry2 = +3.5
    elif ratio_opt == "ratio":
        #ry1 = 0.41
        #ry2 = 1.59
        if cat == "T1" and (var == "LEADTAUPT" or var == "SUBLEADTAUPT" or var == "MET"):
            ry1 = 0.11
            ry2 = 1.89
        elif cat == "T1":
            ry1 = 0.49
            ry2 = 1.51
        elif cat == "T0" and (var == "LEADTAUPT" or var == "SUBLEADTAUPT" or var == "MET"):
            ry1 = 0.11
            ry2 = 1.89
        else:
            ry1 = 0.69
            ry2 = 1.31
            if 'prefit' in filestr:
                ry1 = 0.49
                ry2 = 1.51
    if xunit == None:
        s = re.search(".*\[(.*)\]", xtitle)
        if s: xunit = s.group(1)


    ## Histogram getting and manipulation
    ##===========================================================================
    # read in nominal histograms
    print "Getting nominal histograms..."
    grabber = HistGrabber(filestr=filestr, histstr=histstr, cat=cat, var=var, chan=chan)
    bkgs    = [grabber.hist(name=n, samp=s) for (n,s) in bkgdefs]
    h_data  = grabber.hist(name="data", samp="Data")
    ## get bkg total + error
    print "Getting background sum and errors..."
    h_bkg_tot_nom = grabber.hist(name="tot", samp="Total_prediction")
    h_bkg_tot_up = grabber.hist(name="tot_up", samp="Total_prediction_up")
    h_bkg_tot_dn = grabber.hist(name="tot_dn", samp="Total_prediction_do")
    h_bkg_tot = build_error_hist(h_bkg_tot_nom, h_bkg_tot_up, h_bkg_tot_dn, "error")

    print "data bin 1: ", h_data.GetBinContent(1)

    ## get signals
    (g_xsec_gg, g_xsec_bb) = get_mssm_xsecs()
    sigs = []
    sigs_forsig = []
    for mass in sigmasses:
        if mass == 1500:
          tanb = 25
        elif mass == 1000:
          tanb = 12
        if cat == "T5":
            sname = "SSMZprime{}".format(mass)
            s =  grabber.hist(name=sname, samp=sname)
            sigs.append(s)
        else:
            tags = ''
            if cat == 'T0':
              tags = '0tag'
            elif cat == 'T1' or cat == 'T2':
              tags = '1tag'
            sname_gg = "ggHhh{:s}{}".format(tags,mass)
            ggH =  grabber.hist(name=sname_gg, samp=sname_gg)
            ggH_forsig =  grabber.hist(name=sname_gg, samp=sname_gg)
            xsec_gg = g_xsec_gg.Interpolate(mass, tanb)
            if mass < 1000:
              ggH.Scale(xsec_gg)
            else:
              ggH.Scale(xsec_gg*100)
            #ggH.Scale(xsec_gg)

            sname_bb = "bbHhh{:s}{}".format(tags,mass)
            bbH =  grabber.hist(name=sname_bb, samp=sname_bb)
            bbH_forsig =  grabber.hist(name=sname_bb, samp=sname_bb)
            xsec_bb = g_xsec_bb.Interpolate(mass, tanb)
            if mass < 1000:
              bbH.Scale(xsec_bb)
            else:
              bbH.Scale(xsec_bb*100)
            #bbH.Scale(xsec_bb)

            ggH_forsig.Scale(xsec_gg)
            bbH_forsig.Scale(xsec_bb)

            s = ggH.Clone("H{}".format(mass))
            #s = bbH.Clone("H{}".format(mass))
            s.Add(bbH)
            sigs.append(s)

            ss = ggH_forsig.Clone("H{}".format(mass))
            #ss = bbH_forsig.Clone("H{}".format(mass))
            ss.Add(bbH_forsig)
            sigs_forsig.append(ss)

            print "xsec (gg): {}".format(xsec_gg)
            print "xsec (bb): {}".format(xsec_bb)
    for s in sigs: kill_below(s, 0.0)
    for s in sigs_forsig: kill_below(s, 0.0)

    # reimplement actual bin edges
    if actualbins:
        wcorr = True
        #wcorr = False
        bkgs = [set_bin_edges(h, actualbins, wcorr=wcorr) for h in bkgs]
        sigs_uncorr      = [set_bin_edges(h, actualbins, wcorr=False, name=h.GetName()+"_uncorr") for h in sigs]
        sigs             = [set_bin_edges(h, actualbins, wcorr=wcorr) for h in sigs]
        sigs_forsig_uncorr      = [set_bin_edges(h, actualbins, wcorr=False, name=h.GetName()+"_uncorr") for h in sigs_forsig]
        sigs_forsig             = [set_bin_edges(h, actualbins, wcorr=wcorr) for h in sigs_forsig]

        ## DODGY AS FUCK HACK!
        #if actualbins != actualbins_sig:
        #    #print ""
        #    #print "--------------> Original Signal Contents <--------------------"
        #    #for s in sigs_uncorr:
        #    #    print "SIGNAL UNCORR: ", s.GetName()
        #    #    for i in range(1, s.GetNbinsX() + 1):
        #    #        print "[{}, {}]: {}".format(s.GetBinLowEdge(i), s.GetBinLowEdge(i + 1), s.GetBinContent(i))

        #    sigs_uncorr = [rebin_hist(h, bins=actualbins, name=h.GetName()) for h in sigs_uncorr]
        #    sigs = [rebin_hist(h, bins=actualbins, wcorr=True, name=h.GetName().replace("_uncorr","")) for h in sigs_uncorr]

        #    #print ""
        #    #print "--------------> Merged Signal Contents <--------------------"
        #    #for s in sigs:
        #    #    print "SIGNAL: ", s.GetName()
        #    #    for i in range(1, s.GetNbinsX()+1):
        #    #        print "[{}, {}]: {}".format(s.GetBinLowEdge(i), s.GetBinLowEdge(i+1), s.GetBinContent(i))

        h_data_uncorr    = set_bin_edges(h_data, actualbins, wcorr=False, name="data_uncorr")
        h_data           = set_bin_edges(h_data, actualbins, wcorr=wcorr)
        h_bkg_tot_uncorr = set_bin_edges(h_bkg_tot, actualbins, wcorr=False, name="error_uncorr")
        h_bkg_tot        = set_bin_edges(h_bkg_tot, actualbins, wcorr=wcorr)
    else:
        sigs_uncorr = sigs
        h_data_uncorr = h_data
        h_bkg_tot_uncorr = h_bkg_tot

    """
    if cat == "T1" and not var in [None, "MTTOT"]:
        ## Hack the bins
        def hack_bins(h):
            old_bins = [h.GetBinLowEdge(i+1) for i in range(h.GetNbinsX()+1)]
            new_bins = list((old_bins[:-2]+old_bins[-1:])[::2])
            return rebin_hist(h, bins=new_bins, name=h.GetName())

        bkgs = [hack_bins(h) for h in bkgs]
        sigs_uncorr = [hack_bins(h) for h in sigs_uncorr]
        sigs = [hack_bins(h) for h in sigs]
        h_data_uncorr = hack_bins(h_data_uncorr)
        h_data = hack_bins(h_data)
        h_bkg_tot_uncorr = hack_bins(h_bkg_tot_uncorr)
        h_bkg_tot = hack_bins(h_bkg_tot)
    """


    # post-process
    #if sig_scale is not None:
    #    for s in sigs: s.Scale(sig_scale)
    if not cfg.unblinded: apply_blind(h_data, blind)
    g_data = get_poissonized_graph(h_data)
    for h in [g_data] + bkgs + sigs + [h_bkg_tot]: style_hist(h)
    h_stack = build_stack(bkgs)

    ## determine axis ranges, titles
    if x1 == None: x1 = h_data.GetBinLowEdge(1)
    if x2 == None: x2 = h_data.GetBinLowEdge(h_data.GetNbinsX() + 1)
    if y1 == None:
        #if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum(), h_data.GetMinimum()])
        if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum()])
        else:      y1 = 0.0
    if y2 == None:
        if logy:   y2 = 1000. * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
        else:      y2 = 1.4 * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
    if actualbins: bin_width = None
    else:    bin_width = h_data.GetBinWidth(2)
    ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=bool(actualbins))
    #ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit, wcorr=False)

    ## additional inputs for significance band
    ratios = []
    if ratio_opt == "ratio":
        # data ratio
        h_ratio_data = get_poissonized_ratio_graph(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_data.drawopt = "P0"
        style_hist(h_ratio_data, "data")
        # mc ratio
        h_ratio_mc = get_mc_ratio_band(h_bkg_tot_uncorr)
        h_ratio_mc.drawopt = "E2"
        style_hist(h_ratio_mc, "error")
        #h_ratio_mc_stat = get_mc_ratio_band(h_bkg_tot_stat_uncorr)
        #h_ratio_mc_stat.drawopt = "E2"
        #style_hist(h_ratio_mc_stat, "ratio_stat")
        # line
        #line = ROOT.TLine(x1, 1., x2, 1.)
        #style_hist(line, "ratio_line")
        #line.drawopt = ""
        #if use_sys: ratios = [h_ratio_mc, h_ratio_mc_stat, h_ratio_data]
        #else:       ratios = [h_ratio_mc, h_ratio_data]
        ratios = [h_ratio_mc, h_ratio_data]

    elif ratio_opt == "sig":
        #h_ratio_obs = get_significance_ratio(h_data_uncorr, h_bkg_tot_uncorr)
        h_ratio_obs = get_significance_ratio_diego(h_data_uncorr, h_bkg_tot_uncorr)
        print "Observed significances: ", [h_ratio_obs.GetBinContent(i) for i in xrange(1, h_ratio_obs.GetNbinsX()+1)]
        #h_ratio_obs.drawopt = "HIST"
        #style_hist(h_ratio_obs, "significance")
        h_ratio_obs.drawopt = "PZ"
        style_hist(h_ratio_obs, "data")
        line = ROOT.TLine(x1, 0., x2, 0.)
        line.drawopt = ""
        h_ratio_sigs = []
        for (i, s) in enumerate(sigs_forsig_uncorr):
            #h = get_significance_ratio(s, h_bkg_tot_uncorr, ismc=True)
            h = get_significance_ratio_diego(s, h_bkg_tot_uncorr, ismc=True)
            style_hist(h, sigs_uncorr[i].GetName().replace("_uncorr",""))
            h.drawopt = "HIST]["
            h.line = h.Clone("{}_line".format(h.GetName()))
            h.line.SetLineWidth(3)
            h.line.SetLineColor(h.GetLineColor() - 12)
            h.line.SetLineStyle(2)
            h_ratio_sigs.append(h)
        ratios = h_ratio_sigs + [line] + [h_ratio_obs]
    else:
        print "No ratio configured!"


    for g in sigs:
        g.line = g.Clone("{}_line".format(g.GetName()))
        g.line.tlatex = g.tlatex
        g.line.SetLineWidth(3)
        g.line.SetLineColor(g.GetLineColor() - 12)
        g.line.SetLineStyle(2)

    ## build components
    #leg = build_leg(h_data, bkgs, sigs, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2)
    #if use_sys: leg.AddEntry(h_bkg_tot_stat, h_bkg_tot_stat.tlatex, 'F')
    #leg2 = build_leg(h_data, bkgs, [s.line for s in sigs], h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2, nolabels=True)

    leg = build_leg_bkg(h_data, bkgs, h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2)
    leg2 = build_leg_bkg(h_data, bkgs,h_bkg_tot, leg_xpos=l_x2, leg_ypos=l_y2, nolabels=True)
    leg3 = build_leg_sig(h_data, sigs, leg_xpos=0.73, leg_ypos=l_y2)
    leg4 = build_leg_sig(h_data, [s.line for s in sigs], leg_xpos=0.73, leg_ypos=l_y2, nolabels=True)


    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = RatioCanvas('rc', cfg.c_w, cfg.c_h, cfg.c_ratio_split)
    #h_total.GetXaxis().SetRangeUser(x1, x2)
    rc.draw_frame1(x1, y1, x2, y2, ';;%s' % (ytitle))
    if ratio_opt == "ratio":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Obs. / exp.' % (xtitle))
    elif ratio_opt == "sig":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Significance' % (xtitle))
        rc.pad2.SetGridy()
    rc.scale_text(cfg.t_size)
    if ndivx is not None:
        rc.fr1.GetXaxis().SetNdivisions(ndivx)
        rc.fr2.GetXaxis().SetNdivisions(ndivx)

    # draw pad1
    rc.pad1.cd()
    rc.pad1.SetLogy(logy)
    rc.pad1.SetLogx(logx)
    rc.fr2.GetXaxis().SetMoreLogLabels()
    rc.fr2.GetXaxis().SetNoExponent()
    h_stack.Draw("SAME,HIST")
    g_data.Draw("SAME,PZ")
    h_bkg_tot.Draw("SAME,E2")
    #if use_sys: h_bkg_tot_stat.Draw("SAME,E2")
    for s in sigs:
        s.Draw("SAME,HIST")
        s.line.Draw("SAME,HIST")
    draw_standard_text(atlasx=atlasx, lumix=atlasx, lumi=cfg.lumi, status = cfg.status, compact=True)
    draw_channel_text(x=atlasx, y=0.82, channel="{} {}".format(channame, catname))
    leg.Draw()
    leg2.Draw()
    leg3.Draw()
    leg4.Draw()
    if not cfg.unblinded:
        line = draw_blind(blind, x1, x2, y1, y2, logy=logy)
    ROOT.gPad.RedrawAxis()
    rc.pad1.Update()

    # draw pad 2
    rc.pad2.cd()
    rc.pad2.SetLogx(logx)
    for r in ratios:
        r.Draw("SAME,{}".format(r.drawopt))
        if hasattr(r, "line"):
            r.line.Draw("SAME,{}".format(r.drawopt))


    ## drawing extra ticks
    tl = ROOT.TLatex()
    tl.SetTextFont(42)
    tl.SetTextSize(rc.fr2.GetXaxis().GetLabelSize())
    tl.SetTextAlign(23)
    #tick_offset = -7.9
    tick_offset = -3.95
    if ratio_opt == "ratio":
        if cat == "T1" and (var == "LEADTAUPT" or var == "SUBLEADTAUPT" or var == "MET"):
            #tick_offset = 0.34
            tick_offset = 0.00
            #tick_offset = 0.36
        elif cat == "T1":
            tick_offset = 0.40
        elif cat == "T0" and (var == "LEADTAUPT" or var == "SUBLEADTAUPT" or var == "MET"):
            #tick_offset = 0.34
            tick_offset = 0.00
            #tick_offset = 0.36
        else:
            tick_offset = 0.66
            if 'prefit' in filestr:
                tick_offset = 0.40

    for tick in ticks:
        tl.DrawLatex(tick, tick_offset, str(tick))

    ROOT.gPad.RedrawAxis()
    rc.pad2.Update()


    return (rc.c, locals())




def get_qcd_ffs(fname, prong):
    f = ROOT.TFile.Open(fname.format(prong=prong))
    c = f.Get("{}p_OSAlltag".format(prong))
    g = c.FindObject("mc_syst").Clone("g_{}p".format(prong))
    g_btag = c.FindObject("btag_vs_bincl").Clone("g_btag_{}p".format(prong))
    f.Close()
    return (g, g_btag)


# ________________________________________________________________________________________________
def plot_fakefactors_split():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 65
    x2 = 399.99
    y1 = 0.0001
    #y2_1p = 0.3499
    y2_1p = 0.3999
    y2_3p = 0.019999
    logy = False
    logx = False
    #xtitle = "Tau #it{p}_{T} [GeV]"
    xtitle = "%s #it{p}_{T} [GeV]" % (cfg.tauhadvis)
    #ytitle = "{} fake-factor".format(cfg.tauhadvis)
    #ytitle = "Tau fake-factor"
    ytitle = "%s fake-factor" % (cfg.tauhadvis)
    fsplit = 0.401
    tgap = 0.038

    # decoration placement
    t_x = 0.92                # standard text x
    t_y = 0.85                # standard text y
    t_h = 0.10                # standard text height
    l_x1 = 0.2                # leg xpos right-hand side
    l_y2 = 0.95               # leg ypos top
    l_h  = 0.10               # leg text heigh
    #l_w  = 0.30               # leg width
    l_w = 0.25  # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.8              # atlas y pos

    filestr_qcd = "inputs/hadhad/{prong}p_OS_BTAG_all.root"

    ## Graph getting and manipulation
    ##===========================================================================
    (g_1p, g_1p_btag) = get_qcd_ffs(filestr_qcd, 1)
    (g_3p, g_3p_btag) = get_qcd_ffs(filestr_qcd, 3)

    graphs = [g_1p, g_3p]
    graphs_btag = [g_1p_btag, g_3p_btag]

    def style_qcd(g):
        """
        ## SOLID FILL STYLE
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetLineStyle(1)
        g.SetLineColor(ROOT.kRed)
        g.SetLineWidth(3)
        g.SetFillColor(ROOT.TColor.GetColor(1.0 ,0.5 ,0.5))
        #g.SetFillColorAlpha(ROOT.kRed, 0.5)
        """
        ## HATCHED FILL STYLE
        g.SetMarkerColor(ROOT.kRed)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetFillColor(ROOT.kRed)
        #g.SetFillStyle(3545)
        g.SetFillStyle(3005)
        g.SetLineColor(ROOT.kRed)
        g.SetLineStyle(1)
        g.SetLineWidth(2)

        #g.outline = g.Clone("{}_outline".format(g.GetName()))
        #g.outline.SetFillStyle(0)
        # up and down outlines
        g_up = ROOT.TGraph()
        g_up.SetLineColor(ROOT.kRed)
        g_up.SetLineWidth(3)
        g_up.SetFillStyle(0)
        g_dn = ROOT.TGraph()
        g_dn.SetLineColor(ROOT.kRed)
        g_dn.SetLineWidth(3)
        g_dn.SetFillStyle(0)
        for i in xrange(g.GetN()):
            g_up.SetPoint(g_up.GetN(), g.GetX()[i], g.GetY()[i] + g.GetErrorYhigh(i))
            g_dn.SetPoint(g_dn.GetN(), g.GetX()[i], g.GetY()[i] - g.GetErrorYlow(i))
        g.outline_up = g_up
        g.outline_dn = g_dn
        # background canvas
        g.canvas = g.Clone("{}_canvas".format(g.GetName()))
        g.canvas.SetFillStyle(1001)
        g.canvas.SetFillColor(0)
        #g.outlineSetLineWidth(3)

    def style_btag(g):
        """
        ## SOLID FILL STYLE
        #g.SetMarkerStyle(21)
        #g.SetMarkerColor(ROOT.kBlue)
        #g.SetMarkerSize(0.7)
        g.SetLineStyle(0)
        g.SetLineWidth(0)
        #g.SetLineColor(ROOT.kBlue)
        g.SetFillColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
        #g.SetFillColorAlpha(ROOT.kBlue, 0.5)
        """

        ## HATCHED FILL STYLE
        g.SetMarkerColor(ROOT.kBlue)
        g.SetMarkerStyle(20)
        g.SetMarkerSize(0.7)
        g.SetFillColor(ROOT.kBlue)
        #g.SetFillStyle(3554)
        g.SetFillStyle(3004)
        g.SetLineColor(ROOT.kBlue)
        g.SetLineStyle(1)
        g.SetLineWidth(2)

        #g.outline = g.Clone("{}_outline".format(g.GetName()))
        #g.outline.SetFillStyle(0)
        # up and down outlines
        g_up = ROOT.TGraph()
        g_up.SetLineColor(ROOT.kBlue)
        g_up.SetLineWidth(3)
        g_up.SetFillStyle(0)
        g_dn = ROOT.TGraph()
        g_dn.SetLineColor(ROOT.kBlue)
        g_dn.SetLineWidth(3)
        g_dn.SetFillStyle(0)
        for i in xrange(g.GetN()):
            g_up.SetPoint(g_up.GetN(), g.GetX()[i], g.GetY()[i] + g.GetErrorYhigh(i))
            g_dn.SetPoint(g_dn.GetN(), g.GetX()[i], g.GetY()[i] - g.GetErrorYlow(i))
        g.outline_up = g_up
        g.outline_dn = g_dn

        #g.canvas = g.Clone("{}_canvas".format(g.GetName()))
        #g.canvas.SetFillStyle(1001)
        #g.canvas.SetFillColor(0)
        #g.outlineSetLineWidth(3)


    for g in graphs: style_qcd(g)
    for g in graphs_btag: style_btag(g)

    ## build components
    entries = [
        #[g_1p, "Fake-factor", "FL"],
        #[g_1p_btag, "#it{b}-tag uncertainty", "F"],
        [g_1p, "#it{b}-inclusive", "FL"],
        [g_1p_btag, "#it{b}-tag", "F"],

    ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)
    entries2 = [
        [g_1p.outline_up, "", "F"],
        #[g_1p_btag, "#it{b}-tag uncertainty", "F"],
    ]
    leg2 = make_legend(entries2, width=l_w, height=l_h, x1=l_x1, y2=l_y2)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    c = ROOT.TCanvas('c', 'c', cfg.c_w, cfg.c_h)

    c.p1 = ROOT.TPad("p1", "p1", 0.0, 1.0 - 1. * fsplit - tgap, 1.0, 1.0-tgap)
    c.p2 = ROOT.TPad("p2", "p2", 0.0, 0.0, 1.0, 1.0 - 1. * fsplit - tgap)
    #c.py = ROOT.TPad("py", "py", 0.0, 0.0, 0.05, 1.0 - tgap)
    c.p1.SetBottomMargin(0.005)
    for p in [c.p1, c.p2]:
        p.SetTopMargin(0.005)
        if logx: p.SetLogx()

    h2 = 1.0 - fsplit - tgap
    scale = h2 / fsplit
    c.p2.SetBottomMargin((h2-fsplit)/h2)
    c.p2.SetTopMargin(0.005 / scale)
    c.p1.Draw()
    c.p2.Draw()
    #c.py.Draw()

    c.p1.cd()
    c.fr1 = c.p1.DrawFrame(x1, y1, x2, y2_1p, ";;%s"%ytitle)
    #g_1p_btag.Draw("SAME,3")
    #g_1p.Draw("SAME,3")
    #g_1p.Draw("SAME,5")
    ## HATCHED DRAWING
    g_1p_btag.Draw("SAME,2")
    #g_1p_btag.outline.Draw("SAME,3")
    g_1p_btag.outline_up.Draw("SAME,LX")
    g_1p_btag.outline_dn.Draw("SAME,LX")
    g_1p.canvas.Draw("SAME,2")
    g_1p.Draw("SAME,2")
    #g_1p.outline.Draw("SAME,3")
    g_1p.outline_up.Draw("SAME,LX")
    g_1p.outline_dn.Draw("SAME,LX")
    g_1p.Draw("SAME,LX")


    c.p2.cd()
    c.fr2 = c.p2.DrawFrame(x1, y1, x2, y2_3p, ";%s"%xtitle)
    #g_3p_btag.Draw("SAME,3")
    #g_3p.Draw("SAME,3")
    #g_3p.Draw("SAME,LX")
    ## HATCHED DRAWING
    g_3p_btag.Draw("SAME,2")
    #g_3p_btag.outline.Draw("SAME,3")
    g_3p_btag.outline_up.Draw("SAME,LX")
    g_3p_btag.outline_dn.Draw("SAME,LX")
    g_3p.canvas.Draw("SAME,2")
    g_3p.Draw("SAME,2")
    #g_3p.outline.Draw("SAME,3")
    g_3p.outline_up.Draw("SAME,LX")
    g_3p.outline_dn.Draw("SAME,LX")
    g_3p.Draw("SAME,LX")




    #ROOT.gPad.RedrawAxis()


    title_size = 33
    label_size = 30
    tick_size_x = 0.05
    tick_size_y = 0.015
    for fr in [c.fr1, c.fr2]:
        fr.GetXaxis().SetTitleFont(43)
        fr.GetXaxis().SetTitleSize(0)
        fr.GetXaxis().SetLabelFont(43)
        fr.GetXaxis().SetLabelSize(0)

        fr.GetYaxis().SetTitleFont(43)
        fr.GetYaxis().SetTitleSize(title_size)
        fr.GetYaxis().SetLabelFont(43)
        fr.GetYaxis().SetLabelSize(label_size)
        fr.GetYaxis().SetTitleOffset(1.65)

        fr.GetYaxis().SetNdivisions(505)

        fr.GetXaxis().SetTickSize(tick_size_x)
        fr.GetYaxis().SetTickSize(tick_size_y)


    c.fr2.GetXaxis().SetTitleSize(title_size)
    c.fr2.GetXaxis().SetLabelSize(label_size)
    c.fr2.GetXaxis().SetTickSize(tick_size_x / scale)
    c.fr2.GetYaxis().SetTickSize(tick_size_y * scale)
    c.fr2.GetXaxis().SetTitleOffset(2.5)
    c.fr2.GetXaxis().SetLabelOffset(0.01)
    if logx: c.fr2.GetXaxis().SetMoreLogLabels()



    t_y_2 = (h2-fsplit)/h2 + t_y*fsplit/h2
    # standard atlas text
    #draw_standard_text(atlasx=atlasx, atlasy=atlasy)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(25)
    latex.SetTextAlign(11)
    c.p1.cd()
    #if not cfg.status:
    #    latex.DrawLatex(atlasx, t_y, "#it{#bf{ATLAS}} Internal")
    #else:
    #    latex.DrawLatex(atlasx, t_y, "#it{#bf{ATLAS}} %s"%(cfg.status))
    #latex.SetTextSize(20)
    #latex.DrawLatex(atlasx, t_y-t_h, "#sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    latex.DrawLatex(atlasx, t_y, "#it{#bf{ATLAS}} #sqrt{s} = 13 TeV, %.1f fb^{-1}" % (cfg.lumi))
    #latex.SetTextSize(20)
    #latex.DrawLatex(atlasx, t_y-2.*t_h, "{} channel".format(cfg.hadhad))
    latex.DrawLatex(atlasx, t_y - 1. * t_h, "{} channel".format(cfg.hadhad))

    # pane labels
    latex.SetTextAlign(31)
    c.p1.cd()
    latex.DrawLatex(t_x, t_y, "one-track")

    c.p2.cd()
    latex.DrawLatex(t_x, t_y_2, "three-track")
    leg.Draw()
    leg2.Draw()

    #c.py.cd()
    #latex.SetTextSize(title_size)
    #latex.SetTextAngle(90)
    #latex.SetTextAlign(32)
    #latex.DrawLatex(0.5, 0.985, ytitle)

    #ROOT.gPad.RedrawAxis()
    for p in [c.p1, c.p2]:
        p.RedrawAxis()
        p.Update()

    c.Update()


    #rc.c.SaveAs("test.pdf")
    return (c, locals())



def dump_hepdata():
    """

    :return:
    """
    quals = dict()
    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)
    chan = '2tau_h'
    catdict = {'T0':'b-veto', 'T1':'b-tag', 'T5':'b-inclusive'}

    tanb = 10.0
    hmasses = [300, 500, 800]
    zmasses = [1500, 2000, 2500]


    filestr = "inputs/postfit/2017-07-14/total_Region_BMin0_J0_{cat}_isMVA0_L0_Y2015_dist{var}_D{chan}.root"
    grabber = HistGrabber(filestr=filestr, histstr="{samp}", var="MTTOT", chan="HadHad")
    for cat in ["T0", "T1", "T5"]:
        if cat in ["T0", "T5"]:
            actualbins = [150, 170, 180, 190, 200, 210, 220, 240, 260, 300, 350, 400, 450, 500, 600, 700, 800, 1000]
        elif cat == "T1":
            actualbins = [150, 200, 250, 300, 350, 400, 450, 600, 800]

        h_data  = grabber.hist(name="data", samp="Data", cat=cat)
        h_mc = grabber.hist(name="tot", samp="Total_prediction", cat=cat)
        h_mc_up = grabber.hist(name="tot_up", samp="Total_prediction_up", cat=cat)
        h_mc_dn = grabber.hist(name="tot_dn", samp="Total_prediction_do", cat=cat)
        # set bin error (since it is actually symmetric)
        for i in xrange(0,h_mc.GetNbinsX()+2):
            h_mc.SetBinError(i, abs(h_mc_up.GetBinContent(i) - h_mc.GetBinContent(i)))

        ## get signals
        (g_xsec_gg, g_xsec_bb) = get_mssm_xsecs()
        sigs = []
        sigmasses = zmasses if cat=='T5' else hmasses
        for mass in sigmasses:
            if cat == "T5":
                sname = "SSMZprime{}".format(mass)
                s = grabber.hist(name=sname, samp=sname, cat=cat)
                s.SetName("\'$Z^{\prime} (%d)$\'"%mass)
                sigs.append(s)
            else:
                sname_gg = "ggH{}".format(mass)
                ggH = grabber.hist(name=sname_gg, samp=sname_gg, cat=cat)
                xsec_gg = g_xsec_gg.Interpolate(mass, tanb)
                ggH.Scale(xsec_gg)

                sname_bb = "bbH{}".format(mass)
                bbH = grabber.hist(name=sname_bb, samp=sname_bb, cat=cat)
                xsec_bb = g_xsec_bb.Interpolate(mass, tanb)
                bbH.Scale(xsec_bb)

                s = ggH.Clone("H{}".format(mass))
                s.Add(bbH)
                s.SetName("\'$A/H (%d)$\'" % mass)

                sigs.append(s)
        for s in sigs: kill_below(s, 0.0)



        h_data = set_bin_edges(h_data, actualbins, wcorr=False)
        h_mc = set_bin_edges(h_mc, actualbins, wcorr=False)
        #h_mc_up = set_bin_edges(h_mc_up, actualbins, wcorr=False)
        #h_mc_dn = set_bin_edges(h_mc_dn, actualbins, wcorr=False)

        #dump_hepdist(h_data, h_mc, h_mc_up, h_mc_dn, fname="hepdata_hadhad_{}.yaml".format(cat))
        dump_hepdist(h_data, h_mc, fname="hepdata_hadhad_{}.yaml".format(cat),
                     sigs=sigs, qualifiers=quals + [['Channel', chan], ['Category', catdict[cat]]])






