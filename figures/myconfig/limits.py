# encoding: utf-8
'''
  file:    limits.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
import re
from array import array
import math
import ROOT
import os

import cfg
from plotter.histutils import HistGrabber, log_bins, apply_blind, build_stack, \
    get_poissonized_graph, get_poissonized_ratio_graph, get_mc_ratio_band, get_significance_ratio, \
    build_total_stat_plus_sys, build_error_hist, set_bin_edges, divide_graphs, dump_hepvals_arr
from plotter.plotutils import SingleCanvas, build_leg, draw_standard_text, draw_blind, \
    get_ytitle_events, draw_channel_text, make_legend, set_palette
from plotter.samples import style_hist
from ZprimeXsecProvider import ZprimeXsecProvider


def get_all(f, prefix):
    i=0
    l = []
    while True:
        g = f.Get(prefix+str(i))
        if g: l.append(g)
        else: break
        i+=1
    return l

def get_contour_h(scenario, mass):
    f = ROOT.TFile.Open("inputs/exclusion-limits/Contour_v2.root")
    g = f.Get("g_{scenario}_{mass}".format(scenario=scenario, mass=mass).replace(".","_"))
    g.SetLineColor(ROOT.kRed)
    g.SetLineStyle(7)
    g.SetLineWidth(1)
    g.SetPoint(g.GetN(), 5000, g.GetY()[g.GetN()-1])
    f.Close()
    return g

def get_contour_H(scenario, mass):
    f = ROOT.TFile.Open("inputs/exclusion-limits/Contour_v2.root")
    g = f.Get("g_{scenario}_H_{mass}".format(scenario=scenario, mass=mass).replace(".","_"))
    g.SetLineColor(ROOT.kBlue)
    g.SetLineStyle(7)
    g.SetLineWidth(1)
    f.Close()
    return g


def get_hMSSM_coupling_obs() :
    g_h_coup_obs = ROOT.TGraph()
    g_h_coup_obs.SetPoint(0,358.3590031,102.475)
    g_h_coup_obs.SetPoint(1,358.3593703,89.65168038)
    g_h_coup_obs.SetPoint(2,370,71.78620824)
    g_h_coup_obs.SetPoint(3,374.4549513,71.13481625)
    g_h_coup_obs.SetPoint(4,374.4266154,56.4777662)
    g_h_coup_obs.SetPoint(5,374.5387524,44.8352575)
    g_h_coup_obs.SetPoint(6,374.4980478,35.58728412)
    g_h_coup_obs.SetPoint(7,374.5719312,28.24135775)
    g_h_coup_obs.SetPoint(8,374.6418701,22.40628103)
    g_h_coup_obs.SetPoint(9,374.7401077,17.77131483)
    g_h_coup_obs.SetPoint(10,374.9026093,14.08963032)
    g_h_coup_obs.SetPoint(11,375.1617118,11.16516435)
    g_h_coup_obs.SetPoint(12,375.5549495,8.842178468)
    g_h_coup_obs.SetPoint(13,376.2408985,6.99696519)
    g_h_coup_obs.SetPoint(14,377.1844622,5.531260185)
    g_h_coup_obs.SetPoint(15,378.7245851,4.367009315)
    g_h_coup_obs.SetPoint(16,381.0563183,3.442211977)
    g_h_coup_obs.SetPoint(17,384.2620947,2.70761934)
    g_h_coup_obs.SetPoint(18,385,2.614664459)
    g_h_coup_obs.SetPoint(19,392.128428,2.124111667)
    g_h_coup_obs.SetPoint(20,398.3333333,1.932004247)
    g_h_coup_obs.SetPoint(21,403.2923137,1.660615048)
    g_h_coup_obs.SetPoint(22,413.3333333,1.306873045)
    g_h_coup_obs.SetPoint(23,413.9189152,1.292446596)
    g_h_coup_obs.SetPoint(24,434.9494554,1.064731353)
    g_h_coup_obs.SetPoint(25,440,0.9046462201)
    return g_h_coup_obs


def get_hMSSM_coupling_exp():
    g_h_coup_exp = ROOT.TGraph()
    g_h_coup_exp.SetPoint(0,313.5034198,102.475)
    g_h_coup_exp.SetPoint(1,313.5042862,89.65168038)
    g_h_coup_exp.SetPoint(2,313.5056598,71.13481625)
    g_h_coup_exp.SetPoint(3,313.5078384,56.4777662)
    g_h_coup_exp.SetPoint(4,313.5112941,44.8352575)
    g_h_coup_exp.SetPoint(5,313.5167792,35.58728412)
    g_h_coup_exp.SetPoint(6,313.5254912,28.24135775)
    g_h_coup_exp.SetPoint(7,313.5393468,22.40628103)
    g_h_coup_exp.SetPoint(8,313.5614161,17.77131483)
    g_h_coup_exp.SetPoint(9,313.5966778,14.08963032)
    g_h_coup_exp.SetPoint(10,313.6531889,11.16516435)
    g_h_coup_exp.SetPoint(11,313.7444733,8.842178468)
    g_h_coup_exp.SetPoint(12,313.8931807,6.99696519)
    g_h_coup_exp.SetPoint(13,314.138215,5.531260185)
    g_h_coup_exp.SetPoint(14,314.547356,4.367009315)
    g_h_coup_exp.SetPoint(15,315.2376435,3.442211977)
    g_h_coup_exp.SetPoint(16,316.4073093,2.70761934)
    g_h_coup_exp.SetPoint(17,318.3594914,2.124111667)
    g_h_coup_exp.SetPoint(18,321.494469,1.660615048)
    g_h_coup_exp.SetPoint(19,326.1933935,1.292446596)
    g_h_coup_exp.SetPoint(20,328.3333333,1.224936947)
    g_h_coup_exp.SetPoint(21,329.9282453,1.064731353)
    g_h_coup_exp.SetPoint(22,332.7403963,-1.475)
    return g_h_coup_exp

def get_indep_inputs(fname, prod, mu=False):
    l_mass = []
    l_obs = []
    l_exp = []
    l_up1sig = []
    l_dn1sig = []
    l_up2sig = []
    l_dn2sig = []
    if prod == "bbH":
        bfrac_req = 1.0
        gfrac_req = 0.0
    elif prod == "ggH":
        bfrac_req = 0.0
        gfrac_req = 1.0
    elif prod == "Zprime":
        bfrac_req = 0.0
        gfrac_req = 1.0
        zpxsec = ZprimeXsecProvider("SSM")

    with open(fname) as f:
        for l in f:
            (a,b) = l.split(":")
            (mass, bfrac, gfrac) = a.strip().split()
            if not (float(bfrac) == bfrac_req and float(gfrac) == gfrac_req): continue
            (obs, exp, up2sig, up1sig, dn1sig, dn2sig) = b.strip().split()
            scale = 1.0
            if prod == "Zprime" and not mu:
                scale = zpxsec.getXsec("%.1f"%(int(mass)))

            #print "GUI {} : {}, {}, {}".format(mass, obs, exp, scale)
            print "{} : {} ({}) [pb]".format(mass, float(obs) * scale, float(exp) * scale)

            l_mass.append(float(mass))
            l_obs.append(float(obs) * scale)
            l_exp.append(float(exp) * scale)
            l_up1sig.append(float(up1sig) * scale)
            l_dn1sig.append(float(dn1sig) * scale)
            l_up2sig.append(float(up2sig) * scale)
            l_dn2sig.append(float(dn2sig) * scale)

    a_mass = array('f', l_mass)
    a_obs  = array('f', l_obs)
    a_exp  = array('f', l_exp)
    a_mass2 = array('f', l_mass + list(reversed(l_mass)))
    a_1sig = array('f', l_up1sig + list(reversed(l_dn1sig)))
    a_2sig = array('f', l_up2sig + list(reversed(l_dn2sig)))

    g_obs = ROOT.TGraph(len(a_mass), a_mass, a_obs)
    g_exp = ROOT.TGraph(len(a_mass), a_mass, a_exp)
    g_1sig = ROOT.TGraph(len(a_mass2), a_mass2, a_1sig)
    g_2sig = ROOT.TGraph(len(a_mass2), a_mass2, a_2sig)

    return (g_obs, g_exp, g_1sig, g_2sig)


def get_indep_inputs_dirk(fname, mode):
    l_mass = []
    l_obs = []
    l_exp = []
    l_up1sig = []
    l_dn1sig = []
    l_up2sig = []
    l_dn2sig = []

    if   mode == "SSM":
        zpxsec = ZprimeXsecProvider("SSM")
        param = None
    elif mode   == "L":
        zpxsec = ZprimeXsecProvider("LR")
        param = +1.0
    elif mode == "R":
        zpxsec = ZprimeXsecProvider("LR")
        param = -1.0
    elif mode == "Narrow":
        zpxsec = ZprimeXsecProvider("WIDTH")
        param = 0.005
        #param = 0.02
    elif mode == "Wide":
        zpxsec = ZprimeXsecProvider("WIDTH")
        param = 0.32
        #param = 0.04

    #elif mode == "wide": param =

    assert os.path.exists(fname), "input: {} doesn't exist".format(fname)
    with open(fname) as f:
        #print "opened file: ", fname
        for l in f:
            if l.startswith("#"): continue
            #print "line: ", l
            (a,b) = l.split(":")
            params = a.strip().split()
            mass = params[0]
            p = float(params[1]) if param else None
            if p != param: continue
            (obs, dn2sig, dn1sig, exp, up1sig, up2sig) = b.strip().split()

            if param:
                scale = zpxsec.getXsec("%s %s"%(str(float(mass)), str(param)))
            else:
                scale = zpxsec.getXsec(str(float(mass)))

            print "DIRK {} : {}, {}, {}".format(mass, obs, exp, scale)

            l_mass.append(float(mass))
            l_obs.append(float(obs) * scale)
            l_exp.append(float(exp) * scale)
            l_up1sig.append(float(up1sig) * scale)
            l_dn1sig.append(float(dn1sig) * scale)
            l_up2sig.append(float(up2sig) * scale)
            l_dn2sig.append(float(dn2sig) * scale)

    a_mass = array('f', l_mass)
    a_obs  = array('f', l_obs)
    a_exp  = array('f', l_exp)
    a_mass2 = array('f', l_mass + list(reversed(l_mass)))
    a_1sig = array('f', l_up1sig + list(reversed(l_dn1sig)))
    a_2sig = array('f', l_up2sig + list(reversed(l_dn2sig)))

    g_obs = ROOT.TGraph(len(a_mass), a_mass, a_obs)
    g_exp = ROOT.TGraph(len(a_mass), a_mass, a_exp)
    g_1sig = ROOT.TGraph(len(a_mass2), a_mass2, a_1sig)
    g_2sig = ROOT.TGraph(len(a_mass2), a_mass2, a_2sig)

    return (g_obs, g_exp, g_1sig, g_2sig)


def get_sfm_excl(fname):
    # make 1d graph for each s2p slice
    graphs = dict()
    with open(fname) as f:
        for l in f:
            if l.startswith("#"): continue
            (a,b) = l.split(":")
            (mass, s2p) = a.strip().split()
            (obs, exp, up2sig, up1sig, dn1sig, dn2sig) = b.strip().split()

            if s2p not in graphs:
                g = ROOT.TGraph()
                g.SetName("g_{}".format(s2p))
                graphs[s2p] = g

            g = graphs[s2p]
            g.SetPoint(g.GetN(), float(mass), float(obs))

    g_excl = ROOT.TGraph()
    for (s2p, g) in sorted(graphs.items()):
        best_m = None
        best_l = None
        for m in xrange(500, 3000):
            l = abs(g.Eval(m) - 1.0)
            if best_l is None or l < best_l:
                best_l = l
                best_m = m
        g_excl.SetPoint(g_excl.GetN(), float(s2p), best_m)

    return g_excl
    #return g.GetContourList(1.0).At(0)


def get_indep_inputs_2d(fname, mass):
    #print "get_indep_inputs_2d, mass: ", mass
    scale = 1.e3
    l_gg_95 = []
    l_bb_95 = []
    with open(fname) as f:
        for l in f:
            (a,b) = l.split(":")
            (m, bfrac, gfrac) = a.strip().split()
            if int(m) != mass: continue
            (obs, exp, up2sig, up1sig, dn1sig, dn2sig) = b.strip().split()
            l_gg_95.append(float(obs) * float(gfrac) * scale)
            l_bb_95.append(float(obs) * float(bfrac) * scale)

    l_gg_95 = [0.0] + l_gg_95 + [0.0]
    l_bb_95 = [0.0] + l_bb_95 + [0.0]
    a_gg_95 = array('f', l_gg_95)
    a_bb_95 = array('f', l_bb_95)

    g_95 = ROOT.TGraph(len(a_gg_95), a_gg_95, a_bb_95)

    return g_95


def get_indep_inputs_2d_v2(fname, limtype):
    print "getting 2d graph, omg"
    mass = []
    frac = []
    limit = []
    with open(fname) as f:
        for l in f:
            (a,b) = l.split(":")
            (m, bfrac, gfrac) = a.strip().split()
            (obs, exp, up2sig, up1sig, dn1sig, dn2sig) = b.strip().split()
            if limtype == "exp": lim = float(exp)
            else:                lim = float(obs)
            mass.append(float(m))
            frac.append(float(bfrac))
            limit.append(lim)
    g = ROOT.TGraph2D(len(mass), array('d', mass), array('d', frac), array('d', limit))
    print "got 2d graph"
    return g


def get_sys_graphs(fname):

    #print "parsing: ", fname

    ## parse text file and dump data in dict
    masses = set()
    sysnames = set()
    mass = None
    data = dict()
    with open(fname) as f:
        for l in f:
            if "GeV" in l:
                mass = int(l.replace("GeV","").strip())
                masses.add(mass)
                #print "Got Mass: ", mass
                continue
            if not mass: continue
            elif len(l.split()) in [2,3]:
                name = l.split()[0].replace("-","_")
                mu = l.split()[1]
                #(name, mu, _) = l.split()
                #name = name.replace("-","_")
                #print "got {}: {}".format(name, mu)
                sysnames.add(name)
                if not name in data:
                    data[name] = dict()
                data[name][mass] = float(mu)

    ## convert dict data into graphs
    graphs = []
    for name in sysnames:
        if name == "none": continue
        g = ROOT.TGraph()
        graphs.append(g)
        g.SetName(name)
        for m in sorted(masses):
            mu_none = data["none"][m]
            mu = data[name][m]
            impact = (mu - mu_none) / mu_none
            g.SetPoint(g.GetN(), m, impact)

    name_order = ["nominal", "tau", "tes", "hh_fakes", "lh_fakes", "other"]
    #graphs = sorted(graphs, key=lambda g: -111 if g.GetName() == "other" else max([g.GetY()[i] for i in xrange(g.GetN())]), reverse=True)
    graphs = sorted(graphs, key=lambda g: name_order.index(g.GetName()) if g.GetName() in name_order else 999)
    for g in graphs:
        print "{}, max: {}".format(g.GetName(), max([g.GetY()[i] for i in xrange(g.GetN())]))

    return graphs

def make_ssm_graph():
    zpxsec = ZprimeXsecProvider("SSM")
    masses = list(sorted([float(m) for m in zpxsec.dMassPoint]))
    xsec = [zpxsec.getXsec("%.1f"%(m)) for m in masses]
    g = ROOT.TGraph(len(masses), array('d',masses), array('d', xsec))
    return g


def make_ssm_graph_band():
    zpxsec = ZprimeXsecProvider("SSM")
    masses = list(sorted([float(m) for m in zpxsec.dMassPoint]))
    g = ROOT.TGraphAsymmErrors()
    for m in masses:
        y = zpxsec.getXsec("%.1f"%(m))
        (eup, edn) = zpxsec.getXsecUncAsymm("%.1f"%(m))
        print "mass: {}, xsec: {}, {}, {}".format(m, y, eup, edn)
        i = g.GetN()
        g.SetPoint(i, m, y)
        g.SetPointError(i, 0, 0, -edn, eup)
    return g


def make_zprime_2015(mu=False):
    ## Dodgy rip
    """
    points = [
        [500, 0.4530108431372664],
        [600, 0.20736816348639397],
        [700, 0.15170345614893643],
        [800, 0.09793777341515542],
        [900, 0.07164792543280318],
        [1000, 0.042114765723917195],
        [1250, 0.025541113029436416],
        [1500, 0.02150696783145547],
        [1750, 0.021845727691318925],
        [2000, 0.022539339047347933],
        [2250, 0.024755126980527783],
        [2500, 0.026352054477263596],
        ]
    """
    ## Actual limit
    points = [
        [500.0, 0.451621736865],
        [600.0, 0.206878871343],
        [700.0, 0.150261555932],
        [800.0, 0.0971824417804],
        [900.0, 0.071145757115],
        [1000.0, 0.042047336661],
        [1250.0, 0.0258670091592],
        [1500.0, 0.0217940923869],
        [1750.0, 0.0220828922884],
        [2000.0, 0.0223178415943],
        [2250.0, 0.0241877654913],
        [2500.0, 0.0262696175862],
        ]
    if mu:
        f = ROOT.TFile.Open("inputs/zprime-limits/Limit2015.root")
        gxsec = f.Get("Main").FindObject("Z'_{SSM}")

    g = ROOT.TGraph()
    if not mu: g.SetPoint(0, 500, 1000.)
    for (x, y) in points:
        if mu:
            g.SetPoint(g.GetN(), x, y/gxsec.Eval(x))
        else:
            g.SetPoint(g.GetN(), x, y)
    if not mu: g.SetPoint(g.GetN(), 2500, 1000.)
    return g


def make_zprime_2015_real():
    f = ROOT.TFile.Open("inputs/zprime-limits/Limit2015.root")
    gtmp = f.Get("Main").FindObject("Observed Limit")
    print "Points: ", gtmp.GetN()

    g = ROOT.TGraph()
    g.SetName("g_2015")
    g.SetPoint(0, 500, 1000.)
    for i in xrange(gtmp.GetN()):
        print "Point {}: {}, {}".format(g.GetN(), gtmp.GetX()[i], gtmp.GetY()[i])
        g.SetPoint(g.GetN(), gtmp.GetX()[i], gtmp.GetY()[i])

    g.SetPoint(g.GetN(), 2500, 1000.)
    f.Close()
    return g


def make_nll_graph(fname, gname='g_nll'):
    print "Making nll graph from: {}".format(fname)
    xbb_arr = array('d', [])
    xgg_arr = array('d', [])
    nll_arr = array('d', [])
    with open(fname) as f:
        il = 0
        store = set()
        for l in f.readlines():
            if "FINAL NLL" not in l: continue
            data = l.strip().split()
            if len(data) != 6: continue
            xbb_arr.append(float(data[0]))
            xgg_arr.append(float(data[1]))
            nll_arr.append(float(data[-1]))

            tag = "{:.4g}:{:.4g}".format(float(data[0]), float(data[1]))
            if tag in store:
                print "Found duplicate: ", tag
            store.add(tag)
            il+=1
    print "NLL Points: ", len(nll_arr)

    nll_min = min(nll_arr)
    index_min = min(xrange(len(nll_arr)), key=nll_arr.__getitem__)
    xbb_min = xbb_arr[index_min]
    xgg_min = xgg_arr[index_min]

    print "nll min: {}, ({},{})".format(nll_min, xbb_min, xgg_min)
    dchi2_arr = array('d', [2.0 * (nll - nll_min) for nll in nll_arr])
    g = ROOT.TGraph2D(len(xbb_arr), xgg_arr, xbb_arr, dchi2_arr)
    g.SetName(gname)
    g.bestfit = [xgg_min, xbb_min]
    return g



# ________________________________________________________________________________________________
def plot_limit_mssm(scenario="hMSSM", overlay=False):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## defaults

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 250
    x2 = 1700
    y1 = 1.0
    y2 = 77
    #logx = logy = False      # use logarithmic axis scales
    xtitle = "#it{m}_{#it{A}} [GeV]"    #
    ytitle = "tan#it{#beta}"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.75                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos
    contour_scenario = scenario

    #filestr = "inputs/21May/{scenario}_{chan}{cat}.root"
    #filestr = "inputs/guillermo-results-060817/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"
    #filestr = "inputs/guillermo-results-06-26-17/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"
    #filestr = "inputs/guillermo-results-06-27-17/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"
    #filestr = "inputs/guillermo-results-06-29-17-rebin/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"
    #filestr = "inputs/guillermo-results-07-11-17/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"
    filestr = "inputs/guillermo-results-07-14-17/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"

    # scenario specifics
    if scenario == "hMSSM":
        scenario_text = "hMSSM scenario"
    elif scenario == "mhmodm":
        scenario_text = "MSSM m_{h}^{mod-} scenario, M_{SUSY} = 1 TeV"
    elif scenario == "mhmodp":
        scenario_text = "MSSM m_{h}^{mod+} scenario, M_{SUSY} = 1 TeV"
    elif scenario == "newmhmax":
        scenario_text = "MSSM m_{h}^{max} scenario, M_{SUSY} = 1 TeV"
        contour_scenario = "mhmax"
    elif scenario == "lightstau1":
        scenario_text = "Light stau scenario, M_{SUSY} = 1 TeV ????"
        contour_scenario = "lightstau"
    elif scenario == "lightstopmod":
        scenario_text = "Light stop mod scenario, M_{SUSY} = 1 TeV ????"
        contour_scenario = "lightstop"
    elif scenario == "low":
        scenario_text = "Low scenario, M_{SUSY} = 1 TeV ????"
    elif scenario == "tauphobic":
        scenario_text = "Tauphobic scenario, M_{SUSY} = 1 TeV ????"



    ## Graph getting and manipulation
    ##===========================================================================
    # Main plots
    f = ROOT.TFile.Open(filestr.format(scenario=scenario, chan="tt", cat=""))
    g_obs = []
    g_exp = []
    g_1sig = []
    g_2sig = []

    g_obs = get_all(f, "obs_")
    g_exp = get_all(f, "exp_")
    g_1sig = get_all(f, "1sig_")
    g_2sig = get_all(f, "2sig_")
    f.Close()

    for g in g_obs: style_hist(g, "obs")
    for g in g_exp: style_hist(g, "exp")
    for g in g_1sig: style_hist(g, "err1sig")
    for g in g_2sig: style_hist(g, "err2sig")

    f = ROOT.TFile.Open(filestr.format(scenario=scenario, chan="lh", cat="-tag"))
    g_lh_tag = get_all(f, "exp_")
    f.Close()
    f = ROOT.TFile.Open(filestr.format(scenario=scenario, chan="lh", cat="-veto"))
    g_lh_veto = get_all(f, "exp_")
    f.Close()
    f = ROOT.TFile.Open(filestr.format(scenario=scenario, chan="hh", cat="-tag"))
    g_hh_tag = get_all(f, "exp_")
    f.Close()
    f = ROOT.TFile.Open(filestr.format(scenario=scenario, chan="hh", cat="-veto"))
    g_hh_veto = get_all(f, "exp_")
    f.Close()

    # extras
    for g in g_hh_tag:
        g.SetLineColor(ROOT.kRed)
        g.SetLineStyle(9)
        g.SetLineWidth(2)
    for g in g_hh_veto:
        g.SetLineColor(ROOT.kRed)
        g.SetLineStyle(3)
        g.SetLineWidth(2)
    for g in g_lh_tag:
        g.SetLineColor(ROOT.kBlue)
        g.SetLineStyle(9)
        g.SetLineWidth(2)
    for g in g_lh_veto:
        g.SetLineColor(ROOT.kBlue)
        g.SetLineStyle(3)
        g.SetLineWidth(2)

    # 2015 limit
    f2015 = ROOT.TFile.Open("inputs/Paper2015/2016_Paper.root")
    g_2015 = f2015.Get("MSSM_LHHH_hMSSM/Observed").Clone()
    xmin = min([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
    xmax = max([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
    g_2015.SetPoint(g_2015.GetN(), xmax, y2)
    g_2015.SetPoint(g_2015.GetN(), xmin, y2)

    """
    ## TRANSPARENT
    g_2015.SetFillColorAlpha(ROOT.kBlue-9, 0.3)
    g_2015.SetLineWidth(0)
    """
    # CROSS-HATCHED
    g_2015.SetFillColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
    g_2015.SetFillStyle(3545)
    g_2015.SetLineColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
    g_2015.SetLineWidth(2)
    g_2015.SetLineStyle(1)
    f2015.Close()

    ## Make legend
    entries2b = [
        [g_2015, "#bf{#it{ATLAS}} 2015", "F"],
    ]
    leg2b = make_legend(entries2b, width=0.30, height=l_h * 0.8, x1=l_x1 - 0.24, y2=l_y2)
    leg2b.Draw()

    ## build components
    entries = []
    if g_obs: entries += [[g_obs[0], g_obs[0].tlatex, "L"]]
    if g_exp: entries += [[g_exp[0], g_exp[0].tlatex, "L"]]
    if g_1sig: entries += [[g_1sig[0], g_1sig[0].tlatex, "F"]]
    if g_2sig: entries += [[g_2sig[0], g_2sig[0].tlatex, "F"]]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)



    entries2 = [
        [g_lh_tag[0],  "{} {}".format(cfg.lephad, cfg.btag),  "L"],
        [g_lh_veto[0], "{} {}".format(cfg.lephad, cfg.bveto), "L"],
        [g_hh_tag[0],  "{} {}".format(cfg.hadhad, cfg.btag),  "L"],
        [g_hh_veto[0], "{} {}".format(cfg.hadhad, cfg.bveto), "L"],
        ]
    leg2 = make_legend(entries2, width=l_w, height=l_h, x1=l_x1-0.02, y1=0.19)
    leg2.tlegend.SetHeader("Expected")
    leg2.tlegend.SetFillStyle(1001)
    leg2.tlegend.SetFillColor(0)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    rc.c.cd()

    for g in g_2sig: g.Draw("F")
    for g in g_1sig: g.Draw("F")
    if scenario == "hMSSM":
        g_2015.Draw("F")
        g_2015.Draw("L") ## WHen Hatching

    for g in g_exp: g.Draw("L")
    for g in g_obs: g.Draw("L")

    if overlay:
        for g in g_hh_tag: g.Draw("L")
        for g in g_hh_veto: g.Draw("L")
        for g in g_lh_tag: g.Draw("L")
        for g in g_lh_veto: g.Draw("L")


    # overlay couplings limit
    if scenario == "hMSSM":
        coup_c = ROOT.kMagenta+2
        g_coup = get_hMSSM_coupling_obs()
        #g_coup.SetLineColor(ROOT.kBlue)
        #g_coup.SetFillColor(ROOT.kBlue)
        g_coup.SetLineColor(coup_c)
        g_coup.SetFillColor(coup_c)
        g_coup.SetLineWidth(-702)
        #g_coup.SetFillStyle(3005)
        g_coup.SetFillStyle(3354)
        g_coup.Draw("L,SAME")

        bx = 420.
        by = 45.
        bw = 50.
        bh = 25.
        b_coup = ROOT.TBox(bx-bw/1.5, by-bh/2., bx+bw/2., by+bh/2.)
        b_coup.SetLineColor(0)
        b_coup.SetFillColor(0)
        b_coup.Draw()

        t_coup = ROOT.TLatex()
        t_coup.SetTextColor(coup_c)
        t_coup.SetTextSize(0.024)
        t_coup.SetTextAngle(270)
        t_coup.SetTextFont(42)
        t_coup.SetTextAlign(22)
        #t_coup.DrawLatex(400, 59, "              Run-1 SM Higgs boson couplings")
        t_coup.DrawLatex(bx, by, "#it{#bf{ATLAS}} h(125) couplings")



    # overlay contours
    t_mass = ROOT.TLatex()
    #t_mass.SetTextSize(0.015)
    t_mass.SetTextSize(0.020)
    t_mass.SetTextFont(42)
    t_mass.SetTextAlign(31)
    tmx1 = x2 - 25

    H_masses = [300, 500, 1000]
    if scenario == "mhmodm":
        h_masses = [123.5, 124, 125, 126, 126.2, 126.3, 126.5]
    elif scenario == "mhmodp":
        h_masses = [123.5, 124, 125, 126, 126.2, 126.3]
    elif scenario == "newmhmax":
        h_masses = [122, 125, 128, 129, 129.3]
    elif scenario == "hMSSM":
        #H_masses = [300, 500, 800, 1000]
        H_masses = []
        h_masses = []
    elif scenario == "lightstau1":
        h_masses = [115, 122, 125, 126]
    elif scenario == "lightstopmod":
        H_masses = [300, 400, 500]
        h_masses = [115, 122, 123, 123.2]
    elif scenario == "low":
        H_masses = h_masses = []
    elif scenario == "tauphobic":
        h_masses = [115, 122, 123, 125]

    # mh
    t_mass.SetTextColor(ROOT.kRed)
    for m in h_masses:
        g = get_contour_h(contour_scenario, "{:g}".format(m))
        g.Draw("L,SAME")
        x = tmx1
        #y = g.GetY()[g.GetN()-1] + 0.7
        y = g.Eval(tmx1) + 0.7
        t_mass.SetTextAngle(0)
        if scenario in ["mhmodm", "mhmodp"] and m == 123.5: y -= 3.0
        elif scenario == "mhmodm" and m == 126.5:
            x = 490
            y = 57.3
            t_mass.SetTextAngle(23)
        elif scenario == "mhmodp" and m == 126.3:
            x = 500
            y = 56.0
            t_mass.SetTextAngle(14)
        elif scenario == "mhmodm" and m == 126.3:
            x = 850
            y = 48.5
            t_mass.SetTextAngle(7)
        elif scenario == "mhmodp" and m == 126.2:
            y -= 0.5
            x = 875
            t_mass.SetTextAngle(3.5)
        elif scenario == "newmhmax" and m == 122: y -= 2.7
        elif scenario == "newmhmax" and m == 129.3:
            y -= 2.0
            x = 875
            t_mass.SetTextAngle(4.5)
        else:
            pass

        t_mass.DrawLatex(x, y, "m_{{h}} = {:g} GeV".format(m) )

        if scenario == "lightstau1" and m == 126:
            g = get_contour_h(contour_scenario, "126_up")
            g.Draw("L,SAME")
            x = tmx1 - 400
            y = g.GetY()[g.GetN() - 1] + 0.7
            t_mass.DrawLatex(x, y, "m_{{h}} = {:g} GeV".format(m))

        if scenario == "tauphobic" and m in [122, 123, 125]:
            g = get_contour_h(contour_scenario, "{}_up".format(m))
            g.Draw("L,SAME")
            x = tmx1
            y = g.GetY()[g.GetN() - 1] + 0.7
            t_mass.DrawLatex(x, y, "m_{{h}} = {:g} GeV".format(m))


    # mH
    t_mass.SetTextColor(ROOT.kBlue)
    t_mass.SetTextAngle(90)
    t_mass.SetTextAlign(33)
    for m in H_masses:
        g = get_contour_H(contour_scenario, "{:g}".format(m))
        g.Draw("L,SAME")
        x = g.GetX()[g.GetN()-1] + 10
        y = 57
        if scenario in ["mhmodm", "mhmodp"] and m == 300: y = 50
        if scenario in ["mhmodm", "mhmodp"] and m == 500: y = 55
        if m == 1000: x-= 50

        t_mass.DrawLatex(x, y, "m_{{H}} = {:g} GeV".format(m) )






    box = ROOT.TBox(x1, 58.00, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy-2.*t_h, scenario_text)
    latex.DrawLatex(atlasx, atlasy-3.*t_h, "#it{H}/#it{A} #rightarrow #tau#tau 95% CL limits")


    leg.Draw()
    if overlay: leg2.Draw()
    if scenario == "hMSSM": leg2b.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())



# ________________________________________________________________________________________________
def plot_limit_indep(prod, overlay=False):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 200
    x2 = 1500
    y1 = 0.002
    y2 = 25.0
    logx = False
    logy = True
    xtitle = None
    ytitle = None

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.75                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/limit-scans/limit-scan-tautau-lh_combined.18April.txt"
    #filestr = "inputs/21May/limit-scan-tautau-{reg}{chan}_combined.20May.limits.txt"
    #filestr = "inputs/guillermo-results-06-26-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.25June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-06-27-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.25June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-06-29-17-rebin/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.29June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-07-11-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.11July.limits-obs.txt"
    filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.13July.limits-obs.txt"

    # production specifics
    if prod == "Zprime":
        #filestr = "inputs/guillermo-results-060817/SSM-limit/limit-scan-tautau-{chan}-{reg}-ssm_combined.30May.limits-obs.txt"
        #filestr = "inputs/guillermo-results-06-27-17/ssm-limits/limit-scan-tautau-{chan}-{reg}-ssm_combined.26June.limits-obs.txt"
        filestr = "inputs/guillermo-results-06-27-17/ssm-limits/limit-scan-tautau-{chan}-{reg}-ssm_combined.29June.limits-obs.txt"
        lim_text = "#it{Z'} #rightarrow #tau#tau 95% CL limits"
        prod_text = None
        xtitle = "#it{m}_{#it{Z'}} [GeV]"
        ytitle = "#it{#sigma} #times #it{B}(#it{Z'} #rightarrow #it{#tau#tau}) [pb]"
        x1 = 200
        x2 = 4000
    else:
        #lim_text = "H/A #rightarrow #tau#tau 95% CL limits"
        lim_text = "#it{#phi} #rightarrow #tau#tau 95% CL limits"
        #ytitle = "#it{#sigma} #times #it{B}(#it{A}/#it{H} #rightarrow #it{#tau#tau}) [pb]"
        ytitle = "#it{#sigma} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [pb]"
        #xtitle = "m_{A} [GeV]"
        xtitle = "#it{m}_{#it{#phi}} [GeV]"
        x1 = 200
        x2 = 2250
        if prod == "bbH":   prod_text = "#it{b}-associated production"
        elif prod == "ggH": prod_text = "gluon-gluon fusion"



    ## Graph getting and manipulation
    ##===========================================================================
    if prod == "Zprime":
        (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs(filestr.format(reg="inc", chan="tt"), prod)
    else:
        (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs(filestr.format(reg="tcr_",chan="tt"), prod)
    g_obs = [g_obs]
    g_exp = [g_exp]
    g_1sig = [g_1sig]
    g_2sig = [g_2sig]

    for g in g_obs: style_hist(g, "obs")
    for g in g_exp: style_hist(g, "exp")
    for g in g_1sig: style_hist(g, "err1sig")
    for g in g_2sig: style_hist(g, "err2sig")

    ## build components
    entries = []
    if g_obs: entries += [[g_obs[0], g_obs[0].tlatex, "LP"]]
    if g_exp: entries += [[g_exp[0], g_exp[0].tlatex, "L"]]
    if g_1sig: entries += [[g_1sig[0], g_1sig[0].tlatex, "F"]]
    if g_2sig: entries += [[g_2sig[0], g_2sig[0].tlatex, "F"]]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)
    leg2 = leg2a = leg2b = leg3 = None

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logx: rc.c.SetLogx()
    if logy: rc.c.SetLogy()
    rc.c.cd()

    for g in g_2sig: g.Draw("F")
    for g in g_1sig: g.Draw("F")
    for g in g_exp: g.Draw("L")
    for g in g_obs: g.Draw("LP")


    if prod == "Zprime":
        g_ssm = make_ssm_graph_band()
        g_ssm.SetFillColor(ROOT.kGray+3)
        #g_ssm.Draw("SAME,3")

        g_ssm_line = g_ssm.Clone("g_ssm_line")
        g_ssm_line.SetLineColor(ROOT.kGray+1)
        g_ssm_line.SetLineWidth(3)
        #g_ssm_line.Draw("SAME,LX")

        g_ssm_line2 = g_ssm_line.Clone("g_ssm2")
        g_ssm_line2.SetLineWidth(6)
        g_ssm_line2.SetLineColor(ROOT.kGray + 3)

        #g_ssm = make_ssm_graph()
        #g_ssm.SetLineColor(ROOT.kGray+1)
        #g_ssm.Draw("SAME,L")


    if not prod == "Zprime":

        # TODO: put 2015 before bands are drawn

        """
        # 2015 limit
        f2015 = ROOT.TFile.Open("inputs/Paper2015/2016_Paper.root")
        g_2015 = f2015.Get("LHHH_bbH/Observed").Clone()
        xmin = min([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
        xmax = max([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
        g_2015.SetPoint(g_2015.GetN(), xmax, y2)
        g_2015.SetPoint(g_2015.GetN(), xmin, y2)
        g_2015.SetFillColorAlpha(ROOT.kBlue-9, 0.3)
        g_2015.SetLineWidth(0)
        f2015.Close()

        g_2015.Draw("F")
        """


        # 2015 limit - CROSS-HATCHED
        f2015 = ROOT.TFile.Open("inputs/Paper2015/2016_Paper.root")
        g_2015 = f2015.Get("LHHH_bbH/Observed").Clone()
        xmin = min([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
        xmax = max([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
        g_2015.SetPoint(g_2015.GetN(), xmax, y2)
        g_2015.SetPoint(g_2015.GetN(), xmin, y2)
        g_2015.SetFillColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
        g_2015.SetFillStyle(3554)
        g_2015.SetLineColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
        g_2015.SetLineWidth(2)
        g_2015.SetLineStyle(1)
        f2015.Close()

        #g_2015.Draw("FL")
        g_2015.Draw("F")
        g_2015.Draw("L")


        """
        ## LINE STYLE
        # 2015 limit
        f2015 = ROOT.TFile.Open("inputs/Paper2015/2016_Paper.root")
        g_2015 = f2015.Get("LHHH_bbH/Observed").Clone()
        xmin = min([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
        xmax = max([g_2015.GetX()[i] for i in xrange(g_2015.GetN())])
        g_2015.SetPoint(g_2015.GetN(), xmax, y2)
        g_2015.SetPoint(g_2015.GetN(), xmin, y2)
        g_2015.SetLineWidth(3)
        g_2015.SetLineColor(ROOT.kBlue-9)
        f2015.Close()
        g_2015.Draw("L")
        """



        entries2b = [
            [g_2015, "#bf{#it{ATLAS}} 2015", "F"],
            #[g_2015, "#bf{#it{ATLAS}} 2015", "L"], ## LINE STYLE
        ]
        leg2b = make_legend(entries2b, width=0.30, height=l_h * 0.8, x1=l_x1 - 0.24, y2=l_y2)
        #leg2b.Draw()

        # extras
        if overlay:
            # naming convention for May20
            #g_hh_tag = get_indep_inputs(filestr.format(reg="tag_", chan="hh"), prod)[0]
            #g_hh_veto = get_indep_inputs(filestr.format(reg="veto_", chan="hh"), prod)[0]
            #g_lh_tag = get_indep_inputs(filestr.format(reg="tag_", chan="lh"), prod)[0]
            #g_lh_veto = get_indep_inputs(filestr.format(reg="veto_", chan="lh"), prod)[0]
            # naming convention for June25
            g_hh_tag = get_indep_inputs(filestr.format(reg="tag_", chan="hh"), prod)[1]
            g_hh_veto = get_indep_inputs(filestr.format(reg="veto_", chan="hh"), prod)[1]
            g_lh_tag = get_indep_inputs(filestr.format(reg="tcr_tag_", chan="lh"), prod)[1]
            g_lh_veto = get_indep_inputs(filestr.format(reg="tcr_veto_", chan="lh"), prod)[1]


            g_hh_tag.SetLineColor(ROOT.kRed)
            g_hh_tag.SetLineStyle(9)
            g_hh_tag.SetLineWidth(2)
            g_hh_veto.SetLineColor(ROOT.kRed)
            g_hh_veto.SetLineStyle(3)
            g_hh_veto.SetLineWidth(2)

            g_lh_tag.SetLineColor(ROOT.kBlue)
            g_lh_tag.SetLineStyle(9)
            g_lh_tag.SetLineWidth(2)
            g_lh_veto.SetLineColor(ROOT.kBlue)
            g_lh_veto.SetLineStyle(3)
            g_lh_veto.SetLineWidth(2)

            entries2 = [
                [g_lh_tag,  "{} {}".format(cfg.lephad, cfg.btag),  "L"],
                [g_lh_veto, "{} {}".format(cfg.lephad, cfg.bveto), "L"],
                [g_hh_tag,  "{} {}".format(cfg.hadhad, cfg.btag),  "L"],
                [g_hh_veto, "{} {}".format(cfg.hadhad, cfg.bveto), "L"],
                ]
            leg2 = make_legend(entries2, width=l_w, height=l_h, x1=l_x1-0.02, y2=l_y2-0.20)
            #leg2.tlegend.SetHeader("Observed")
            leg2.tlegend.SetHeader("Expected")
            leg2.tlegend.SetFillStyle(1001)
            leg2.tlegend.SetFillColor(0)


            g_hh_tag.Draw("L")
            g_hh_veto.Draw("L")
            g_lh_tag.Draw("L")
            g_lh_veto.Draw("L")
            leg2.Draw()

    if prod == "Zprime":
        g_2015 = make_zprime_2015()
        """
        ## TRANSPARENT
        g_2015.SetFillColorAlpha(ROOT.kBlue-9, 0.3)
        g_2015.SetLineWidth(0)
        g_2015.Draw("F")
        """
        # CROSS-HATCHED
        g_2015.SetFillColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
        g_2015.SetFillStyle(3554)
        g_2015.SetLineColor(ROOT.TColor.GetColor(0.5, 0.5, 1.0))
        g_2015.SetLineWidth(2)
        g_2015.SetLineStyle(1)
        g_2015.Draw("F")
        g_2015.Draw("L")

        ## Make legend
        g_2015_clone = g_2015.Clone("g_2015_clone")
        g_2015_clone.SetFillColor(ROOT.kWhite)
        entries2a = [
            [g_2015_clone, "", "F"],
            [g_ssm_line2,  "",  "L"],
            ]
        leg2a = make_legend(entries2a, width=0.22, height=l_h*0.8, x1=l_x1-0.24, y2=l_y2)
        entries2b = [
            [g_2015, "#bf{#it{ATLAS}} 2015", "F"],
            [g_ssm_line,  "#it{Z'}_{SSM}",  "L"],
            ]
        leg2b = make_legend(entries2b, width=0.22, height=l_h*0.8, x1=l_x1-0.24, y2=l_y2)


        if overlay:
            g_hh = get_indep_inputs(filestr.format(reg="inc", chan="hh"), prod)[1]
            g_lh = get_indep_inputs(filestr.format(reg="inc", chan="lh"), prod)[1]

            g_hh.SetLineColor(ROOT.kRed)
            g_hh.SetLineStyle(3)
            g_hh.SetLineWidth(2)

            g_lh.SetLineColor(ROOT.kBlue)
            g_lh.SetLineStyle(3)
            g_lh.SetLineWidth(2)

            entries2 = [
                [g_lh, cfg.lephad, "L"],
                [g_hh, cfg.hadhad, "L"],
                ]
            leg3 = make_legend(entries2, width=l_w, height=l_h, x1=l_x1-0.02, y2=l_y2-0.20)
            #leg3.tlegend.SetHeader("Observed")
            leg3.tlegend.SetHeader("Expected")
            leg3.tlegend.SetFillStyle(1001)
            leg3.tlegend.SetFillColor(0)

            g_hh.Draw("L")
            g_lh.Draw("L")

            #leg3.Draw()


    if prod == "Zprime":
        g_ssm.Draw("SAME,3")
        g_ssm_line.Draw("SAME,LX")


    # top box
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")

    # standard text + legend
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy - t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy - 2. * t_h, lim_text)
    if prod_text: latex.DrawLatex(atlasx, atlasy - 3. * t_h, prod_text)
    leg.Draw()

    if leg2a: leg2a.Draw()
    if leg2b: leg2b.Draw()
    if leg2: leg2.Draw()
    if leg3: leg3.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    """
    # save out limit graphs
    fhepdata = ROOT.TFile.Open("HepInputs_limits_{}.root".format(prod), "RECREATE")
    g_copy_exp  = g_exp[0].Clone()
    g_copy_1sig = g_1sig[0].Clone()
    g_copy_2sig = g_2sig[0].Clone()
    g_copy_obs = g_obs[0].Clone()
    fhepdata.WriteTObject(g_copy_exp, "g_expected")
    fhepdata.WriteTObject(g_copy_1sig, "gae_1sigma")
    fhepdata.WriteTObject(g_copy_2sig, "gae_2sigma")
    fhepdata.WriteTObject(g_copy_obs, "g_observed")
    fhepdata.Close()
    """


    """
    # Save out channel specific limits
    fout = ROOT.TFile.Open("limit_graphs.root","RECREATE")
    fout.WriteTObject(g_lh_tag, "g_lh_tag")
    fout.WriteTObject(g_lh_veto, "g_lh_veto")
    fout.WriteTObject(g_hh_tag, "g_hh_tag")
    fout.WriteTObject(g_hh_veto, "g_hh_veto")
    fout.Close()
    """

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())


# ________________________________________________________________________________________________
def plot_limit_zpoverlay(relative=False):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 200
    x2 = 3999
    y1 = 0.002
    y2 = 25.0
    logx = False
    logy = True
    xtitle = "#it{m}_{#it{Z'}} [GeV]"
    ytitle = "#it{#sigma} #times #it{B}(#it{Z'} #rightarrow #it{#tau#tau}) [pb]"

    if relative:
        logy = False
        y2 = 2.5
        y1 = 0.4
        ytitle = "(#it{#sigma} #times #it{B}) / (#it{#sigma} #times #it{B})_{SSM}"

    # decoration placement
    t_x = 0.70                # standard text x
    t_y = 0.73                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos


    #filestr = "inputs/guillermo-results-06-27-17/ssm-limits/limit-scan-tautau-{chan}-{reg}-ssm_combined.26June.limits-obs.txt"
    lim_text = "#it{Z'} #rightarrow #tau#tau 95% CL limits"
    #filestr_dirk = "inputs/zprime-limits/20170620-tt-ssm.SSM_TT.txt"
    #filestr_dirk = "inputs/zprime-limits/20170620-tt-{model}_TT.txt"
    filestr_dirk = "inputs/zprime-limits/20170620-tt-{model}_FINAL.txt"

    ## Graph getting and manipulation
    ##===========================================================================
    #(g_obs_ssm, g_exp_ssm, _, _) = get_indep_inputs(filestr.format(reg="inc", chan="tt"), "Zprime")
    (g_obs_ssm, g_exp_ssm, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="ssm.SSM"), mode="SSM")
    (g_obs_lh, g_exp_lh, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="lr.LR"), mode="L")
    (g_obs_rh, g_exp_rh, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="lr.LR"), mode="R")
    (g_obs_nar, g_exp_nar, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="width.WIDTH"), mode="Narrow")
    (g_obs_wid, g_exp_wid, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="width.WIDTH"), mode="Wide")

    if relative:
        g_obs_lh = divide_graphs(g_obs_lh, g_obs_ssm)
        g_exp_lh = divide_graphs(g_exp_lh, g_exp_ssm)
        g_obs_rh = divide_graphs(g_obs_rh, g_obs_ssm)
        g_exp_rh = divide_graphs(g_exp_rh, g_exp_ssm)
        g_obs_nar = divide_graphs(g_obs_nar, g_obs_ssm)
        g_exp_nar = divide_graphs(g_exp_nar, g_exp_ssm)
        g_obs_wid = divide_graphs(g_obs_wid, g_obs_ssm)
        g_exp_wid = divide_graphs(g_exp_wid, g_exp_ssm)

        g_obs_ssm = divide_graphs(g_obs_ssm, g_obs_ssm)
        g_exp_ssm = divide_graphs(g_exp_ssm, g_exp_ssm)

    g_obs_ssm.SetName("SSM")
    g_obs_lh.SetName("LH")
    g_obs_rh.SetName("RH")
    g_obs_nar.SetName("Narrow")
    g_obs_wid.SetName("Wide")


    g_obs_list = [g_obs_ssm, g_obs_lh, g_obs_rh, g_obs_nar, g_obs_wid]
    g_exp_list = [g_exp_ssm, g_exp_lh, g_exp_rh, g_exp_nar, g_exp_wid]

    for g in g_obs_list:
        g.SetLineWidth(3)
    for g in g_exp_list:
        g.SetLineWidth(3)
        g.SetLineStyle(7)

    for g in [g_obs_ssm, g_exp_ssm]: g.SetLineColor(ROOT.kBlack)
    for g in [g_obs_lh, g_exp_lh]: g.SetLineColor(ROOT.kBlue)
    for g in [g_obs_rh, g_exp_rh]: g.SetLineColor(ROOT.kRed)
    for g in [g_obs_nar, g_exp_nar]: g.SetLineColor(ROOT.kGreen+2)
    for g in [g_obs_wid, g_exp_wid]: g.SetLineColor(ROOT.kMagenta)



    ## build components
    entries = [[g, g.GetName(), "L"] for g in g_obs_list[1:]]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)


    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logx: rc.c.SetLogx()
    if logy: rc.c.SetLogy()
    rc.c.cd()

    for g_o, g_e in zip(g_obs_list, g_exp_list):
        g_e.Draw("L")
        g_o.Draw("L")


    """
    # top box
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """

    # standard text + legend
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status, lumi=cfg.lumi, compact=True)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx, atlasy - t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy - 1. * t_h, lim_text)

    #latex.DrawLatex(t_x, t_y,     "Solid (expected), Dashed (observed)")
    latex.DrawLatex(t_x, t_y,     "Solid (observed)")
    latex.DrawLatex(t_x, t_y-t_h, "Dashed (expected)")

    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())



# ________________________________________________________________________________________________
def plot_limit_zp_history():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 0
    x2 = 4200
    y1 = 0.002
    y2 = 100.0
    logx = False
    logy = True
    xtitle = "#it{m}_{#it{Z'}} [GeV]"
    ytitle = "#it{#sigma}_{limit} / #it{#sigma}_{SSM}"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.40                # leg xpos right-hand side
    l_y2 = 0.35               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.50               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos
    lim_text = "#it{Z'} #rightarrow #tau#tau 95% CL limits"

    filestr = "inputs/guillermo-results-06-27-17/ssm-limits/limit-scan-tautau-{chan}-{reg}-ssm_combined.29June.limits-obs.txt"


    ## Graph getting and manipulation
    ##===========================================================================
    f_7TeV = ROOT.TFile.Open("inputs/zprime-limits/Limit7TeV.root")
    g_7TeV = f_7TeV.Get("mu_UL_vs_mass").FindObject("g_obs").Clone("g_7TeV")
    g_7TeV.tlatex = "#it{#bf{ATLAS}} #sqrt{s} = 7 TeV, 4.6 fb^{-1}"
    g_7TeV.SetLineColor(ROOT.kBlue)
    g_7TeV.SetMarkerColor(ROOT.kBlue)
    g_7TeV.SetMarkerStyle(22)

    f_8TeV = ROOT.TFile.Open("inputs/zprime-limits/Limit8TeV.root")
    g_8TeV = f_8TeV.Get("mu_UL_vs_mass").FindObject("g_obs").Clone("g_8TeV")
    g_8TeV.tlatex = "#it{#bf{ATLAS}} #sqrt{s} = 8 TeV, 20.3 fb^{-1}"
    g_8TeV.SetLineColor(ROOT.kRed)
    g_8TeV.SetMarkerColor(ROOT.kRed)
    g_8TeV.SetMarkerStyle(21)

    for f in [f_7TeV, f_8TeV]:
        f.Close()

    g_13TeV = make_zprime_2015(mu=True)
    g_13TeV.tlatex = "#it{#bf{ATLAS}} #sqrt{s} = 13 TeV, 3.2 fb^{-1}"
    g_13TeV.SetLineColor(ROOT.kGreen+2)
    g_13TeV.SetMarkerColor(ROOT.kGreen+2)
    g_13TeV.SetMarkerStyle(23)


    (g_new, _, _, _) = get_indep_inputs(filestr.format(reg="inc", chan="tt"), "Zprime", mu=True)
    g_new.tlatex = "#it{#bf{ATLAS}} #sqrt{s} = 13 TeV, 36.1 fb^{-1}"

    graphs = [g_7TeV, g_8TeV, g_13TeV, g_new]

    for g in graphs:
        g.SetLineWidth(2)
        g.SetMarkerSize(1.5)


    ## build components
    entries = [(g, g.tlatex, "LP") for g in graphs]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logx: rc.c.SetLogx()
    if logy: rc.c.SetLogy()
    rc.c.cd()

    line = ROOT.TLine()
    line.SetLineStyle(7)
    line.DrawLine(x1, 1., x2, 1.)


    for g in graphs: g.Draw("LP")


    """
    # top box
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """

    # standard text + legend
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx, atlasy - t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy - 1. * t_h, lim_text)
    #if prod_text: latex.DrawLatex(atlasx, atlasy - 3. * t_h, prod_text)
    leg.Draw()


    ROOT.gPad.RedrawAxis()
    rc.c.Update()


    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())






# ________________________________________________________________________________________________
def plot_limit_indep_2d(mass):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 0.0
    x2 = 100
    y1 = 0.0
    y2 = 100
    logy = False
    xtitle = "#it{#sigma}(gg) #times #it{B}(#it{A}/#it{H} #rightarrow #it{#tau#tau}) [fb]"
    ytitle = "#it{#sigma}(bb) #times #it{B}(#it{A}/#it{H} #rightarrow #it{#tau#tau}) [fb]"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.25               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/limit-scans/limit-scan-tautau-lh_combined.18April.txt"
    filestr = "inputs/21May/limit-scan-tautau-tcr_tt_combined.20May.limits.txt"

    if mass >= 1000:
        x2 = 12
        y2 = 12

    ## Graph getting and manipulation
    ##===========================================================================
    g_95 = get_indep_inputs_2d(filestr, mass)
    g_95.SetFillColor(ROOT.kBlue-9)
    g_95.SetLineColor(ROOT.kBlue +2)
    g_95.SetLineWidth(3)

    ## build components
    entries = [
        [g_95, "95% CL", "F"],
        ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)


    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()

    g_95.Draw("F,SAME")
    g_95.Draw("L,SAME")

    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy-2.*t_h, "#it{{m}}_{{H/A}} = {} GeV".format(mass))
    latex.DrawLatex(atlasx, atlasy-3.*t_h, "H/A #rightarrow #tau#tau observed limits")


    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())



# ________________________________________________________________________________________________
def plot_limit_indep_2d_v2(limtype="obs"):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    #x1 = 250
    #x2 = 1500
    x1 = 200
    x2 = 2250
    y1 = 0.0
    y2 = 1.18
    #z1 = 4.e-3
    #z2 = 0.9
    z1 = 3.e-3
    z2 = 0.96
    logx = False
    logy = False
    #xtitle = "m_{A} [GeV]"
    xtitle = "#it{m}_{#it{#phi}} [GeV]"
    #ytitle = "#it{#sigma}(bb) / [#it{#sigma}(gg) + #it{#sigma}(bb)]"
    ytitle = "#it{#sigma_{bb}} / [#it{#sigma_{gg}} + #it{#sigma_{bb}}]"
    #ztitle = "#it{#sigma} #times #it{B}(#it{A}/#it{H} #rightarrow #it{#tau#tau}) [fb]"
    ztitle = "#it{#sigma} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [pb]"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/21May/limit-scan-tautau-tcr_tt_combined.20May.limits.txt"
    #filestr = "inputs/guillermo-results-06-26-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.25June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-06-27-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.25June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-06-29-17-rebin/mssm-limits/limit-scan-tautau-tcr_tt_combined.29June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-07-11-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.11July.limits-obs.txt"
    filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.13July.limits-obs.txt"

    #set_palette(name="green")
    ROOT.gStyle.SetPalette(ROOT.kBird)
    #ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)
    #ROOT.gStyle.SetPalette(ROOT.kCandy)
    #ROOT.gStyle.SetPalette(ROOT.kCherry)
    #ROOT.gStyle.SetPalette(ROOT.kLake)
    ROOT.gStyle.SetNumberContours(255)

    ## Graph getting and manipulation
    ##===========================================================================
    g_95 = get_indep_inputs_2d_v2(filestr, limtype)
    g_95.SetMinimum(z1)
    g_95.SetMaximum(z2)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logx: rc.c.SetLogx()
    if logy: rc.c.SetLogy()
    rc.c.SetLogz()
    rc.c.SetRightMargin(0.2)
    rc.c.cd()

    g_95.Draw("SAME,COLZ")



    #iso_lvls = [0.01, 0.05, 0.1, 0.15, 0.20]
    if limtype == "exp": iso_lvls = [0.01, 0.1]
    else:                iso_lvls = [0.01, 0.1]
    for lvl in iso_lvls:
        contours = g_95.GetContourList(lvl)
        itr = contours.MakeIterator()
        while True:
            g = itr.Next()
            if not g: break
            g.SetLineWidth(2)
            g.Draw("SAME,L")
    ## Many lines
    #iso_lvls = [0.025, 0.075, 0.125, 0.175, 0.225]
    #iso_lvls = [0.005, 0.006, 0.007, 0.008, 0.009, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.2]
    #iso_lvls = [0.005, 0.007, 0.009, 0.03, 0.05, 0.07, 0.09]
    ## Mass range 250--1500
    #if limtype == "exp": iso_lvls = [0.007, 0.009, 0.03, 0.05, 0.07, 0.3]
    #else:                iso_lvls = [0.005, 0.007, 0.009, 0.03, 0.05, 0.07]
    ## Mass range 200-2500
    if limtype == "exp": iso_lvls = [0.007, 0.03, 0.07, 0.3]
    else:                iso_lvls = [0.007, 0.03, 0.07, 0.3]
    for lvl in iso_lvls:
        contours = g_95.GetContourList(lvl)
        itr = contours.MakeIterator()
        while True:
            g = itr.Next()
            if not g: break
            g.SetLineWidth(1)
            g.SetLineStyle(2)
            g.Draw("SAME,L")


    boxfrac = 1. / y2
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy-2.*t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")

    ## Good Order (revert to this for paper)
    #latex.DrawLatex(atlasx + 0.3, atlasy - 0. * t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")
    #if limtype == "exp":
    #    latex.DrawLatex(atlasx + 0.3, atlasy - 1. * t_h, "Expected")
    #else:
    #    latex.DrawLatex(atlasx + 0.3, atlasy - 1. * t_h, "Observed")

    ## Crap order (for CONF)
    latex.DrawLatex(atlasx + 0.3, atlasy - 1. * t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")
    latex.SetTextAlign(31)
    if limtype == "exp":
        latex.DrawLatex(0.76, atlasy - 0. * t_h, "Expected")
    else:
        latex.DrawLatex(0.76, atlasy - 0. * t_h, "Observed")


    ## NEW STYLE
    #lim_text = "#it{#phi} #rightarrow #tau#tau 95% CL limits"
    #if limtype == "exp": lim_text = "Expected " + lim_text
    #else:                lim_text = "Observed " + lim_text
    #latex.DrawLatex(0.2, atlasy - 1. * t_h, lim_text)



    ## z-axis title
    latex.SetTextAlign(31)
    latex.SetTextAngle(90)
    latex.SetTextSize(0.05)
    latex.DrawLatex(0.98, 0.95, ztitle)


    """
    ## Settings for 250 -- 1500 mass range
    if limtype == "exp":
        # iso bar numbers
        xoff = -0.02
        latex.SetTextAngle(90+180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.21+xoff, 0.25, "0.3")
        latex.DrawLatex(0.270+xoff, 0.25, "#bf{0.1}")
        latex.DrawLatex(0.305+xoff, 0.25, "0.07")
        latex.DrawLatex(0.343+xoff, 0.25, "0.05")
        latex.DrawLatex(0.400+xoff, 0.25, "0.03")
        latex.SetTextAngle(95+180)
        latex.DrawLatex(0.625+xoff, 0.26, "#bf{0.01}")
        latex.SetTextAngle(95+180)
        latex.DrawLatex(0.658+xoff, 0.26, "0.009")
        latex.SetTextAngle(105+180)
        latex.DrawLatex(0.755+xoff, 0.50, "0.007")
    else:
        # iso bar numbers
        xoff = 0.01
        latex.SetTextAngle(90 + 180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.275 + xoff, 0.25, "#bf{0.1}")
        latex.SetTextAngle(95 + 180)
        latex.DrawLatex(0.310 + xoff, 0.25, "0.07")
        latex.DrawLatex(0.347 + xoff, 0.25, "0.05")
        latex.DrawLatex(0.382 + xoff, 0.25, "0.03")
        latex.SetTextAngle(110 + 180)
        latex.DrawLatex(0.530 + xoff, 0.26, "#bf{0.01}")
        latex.SetTextAngle(115 + 180)
        latex.DrawLatex(0.570 + xoff, 0.26, "0.009")
        latex.SetTextAngle(125 + 180)
        latex.DrawLatex(0.705 + xoff, 0.26, "0.007")
        latex.SetTextAngle(135 + 180)
        latex.DrawLatex(0.700 + xoff, 0.705, "0.005")
    """

    """
    ## Settings for 200 -- 2500 mass range
    if limtype == "exp":
        # iso bar numbers
        xoff = -0.02
        latex.SetTextAngle(90+180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.21+xoff, 0.25, "0.3")
        latex.DrawLatex(0.270+xoff, 0.25, "#bf{0.1}")
        latex.DrawLatex(0.305+xoff, 0.25, "0.07")
        latex.DrawLatex(0.343+xoff, 0.25, "0.05")
        latex.DrawLatex(0.400+xoff, 0.25, "0.03")
        latex.SetTextAngle(95+180)
        latex.DrawLatex(0.625+xoff, 0.26, "#bf{0.01}")
        latex.SetTextAngle(95+180)
        latex.DrawLatex(0.658+xoff, 0.26, "0.009")
        latex.SetTextAngle(105+180)
        latex.DrawLatex(0.755+xoff, 0.50, "0.007")
    else:
        # iso bar numbers
        xoff = 0.0
        latex.SetTextAngle(90 + 180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.18 + xoff, 0.25, "0.3")
        latex.DrawLatex(0.215 + xoff, 0.25, "#bf{0.1}")
        latex.SetTextAngle(95 + 180)
        latex.DrawLatex(0.265 + xoff, 0.25, "0.07")
        #latex.DrawLatex(0.347 + xoff, 0.25, "0.05")
        latex.DrawLatex(0.305 + xoff, 0.25, "0.03")
        latex.SetTextAngle(105 + 180)
        latex.DrawLatex(0.385 + xoff, 0.25, "#bf{0.01}")
        latex.SetTextAngle(115 + 180)
        #latex.DrawLatex(0.570 + xoff, 0.26, "0.009")
        latex.SetTextAngle(110 + 180)
        latex.DrawLatex(0.48 + xoff, 0.25, "0.007")
        latex.SetTextAlign(31)
        latex.SetTextAngle(55)
        latex.DrawLatex(0.68 + xoff, 0.25, "0.007")
        #latex.SetTextAngle(135 + 180)
        #latex.DrawLatex(0.700 + xoff, 0.705, "0.005")
    """
    ## Settings for 200 -- 2250 mass range
    if limtype == "exp":
        # iso bar numbers
        latex.SetTextAngle(90+180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.195, 0.37, "0.3")
        latex.DrawLatex(0.200, 0.25, "#bf{0.1}")
        latex.DrawLatex(0.250, 0.25, "0.07")
        #latex.DrawLatex(0.343, 0.25, "0.05")
        latex.DrawLatex(0.310, 0.25, "0.03")
        #latex.SetTextAngle(95+180)
        latex.DrawLatex(0.435, 0.25, "#bf{0.01}")
        latex.SetTextAngle(95+180)
        #latex.DrawLatex(0.658, 0.26, "0.009")
        #latex.SetTextAngle(105+180)
        latex.SetTextAngle(90 + 180)
        latex.DrawLatex(0.520, 0.25, "0.007")
        #latex.SetTextAlign(31)
        #latex.SetTextAngle(90)
        #latex.DrawLatex(0.72, 0.25, "0.007")

    else:
        # iso bar numbers
        latex.SetTextAngle(90 + 180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.182, 0.25, "0.3")
        latex.DrawLatex(0.220, 0.25, "#bf{0.1}")
        #latex.SetTextAngle(95 + 180)
        latex.DrawLatex(0.270, 0.25, "0.07")
        #latex.DrawLatex(0.347, 0.25, "0.05")
        latex.SetTextAngle(92 + 180)
        latex.DrawLatex(0.317, 0.25, "0.03")
        latex.SetTextAngle(95 + 180)
        latex.DrawLatex(0.406, 0.25, "#bf{0.01}")
        latex.SetTextAngle(115 + 180)
        #latex.DrawLatex(0.570, 0.26, "0.009")
        latex.SetTextAngle(100 + 180)
        latex.DrawLatex(0.485, 0.25, "0.007")
        #latex.SetTextAlign(31)
        #latex.SetTextAngle(55)
        #latex.DrawLatex(0.745, 0.25, "0.007")
        #latex.SetTextAngle(135 + 180)
        #latex.DrawLatex(0.700, 0.705, "0.005")




    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    """
    # save out limit graphs
    fhepdata = ROOT.TFile.Open("HepInputs_limits_2d_{}.root".format(limtype), "RECREATE")
    g_copy = g_95.Clone("")
    fhepdata.WriteTObject(g_copy_exp, "g_expected")
    fhepdata.WriteTObject(g_copy_1sig, "gae_1sigma")
    fhepdata.WriteTObject(g_copy_2sig, "gae_2sigma")
    fhepdata.WriteTObject(g_copy_obs, "g_observed")
    fhepdata.Close()
    """


    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())




# ________________________________________________________________________________________________
def plot_dchi2_2d(mass):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = y1 = 0.0
    x2 = y2 = 2
    logy = False
    xtitle = "#it{#sigma_{gg}} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [fb]"
    ytitle = "#it{#sigma_{bb}} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [fb]"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.75                # leg xpos right-hand side
    l_y2 = 0.89               # leg ypos top
    l_h  = 0.05               # leg text heigh
    l_w  = 0.20               # leg width
    #atlasx = 0.65              # atlas x pos
    atlasx = 0.20             # atlas x pos
    atlasy = 0.9              # atlas y pos

    filestr = "inputs/nllscan/GlobalFit_{mass}.txt"
    #filestr = "inputs/nllscan/v2/GlobalFit_{mass}.txt"
    #filestr = "inputs/nllscan/v3/obs/GlobalFit_all_{mass}.txt"
    leg_title = "Observed"

    ## Graph getting and manipulation
    ##===========================================================================
    g_dchi2 = make_nll_graph(filestr.format(mass=mass))
    f = ROOT.TFile.Open("test.root", "RECREATE")
    f.WriteTObject(g_dchi2,"g")
    f.Close()


    g_1sig_clist = g_dchi2.GetContourList(2.30)
    g_1sig_itr = g_1sig_clist.MakeIterator()
    g_2sig_clist = g_dchi2.GetContourList(5.99)
    g_2sig_itr = g_2sig_clist.MakeIterator()
    g_1sig_list = []
    g_2sig_list = []
    doReversed = False
    while True:
        o = g_1sig_itr.Next()
        if not o: break
        o.SetPoint(o.GetN(),-1, -1)
        o.SetLineWidth(3)
        o.SetFillColor(ROOT.TColor.GetColor("#f03b20"))
        #o.SetFillColor(ROOT.kRed)
        g_1sig_list.append(o)
    while True:
        o = g_2sig_itr.Next()
        if not o: break
        o.SetPoint(o.GetN(), -1, -1)
        o.SetLineWidth(3)
        o.SetFillColor(ROOT.TColor.GetColor("#ffeda0"))
        #o.SetFillColor(ROOT.kOrange)
        g_2sig_list.append(o)

    if not len(g_2sig_list) == 1:
        doReversed = True
        g_2sig_list[0].SetFillColor(ROOT.kWhite)

    # Convert pb to fb!
    for g in g_1sig_list: convert_pb2fb(g)
    for g in g_2sig_list: convert_pb2fb(g)
    g_dchi2.bestfit[0] *= 1000.0
    g_dchi2.bestfit[1] *= 1000.0


    ROOT.gStyle.SetPalette(ROOT.kBird)
    ROOT.gStyle.SetNumberContours(255)

    (xfit, yfit) = g_dchi2.bestfit
    bestfit = ROOT.TMarker(xfit, yfit, 34)
    bestfit.SetMarkerSize(3)

    ## get x and y ranges
    x2 = 1.2 * max([g.GetX()[i] for g in g_2sig_list for i in xrange(g.GetN())])
    y2 = 1.2 * max([g.GetY()[i] for g in g_2sig_list for i in xrange(g.GetN())])
    x1 = x2 * 1.e-4
    y1 = y2 * 1.e-4


    ## build components
    entries = [
        [g_1sig_list[0], "68% CL", "F"],
        [g_2sig_list[-1], "95% CL", "F"],
        [bestfit, "Best fit", "P"],
        ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)


    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()

    #g_dchi2.GetXaxis().SetRange(0,1000)
    #g_dchi2.GetYaxis().SetRange(0,1000)
    


    #g_dchi2.Draw("COLZ")
    if doReversed == True: g_2sig_list_new = reversed(g_2sig_list)
    else: g_2sig_list_new = g_2sig_list
    for g in g_2sig_list_new: g.Draw("F,SAME")
    for g in g_2sig_list: g.Draw("L,SAME")
    for g in g_1sig_list: g.Draw("F,SAME")
    for g in g_1sig_list: g.Draw("L,SAME")

    bestfit.Draw()

    #g_95.Draw("F,SAME")
    #g_95.Draw("L,SAME")

    """
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    #latex.SetTextAlign(31)
    latex.SetTextSize(0.040 / ROOT.gPad.GetHNDC())
    #latex.DrawLatex(atlasx+0.25, atlasy, "#it{#bf{ATLAS}}")
    #latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%d fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    #latex.DrawLatex(atlasx+0.25, atlasy-t_h, "%s, %s" % (com_text, lumi_text))

    if cfg.status == '' or cfg.status == None:
        latex.DrawLatex(atlasx, atlasy, "#it{#bf{ATLAS}}")
    else:
        latex.DrawLatex(atlasx, atlasy, "#it{#bf{ATLAS}} %s"%(cfg.status))

    #latex.DrawLatex(atlasx+0.25, atlasy-2.*t_h, "#it{m_{#phi}} = %d GeV" % (mass))
    #latex.DrawLatex(atlasx, atlasy - 1. * t_h, "#it{m_{#phi}} = %d GeV" % (mass))
    latex.DrawLatex(atlasx, atlasy - 1. * t_h, "%s, %s" % (com_text, lumi_text))
    #latex.SetTextAlign(31)
    #latex.DrawLatex(0.9, atlasy, "#it{m_{#phi}} = %d GeV" % (mass))
    #latex.DrawLatex(atlasx, atlasy-3.*t_h, "H/A #rightarrow #tau#tau observed limits")

    latex.SetTextAlign(22)
    latex.SetTextFont(43)
    latex.SetTextSize(25)
    latex.DrawLatex(l_x1+0.5*l_w-0.015, l_y2+0.02, leg_title)
    latex.DrawLatex(l_x1, l_y2-4.0*l_h-0.02, "#it{m_{#phi}} = %d GeV" % (mass))

    

    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    return (rc.c, locals())


def convert_pb2fb(g):
    for i in xrange(g.GetN()):
        x = g.GetX()[i]
        y = g.GetY()[i]
        g.SetPoint(i, x*1000., y*1000.)

# ________________________________________________________________________________________________
def plot_dchi2_onslaught(limtype='obs'):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = y1 = 0.0
    x2 = y2 = 2
    logy = False
    xtitle = "#it{#sigma_{gg}} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [fb]"
    ytitle = "#it{#sigma_{bb}} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [fb]"


    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.75                # leg xpos right-hand side
    l_y2 = 0.80               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.25               # leg width
    atlasx = 0.65              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/nllscan/GlobalFit_{mass}.txt"
    if limtype == 'obs':
        #filestr = "inputs/nllscan/obs/GlobalFit_{mass}_v2.txt"
        filestr = "inputs/nllscan_largeXY/GlobalFit_{mass}_obs_largeXYRange.txt"
        leg_title = "Observed"
    elif limtype == 'exp':
        filestr = "inputs/nllscan/exp/GlobalFit_{mass}_v2.txt"
        leg_title = "Expected"


    ## Graph getting and manipulation
    ##===========================================================================
    #masses = [200, 250, 300, 350, 400, 500, 600, 700, 800, 1000, 1200, 1500, 2000, 2500]
    masses = [400, 600, 1500, 2000]
    graphs = []
    fgraphs = []
    graphs_axis_range = []
    doReversed = False
    for mass in masses:
        g_dchi2 = make_nll_graph(filestr.format(mass=mass))
        g_1sig_clist = g_dchi2.GetContourList(2.30)
        g_1sig_itr = g_1sig_clist.MakeIterator()
        g_2sig_clist = g_dchi2.GetContourList(5.99)
        g_2sig_itr = g_2sig_clist.MakeIterator()
        g_1sig_list = []
        g_2sig_list = []
        while True:
            o = g_1sig_itr.Next()
            if not o: break
            o.SetPoint(o.GetN(),-1, -1)
            o.SetLineWidth(3)
            o.SetFillColor(ROOT.TColor.GetColor("#f03b20"))
            g_1sig_list.append(o)
        while True:
            o = g_2sig_itr.Next()
            if not o: break
            o.SetPoint(o.GetN(), -1, -1)
            o.SetLineWidth(3)
            o.SetFillColor(ROOT.TColor.GetColor("#ffeda0"))
            g_2sig_list.append(o)
       
        if not len(g_1sig_list) == 1:
            print "WARNING multiple 1sig contours for mass ", mass
        if not len(g_2sig_list) == 1:
            print "WARNING multiple 2sig contours for mass ", mass
            g_2sig_list[0].SetFillColor(ROOT.kWhite)
            doReversed = True
        g_1sig = g_1sig_list[0]
        g_2sig = g_2sig_list[0]

        # Convert pb to fb!
        #convert_pb2fb(g_1sig)
        #convert_pb2fb(g_2sig)
        for g in g_1sig_list: convert_pb2fb(g)
        for g in g_2sig_list: convert_pb2fb(g)
        g_dchi2.bestfit[0] *= 1000.0
        g_dchi2.bestfit[1] *= 1000.0

        (xfit, yfit) = g_dchi2.bestfit
        bestfit = ROOT.TMarker(xfit, yfit, 34)
        bestfit.SetMarkerSize(2)

        #graphs += [[g_1sig, g_2sig, bestfit]]
        #graphs += [[g_1sig_list[0], g_2sig_list[0], bestfit]]
        graphs += [[g_1sig_list, g_2sig_list, bestfit]]
        fgraphs += [[g_1sig, g_2sig, bestfit]]

        ## get x and y ranges
        #g_2sig.x2 = 1.2 * max([g_2sig.GetX()[i] for i in xrange(g_2sig.GetN())])
        #g_2sig.y2 = 1.2 * max([g_2sig.GetY()[i] for i in xrange(g_2sig.GetN())])
        #g_2sig.x2 = 1.2 * max([g.GetX()[i] for g in g_2sig_list for i in xrange(g.GetN())])
        #g_2sig.y2 = 1.2 * max([g.GetY()[i] for g in g_2sig_list for i in xrange(g.GetN())])
        #g_2sig.x1 = g_2sig.x2 * 1.e-4
        #g_2sig.y1 = g_2sig.y2 * 1.e-4
        graphx2 = 1.2 * max([g.GetX()[i] for g in g_2sig_list for i in xrange(g.GetN())])
        graphy2 = 1.2 * max([g.GetY()[i] for g in g_2sig_list for i in xrange(g.GetN())])
        graphx1 = graphx2 * 1.e-4
        graphy1 = graphy2 * 1.e-4

        graphs_axis_range +=[[graphx2,graphy2,graphx1,graphy1]]

    ## build components
    entries = [
        [fgraphs[0][0], "68% CL", "F"],
        [fgraphs[0][1], "95% CL", "F"],
        [fgraphs[0][2], "Best fit", "P"],
        ]
    leg = make_legend(entries, width=0.7, height=0.15, x1=0.30, y2=0.85)
    #leg.tlegend.SetHeader(leg_title)
    #leg.tlegend.SetTextSizePixels(10)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    c = ROOT.TCanvas('c', 'c', cfg.c_w, cfg.c_h)
    nx = 4
    ny = 4
    tgap = 0.01
    bgap = 0.06
    rgap = 0.01
    lgap = 0.06
    htot = 1.0 - tgap - bgap
    h = htot / float(ny)
    wtot = 1.0 - rgap - lgap
    w = wtot / float(nx)
    pads = [[None for x in range(nx)] for y in range(ny)]
    igraph = 0
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(10)
    latex.SetTextAlign(33)
    #ROOT.TGaxis.SetMaxDigits(2)
    for j in xrange(ny):
        y2 = 1.0 - tgap - float(j) * h
        y1 = y2 - h
        for i in xrange(nx):
            x1 = lgap + float(i) * w
            x2 = x1 + w
            p = ROOT.TPad("p{}{}".format(i,j), "", x1, y1, x2, y2)
            p.SetTopMargin(0.05)
            #p.SetTopMargin(0.11)
            p.SetRightMargin(0.05)
            p.SetBottomMargin(0.13)
            p.SetLeftMargin(0.18)
            pads[i][j] = p

            c.cd()
            p.Draw()
            p.cd()

            if igraph < len(graphs):
                mass = masses[igraph]
                (g1, g2, bf) = graphs[igraph]
                (graphx2,graphy2,graphx1,graphy1) = graphs_axis_range[igraph]
                #if limtype == 'obs':
                #    if igraph == 0: g2.x2 = g2.y2 = 990
                #    if igraph == 3: g2.x2 = 290
                #    if igraph == 6: g2.y2 = 5.9
                #if limtype == 'exp':
                #    if igraph == 1: g2.x2 = g2.y2 = 590
                #    if igraph == 5: g2.x2 = 85
                #    if igraph == 6: g2.y2 = 42

                #p.fr = p.DrawFrame(g2.x1, g2.y1, g2.x2, g2.y2, "")
                p.fr = p.DrawFrame(graphx1, graphy1, graphx2, graphy2, "")
                xaxis = p.fr.GetXaxis()
                yaxis = p.fr.GetYaxis()
                xaxis.SetNdivisions(505)
                yaxis.SetNdivisions(505)
                xaxis.SetLabelSize(0.09)
                yaxis.SetLabelSize(0.09)
                xaxis.SetTickLength(0.04)
                yaxis.SetTickLength(0.04)
                if doReversed == True: g2_new = reversed(g2)
                else: g2_new = g2
                for g in g2_new: g.Draw("F,SAME")
                for g in g2: g.Draw("L,SAME")
                for g in g1: g.Draw("F,SAME")
                for g in g1: g.Draw("L,SAME")
                bf.Draw()
                p.RedrawAxis()
                latex.SetTextSize(15)
                latex.DrawLatex(0.9, 0.9, "#it{m_{#phi}} = %d GeV" % (mass))

            igraph += 1

    # Legend and Labels
    pads[nx-1][ny-1].cd()
    leg.Draw()
    t_x = 0.45
    t_y = 0.30
    t_h = 0.15
    latex.SetTextAlign(22)
    latex.SetTextSize(19)
    latex.DrawLatex(t_x, 0.93, leg_title)
    latex.SetTextSize(21)
    if cfg.status == "":
        latex.DrawLatex(t_x, t_y, "#it{#bf{ATLAS}}")
    elif cfg.status is None:
        latex.DrawLatex(t_x, t_y, "#it{#bf{ATLAS}} Internal")
    else:
        latex.DrawLatex(t_x, t_y, "#it{#bf{ATLAS}} %s"%(cfg.status))
    latex.SetTextSize(15)
    latex.DrawLatex(t_x, t_y-t_h, "#sqrt{s} = 13 TeV, %d fb^{-1}" % (cfg.lumi))

    latex.SetTextSize(30)
    latex.SetTextAlign(32)
    c.cd()
    pxaxis = ROOT.TPad("pxaxis", "", lgap, 0.0, 1.0-rgap, bgap)
    pxaxis.Draw()
    pxaxis.cd()
    latex.DrawLatex(0.98, 0.5, xtitle)

    c.cd()
    pyaxis = ROOT.TPad("pyaxis", "", 0.0, bgap, lgap, 1.0-tgap)
    pyaxis.Draw()
    pyaxis.cd()
    latex.SetTextAlign(32)
    latex.SetTextAngle(90)
    latex.DrawLatex(0.5, 0.98, ytitle)

    c.Update()


    return (c, locals())



# ________________________________________________________________________________________________
def plot_nll_crosscheck(limtype="obs"):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    #x1 = 250
    #x2 = 1500
    x1 = 200
    x2 = 2250
    y1 = 0.0
    y2 = 1.18
    #z1 = 4.e-3
    #z2 = 0.9
    z1 = 3.e-3
    z2 = 0.96
    logx = False
    logy = False
    #xtitle = "m_{A} [GeV]"
    xtitle = "m_{#it{#phi}} [GeV]"
    #ytitle = "#it{#sigma}(bb) / [#it{#sigma}(gg) + #it{#sigma}(bb)]"
    ytitle = "#it{#sigma}_{bb} / [#it{#sigma}_{gg} + #it{#sigma}_{bb}]"
    #ztitle = "#it{#sigma} #times #it{B}(#it{A}/#it{H} #rightarrow #it{#tau#tau}) [fb]"
    ztitle = "#it{#sigma} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [pb]"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.7                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/21May/limit-scan-tautau-tcr_tt_combined.20May.limits.txt"
    #filestr = "inputs/guillermo-results-06-26-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.25June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-06-27-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.25June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-06-29-17-rebin/mssm-limits/limit-scan-tautau-tcr_tt_combined.29June.limits-obs.txt"
    #filestr = "inputs/guillermo-results-07-11-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.11July.limits-obs.txt"
    filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.13July.limits-obs.txt"

    #set_palette(name="green")
    ROOT.gStyle.SetPalette(ROOT.kBird)
    #ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)
    #ROOT.gStyle.SetPalette(ROOT.kCandy)
    #ROOT.gStyle.SetPalette(ROOT.kCherry)
    #ROOT.gStyle.SetPalette(ROOT.kLake)
    ROOT.gStyle.SetNumberContours(255)

    ## Graph getting and manipulation
    ##===========================================================================
    g_95 = get_indep_inputs_2d_v2(filestr, limtype)
    g_95.SetMinimum(z1)
    g_95.SetMaximum(z2)

    ## NLL-based curves
    if limtype == 'obs':
        filestr = "inputs/nllscan/v3/obs/GlobalFit_all_{mass}.txt"
    elif limtype == 'exp':
        filestr = "inputs/nllscan/v3/exp/AsimovFit_all_{mass}.txt"
    masses = [200, 250, 300, 350, 400, 500, 600, 700, 800, 1000, 1200, 1500, 1750, 2000, 2250]
    graphs = []
    g_95_nll = ROOT.TGraph2D()
    for mass in masses:
        g_dchi2 = make_nll_graph(filestr.format(mass=mass))
        #g_2sig_clist = g_dchi2.GetContourList(2.3)
        #g_2sig_clist = g_dchi2.GetContourList(3.0) # aparently this matches
        g_2sig_clist = g_dchi2.GetContourList(5.99)
        #g_2sig_clist = g_dchi2.GetContourList(7.375) ## This is for 95% CLs!!!
        #g_2sig_clist = g_dchi2.GetContourList(5.99/2.0) ##
        #g_2sig_clist = g_dchi2.GetContourList(3.84) ## 1D 95%
        #g_2sig_clist = g_dchi2.GetContourList(5.02)  ## 1D 95% CLs
        g_2sig_itr = g_2sig_clist.MakeIterator()
        while True:
            o = g_2sig_itr.Next()
            if not o: break
            for i in xrange(o.GetN()):
                x = o.GetX()[i]
                y = o.GetY()[i]
                g_95_nll.SetPoint(g_95_nll.GetN(), mass, y / (x + y), x + y)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logx: rc.c.SetLogx()
    if logy: rc.c.SetLogy()
    rc.c.SetLogz()
    rc.c.SetRightMargin(0.2)
    rc.c.cd()

    #g_95.Draw("SAME,COLZ")



    #iso_lvls = [0.01, 0.05, 0.1, 0.15, 0.20]
    if limtype == "exp": iso_lvls = [0.01, 0.1]
    else:                iso_lvls = [0.01, 0.1]
    for lvl in iso_lvls:
        contours = g_95.GetContourList(lvl)
        itr = contours.MakeIterator()
        while True:
            g = itr.Next()
            if not g: break
            g.SetLineWidth(2)
            g.Draw("SAME,L")

        contours_nll = g_95_nll.GetContourList(lvl)
        if contours_nll:
            itr_nll = contours_nll.MakeIterator()
            while True:
                g = itr_nll.Next()
                if not g: break
                g.SetLineWidth(2)
                g.SetLineColor(ROOT.kRed)
                g.Draw("SAME,L")

    ## Many lines
    #iso_lvls = [0.025, 0.075, 0.125, 0.175, 0.225]
    #iso_lvls = [0.005, 0.006, 0.007, 0.008, 0.009, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.2]
    #iso_lvls = [0.005, 0.007, 0.009, 0.03, 0.05, 0.07, 0.09]
    ## Mass range 250--1500
    #if limtype == "exp": iso_lvls = [0.007, 0.009, 0.03, 0.05, 0.07, 0.3]
    #else:                iso_lvls = [0.005, 0.007, 0.009, 0.03, 0.05, 0.07]
    ## Mass range 200-2500
    if limtype == "exp": iso_lvls = [0.007, 0.03, 0.07, 0.3]
    else:                iso_lvls = [0.007, 0.03, 0.07, 0.3]
    iso_lvls = []
    for lvl in iso_lvls:
        contours = g_95.GetContourList(lvl)
        itr = contours.MakeIterator()
        while True:
            g = itr.Next()
            if not g: break
            g.SetLineWidth(1)
            g.SetLineStyle(2)
            g.Draw("SAME,L")

        contours_nll = g_95_nll.GetContourList(lvl)
        if contours_nll:
            itr_nll = contours_nll.MakeIterator()
            while True:
                g = itr_nll.Next()
                if not g: break
                g.SetLineWidth(1)
                g.SetLineStyle(2)
                g.SetLineColor(ROOT.kRed)
                g.Draw("SAME,L")



    boxfrac = 1. / y2
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy-2.*t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")

    ## Good Order (revert to this for paper)
    #latex.DrawLatex(atlasx + 0.3, atlasy - 0. * t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")
    #if limtype == "exp":
    #    latex.DrawLatex(atlasx + 0.3, atlasy - 1. * t_h, "Expected")
    #else:
    #    latex.DrawLatex(atlasx + 0.3, atlasy - 1. * t_h, "Observed")

    ## Crap order (for CONF)
    latex.DrawLatex(atlasx + 0.3, atlasy - 1. * t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")
    latex.SetTextAlign(31)
    if limtype == "exp":
        latex.DrawLatex(0.76, atlasy - 0. * t_h, "Expected")
    else:
        latex.DrawLatex(0.76, atlasy - 0. * t_h, "Observed")



    ## z-axis title
    latex.SetTextAlign(31)
    latex.SetTextAngle(90)
    latex.SetTextSize(0.05)
    latex.DrawLatex(0.98, 0.95, ztitle)

    ## Settings for 200 -- 2250 mass range
    if limtype == "exp":
        # iso bar numbers
        latex.SetTextAngle(90+180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.195, 0.37, "0.3")
        latex.DrawLatex(0.200, 0.25, "#bf{0.1}")
        latex.DrawLatex(0.250, 0.25, "0.07")
        latex.DrawLatex(0.310, 0.25, "0.03")
        latex.DrawLatex(0.435, 0.25, "#bf{0.01}")
        latex.SetTextAngle(95+180)
        latex.SetTextAngle(90 + 180)
        latex.DrawLatex(0.520, 0.25, "0.007")
    else:
        # iso bar numbers
        latex.SetTextAngle(90 + 180)
        latex.SetTextAlign(11)
        latex.SetTextFont(42)
        latex.SetTextSize(0.03)
        latex.DrawLatex(0.182, 0.25, "0.3")
        latex.DrawLatex(0.220, 0.25, "#bf{0.1}")
        latex.DrawLatex(0.270, 0.25, "0.07")
        latex.SetTextAngle(92 + 180)
        latex.DrawLatex(0.317, 0.25, "0.03")
        latex.SetTextAngle(95 + 180)
        latex.DrawLatex(0.406, 0.25, "#bf{0.01}")
        latex.SetTextAngle(115 + 180)
        latex.SetTextAngle(100 + 180)
        latex.DrawLatex(0.485, 0.25, "0.007")

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    return (rc.c, locals())




# ________________________________________________________________________________________________
def plot_nll_crosscheck_1D(prod):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 200
    x2 = 1500
    y1 = 0.002
    y2 = 25.0
    logx = False
    logy = True
    xtitle = None
    ytitle = None

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.75                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.20               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.13July.limits-obs.txt"
    # production specifics
    if prod == "Zprime":
        pass
    else:
        lim_text = "#it{#phi} #rightarrow #tau#tau 95% CL limits"
        ytitle = "#it{#sigma} #times #it{B}(#it{#phi} #rightarrow #it{#tau#tau}) [pb]"
        xtitle = "m_{#it{#phi}} [GeV]"
        x1 = 200
        x2 = 2250
        if prod == "bbH":   prod_text = "#it{b}-associated production"
        elif prod == "ggH": prod_text = "gluon-gluon fusion"



    ## Graph getting and manipulation
    ##===========================================================================
    (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs(filestr.format(reg="tcr_",chan="tt"), prod)
    g_obs = [g_obs]
    g_exp = [g_exp]
    g_1sig = [g_1sig]
    g_2sig = [g_2sig]

    for g in g_obs: style_hist(g, "obs")
    for g in g_exp: style_hist(g, "exp")
    for g in g_1sig: style_hist(g, "err1sig")
    for g in g_2sig: style_hist(g, "err2sig")


    ## NLL-based curves
    filestrobs = "inputs/nllscan/v3/obs/GlobalFit_all_{mass}.txt"
    filestrexp = "inputs/nllscan/v3/exp/AsimovFit_all_{mass}.txt"
    masses = [200, 250, 300, 350, 400, 500, 600, 700, 800, 1000, 1200, 1500, 1750, 2000, 2250]
    g_nll_obs = ROOT.TGraph2D()
    g_nll_exp = ROOT.TGraph2D()
    for mass in masses:
        # obs
        g = make_nll_graph(filestrobs.format(mass=mass))
        for i in xrange(g.GetN()):
            if prod == 'ggH' and g.GetY()[i] == 0:
                g_nll_obs.SetPoint(g_nll_obs.GetN(), mass, g.GetX()[i], g.GetZ()[i])
            elif prod == 'bbH' and g.GetX()[i] == 0:
                g_nll_obs.SetPoint(g_nll_obs.GetN(), mass, g.GetY()[i], g.GetZ()[i])
        # exp
        g = make_nll_graph(filestrexp.format(mass=mass))
        for i in xrange(g.GetN()):
            if prod == 'ggH' and g.GetY()[i] == 0:
                g_nll_exp.SetPoint(g_nll_exp.GetN(), mass, g.GetX()[i], g.GetZ()[i])
            elif prod == 'bbH' and g.GetX()[i] == 0:
                g_nll_exp.SetPoint(g_nll_exp.GetN(), mass, g.GetY()[i], g.GetZ()[i])

    chi2 = 3.84 # 1D 95%
    #chi2 = 5.02 # 1D 95% CLs
    itr_obs = g_nll_obs.GetContourList(chi2).MakeIterator()
    itr_exp = g_nll_exp.GetContourList(chi2).MakeIterator()
    g_nll_obs_list = []
    g_nll_exp_list = []
    while True:
        o = itr_obs.Next()
        if not o: break
        g_nll_obs_list.append(o)
    while True:
        o = itr_exp.Next()
        if not o: break
        g_nll_exp_list.append(o)

    g_nll_obs_final = g_nll_obs_list[0]
    g_nll_exp_final = g_nll_exp_list[0]

    g_nll_obs_final.SetLineColor(ROOT.kRed)
    g_nll_obs_final.SetLineWidth(2)

    g_nll_exp_final.SetLineColor(ROOT.kRed)
    g_nll_exp_final.SetLineStyle(2)
    g_nll_exp_final.SetLineWidth(2)



    ## build components
    entries = []
    if g_obs: entries += [[g_obs[0], g_obs[0].tlatex, "LP"]]
    if g_exp: entries += [[g_exp[0], g_exp[0].tlatex, "L"]]
    if g_1sig: entries += [[g_1sig[0], g_1sig[0].tlatex, "F"]]
    if g_2sig: entries += [[g_2sig[0], g_2sig[0].tlatex, "F"]]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)
    leg2 = leg2a = leg2b = leg3 = None

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logx: rc.c.SetLogx()
    if logy: rc.c.SetLogy()
    rc.c.cd()

    for g in g_2sig: g.Draw("F")
    for g in g_1sig: g.Draw("F")
    for g in g_exp: g.Draw("L")
    for g in g_obs: g.Draw("LP")
    g_nll_obs_final.Draw("L")
    g_nll_exp_final.Draw("L")

    # top box
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")

    # standard text + legend
    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy - t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy - 2. * t_h, lim_text)
    if prod_text: latex.DrawLatex(atlasx, atlasy - 3. * t_h, prod_text)
    leg.Draw()

    if leg2a: leg2a.Draw()
    if leg2b: leg2b.Draw()
    if leg2: leg2.Draw()
    if leg3: leg3.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    return (rc.c, locals())





# ________________________________________________________________________________________________
def plot_sys(prod):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    #x1 = 250
    #x2 = 1550
    x1 = 150
    x2 = 2300
    y1 = 0.0
    y2 = 0.99
    logy = False
    xtitle = "#it{m}_{#it{#phi}} [GeV]"
    ytitle = "[ #it{#mu}^{95}_{i} - #it{#mu}^{95}_{stat} ] / #it{#mu}^{95}_{stat}"

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.6                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos



    # production specifics
    if prod == "bbH":
        prod_text = "#it{b}-associated production"
        #filestr = "inputs/sys/tata-np-bbh-formatted.txt"
        #filestr = "inputs/guillermo-results-06-27-17/mssm-np-study/np-bbh-27June-formatted.txt"
        #filestr = "inputs/guillermo-results-06-27-17/mssm-np-study/np-bbh-28June-formatted.txt"
        #filestr = "inputs/guillermo-results-06-29-17-rebin/mssm-np-study/np-bbh-29June-formatted.txt"
        #filestr = "inputs/guillermo-results-07-11-17/mssm-np-study/bbh-impact.txt"
        filestr = "inputs/guillermo-results-07-14-17/mssm-np-study/bbh-impact.txt"
    elif prod == "ggH":
        prod_text = "gluon-gluon fusion"
        #filestr = "inputs/sys/tata-np-formatted.txt"
        #filestr = "inputs/guillermo-results-06-27-17/mssm-np-study/np-ggh-27June-formatted.txt"
        #filestr = "inputs/guillermo-results-06-27-17/mssm-np-study/np-ggh-28June-formatted.txt"
        #filestr = "inputs/guillermo-results-06-29-17-rebin/mssm-np-study/np-ggh-29June-formatted.txt"
        #filestr = "inputs/guillermo-results-07-11-17/mssm-np-study/ggh-impact.txt"
        filestr = "inputs/guillermo-results-07-14-17/mssm-np-study/ggh-impact.txt"

    ## Graph getting and manipulation
    ##===========================================================================
    graphs = get_sys_graphs(filestr)

    # style em
    for g in graphs: style_hist(g)

    ## build components
    entries = [[g, g.tlatex, "LP"] for g in graphs]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    if logy: rc.c.SetLogy()
    rc.c.cd()


    for g in graphs: g.Draw("LP")

    """
    boxfrac = 0.75
    if logy:
        by1 = math.exp(math.log(y1) + boxfrac * (math.log(y2) - math.log(y1)))
    else:
        by1 = y1 + boxfrac * (y2 - y1)
    box = ROOT.TBox(x1, by1, x2, y2)
    box.SetLineColor(ROOT.kBlack)
    box.SetLineWidth(1)
    box.SetFillColor(ROOT.kWhite)
    box.Draw("LF")
    """

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    latex.DrawLatex(atlasx, atlasy-2.*t_h, prod_text)
    #latex.DrawLatex(atlasx, atlasy-3.*t_h, "#it{#phi} #rightarrow #tau#tau 95% CL limits")


    leg.Draw()
    #leg2.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())



# ________________________________________________________________________________________________
def plot_limit_sfm():
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## defaults

    ## configuration (much will be overwritten by variable-specific config)
    x1 = 0.03
    x2 = 0.5
    y1 = 500
    y2 = 3499
    #logx = logy = False      # use logarithmic axis scales
    xtitle = 'sin^{2}#it{#phi}'
    ytitle = '#it{m_{Z\'}} [GeV]'

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.04                # standard text height
    l_x1 = 0.6                # leg xpos right-hand side
    l_y2 = 0.94               # leg ypos top
    l_h  = 0.04               # leg text heigh
    l_w  = 0.30               # leg width
    atlasx = 0.2              # atlas x pos
    atlasy = 0.9              # atlas y pos

    #filestr = "inputs/21May/{scenario}_{chan}{cat}.root"
    #filestr = "inputs/guillermo-results-060817/mssm-exclusion/{scenario}_{chan}{cat}_linear.root"
    #filestr = "inputs/guillermo-results-060817/SFM-limit/limit-scan-tautau-{chan}-{reg}-sfm.31May.SFM.txt"
    #filestr = "inputs/zprime-limits/20170620-tt-sfm.SFM_TT.txt"
    filestr = "inputs/zprime-limits/20170620-tt-sfm.SFM_FINAL.txt"


    ## Graph getting and manipulation
    ##===========================================================================
    # Main results
    #g_excl = get_sfm_excl(filestr.format(chan="tt", reg="inc"))
    g_excl = get_sfm_excl(filestr)

    # previous results
    fin = ROOT.TFile.Open("inputs/exclusion-limits/sfm_limit_inputs.root")
    g_ind_orig = fin.Get("g_ind_orig").Clone()
    g_ind_apv = fin.Get("g_ind_apv").Clone()
    g_ind_lfv = fin.Get("g_ind_lfv").Clone()
    g_ind_ckm = fin.Get("g_ind_ckm").Clone()
    g_ind_new = fin.Get("g_ind_new").Clone()
    g_zll_lee = fin.Get("g_zll_lee").Clone()
    fin.Close()




    fill_graph_to_bottom(g_excl)
    g_excl.SetLineColor(ROOT.kAzure + 6)
    g_excl.SetLineWidth(2)
    g_excl.SetFillColor(ROOT.kAzure + 6)

    # g_ind_orig.SetLineColor(ROOT.kRed)
    g_ind_lfv.SetLineStyle(2)
    #g_ind_ckm.SetLineStyle(3)
    g_ind_ckm.SetLineStyle(9)
    g_ind_orig.SetLineStyle(4)
    for g in [g_ind_new, g_ind_lfv, g_ind_ckm, g_ind_orig]:
        g.SetLineWidth(3)


    ## build components
    entries = [
        [g_excl, '#it{#bf{ATLAS}} #it{#tau#tau} search', 'F'],
        [g_ind_new,'Indirect (EWPT)', 'L'],
        [g_ind_lfv,'Indirect (LFV)', 'L'],
        [g_ind_ckm,'Indirect (CKM)', 'L'],
        [g_ind_orig,'Indirect (#it{Z}-pole)', 'L'],
        ]
    leg = make_legend(entries, width=l_w, height=l_h, x1=l_x1, y2=l_y2)

    #leg = make_legend(hists, names, styles, width=0.35, x1=0.20, y2=0.91, height=0.045)
    #leg2 = make_legend(hists, names, styles, width=0.35, x1=0.55, y2=0.44, height=0.040)
    #leg2.SetFillColor(ROOT.kWhite)
    #leg2.SetFillStyle(1001)

    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    rc.draw_frame(x1, y1, x2, y2, ';%s;%s' % (xtitle, ytitle))
    rc.c.cd()

    g_excl.Draw("F,SAME")
    g_ind_new.Draw("L,SAME")
    g_ind_lfv.Draw("L,SAME")
    g_ind_ckm.Draw("L,SAME")
    g_ind_orig.Draw("L,SAME")


    #box = ROOT.TBox(x1, 59.90, x2, y2)
    #box.SetLineColor(ROOT.kBlack)
    #box.SetLineWidth(1)
    #box.SetFillColor(ROOT.kWhite)
    #box.Draw("LF")

    #draw_standard_text(atlasx=atlasx, atlasy=atlasy, lumi=cfg.lumi)
    #draw_channel_text(channel="{} {}".format(cfg.lephad, catname))

    draw_standard_text(atlasx=atlasx, atlasy=atlasy, status=cfg.status)
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextSize(0.030 / ROOT.gPad.GetHNDC())
    lumi_text = '%.1f fb^{-1}' % (cfg.lumi)
    com_text = '#sqrt{s} = 13 TeV'
    latex.DrawLatex(atlasx, atlasy-t_h, "%s, %s" % (com_text, lumi_text))
    #latex.DrawLatex(atlasx, atlasy-2.*t_h, "#it{Z'}_{SFM} #rightarrow #tau#tau 95% CL limits")
    latex.DrawLatex(atlasx, atlasy - 2. * t_h, "95% CL limits")

    latex.DrawLatex(0.57, 0.2, "Non-universal #it{G}(221) model")

    leg.Draw()

    ROOT.gPad.RedrawAxis()
    rc.c.Update()

    #ftmp = ROOT.TFile.Open("ftmp.root","RECREATE")
    #ftmp.WriteTObject(g_excl, "g_zprime")
    #ftmp.Close()
    #exit(1)

    #rc.c.SaveAs("test.pdf")
    return (rc.c, locals())




def fill_graph_to_bottom(g,bottom=0.0):
  xarr = []
  yarr = []

  xarr.append(g.GetX()[0])
  yarr.append(bottom)
  for i in xrange(g.GetN()):
    xarr.append(g.GetX()[i])
    yarr.append(g.GetY()[i])
  xarr.append(g.GetX()[g.GetN()-1])
  yarr.append(bottom)

  g.Set(g.GetN()+2)
  for i,x,y in zip(xrange(len(xarr)),xarr,yarr):
    g.SetPoint(i,x,y)






def get_indep_inputs_forhep(fname, prod):
    """Replicates get_indep_inputs, but uses TGraphAsymmErrors format for bands

    :param fname:
    :param prod:
    :return:
    """
    if prod == "bbH":
        bfrac_req = 1.0
        gfrac_req = 0.0
    elif prod == "ggH":
        bfrac_req = 0.0
        gfrac_req = 1.0
    elif prod == "Zprime":
        bfrac_req = 0.0
        gfrac_req = 1.0
        zpxsec = ZprimeXsecProvider("SSM")

    g_obs = ROOT.TGraph()
    g_exp = ROOT.TGraph()
    g_1sig = ROOT.TGraphAsymmErrors()
    g_2sig = ROOT.TGraphAsymmErrors()
    with open(fname) as f:
        for l in f:
            (a,b) = l.split(":")
            (mass, bfrac, gfrac) = a.strip().split()
            if not (float(bfrac) == bfrac_req and float(gfrac) == gfrac_req): continue
            (obs, exp, up2sig, up1sig, dn1sig, dn2sig) = b.strip().split()
            scale = 1.0
            if prod == "Zprime":
                scale = zpxsec.getXsec("%.1f"%(int(mass)))

            #print "GUI {} : {}, {}, {}".format(mass, obs, exp, scale)
            print "{} : {} ({}) [pb]".format(mass, float(obs) * scale, float(exp) * scale)
            n = g_obs.GetN()
            g_obs.SetPoint(n, float(mass), float(obs) * scale)
            g_exp.SetPoint(n, float(mass), float(exp) * scale)
            g_1sig.SetPoint(n, float(mass), float(exp) * scale)
            g_1sig.SetPointError(n, 0, 0, abs(float(exp) - float(dn1sig)) * scale, abs(float(up1sig) - float(exp)) * scale)
            g_2sig.SetPoint(n, float(mass), float(exp) * scale)
            g_2sig.SetPointError(n, 0, 0, abs(float(exp) - float(dn2sig)) * scale, abs(float(up2sig) - float(exp)) * scale)

    return (g_obs, g_exp, g_1sig, g_2sig)



def generate_hepdata_inputs():
    """

    :return:
    """


    ## 1D Limits
    for prod in ["ggH", "bbH", "Zprime"]:
        if prod == "Zprime":
            filestr = "inputs/guillermo-results-06-27-17/ssm-limits/limit-scan-tautau-{chan}-{reg}-ssm_combined.29June.limits-obs.txt"
            (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs_forhep(filestr.format(reg="inc", chan="tt"), prod)
        else:
            filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.13July.limits-obs.txt"
            (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs_forhep(filestr.format(reg="tcr_",chan="tt"), prod)

        fname = "HepInputs_limits_{}.root".format(prod)
        f = ROOT.TFile.Open(fname,"RECREATE")
        f.WriteTObject(g_obs, "observed")
        f.WriteTObject(g_exp, "expected")
        f.WriteTObject(g_1sig, "1sigma")
        f.WriteTObject(g_2sig, "2sigma")
        f.Close()

    ## 2D Limits
    """
    ## HACKS NEEDED TO USE EXISTING TH2F BASED SCRIPT
    def get_lim_hist(g):
        def find_closest_bin(axis, x):
            bestdist = None
            bestbin = None
            for i in xrange(axis.GetNbins()+1):
                binpos = axis.GetBinLowEdge(i)
                bindist = abs(x - binpos)
                if bestbin is None or bindist < bestdist:
                    bestbin = i
                    bestdist = bindist
            return bestbin

        frac_points = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        mass_points = [200, 250, 300, 400, 500, 600, 700, 800, 1000, 1200, 1500, 1750, 2000, 2250]
        # add dummy last bin edge
        frac_points += [2.*frac_points[-1] - frac_points[-2]]
        mass_points += [2.*mass_points[-1] - mass_points[-2]]
        # construct hist
        frac_bins = array('f', frac_points)
        mass_bins = array('f', mass_points)
        h2 = ROOT.TH2F("h2", "h2", len(mass_bins)-1, mass_bins, len(frac_bins)-1, frac_bins)
        for i in xrange(g.GetN()):
            x = float("%.0f"%(g.GetX()[i]))
            y = float("%.1f"%(g.GetY()[i]))
            #ix = h2.GetXaxis().FindBin(x)
            #iy = h2.GetYaxis().FindBin(y)
            ix = find_closest_bin(h2.GetXaxis(), x)
            iy = find_closest_bin(h2.GetYaxis(), y)
            print "x: {} ({}), y: {} ({})".format(x,ix,y,iy)
            h2.SetBinContent(ix,iy,g.GetZ()[i])

        #for i in xrange(1, h2.GetNbinsX()+1):
        #    x = h2.GetXaxis().GetBinCenter(i)
        #    for j in xrange(1, h2.GetNbinsY() + 1):
        #        y = h2.GetYaxis().GetBinCenter(j)
        #        h2.SetBinContent(i, j, g.Interpolate(x,y))
        return h2
    """
    filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.13July.limits-obs.txt"
    g2_obs = get_indep_inputs_2d_v2(filestr, "obs")
    #h2_obs = get_lim_hist(g2_obs)
    g2_exp = get_indep_inputs_2d_v2(filestr, "exp")
    #h2_exp = get_lim_hist(g2_exp)
    f = ROOT.TFile.Open("HepInputs_limit2D.root", "RECREATE")
    #f.WriteTObject(h2_obs, "Observed")
    #f.WriteTObject(h2_exp, "Expected")
    f.WriteTObject(g2_obs, "Observed")
    f.WriteTObject(g2_exp, "Expected")


    pass


def print_hepdata_limit(prod):
    ROOT.gROOT.ProcessLine(".L Limit1D_to_YAML.C+")
    ROOT.Limit1D_to_YAML("HepInputs_limits_{}.root".format(prod))

def print_hepdata_limit2D():
    #ROOT.gROOT.ProcessLine(".L Limit2D_to_YAML.C+")
    #ROOT.Limit2D_to_YAML("HepInputs_limit2D.root")
    ROOT.gROOT.ProcessLine(".L Limit2DGraph_to_YAML.C+")
    ROOT.Limit2DGraph_to_YAML("HepInputs_limit2D.root")



def dump_hepdata_limit(prod):
    """

    :param prod:
    :return:
    """
    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)
    if prod == "Zprime":
        filestr = "inputs/guillermo-results-06-27-17/ssm-limits/limit-scan-tautau-{chan}-{reg}-ssm_combined.29June.limits-obs.txt"
        (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs_forhep(filestr.format(reg="inc", chan="tt"), prod)
        fname = "hepdata_limit_zprime.yaml"
        prod_text = r"'$Z^{\prime}$ (Drell-Yan)'"
    else:
        filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-{reg}{chan}_combined.13July.limits-obs.txt"
        (g_obs, g_exp, g_1sig, g_2sig) = get_indep_inputs_forhep(filestr.format(reg="tcr_",chan="tt"), prod)
        fname = "hepdata_limit_{}.yaml".format(prod)
        if prod == 'ggH':
            prod_text = "'Higgs boson (gluon-gluon fusion)'"
        elif prod == 'bbH':
            prod_text = "'Higgs boson (b-associated production)'"
    quals += [['Production',prod_text]]

    print "Creating {}...".format(fname)
    f = open(fname, 'w')

    a_x = array('d', [g_obs.GetX()[i] for i in xrange(g_obs.GetN())])
    a_obs = array('d', [g_obs.GetY()[i] for i in xrange(g_obs.GetN())])
    a_exp = array('d', [g_exp.GetY()[i] for i in xrange(g_exp.GetN())])
    a_up1 = array('d', [g_1sig.GetErrorYhigh(i) for i in xrange(g_1sig.GetN())])
    a_dn1 = array('d', [-g_1sig.GetErrorYlow(i) for i in xrange(g_1sig.GetN())])
    a_up2 = array('d', [g_2sig.GetErrorYhigh(i) for i in xrange(g_2sig.GetN())])
    a_dn2 = array('d', [-g_2sig.GetErrorYlow(i) for i in xrange(g_2sig.GetN())])


    ## indep
    f.write("independent_variables:\n")
    dump_hepvals_arr(f, a_x, title="Mass", units="GeV")

    ## dep
    f.write("dependent_variables:\n")
    dump_hepvals_arr(f, a_obs, title=r'$\sigma \times B$', units="pb", qualifiers=quals + [['Limit','Observed']])
    dump_hepvals_arr(f, a_exp, arr_up=a_up1, arr_dn=a_dn1, arr_up2=a_up2, arr_dn2=a_dn2,
                     title=r'$\sigma \times B$', units="pb", qualifiers=quals + [['Limit', 'Expected']])

    f.close()


def dump_hepdata_limit2D():
    """

    :return:
    """
    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)
    fname = "hepdata_limit2D.yaml"
    filestr = "inputs/guillermo-results-07-14-17/mssm-limits/limit-scan-tautau-tcr_tt_combined.13July.limits-obs.txt"
    g2_obs = get_indep_inputs_2d_v2(filestr, "obs")
    g2_exp = get_indep_inputs_2d_v2(filestr, "exp")


    a_x = array('d', [g2_obs.GetX()[i] for i in xrange(g2_obs.GetN())])
    a_y = array('d', [g2_obs.GetY()[i] for i in xrange(g2_obs.GetN())])
    a_obs = array('d', [g2_obs.GetZ()[i] for i in xrange(g2_obs.GetN())])
    a_exp = array('d', [g2_exp.GetZ()[i] for i in xrange(g2_exp.GetN())])


    print "Creating {}...".format(fname)
    f = open(fname, 'w')

    ## indep
    f.write("independent_variables:\n")
    dump_hepvals_arr(f, a_x, title="Mass", units="GeV")
    dump_hepvals_arr(f, a_y, title="bbH fraction")

    ## dep
    f.write("dependent_variables:\n")
    dump_hepvals_arr(f, a_obs, title=r'$\sigma \times B$', units="pb", qualifiers=quals + [['Limit','Observed']])
    dump_hepvals_arr(f, a_exp, title=r'$\sigma \times B$', units="pb", qualifiers=quals + [['Limit', 'Expected']])



# ______________________________________________________________________________=buf=
def dump_hepnll2d(g_obs, g_exp, mass, fname="hepdata.yaml"):
    """Dump Observed and Expected distribution to YAML HepData format

    Asymmetric uncertainties can be provided via *h_mc_up* and *h_mc_dn*

    :param h_data: data distribution
    :param h_mc: expected distribution
    :param h_mc_up: expected distribution +1 sigma
    :param h_mc_dn: expected distribution -1 sigma
    :param fname:
    :return:
    """

    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)

    xobs = array('d', [g_obs.GetX()[i] for i in xrange(g_obs.GetN())])
    yobs = array('d', [g_obs.GetY()[i] for i in xrange(g_obs.GetN())])
    zobs = array('d', [g_obs.GetZ()[i] for i in xrange(g_obs.GetN())])

    xexp = array('d', [g_exp.GetX()[i] for i in xrange(g_exp.GetN())])
    yexp = array('d', [g_exp.GetY()[i] for i in xrange(g_exp.GetN())])
    zexp = array('d', [g_exp.GetZ()[i] for i in xrange(g_exp.GetN())])

    if not xobs == xexp:
        print "x-axis failed match!"
        return 1
    if not yobs == yexp:
        print "y-axis failed match!"
        return 1

    print "Creating {}...".format(fname)
    f = open(fname, 'w')

    ## indep
    f.write("independent_variables:\n")
    #dump_hepvals_arr(f, xobs, title=r'$\sigma(gg\phi)\times B(\phi\to\tau\tau)$', units="pb")
    #dump_hepvals_arr(f, yobs, title=r'$\sigma(bb\phi)\times B(\phi\to\tau\tau)$', units="pb")
    dump_hepvals_arr(f, xobs, title=r'$\sigma(gg\phi)B$', units="pb")
    dump_hepvals_arr(f, yobs, title=r'$\sigma(bb\phi)B$', units="pb")

    ## dep
    f.write("dependent_variables:\n")
    dump_hepvals_arr(f, zobs, title=r'$2\Delta(\mathrm{NLL})$',
                     qualifiers=quals+[['Data', 'Observed'], [r"'$m_{\phi}$'", {'value': mass, 'units':'GeV'}]])
    dump_hepvals_arr(f, zexp, title=r'$2\Delta(\mathrm{NLL})$',
                     qualifiers=quals+[['Data', 'Expected'], [r"'$m_{\phi}$'", {'value': mass, 'units':'GeV'}]])

    f.close()




def dump_hepdata_nll():
    """

    :param limtype:
    :return:
    """
    #filestr = "inputs/nllscan/GlobalFit_{mass}.txt"
    filestrobs = "inputs/nllscan/v3/obs/GlobalFit_all_{mass}.txt"
    filestrexp = "inputs/nllscan/v3/exp/AsimovFit_all_{mass}.txt"

    ## Graph getting and manipulation
    ##===========================================================================
    masses = [200, 250, 300, 350, 400, 500, 600, 700, 800, 1000, 1200, 1500, 1750, 2000, 2250]
    #masses = [250]
    for mass in masses:
        g_obs = make_nll_graph(filestrobs.format(mass=mass), "g_obs_{}".format(mass))
        g_exp = make_nll_graph(filestrexp.format(mass=mass), "g_exp_{}".format(mass))

        dump_hepnll2d(g_obs, g_exp, mass, "hepdata_nll_{}.yaml".format(mass))

    pass


def dump_hepdata_zprime_models():
    """

    :param limtype:
    :return:
    """
    filestr_dirk = "inputs/zprime-limits/20170620-tt-{model}_FINAL.txt"

    from cfg import hepdata_qualifiers
    quals = list(hepdata_qualifiers)
    #title = "Ratio of 95% CL cross section upper limit w.r.t. the SSM"
    title = r"$(\sigma\times B) / (\sigma\times B)_\mathrm{SSM}$"

    ## Graph getting and manipulation
    ##===========================================================================
    (g_obs_ssm, g_exp_ssm, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="ssm.SSM"), mode="SSM")
    (g_obs_lh, g_exp_lh, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="lr.LR"), mode="L")
    (g_obs_rh, g_exp_rh, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="lr.LR"), mode="R")
    (g_obs_nar, g_exp_nar, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="width.WIDTH"), mode="Narrow")
    (g_obs_wid, g_exp_wid, _, _) = get_indep_inputs_dirk(filestr_dirk.format(model="width.WIDTH"), mode="Wide")

    g_obs_lh = divide_graphs(g_obs_lh, g_obs_ssm)
    g_exp_lh = divide_graphs(g_exp_lh, g_exp_ssm)
    g_obs_rh = divide_graphs(g_obs_rh, g_obs_ssm)
    g_exp_rh = divide_graphs(g_exp_rh, g_exp_ssm)
    g_obs_nar = divide_graphs(g_obs_nar, g_obs_ssm)
    g_exp_nar = divide_graphs(g_exp_nar, g_exp_ssm)
    g_obs_wid = divide_graphs(g_obs_wid, g_obs_ssm)
    g_exp_wid = divide_graphs(g_exp_wid, g_exp_ssm)

    a_x = array('d', [g_obs_lh.GetX()[i] for i in xrange(g_obs_lh.GetN())])
    a_obs_lh = array('d', [g_obs_lh.GetY()[i] for i in xrange(g_obs_lh.GetN())])
    a_obs_lh = array('d', [g_obs_lh.GetY()[i] for i in xrange(g_obs_lh.GetN())])
    a_exp_lh = array('d', [g_exp_lh.GetY()[i] for i in xrange(g_exp_lh.GetN())])
    a_obs_rh = array('d', [g_obs_rh.GetY()[i] for i in xrange(g_obs_rh.GetN())])
    a_exp_rh = array('d', [g_exp_rh.GetY()[i] for i in xrange(g_exp_rh.GetN())])
    a_obs_nar = array('d', [g_obs_nar.GetY()[i] for i in xrange(g_obs_nar.GetN())])
    a_exp_nar = array('d', [g_exp_nar.GetY()[i] for i in xrange(g_exp_nar.GetN())])
    a_obs_wid = array('d', [g_obs_wid.GetY()[i] for i in xrange(g_obs_wid.GetN())])
    a_exp_wid = array('d', [g_exp_wid.GetY()[i] for i in xrange(g_exp_wid.GetN())])


    fname = "hepdata_zp_models.yaml"
    print "Creating {}...".format(fname)
    f = open(fname, 'w')

    ## indep
    f.write("independent_variables:\n")
    dump_hepvals_arr(f, a_x, title="Mass", units="GeV")

    ## dep
    f.write("dependent_variables:\n")
    dump_hepvals_arr(f, a_obs_lh,  title=title, qualifiers=quals+[['Model', 'LH'],     ['Limit', 'Observed']])
    dump_hepvals_arr(f, a_exp_lh,  title=title, qualifiers=quals+[['Model', 'LH'],     ['Limit', 'Expected']])
    dump_hepvals_arr(f, a_obs_rh,  title=title, qualifiers=quals+[['Model', 'RH'],     ['Limit', 'Observed']])
    dump_hepvals_arr(f, a_exp_rh,  title=title, qualifiers=quals+[['Model', 'RH'],     ['Limit', 'Expected']])
    dump_hepvals_arr(f, a_obs_nar, title=title, qualifiers=quals+[['Model', 'Narrow'], ['Limit', 'Observed']])
    dump_hepvals_arr(f, a_exp_nar, title=title, qualifiers=quals+[['Model', 'Narrow'], ['Limit', 'Expected']])
    dump_hepvals_arr(f, a_obs_wid, title=title, qualifiers=quals+[['Model', 'Wide'],   ['Limit', 'Observed']])
    dump_hepvals_arr(f, a_exp_wid, title=title, qualifiers=quals+[['Model', 'Wide'],   ['Limit', 'Expected']])

    f.close()

    pass





