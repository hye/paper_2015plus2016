# encoding: utf-8
"""
cfg.py
~~~~~~

this is an auto-generated example config file

define any general config items here that you might want
to use in your plotting scripts.
"""

## anaysis info
#lumi = 13.3  # fb
lumi = 139  # fb
unblinded = True
approved = False
#status = "Internal"
#status = "Preliminary"
status = ""

## plot layout
c_w = 700  # canvas width
c_h = 700  # canvas height (square)
# c_h           = 500 # canvas height (landscape)
c_ratio_split = 0.3
t_size = 0.7
label_size = 0.04

## write out types (can import and modify)
from plotter import plotutils
plotutils.do_eps = True
plotutils.do_pdf = True
plotutils.do_png = True
plotutils.do_greyscale = True

## common tlatex expressions can go here
mTtot = '#it{m}_{T}^{tot}'
mvis = '#it{m}_{vis}'
dphi = '#it{#Delta#phi}'
tauh = '#it{#tau}_{had}'
tauhvis = '#it{#tau}_{had-vis}'
taue = '#it{#tau_{e}}'
taumu = '#it{#tau_{#mu}}'
taulep = '#it{#tau}_{lep}'
lep = '#it{lep}'
tauhad = tauh
tauhadvis = tauhvis
met = "#it{E}_{T}^{miss}"
ehad = taue + tauhad
muhad = taumu + tauhad
lephad = taulep + tauhad
hadhad = tauhad + tauhad
btag  = "#it{b}-tag"
bveto = "#it{b}-veto"
bincl = "#it{b}-inclusive"
#tcr = "top control region"
tcr = "T-CR"

# process names
ztautau = '#it{Z}/#it{#gamma}*#rightarrow#it{#tau#tau}'

hepdata_qualifiers = [
    ['SQRT(S)', {'value':13000, 'units':'GeV'}],
    ['LUMINOSITY', {'value':139, 'units': r"'fb$^{-1}$'"}],
    ]



# add in some analysis specific samples 
from plotter import samples
import ROOT
samples.bkg1 = samples.Sample('Background 1',
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kYellow - 10,
    _fill_style=1000)

samples.bkg2 = samples.Sample('Background 2',
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kBlue - 9,
    _fill_style=1000)

samples.sig = samples.Sample('Signal',
    _line_color=ROOT.kMagenta,
    _fill_color=ROOT.kWhite,
    _fill_style=0)


samples.taufakes = samples.Sample('Jet#rightarrow#it{#tau} fake',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kRed+1,
    #_fill_color = ROOT.TColor.GetColor(0.75 ,0.45 ,0.85),
    _fill_style = 1000)


samples.zjets = samples.Sample('#it{Z}/#it{#gamma}*+jets',
    #_line_color = ROOT.kBlack,
    #_fill_color = ROOT. kYellow -10,
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kBlue - 9,
    _fill_style = 1000)

samples.dy = samples.Sample('#it{Z}/#it{#gamma}*',
    #_line_color = ROOT.kBlack,
    #_fill_color = ROOT. kYellow -10,
    _line_color=ROOT.kBlack,
    _fill_color=ROOT.kBlue + 3,
    _fill_style = 1000)

samples.diboson = samples.Sample('Diboson',
    _line_color = ROOT.kBlack,
    _fill_color = ROOT. kYellow -8,
    _fill_style = 1000)


samples.H1500 = samples.Sample('#it{A}/#it{H} (1500)#times100',
#samples.H1500 = samples.Sample('#it{A}/#it{H} (1500)',
    _line_width=5,
    _line_color=ROOT.kOrange+2,
    _fill_color=0,
    _fill_style=0)

samples.H1000 = samples.Sample('#it{A}/#it{H} (1000)#times100',
#samples.H1000 = samples.Sample('#it{A}/#it{H} (1000)',
    _line_width=5,
    _line_color=ROOT.kRed+2,
    _fill_color=0,
    _fill_style=0)

samples.H500 = samples.Sample('#it{A}/#it{H} (500)',
    _line_width=5,
    _line_color=ROOT.kGreen+2,
    _fill_color=0,
    _fill_style=0)

samples.H600 = samples.Sample('#it{A}/#it{H} (600)',
    _line_width=5,
    _line_color=ROOT.kGreen+2,
    _fill_color=0,
    _fill_style=0)

samples.H400 = samples.Sample('#it{A}/#it{H} (400)',
    _line_width=5,
    _line_color=ROOT.kYellow+2,
    _fill_color=0,
    _fill_style=0)

samples.H300 = samples.Sample('#it{A}/#it{H} (300)',
    _line_width=5,
    _line_color=ROOT.kBlue,
    _fill_color=0,
    _fill_style=0)

samples.H200 = samples.Sample('#it{A}/#it{H} (200)',
    _line_width=5,
    _line_color=ROOT.kMagenta,
    _fill_color=0,
    _fill_style=0)


#samples.SSMZprime500 = samples.Sample("#it{Z'} (500)",
#    _line_width=5,
#    _line_color=ROOT.kRed+2,
#    _fill_color=0,
#    _fill_style=0)

#samples.SSMZprime1000 = samples.Sample("#it{Z'} (1000)",
#    _line_width=5,
#    _line_color=ROOT.kRed+2,
#    _fill_color=0,
#    _fill_style=0)

#samples.SSMZprime1500 = samples.Sample("#it{Z'} (1500)",
#    _line_width=5,
#    _line_color=ROOT.kRed+2,
#    _fill_color=0,
#    _fill_style=0)

#samples.SSMZprime2000 = samples.Sample("#it{Z'} (2000)",
#    _line_width=5,
#    _line_color=ROOT.kGreen+2,
#    _fill_color=0,
#    _fill_style=0)

#samples.SSMZprime2500 = samples.Sample("#it{Z'} (2500)",
#    _line_width=5,
#    _line_color=ROOT.kBlue+2,
#    _fill_color=0,
#    _fill_style=0)

#samples.SSMZprime3000 = samples.Sample("#it{Z'} (3000)",
#    _line_width=5,
#    _line_color=ROOT.kBlue+2,
#    _fill_color=0,
#    _fill_style=0)





## Limit contributions
samples.obs = samples.Sample('Observed',
    _line_width=2,
    _line_color=ROOT.kBlack,
    _fill_color=0,
    _fill_style=0)


samples.exp = samples.Sample('Expected',
    _line_width=2,
    _line_style=7,
    _line_color=ROOT.kBlack,
    _fill_color=0,
    _fill_style=0)

samples.err1sig = samples.Sample('#pm 1#scale[0.5]{ }#sigma',
    _fill_color=ROOT.kGreen,
    _line_width=0)

samples.err2sig = samples.Sample('#pm 2#scale[0.5]{ }#sigma',
    _fill_color=ROOT.kYellow,
    _line_width=0)




## systematics styles
samples.nominal  = samples.Sample('All',        _line_color = ROOT.kBlack, _marker_color = ROOT.kBlack, _line_width=3)

samples.tau      = samples.Sample('Tau efficiency', _line_color = ROOT.kBlue,  _marker_color = ROOT.kBlue, _marker_style = 21)
samples.tes      = samples.Sample('Tau energy scale',   _line_color = ROOT.kBlue,  _marker_color = ROOT.kBlue, _marker_style = 25, _line_style=2)
samples.hh_fakes = samples.Sample('{} fakes'.format(hadhad), _line_color = ROOT.kRed,  _marker_color = ROOT.kRed, _marker_style = 22)
samples.lh_fakes = samples.Sample('{} fakes'.format(lephad), _line_color = ROOT.kRed,  _marker_color = ROOT.kRed, _marker_style = 26, _line_style=2)
samples.signal   = samples.Sample('Signal acceptance',     _line_color = ROOT.kGreen+2,   _marker_color = ROOT.kGreen+2,  _marker_style = 34)
samples.other    = samples.Sample('Other', _line_color = ROOT.kGray+2,  _marker_color = ROOT.kGray+2, _marker_style = 24, _line_style=2)

samples.xs      = samples.Sample('Cross section', _line_color = ROOT.kGreen+2,  _marker_color = ROOT.kGreen+2, _marker_style = 23)
samples.jet_met_lumi_prw = samples.Sample('Other', _line_color = ROOT.kGray+1,  _marker_color = ROOT.kGray+1, _marker_style = 33)
samples.lepton   = samples.Sample('Lepton',    _line_color = ROOT.kYellow+2,  _marker_color = ROOT.kYellow+2, _marker_style = 20)
samples.btag    = samples.Sample('#it{b}-tag', _line_color = ROOT.kBlue+2,  _marker_color = ROOT.kBlue+2, _marker_style = 25)
samples.ttbar   = samples.Sample('ttbar',      _line_color = ROOT.kCyan-3,  _marker_color = ROOT.kCyan-3, _marker_style = 22)


samples.sys  = samples.Sample('Sys',  _fill_color=ROOT.kBlue)
samples.stat = samples.Sample('Stat', _fill_color=ROOT.kRed, _marker_style=20, _marker_color=ROOT.kBlack)
