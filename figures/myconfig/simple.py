# encoding: utf-8
'''
  file:    hadhad.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012

  description:
    This macro produces all the plots for the Z'tautau analysis.
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs.

    To run the macro, you need to setup a ROOT release compiled
    against PyROOT (any recent release is probaly fine).
    Then simply execute this macro:

      python plots.py

'''
import os, re
import cfg
from plotter.plotutils import SingleCanvas
from ROOT import TLatex, TH1F, TFile, gRandom, gPad
from math import sqrt

from plotter.histutils import HistGrabber, log_bins, apply_blind, build_stack, \
    get_poissonized_graph, get_poissonized_ratio_graph, get_mc_ratio_band, get_significance_ratio, \
    build_total_stat_plus_sys
from plotter.plotutils import RatioCanvas, build_leg, draw_standard_text, draw_blind, \
    get_ytitle_events
from plotter.samples import style_hist


# ________________________________________________________________________________________________
def placeholder():
    # config
    x1 = y1 = 0.0
    x2 = y2 = 1.0

    # draw
    sc = SingleCanvas('rc', cfg.c_w, cfg.c_h)
    sc.draw_frame(x1, y1, x2, y2, ';Variable;Events')
    sc.scale_text(cfg.t_size)

    # draw pad1
    sc.c.cd()
    latex = TLatex()
    latex.SetTextFont(42)
    latex.SetTextSize(0.10)
    latex.SetTextAngle(25)
    latex.SetTextAlign(22)
    latex.DrawLatex(0.5, 0.5, "Place Holder")

    return (sc.c, locals())



# ________________________________________________________________________________________________
def generate_dummy_inputs():
    """
    """

    fname = "inputs/test.root"
    if os.path.exists(fname): 
        return True

    f = TFile.Open(fname, "RECREATE")
    h_bkg1 = TH1F("h_bkg1", "", 100, 0., 100.)
    h_bkg2 = TH1F("h_bkg2", "", 100, 0., 100.)
    h_data = TH1F("h_data", "", 100, 0., 100.)
    h_sig  = TH1F("h_sig", "", 100, 0., 100.)
    for h in [h_bkg1, h_bkg2, h_sig]: h.Sumw2()

    for i in xrange(5000): h_bkg1.Fill(gRandom.Landau(15, 10))
    for i in xrange(1000): h_bkg2.Fill(gRandom.Landau(40, 15))
    for i in xrange(1000): h_sig.Fill(gRandom.Gaus(50, 10))
    h_data.Add(h_bkg1)
    h_data.Add(h_bkg2)
    for i in xrange(0,h_data.GetNbinsX()+2): 
        h_data.SetBinContent(i, gRandom.Poisson(h_data.GetBinContent(i)))
        h_data.SetBinError(i, sqrt(h_data.GetBinContent(i)))
    f.Write()
    f.Close()


# ________________________________________________________________________________________________
def plot_stack(var=None):
    """
      make stack plots for had-had
    """
    ## Configuration
    ##===========================================================================

    ## configuration (much will be overwritten by variable-specific config)
    x1 = x2 = y1 = y2 = None  # canvas boundaries
    logx = logy = False       # use logarithmic axis scales
    rebin = None              # fixed-width rebin factor
    xtitle = None             #
    xunit = None              #
    ndivx = None              # x-axis tick divisions
    blind = True              # position of blind
    ratio_opt = "sig"         # options: ratio, sig
    sig_scale = None          # signal scale factor

    # decoration placement
    t_x = 0.31                # standard text x
    t_y = 0.87                # standard text y
    t_h = 0.05                # standard text height
    l_x2 = 0.93               # leg xpos
    leg_left = True           # place legend on LHS
    atlasx = None             # atlas x pos

    # samples
    filestr = "inputs/test.root"
    histstr = "h_{samp}"
    bkgnames = ["bkg1", "bkg2"]
    signames = ["sig"]

    ## plot-dependent configuration
    if var in [None, "somevar"]:
        rebin = 2
        logx = False
        logy = False
        xtitle = "Mass [GeV]"
        blind = 50

    ## auto-configuration
    if logy: y1 = 0.01
    # ratio y-axis range
    if ratio_opt == "sig":
        ry1 = -7.0
        ry2 = +7.0
    elif ratio_opt == "ratio":
        ry1 = 0.41
        ry2 = 1.59
    if xunit == None:
        s = re.search(".*\[(.*)\]", xtitle)
        if s: xunit = s.group(1)


    ## Histogram getting and manipulation
    ##===========================================================================
    # read in nominal histograms
    print "Getting nominal histograms..."
    grabber = HistGrabber(filestr=filestr, histstr=histstr, rebin=rebin)
    bkgs   = [grabber.hist(samp=s, name=s.lower()) for s in bkgnames]
    sigs   = [grabber.hist(samp=s, name=s.lower()) for s in signames]
    h_data = grabber.hist(samp="data", name="data")
    # post-process
    if sig_scale is not None:
        for s in sigs: s.Scale(sig_scale)
    if not cfg.unblinded: apply_blind(h_data, blind)
    g_data = get_poissonized_graph(h_data)
    for h in [g_data] + bkgs + sigs: style_hist(h)
    h_stack = build_stack(bkgs)
    ## get bkg total + error
    print "Getting background sum and errors..."
    h_bkg_tot = grabber.histsum(samps=bkgnames, name="error")
    style_hist(h_bkg_tot)

    ## determine axis ranges, titles
    if x1 == None: x1 = h_data.GetBinLowEdge(1)
    if x2 == None: x2 = h_data.GetBinLowEdge(h_data.GetNbinsX() + 1)
    if y1 == None:
        if logy:   y1 = 0.1 * min([h_bkg_tot.GetMinimum(), h_data.GetMinimum()])
        else:      y1 = 0.0
    if y2 == None:
        if logy:   y2 = 10. * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
        else:      y2 = 1.2 * max([h_bkg_tot.GetMaximum(), h_data.GetMaximum()])
    bin_width = h_data.GetBinWidth(2)
    ytitle = get_ytitle_events(bin_width=bin_width, xunit=xunit)

    ## additional inputs for ratio band
    ratios = []
    # standard ratio
    if ratio_opt == "ratio":
        # data ratio
        h_ratio_data = get_poissonized_ratio_graph(h_data, h_bkg_tot)
        h_ratio_data.drawopt = "P"
        style_hist(h_ratio_data, "data")
        # mc ratio
        h_ratio_mc = get_mc_ratio_band(h_bkg_tot)
        h_ratio_mc.drawopt = "E2"
        style_hist(h_ratio_mc, "ratio")
        ratios = [h_ratio_mc, h_ratio_data]
    # significance ratio
    elif ratio_opt == "sig":
        h_ratio_obs = get_significance_ratio(h_data, h_bkg_tot)
        h_ratio_obs.drawopt = "HIST"
        style_hist(h_ratio_obs, "significance")
        h_ratio_sigs = []
        for (i, s) in enumerate(sigs):
            h = get_significance_ratio(s, h_bkg_tot, ismc=True)
            style_hist(h, sigs[i].GetName())
            h.drawopt = "HIST"
            h_ratio_sigs.append(h)
        ratios = [h_ratio_obs] + h_ratio_sigs
    else:
        print "No ratio configured!"

    ## build components
    leg = build_leg(h_data, bkgs, sigs, h_bkg_tot)



    ## Construct Canvas
    ##===========================================================================
    print "Constructing canvas..."
    rc = RatioCanvas('rc', cfg.c_w, cfg.c_h, cfg.c_ratio_split)
    rc.draw_frame1(x1, y1, x2, y2, ';;%s' % (ytitle))
    if ratio_opt == "ratio":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Obs. / exp.' % (xtitle))
    elif ratio_opt == "sig":
        rc.draw_frame2(x1, ry1, x2, ry2, ';%s;Significance' % (xtitle))
    rc.scale_text(cfg.t_size)
    if ndivx is not None:
        rc.fr1.GetXaxis().SetNdivisions(ndivx)
        rc.fr2.GetXaxis().SetNdivisions(ndivx)

    # draw pad1
    rc.pad1.cd()
    rc.pad1.SetLogy(logy)
    rc.pad1.SetLogx(logx)
    rc.fr2.GetXaxis().SetMoreLogLabels()
    rc.fr2.GetXaxis().SetNoExponent()
    h_stack.Draw("SAME,HIST")
    g_data.Draw("SAME,PZ")
    h_bkg_tot.Draw("SAME,E2")
    for s in sigs:
        s.Draw("SAME,HIST")
    draw_standard_text(atlasx=atlasx, lumi=cfg.lumi)
    leg.Draw()
    if not cfg.unblinded:
        line = draw_blind(blind, x1, x2, y1, y2, logy=logy)
    gPad.RedrawAxis()
    rc.pad1.Update()

    # draw pad 2
    rc.pad2.cd()
    rc.pad2.SetLogx(logx)
    for r in ratios:
        r.Draw("SAME,{}".format(r.drawopt))

    gPad.RedrawAxis()
    rc.pad2.Update()


    return (rc.c, locals())




