# encoding: utf-8
'''
  file:    plots.py
  authors: Will Davey <will.davey@cern.ch>
           Ryan Reece <ryan.reece@cern.ch>
  created: Apr 2012 

  description:
    This macro produces all the plots for the Z'tautau analysis. 
    The inputs are saved in ROOT files in the 'inputs' directory.
    The macro provides a common style for all plots, and produces
    PDF, EPS and PNG outputs. 

    To run the macro, you need to setup a ROOT release compiled 
    against PyROOT (any recent release is probaly fine). 
    Then simply execute this macro:

      python plots.py

'''

## modules
from plotter.plotutils import Plot
from myconfig.simple import placeholder
from myconfig.hadhad import plot_stack as plot_hadhad_stack
from myconfig.lephad import plot_stack as plot_lephad_stack
from myconfig.hadhad import plot_stack_postfit as plot_hadhad_stack_postfit
from myconfig.lephad import plot_stack_postfit as plot_lephad_stack_postfit
from myconfig.lephad import plot_fakefactors as plot_lephad_fakefactors
from myconfig.lephad import plot_fakefactors_split as plot_lephad_fakefactors_split
from myconfig.lephad import plot_fakefactors_split_wcorr as plot_lephad_fakefactors_split_wcorr
from myconfig.lephad import plot_fakefactors_split_2x2 as plot_lephad_fakefactors_split_2x2
from myconfig.lephad import plot_wcorr as plot_lephad_wcorr
from myconfig.lephad import plot_acc, plot_btag_fraction, plot_btag_fraction_single
from myconfig.hadhad import plot_fakefactors_split as plot_hadhad_fakefactors_split
from myconfig.limits import plot_limit_mssm, plot_limit_indep, plot_limit_indep_2d, plot_limit_indep_2d_v2, plot_sys, \
    plot_limit_sfm, plot_limit_zpoverlay, plot_dchi2_2d, plot_dchi2_onslaught, plot_nll_crosscheck, \
    plot_nll_crosscheck_1D, plot_limit_zp_history

## define all plots
plots = [

    #Plot(0, name="placeholder", func=placeholder),
    ### hadhad stack plots
    ## Pre-fit plots 
    #Plot(11021, name="prefit_hadhad_btag_SumOfPt_ratio", func=plot_hadhad_stack_postfit, var="SumOfPt", cat="T1", ratio_opt="ratio"),
    ## Post-fit plots
    #Plot(721, name="postfit_hadhad_btag_SumOfPt_sig", func=plot_hadhad_stack_postfit, var="SumOfPt", cat="T1", ratio_opt="sig"),
    #Plot(1021, name="postfit_hadhad_btag_SumOfPt_ratio", func=plot_hadhad_stack_postfit, var="SumOfPt", cat="T1", ratio_opt="ratio"),

    #Plot(1022, name="postfit_hadhad_bveto_LEADTAUPT_ratio", func=plot_hadhad_stack_postfit, var="LEADTAUPT", cat="T0", ratio_opt="ratio"),
    #Plot(1023, name="postfit_hadhad_btag_LEADTAUPT_ratio", func=plot_hadhad_stack_postfit, var="LEADTAUPT", cat="T1", ratio_opt="ratio"),

    ### lephad stack plots
    ## Pre-fit plots
    #Plot(11171, name="prefit_lephad_btag_SumOfPt_ratio", func=plot_lephad_stack_postfit, var="SumOfPt", cat="T1", chan="LepHad", ratio_opt="ratio"),
    ## Post-fit plots
    #Plot(1722, name="postfit_lephad_btag_SumOfPt_ratio", func=plot_lephad_stack_postfit, var="SumOfPt", cat="T1", chan="LepHad", ratio_opt="ratio"),
    #Plot(1711, name="postfit_lephad_btag_SumOfPt_sig", func=plot_lephad_stack_postfit, var="SumOfPt", cat="T1", chan="LepHad", ratio_opt="sig"),


    #Plot(1173, name="postfit_lephad_btag_TauPt_ratio", func=plot_lephad_stack_postfit, var="TauPt", cat="T1", chan="LepHad", ratio_opt="ratio"),

    Plot(11811, name="postfit_lephad_vr_SumOfPt_ratio", func=plot_lephad_stack_postfit, var="SumOfPt", cat="T1", chan="LepHad", ratio_opt="ratio"),



    ## limit plots
    #Plot(200, name="limit_hMSSM", func=plot_limit_mssm, scenario="hMSSM"),
    #Plot(201, name="limit_mhmodm", func=plot_limit_mssm, scenario="mhmodm"),
    #Plot(202, name="limit_mhmodp", func=plot_limit_mssm, scenario="mhmodp"),
    #Plot(203, name="limit_mhmax", func=plot_limit_mssm, scenario="newmhmax"),
    #Plot(204, name="limit_lightstau1", func=plot_limit_mssm, scenario="lightstau1"),
    #Plot(205, name="limit_lightstopmod", func=plot_limit_mssm, scenario="lightstopmod"),
    #Plot(206, name="limit_low", func=plot_limit_mssm, scenario="low"),
    #Plot(207, name="limit_tauphobic", func=plot_limit_mssm, scenario="tauphobic"),

    #Plot(210, name="limit_hMSSM_all", func=plot_limit_mssm, scenario="hMSSM", overlay=True),

    #Plot(220, name="limit_bbH", func=plot_limit_indep, prod="bbH"),
    #Plot(221, name="limit_ggH", func=plot_limit_indep, prod="ggH"),
    #Plot(222, name="limit_bbH_all", func=plot_limit_indep, prod="bbH", overlay=True),
    #Plot(223, name="limit_ggH_all", func=plot_limit_indep, prod="ggH", overlay=True),
    #Plot(224, name="limit_zprime", func=plot_limit_indep, prod="Zprime"),
    #Plot(225, name="limit_zprime_all", func=plot_limit_indep, prod="Zprime", overlay=True),

    #Plot(230, name="limit_zprime_sfm", func=plot_limit_sfm),
    #Plot(240, name="limit_zpoverlay", func=plot_limit_zpoverlay),
    #Plot(241, name="limit_zpoverlay", func=plot_limit_zpoverlay, relative=True),
    #Plot(242, name="limit_zphistory", func=plot_limit_zp_history),

    #Plot(300, name="limit_gg_vs_bb_m500", func=plot_limit_indep_2d, mass=500),
    #Plot(301, name="limit_gg_vs_bb_m1000", func=plot_limit_indep_2d, mass=1000),

    #Plot(310, name="limit_frac_vs_mass", func=plot_limit_indep_2d_v2),
    #Plot(311, name="limit_frac_vs_mass_exp", func=plot_limit_indep_2d_v2, limtype="exp"),

    #Plot(320, name="dchi2_200", func=plot_dchi2_2d, mass=200),
    #Plot(321, name="dchi2_250", func=plot_dchi2_2d, mass=250),
    #Plot(322, name="dchi2_300", func=plot_dchi2_2d, mass=300),
    #Plot(323, name="dchi2_350", func=plot_dchi2_2d, mass=350),
    #Plot(324, name="dchi2_400", func=plot_dchi2_2d, mass=400),
    #Plot(325, name="dchi2_500", func=plot_dchi2_2d, mass=500),
    #Plot(326, name="dchi2_600", func=plot_dchi2_2d, mass=600),
    #Plot(327, name="dchi2_700", func=plot_dchi2_2d, mass=700),
    #Plot(328, name="dchi2_800", func=plot_dchi2_2d, mass=800),
    #Plot(329, name="dchi2_1000", func=plot_dchi2_2d, mass=1000),
    #Plot(330, name="dchi2_1200", func=plot_dchi2_2d, mass=1200),
    #Plot(331, name="dchi2_1500", func=plot_dchi2_2d, mass=1500),
    #Plot(332, name="dchi2_1750", func=plot_dchi2_2d, mass=1750),
    #Plot(333, name="dchi2_2000", func=plot_dchi2_2d, mass=2000),
    #Plot(334, name="dchi2_2500", func=plot_dchi2_2d, mass=2500),

    #Plot(340, name="dchi2_onslaught", func=plot_dchi2_onslaught, limtype='obs'),
    #Plot(341, name="dchi2_onslaught_exp", func=plot_dchi2_onslaught, limtype='exp'),
    #Plot(350, name="dchi2_crosscheck", func=plot_nll_crosscheck, limtype='obs'),
    #Plot(351, name="dchi2_crosscheck_exp", func=plot_nll_crosscheck, limtype='exp'),

    #Plot(360, name="dchi2_crosscheck_1D_ggH", func=plot_nll_crosscheck_1D, prod='ggH'),
    #Plot(361, name="dchi2_crosscheck_1D_bbH", func=plot_nll_crosscheck_1D, prod='bbH'),


    #Plot(400, name="sys_vs_mass_gg", func=plot_sys, prod="ggH"),
    #Plot(401, name="sys_vs_mass_bb", func=plot_sys, prod="bbH"),

    #Plot(500, name="lephad_ff_1p", func=plot_lephad_fakefactors, prong=1),
    #Plot(501, name="lephad_ff_3p", func=plot_lephad_fakefactors, prong=3),

    #Plot(510, name="lephad_ff_split", func=plot_lephad_fakefactors_split),
    #Plot(511, name="lephad_ff_split_wcorr", func=plot_lephad_fakefactors_split_wcorr),
    #Plot(512, name="lephad_ff_split_2x2", func=plot_lephad_fakefactors_split_2x2),
    #Plot(513, name="lephad_ff_wcorr", func=plot_lephad_wcorr),

    #Plot(520, name="hadhad_ff_split", func=plot_hadhad_fakefactors_split),

    #Plot(530, name="acc_ggH", func=plot_acc, prod="ggH"),
    #Plot(531, name="acc_bbH", func=plot_acc, prod="bbH"),
    #Plot(532, name="acc_Zprime", func=plot_acc, prod="Zprime"),
    #Plot(533, name="btagfrac", func=plot_btag_fraction),
    #Plot(534, name="btagfrac_bbH", func=plot_btag_fraction_single, prod="bbH"),
    #Plot(535, name="btagfrac_ggH", func=plot_btag_fraction_single, prod="ggH"),
]
