//// Convert a TH2D of VLQ mass limit vs. BR into HEP data format (YAML) 
//// J. Haley <joseph.haley@cern.ch>
//// 2017 April 14

#include <TFile.h>
#include <TGraph2D.h>
#include <string>
#include <stdio.h>
#include <iostream> 

using namespace std;

string lumi = "36.1"; // integrated luminosity
string cme = "13000"; // center of mass energy [GeV]

void dump_TGraph2D(TGraph2D* g, string limit_type="Observed"){

  cout << "- header: {name: \'Lower limit on VLQ mass at 95% CL\', units: GeV}" << endl;
  cout << "  qualifiers:" << endl;
  cout << "  - {name: Limit, value: " << limit_type << "}" << endl;
  cout << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  cout << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  cout << "  values:" << endl;

  for(int i=0; i<g->GetN(); i++){
    cout << "  - {value: " << g->GetZ()[i] << "}" << endl;
  }
}


void Limit2DGraph_to_YAML(string fn1 = "2D_TTS_BRscan_HEP.root")
{
  
  TFile *_file1 = TFile::Open(fn1.c_str());
  
  TGraph2D* g_expected = 0;
  TGraph2D* g_observed = 0;

  g_expected    = (TGraph2D*)_file1->Get("Expected");
  g_observed    = (TGraph2D*)_file1->Get("Observed");
  
  assert(g_expected);
  assert(g_observed);

  TGraph2D* g = g_expected;

  cout << "independent_variables:" << endl;

  //cout << "- header: {name: BR(T->Wb)}" << endl;
  cout << "- header: {name: Mass, units: GeV}" << endl;
  cout << "  values:" << endl;
  for(int i=0; i<g->GetN(); i++){
    cout << "  - {value: " << g->GetX()[i] << "}" << endl;
  }

  //cout << "- header: {name: BR(T->Ht)}" << endl;
  cout << "- header: {name: bbH fraction}" << endl;
  cout << "  values:" << endl;
  for(int i=0; i<g->GetN(); i++){
    cout << "  - {value: " << g->GetY()[i] << "}" << endl;
  }
  
  cout << "dependent_variables:" << endl;

  dump_TGraph2D(g_expected, "Expected");
  dump_TGraph2D(g_observed, "Observed");

}
