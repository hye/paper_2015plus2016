Many thanks Michael and Fabio.  This is now done and we're uploading Draft v1.0 to CDS.

Best regards,

Trevor & Will

>>Dear all,
>>
>>I am signing off now for the Higgs group to avoid delay's later on when the EB is ready to sign-off. Please try to implement the comments as much as possible. None of them are crucial for the results, but they can hopefully help to reduce confusion. I am adding below also the Draft 1 check-list from PubCom. Please try to follow these as much as possible to ensure a speedy circulation after EB sign-off.
>>
>>Cheers,
>>
>>Michael (and Fabio)
>>
>>
>>DRAFT 1 pre-circulation checklist
>>---------------------------------
>>
>>  In order to launch the first circulation of your paper, the following items are needed, see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPaperCheckList#Draft_1_Circulation_in_ATLAS.
>>
>>*  Version 1.0 on title page.

A: Done

>>*  Supporting documentation missing or incomplete on title page.

A: Complete.

>>*  Deadline for comments: Please follow the link above to calculate the number of circulation days.

A: Done

>>*  Egroups for editors and editorial board missing on the second page.

A: Added.

>>*  Top of page 3 should have "ATLAS Paper" instead of "ATLAS Note".
>>
>>*  Enter the reference code (e.g. EXOT-2016-19) for this paper under "ATLAS Paper" above the title and abstract on page 3 (instead of ATL-COM-PHYS...).

A: Done.

>>*  Make sure to move all figures and tables before the conclusions section (use \FloatBarrier).

A: Done for all except for the Auxiliary material

>>*  Please update the acknowledgements to the last one from March 6 https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComAcknowledgements, note that it also includes an additional reference.

A: Done.

>>*  Please move the Auxiliary material after the list of references.

A: Done.

>>*  You currently have no Auxiliary material section, are you really not interested in making any extra plots or tables public?

A: Added.

>>*  Add list of contributions in support note and in the CDS entry for the support note, following the instructions in
>>
>>https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ContributionsToATLASNotes.

A: Done.

>>*  Please send the title and abstract in macro-free LaTeX (e.g. without macros like \GeV or \ifb; this also means no formatting commands like ~ or \, etc.; CDS only understands basic LaTeX math mode, it does not interpret even basic formatting commands).

A: Done.

>>*  Please indicate whether a CONF conversion will be requested. If so, what conference do you aim for?

A: Yes. EPS

>>*  Does this paper supersede results already included in a CONF note? If so, which one?

A: No

>>*  If you plan to request exceptional authorship, now is the time to do so with an email to atlas-authdb-qual@cern.ch stating the specific contributions the authors to be added have made.

A: Done.
 
