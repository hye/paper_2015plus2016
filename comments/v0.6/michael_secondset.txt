Hi Michael,

Many thanks for your comments, and apologies that we did not address them more quickly.  Please find our responses below.  These have been addressed in the CONF and Draft 2.

Best regards,

Trevor & Will

>>Dear all,
>>
>>two more comments on the result section:
>>
>>Table 1: Last sentence of caption got somehow corrupted (or I don't understand it). Is this table pre- or post-fit? Looking at Fig 11 and the limits in Fig 14, I would assume its pre-fit. If correct, this should be clarified in the Table caption, as the reader might otherwise doubt the statement in L458 that data is in good agreement with the prediction for lephad b-veto. How significant is the data-MC difference?

A: Caption issue was a typo.  These numbers were all post-fit, but the CONF and Draft 2 contains an expanded Table 1 containing both pre- and post-fit numbers.  The signifcance of the data-MC difference is quantified in the "Significance" pads just below the plots in Fig. 5, for example.

>>Fig 5: you might want to adapt the scale a bit to make the 800 GeV signal better visible (but not needed for circulation)

A: Thanks for your suggestion.  Ticker lines for the signal were added in Draft 1.0, and the CONF as well as Draft 2 contain two-tone lines so that the signal stands out against both light and dark backgrounds.

>>Cheers,
>>
>>Michael

 
