Dear reviewer, 

thank you for your useful comments. They helped to improve the clarity of the paper, in particular the description of the background estimation. Please find our responses below.

Kind Regards, 
The ATLAS Collaboration


The paper describes a search for new heavy resonances decaying to a pair of tau leptons. Both new scalars and vectors are considered. The analysis is carefully conducted. The result is interesting and robust. The work should be published on JHEP. On the other hand, the paper is really difficult to read. The different ingredients to the analysis are not presented in a logic order (at least in my opinion). There are too many forward references and it is impossible to follow the strategy without reading the text at least twice. I think that the structure of the paper should be revisited in depth. I list below a few examples of choices that the authors should reconsider:

* The discussion of systematic uncertainties is scattered between section 7 (where they belong) and several paragraphs in section 6. Not clear why.

----> We thought that it is more natural to describe the uncertainties associated with data-driven methods when discussing the methods themselves.  This gives a complete description of each method in a single location. If the reader is more interested in the systematics as a whole, though, rather than the methods, then the grouping you suggest is probably more natural. We have modified the text as you suggest. This has also required the introduction of forward references to the description of the uncertainties on the data-driven background estimates, which are necessary to understand the fake-factor figures. 



* The discussion in sec 6.1 (jet background estimate) should be turned upside down. One should start with the basic ingredients, then describe how these ingredients are put together in the sidebands, and finally how one goes from there to the signal region.

----> We have made the following changes: 
- the had-had background estimate is now described before lep-had as it is simpler and helps introduce the basic concepts. 
- abstract introduction of 'fake-factor' methods at end of p.10 removed in favour of simply introducing the concepts with the had-had description
- reversed the order of describing multijet and W+jets components for lep-had channel.
- dropped all function arguments in fake-factor equations and simply state how the fake-factors are parameterised in the text (former was abstract and causing confusion)
- split up long sentence introducing had-had fake-factor method
- split table 1 into separate tables for had-had and lep-had channel and reference at start of their respective sections.


* There is an intense use of jargon in the manuscript. Ofter HEP specific, sometimes ATLAS specific. I would have expected an explanation of concepts like ”fake factor technique” (referred to several times at the beginning of sec 6 but only discussed in 6.1), sequential correction, and similar. What is an auxiliary variable? Do you mean that a given quantities is estimated in bins of other quantities?

----> Regarding the use of jargon: 
- 'fake-factor technique': we removed the abstract 'fake-factor' definition in section 6 introduction and define the concepts on first use in the description of the had-had estimate. We also replaced the terminology 'fake-factor technique' with 'data-driven technique'. 
- the term 'auxiliary variable' has been removed when simplifying the formulae 
- 'sequential correction' in our opinion this term is self explanatory 
 

* Notation should be revised. For instance, in Fig. 2 X is used for the event category while x is used for the variable of interest. While different (capital vs low-case), the symbols are quite similar.

----> Arguments have been removed from data-driven estimate equations, so there should not be any ambiguity. 


I have the impression that a fresh reading from an author not directly involved with the analysis could be very helpful.

I also list below a set of questions specific of the analysis:

* Why the range in which the new resonance is probed depends on the spin of the resonance?

----> Higgs boson signals are typically weaker than Z' and run out of sensitivity to relevant models at lower masses (compare Fig10 to Fig7(c) or Fig11).


* Is the narrow-approximation assumption a reasonable choice for the models you consider?

----> For the Higgs boson models we consider, the width doesn't get larger than a few percent, which is negligible compared to the experimental resolution. For the Z' models we do not assume narrow width. We have investigated the impact of width to help theorists reinterpret the results. The result can be found here: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_10.png


* Why not using a di-tau trigger?

----> Ditau triggers have lower plateau efficiency than singletau triggers, thus only help at low-mass where the lower pt thresholds increase acceptance. However, in this mass range the lep-had channel dominates the sensitivity, so there is little gain from using a ditau trigger. This can be seen in:  https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_07a.png and https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_07b.png 


* On Fig.5, it seems to me that your points are quite scattered and that the fraction of points outside the band (which are interpreted as the 1σ band) is too large. This makes me doubt on the coverage properties of the quoted uncertainties (which, I assume, are the post-fit ones). To a certain extent, the same comment applies to Fig.6.

----> In the upper panel, the hatched area in the top panel shows the 1-sigma uncertainty on the background model. In the lower panel, the circle markers represent the statistical significance of the data given the background model (including its associated uncertainties) and the 68% interval would be from +1 to -1. For the 4 distributions shown in Fig5, the significance values follow a normal distribution with mean: -0.1 and sdev: 0.74, which does not suggest any problems with coverage. Alternate versions of these figures where the lower panel includes the more common ratio of data to the background model can be found here: 
- https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_01a.png
- https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_01b.png
- https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_01c.png
- https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_01d.png 


Last but not least, a few suggestions on the editorial aspects:

* In the introduction, I would not use the word ”favoured” but ”predicted”. There is no experimental indication that seems to favour BSM models.

----> Done


* It is not clear to me what the second sentence of pag.2 is needed for (”In some scenarios...”).

----> These sentences describe the particular scenarios that are considered in the paper. Previously we had considered more than just the mHmod+ and hMSSM, which lead to the potentially confusing phrasing. We have changed the text to: 

"In the $m_{h}^{\text{mod}+}$ scenario~\cite{MSSMBenchmarks}, the top-squark mixing parameter is chosen such that the mass of the lightest CP-even Higgs boson, $m_h$, is close to the measured mass of the Higgs boson that was discovered at the LHC."


* Not sure why the super-seeded results from Run I are referred to.

----> To clarify, the most recent search from ATLAS for A/H/Z'->tautau is the 2015 analsysis [31]. 
- We removed [26] and [27] as our results show they were superseded by [31]: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2015-08/fig_05d.png
- We kept [43] as it was not superseded by [31]: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-12/figaux_11.png
- The other citations are for other experiments, which we are compelled to include. 


* equations should be numbered and the number should be used in the text to refer to them.

----> As we never need to refer to the equations, we have not numbered them. 


* The black lines on Fig.8 are difficult to see on a black background. A different choice of colors might be preferable.

----> We have already tuned the color palette for best visibility, and we would prefer to keep the current one. In all versions of the plot we have, it has been easy to differentiate the contours from the surface.

