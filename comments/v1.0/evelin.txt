Dear Evelin,

Many thanks for looking at the draft and providing your comments. They have really helped to improve the quality of the document. Please find our responses below.

Kind regards,

Trevor & Will

>>Dear all,
>>
>>I am reading this paper on behalf of the Publication Committee.
>>It is a nice analysis and the paper is in general well written.
>>
>>
>>Please find below few comments, I marked with “(***)” the main ones.
>>
>>Best regards,
>>Evelin
>>
>>
>>TEXT
>>====
>>
>>l.58: There isn't a more extended way to define the "m_h^mod+" scenario?

A: Benchmark scenarios like mhmax, mhmod and hMSSM are very common in the literature.  The last part of the sentence that begins on Line 58 explains what makes mhmod+ different from mhmax (a benchmark scenario that had been used previously for many years).  We think that the text gives enough information, and have provided a reference in case the reader would like more detail on this and the other scenarios.

>>ll. 76-79: It seems to be better first indirect limits and later results from 
direct searches.

A: Thanks for the suggestion.  Done.

>>ll. 130-132: The sentence on the primary vertex should go in the event selection 
>>section 

A: We feel that the current location in the paper is a good fit for this bit of text.

>>ll. 133-135: Is this sentence really needed? It is general and valid for all papers.

A: For the sake of completeness, we would prefer to leave this sentence in.

>>ll. 138: Remove "or tune"

A: Done.

>>ll. 142 and 145: maybe you could consider to add one line to explain what "four-flavour
 scheme" and "five-flavour scheme" mean.

A: Thanks, we've added a minor comment in this sentence mentioning that in the 4FS the bottom quark is not considered as a parton in the proton.

>>ll.157-163: You should mention how background from MC have been normalised

A: fixed. The section has been revamped using PMG recommendations.

>>l.168: "Each sample is.." start a new paragraph here

A: Done.

>>l. 211: "Geometrically overlapping" what about just "overlapping"?

A: We think that the text is okay as-is, since it is more specific.

>> Section 5: I think that having a table with the overall event selection could 
>>help the reader. Or at least put it as auxiliary material that helps speakers at conferences.

A: we will try to create a table summarising the selection for all signal/control regions for draft 2.0 (we did not have time for the CONF conversion). 

>>ll.256-259: What about the normalisation of the other background? Better to clarify.

A: added: "The other major background contributions can be adequately constrained in the signal regions."

>>l. 302:  more narrow -> narrower

A: We'll keep "more narrow".

>>(***) l. 322: How this corretion is determined?

A: The text has now been updated to read, "Due to the large \ttbar
contamination, \fW is not split in category, but the \bveto parameterisation is
used in the \btag region, with a single correction factor of 0.8 (0.66) for
1-prong (3-prong) \tauhadvis. The correction factor is obtained from a direct
measurement of the fake-factors in \btag events."

>>(***) l. 363: How this additional uncertainty is determined?

A: This additional uncertainty is determined by calculating the fake-rate in the b-tagged region.  

>>(***) Figure 2: I understand that the explanation of the background data-driven technique 
>>is challenging, but I don't think this schematisation is really needed in the paper, while 
you could include it as auxiliary material.

A: We've overhauled the diagram to make it a bit easier to read (addressing comments from others).  We would like to include the diagram in the paper and have left it in for the time being.  

>>(***) Section 6: What about adding a table summarising CR definitions?
>>If you don't want to include it in the main body of the text, it should be added 
>>at least in the auxiliary material.

A: Thanks for the nice suggestion.  We will add a table summarizing the CR definitions for draft 2.0.

>>l.401 "tau_had-vis efficiency" do you mean reconstruction efficiency? Better to specify.

A: identification (and specified in text)

>>
>>(***) l.403 "3-14%" how this number is determined?

A: This uncertainty comes from the Tau Performance group.  We will add a reference to the latest Tau Perf CONF, which is here:  https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2017-029/

>>l.409 "A simplified treatment of the PDF.." you should briefly explain what is this 
>>simplified method.

A: rewrote as: 

A single 90\% CL eigen-vector variation uncertainty is used, based on the CT14nnlo PDF set.

>>
>>
>>REFERENCES
>>==========
>>[31] Is now:  Eur. Phys. J. C76 (2016) 585

A: References 31 and 120 are the same, and the latter had the updated journal info.  We've updated the first instance and removed the second.
